#include <test.h>
#include "BQ24250.h"
#include "cmsis_os.h"
#include "stdio.h"
#include "task_tag.h"
#include "port.h"
#include "stm32l4xx_hal_rtc.h"

#include "main.h"

#include <config.h>
#include <task_ctrl.h>
#include <task_flush.h>
#include "stm32l4xx_hal.h"

#include "i2c.h"
#include "nxp_pcal6416a.h"

#include "uart_command.h"
#include "inertial_sensing.h"
#include "cmd_def.h"

#include "stm32l4xx_it.h"

//#define UWB_WAIT_TIME	(3000) 		//ms ORIGINALE
#define UWB_WAIT_TIME	(6000) 		//ms UNDO
#define MOTION_WAIT_TIME (10000) 	//10s
#define WAKE_UP_TIMER	(2000) 		//1s

#define INIT_ADDR	0x202000
#define SERIAL_ADDR	0x204000


//UWB
static testParameter test_parameter;
osThreadId testTaskHandle;
int uwbTimeout;

//CAN
CAN_TxHeaderTypeDef   TxHeader;
uint8_t               TxData[8];

CAN_RxHeaderTypeDef   RxHeader;
uint8_t               RxData[8];
uint32_t              TxMailbox;

//BLE
#define MAX_DEVICES 64
bd_addr connect_addr;
bd_addr found_devices[MAX_DEVICES];
bleStruct found_ble[MAX_DEVICES];

int found_devices_count = 0;
uint8_t beaconTrovato=0;

uint8_t retval = 0;
uint8_t addr[6];
int bleTimeout;


/**
  * @brief  Test the pedestrian tag charger
  *
  *
  * @retval TRUE 	- 	the charger is working [CHRG_STATUS_CHARGE_IN_PROGRESS, CHRG_STATUS_CHARGE_COMPLETE]
  * 		FALSE 	- 	The charger is not working [the other statuses]
  */
bool test_charger()
{
	CHRG_Status_t status = CHRG_GetStatus();
	bool result = false;

	if((status == CHRG_STATUS_CHARGE_IN_PROGRESS) || (status == CHRG_STATUS_CHARGE_COMPLETE))
	{
		result = true;
	}
	else
	{
		result = false;
	}

	return result;
}

/* *
	* @brief Test UWB module.
	*
	* @param address address of the reference tag.
	*
	* @retval the distance calculated
	*
*/
//return -1: distanza non calcolata
//return -2: No connection
float test_ultrawideband(char* tdt_id, char* my_ID, char* timeOut )
{
	uint32_t address;
	uint32_t myHW_ID;
	int slen;

	slen=strlen(tdt_id);

	if (memcmp(tdt_id, "-1", slen)==0)
		address=0;
	else
		address = (uint32_t) atoi(tdt_id);

	myHW_ID= GetTagId();
	uwbTimeout= atoi(timeOut);

	//snprintf( my_ID, size, "%d", myHW_IDx );
	//sprintf(my_ID, "%d",myHW_ID);
	itoa(myHW_ID, my_ID, 10);

	//Reset distance to default value
	test_parameter.uwb_distance = -1;
	test_parameter.uwb_connected = false;

	//MYMOD
	test_parameter.min_distance = 99999;
	test_parameter.mean_distance=0;
	test_parameter.max_distance=0;
	test_parameter.variance_distance=0;
	test_parameter.number_of_measurement=0;

	if(address == 0)
	{
		test_parameter.uwb_test_address = ADDRESS_BROADCAST;
	}
	else
	{
		test_parameter.uwb_test_address = address;
	}

	//enable uwb communication
	//test_parameter.uwb_test_enable = true;

	startUWBTask();
	/* TEST */

	uint32_t start_time = xTaskGetTickCount();

	while(/*(test_parameter.uwb_distance == -1) &&*/ ((xTaskGetTickCount() - start_time) < uwbTimeout))
	{
		osDelay(1000);
		osThreadYield();
	}

	//if (test_parameter.mean_distance!=0)
	if(test_parameter.number_of_measurement!=0)
	{
		test_parameter.mean_distance = test_parameter.mean_distance/test_parameter.number_of_measurement;
		test_parameter.uwb_distance  = test_parameter.mean_distance;
		test_parameter.variance_distance = (((test_parameter.max_distance - test_parameter.uwb_distance) * (test_parameter.max_distance - test_parameter.uwb_distance))+((test_parameter.min_distance - test_parameter.uwb_distance) * (test_parameter.min_distance - test_parameter.uwb_distance)))/2;
	}
	else
	{
		test_parameter.uwb_distance=-1;
	}

	//osDelay(UWB_WAIT_TIME);
	stopUWBTask();
	//disable uwb communication

	float result;

	if(get_uwb_connected_flag() == false)
	{
		result = -2;
	}
	else
	{
		result = test_parameter.uwb_distance;
	}

	return result;
}

bool test_stop_mode()
{

	HAL_NVIC_SetPriority(RTC_WKUP_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);

	HAL_SuspendTick();

	taskDISABLE_INTERRUPTS();
	//taskENTER_CRITICAL();

	__HAL_RCC_PWR_CLK_ENABLE();

	/* Ensure that MSI is wake-up system clock */
	__HAL_RCC_WAKEUPSTOP_CLK_CONFIG(RCC_STOP_WAKEUPCLOCK_MSI);

	/* Configure the system Power */
	CallSystemPower_Config();

    /* Disable all used wakeup source */
    HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);


    /* RTC Wakeup Interrupt Generation:
      RTC_WAKEUPCLOCK_RTCCLK_DIV = RTCCLK_Div16 = 16
      Wakeup Time Base = (RTC_WAKEUPCLOCK_RTCCLK_DIV /(LSI))
      Wakeup Time = Wakeup Time Base * WakeUpCounter
        = (RTC_WAKEUPCLOCK_RTCCLK_DIV /(LSI)) * WakeUpCounter
        ==> WakeUpCounter = Wakeup Time / Wakeup Time Base

      Wakeup Time Base = 16 /(~32.000KHz) = ~0.5 ms
      Wakeup Time = 0.5 ms  * WakeUpCounter
      Therefore, with wake-up counter =  2000
         Wakeup Time =  0,5 ms *  20000 = ~ 1 sec. */
    HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, WAKE_UP_TIMER, RTC_WAKEUPCLOCK_RTCCLK_DIV16);

    /* Enter STOP 2 mode */
    HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFI);
    /* Re-configure the system clock to 80 MHz based on MSI, enable and
       select PLL as system clock source (PLL is disabled in STOP mode) */
    //SYSCLKConfig_STOP();

    CallSystemClock_Config();

    //taskEXIT_CRITICAL();
    taskENABLE_INTERRUPTS();

    HAL_ResumeTick();

    return true;
}



bool test_motion()
{
	uint32_t start_meas;
	bool result;

	uint32_t dt = 0;

	//enableMotion(false);

	//osDelay(500);
	//HAL_Delay(500);

	enableMotion(true);
	//PWR_motionFlagEnableInterrupt(false);

	start_meas = xTaskGetTickCount();

	while((readMotionFlag() == 0) && (dt < MOTION_WAIT_TIME))
	//while((readMotionFlag() == 0))
	{
		dt = (xTaskGetTickCount() - start_meas);
//		osDelay(100);
//		osThreadYield();

		//HAL_Delay(500);
	}

	result = readMotionFlag();
	enableMotion(false);

	//PWR_motionFlagEnableInterrupt(false);

	return result;

}

float test_ultrawideband_get_distance()
{
	return test_parameter.uwb_distance;
}

void test_ultrawideband_set_distance(float input)
{
	test_parameter.uwb_distance = input;
}


void test_param_init()
{
	test_parameter.uwb_distance = -1;
	test_parameter.uwb_test_enable = false;
	test_parameter.uwb_test_address = -1;
	test_parameter.wake_up_counter =0;

	test_parameter.min_distance = 99999;
	test_parameter.max_distance = 0;
	test_parameter.mean_distance = 0;
	test_parameter.variance_distance = 0;
	test_parameter.number_of_measurement = 0;
    /*osThreadDef(testTask, StartTestTask, osPriorityNormal, 0, 384);
    osMutexDef(testMutex);

    //tag_helper(0);

    app.testTask.Handle     = osThreadCreate(osThread(testTask), NULL);
    app.testTask.MutexId    = osMutexCreate(osMutex(testMutex));*/


}

bool get_uwb_enable_flag()
{
	return test_parameter.uwb_test_enable;
}

void set_uwb_enable_flag(bool input)
{
	test_parameter.uwb_test_enable = input;
}

bool get_uwb_connected_flag()
{
	return test_parameter.uwb_connected;
}

void set_uwb_connected_flag(bool input)
{
	test_parameter.uwb_connected = input;
}

uint16_t get_uwb_address_target()
{
	return test_parameter.uwb_test_address;
}

void set_uwb_address_target(uint16_t input)
{
	test_parameter.uwb_test_address = input;
}

uint32_t get_wake_up_counter()
{
	return test_parameter.wake_up_counter;
}

void set_wake_up_counter(uint32_t input)
{
	test_parameter.wake_up_counter = input;
}



uint32_t get_min_distance()
{
	return test_parameter.min_distance;
}
void set_min_distance(uint32_t input)
{
	test_parameter.min_distance = input;
}
uint32_t get_max_distance()
{
	return test_parameter.max_distance;
}
void set_max_distance(uint32_t input)
{
	test_parameter.max_distance = input;
}

//GB
float get_mean_distance()
{
	return test_parameter.mean_distance ;
}
void set_mean_distance(float input)
{
	test_parameter.mean_distance = input;
}
//GB/

uint32_t get_variance_distance()
{
	return test_parameter.variance_distance;
}
void set_variance_distance(uint32_t input)
{
	test_parameter.variance_distance = input;
}
uint8_t get_n_measurement()
{
	return test_parameter.number_of_measurement;
}
void set_n_measurement(uint8_t input)
{
	test_parameter.number_of_measurement = input;
}





#ifdef TAG_PERSONA

//bool get_motion_b1_flag()
//{
//	return test_parameter.motion_bt1_pressed;
//}
//
//void set_motion_b1_flag(bool input)
//{
//	test_parameter.motion_bt1_pressed = input;
//}
//
//bool get_motion_b2_flag()
//{
//	return test_parameter.motion_bt2_pressed;
//}
//
//void set_motion_b2_flag(bool input)
//{
//	test_parameter.motion_bt2_pressed = input;
//}

void testLATLED()
{
	//printf ("INIT TEST LATLED\n ");
	for (char count=0; count<3;count++)
	{
		 //HAL_Delay(500);
		osDelay (500);

		//LATLED: accendo alternativamente i colori Rosso, Verde e Blu ripetendo ciclicamente la sequenza
		 HAL_GPIO_WritePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin, GPIO_PIN_SET);
		 HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin, GPIO_PIN_RESET);

		 //HAL_Delay(500);
		osDelay (500);

		 HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin, GPIO_PIN_SET);
		 HAL_GPIO_WritePin(LLED_GREEN_GPIO_Port, LLED_GREEN_Pin, GPIO_PIN_RESET);

		 //HAL_Delay(500);
		osDelay (500);

		 HAL_GPIO_WritePin(LLED_GREEN_GPIO_Port, LLED_GREEN_Pin, GPIO_PIN_SET);
		 HAL_GPIO_WritePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin, GPIO_PIN_RESET);

	}
}

void testMAINLED()
{

	//printf ("INIT TEST MAINLED\n ");
	for (char count=0; count<3;count++)
	{
		osDelay(500);

		//LATLED: accendo alternativamente i colori Rosso, Verde e Blu ripetendo ciclicamente la sequenza
		setPWM(htim3, TIM_CHANNEL_2, 255, 0); 	//BLUE oFF
		setPWM(htim2, TIM_CHANNEL_2, 255, 255); //RED on

		osDelay(500);

		 setPWM(htim2, TIM_CHANNEL_2, 255, 0); 		//RED off
		 //	stopPWM(htim2, TIM_CHANNEL_2);			//RED off
		 setPWM(htim3, TIM_CHANNEL_1, 255, 255); 	//GREEN on

		 osDelay(500);

		 setPWM(htim3, TIM_CHANNEL_1, 255, 0); 		//GREEN off
		 setPWM(htim3, TIM_CHANNEL_2, 255, 255); 	//BLUE on

	}

	//spengo tutto
	setPWM(htim3, TIM_CHANNEL_2, 255, 0); 	//BLUE oFF

}

void testBUZZER()
{
	//printf ("INIT TEST BUZZER\n ");
	for (char count=0; count<3;count++)
	{
		setPWM(htim1, TIM_CHANNEL_3, 32767, 16383); // Buzzer On 2.2KHz

		 //HAL_Delay(500);
		osDelay (500);

		stopPWM(htim1, TIM_CHANNEL_3); 	// Buzzer Off

		 //HAL_Delay(500);
		 osDelay (500);
	}

}

void testHAPTIC()
{
	uint16_t period, dutyCycle;

	//printf ("INIT TEST HAPTIC\n ");
	for (char count=0; count<3;count++)
	{

		dutyCycle=80*((32767)/100);

		//setPWM(htim3, TIM_CHANNEL_4, 32767, 16383); 	// Originale - MOTOR On 2.2KHz
		setPWM(htim3, TIM_CHANNEL_4, 32767, dutyCycle); // MOTOR On 2.2KHz

		 //HAL_Delay(500);
		osDelay (1000);

		stopPWM(htim3, TIM_CHANNEL_4);	// MOTOR Off

		 //HAL_Delay(500);
		osDelay (500);
	}

}
#endif

#ifdef TAG_CARRELLO
//U|CAN / U|CAN|OK
boolean_t testCAN (CAN_HandleTypeDef* pHandl)
{
	static const  int TIMEOUT_CAN_TEST= 5000U;
	uint32_t tickstart;
	boolean_t retval = FALSE;

	//ENABLE CAN TRANSCEIVER
	 HAL_GPIO_WritePin(GPIOA, CAN_SHDN_Pin, GPIO_PIN_RESET);

	//CAN GIà INIZIALIZZATO e CONFIGURATO FILTRO RX

	 /*##-3- Start the CAN peripheral ###########################################*/
	  if (HAL_CAN_Start(pHandl) != HAL_OK)
	  {
		/* Start Error */
		Error_Handler();
	  }

	  /*##-5- Configure Transmission process #####################################*/
	  TxHeader.StdId = 0x321;
	  TxHeader.ExtId = 0x01;
	  TxHeader.RTR = CAN_RTR_DATA;
	  TxHeader.IDE = CAN_ID_STD;
	  TxHeader.DLC = 4;
	  TxHeader.TransmitGlobalTime = DISABLE;

	  /* Set the data to be transmitted */
	  TxData[0] = 'P';
	  TxData[1] = 'I';
	  TxData[2] = 'N';
	  TxData[3] = 'G';

	  /* Start the Transmission process */
	  if (HAL_CAN_AddTxMessage(pHandl, &TxHeader, TxData, &TxMailbox) != HAL_OK)
	  {
		  /* Transmission request Error */
		  //Error_Handler();
		  return FALSE;
	  }
	  HAL_Delay(10);

	  /* Get tick */
	  tickstart = HAL_GetTick();

	  while ((HAL_GetTick() - tickstart) < TIMEOUT_CAN_TEST)
	  {
		  /* Get RX message */
		  if (HAL_CAN_GetRxMessage(pHandl, CAN_RX_FIFO0, &RxHeader, RxData) == HAL_OK)
		  {
				// risposta arrivata - verifico
				if ((RxHeader.StdId == 0x321) && (RxHeader.IDE == CAN_ID_STD) && (RxHeader.DLC == 4))
				{
					 // se arriva un PING rispondo con PONG
					 if ((RxData[0]=='P') && (RxData[1]=='O') && (RxData[2]=='N') && (RxData[3]=='G'))
					 {
						 return TRUE;
					 }
				}

		  }

	  }
	  /* Update error code */
	  pHandl->ErrorCode |= HAL_CAN_ERROR_TIMEOUT;

	  /* Change CAN state */
	  pHandl->State = HAL_CAN_STATE_ERROR;

	  /*##-3- Start the CAN peripheral ###########################################*/
	  if (HAL_CAN_Stop(pHandl) != HAL_OK)
	  {
		/* Start Error */
		Error_Handler();
	  }


	  return FALSE;

}

void testRELE1()
{
	//printf ("INIT TEST RELE1\n ");

	 //HAL_Delay(1000);
	 //osDelay (2000);

	 //ATTIVO RELE1:
	 HAL_GPIO_WritePin(GPIOA, RELAY_K1_Pin, GPIO_PIN_SET);

	 //HAL_Delay(1000);
	 osDelay (3000);

	 //DISATTIVO RELE1:
	 HAL_GPIO_WritePin(GPIOA, RELAY_K1_Pin, GPIO_PIN_RESET);

}

void testRELE2()
{
	//printf ("INIT TEST RELE2\n ");

	//HAL_Delay(1000);
	//osDelay (2000);

	 //ATTIVO RELE2:
	 HAL_GPIO_WritePin(GPIOA, RELAY_K2_Pin, GPIO_PIN_SET);

	 //HAL_Delay(1000);
	 osDelay (3000);

	 //DISATTIVO RELE2:
	 HAL_GPIO_WritePin(GPIOA, RELAY_K2_Pin, GPIO_PIN_RESET);

}
#endif

boolean_t testFLASH ()
{
	//const uint8_t SIZE=10;
	uint8_t pWBuffer[10] = {0,1,2,3,4,5,6,7,8,9};
	uint8_t pRBuffer[10] = {0};

	boolean_t retval = FALSE;

	if (EFLASH_Init())
	{
		//if (EFLASH_EraseSector(0)) 		//erase I settore (4KB)
		//if (EFLASH_EraseBlock(0))   		//erase I blocco (64KB)
		//if (EFLASH_EraseChip())      		//CHIP ERASE

		//CANCELLO TUTTA LA I PARTE
		for( int i = INIT_ADDR/EFLASH_SECTOR_SIZE ; i <=SERIAL_ADDR/EFLASH_SECTOR_SIZE ; i++)
		{
			if (EFLASH_EraseSector(i)==FALSE)
				return FALSE;
		}

		if (EFLASH_Write(0, pWBuffer, sizeof(pWBuffer)))
		{
			if( EFLASH_Read(0, pRBuffer, sizeof(pRBuffer)) )
			{
				//verifica
				for( char i = 0 ; i < 10 ; i++)
				{
					if (pRBuffer[i]!=pWBuffer[i])
					{
						return FALSE;
					}

				}

				if (EFLASH_EraseSector(0)) 	  	//erase I settore (4KB)
					//verifica ok
					retval=TRUE;
			}
		}
	}

	return retval;
}

// scrivo il codice seriale all'indirizzo 0 della Flash
//-ritorno vero se sono riuscito a scrivere e leggere il codice seriale
boolean_t writeSerialCode(char* serialCode)
{
	//const uint8_t SIZE=10;
	//uint8_t serialCodeRead[50] = {0};

	uint32_t data_size = strlen(serialCode);

	boolean_t retval = FALSE;

	if (EFLASH_Init())
	{
		if (EFLASH_EraseSector(SERIAL_ADDR/EFLASH_SECTOR_SIZE)) 	//erase settore (4KB)
		//EFLASH_EraseBlock(0);   	  	  							//erase blocco (64KB)
		//EFLASH_EraseChip();      									//CHIP ERASE
		{
			if (EFLASH_Write(SERIAL_ADDR, serialCode, data_size))
			{
				//serialCode[0]=0;
				if( EFLASH_Read(SERIAL_ADDR, serialCode, data_size) )
					//verifica ok
					retval=TRUE;
			}
		}
	}

	return retval;

}

// testACC
boolean_t testACC (ACC_Measure_t* acc)
{
	ISM_Measure_t measure={0,0,0,0,0,0,0};

	boolean_t retval = FALSE;

	//IMM_ACC_ODR_416_HZ,IMM_ACC_RANGE_16G,IMM_GYRO_ODR_416_HZ,IMM_GYRO_RANGE_245_DPS
	// AccSensitivity:0, GyroSensitivity:0
	if (ISM_Init())
	{

		for( char i = 0 ; i < 5 ; i++)
		{
			// valori in m/s^2
			ISM_GetLastMeasure(&measure);

			acc->AccX+=measure.AccX;
			acc->AccY+=measure.AccY;
			acc->AccZ+=measure.AccZ;

			//HAL_Delay(100);
			osDelay (100);
		}

		acc->AccX=acc->AccX/5;
		acc->AccY=acc->AccY/5;
		acc->AccZ=acc->AccZ/5;

		//converto m/s^2->g
		acc->AccX=acc->AccX/ISM_G_TO_MS2;
		acc->AccY=acc->AccY/ISM_G_TO_MS2;
		acc->AccZ=acc->AccZ/ISM_G_TO_MS2;

		//verifica ok
		retval=TRUE;

		ISM_DeInit();
	}

	return retval;
}


/**
 * Compare Bluetooth addresses
 *
 * @param first First address
 * @param second Second address
 * @return Zero if addresses are equal
 */
int cmp_bdaddr(bd_addr first, bd_addr second)
{
    int i;
    for (i = 0; i < sizeof(bd_addr); i++)
    {
        if (first.addr[i] != second.addr[i])
        	return 1;
    }
    return 0;
}

extern UART_HandleTypeDef huart5;
#define DBG_ENTRY(value)  HAL_UART_Transmit(&huart5, &value, sizeof(value), 1000);

void ble_evt_gap_scan_response(const struct ble_msg_gap_scan_response_evt_t *msg)
{

	//DBG_ENTRY("ble_evt_gap_scan_response");

	//beacon non trovato
    if (found_devices_count >= MAX_DEVICES)
    	beaconTrovato=0;

    int i;
    char *name = NULL;

    // Check if this device already found
    for (i = 0; i < found_devices_count; i++)
    {
        if (!cmp_bdaddr(msg->sender, found_ble[i].bdAddr)) return;
    }
    found_devices_count++;
    memcpy(found_ble[i].bdAddr.addr, msg->sender.addr, sizeof(bd_addr));

    // Parse data
    for (i = 0; i < msg->data.len; )
    {
        int8 len = msg->data.data[i++];
        if (!len) continue;
        if (i + len > msg->data.len) break; // not enough data
        uint8 type = msg->data.data[i++];
        switch (type)
        {
        	case 0x09:
				name = malloc(len);
				memcpy(name, msg->data.data + i, len - 1);
				name[len - 1] = '\0';
        }
        i += len - 1;
    }

    found_ble[found_devices_count-1].bdAddr=msg->sender;
    found_ble[found_devices_count-1].rssi=msg->rssi;

    free(name);
}


//true se trovato
uint8_t trovaBeacon (bd_addr connect_addr, char* rssi)
{
	 int i;
	 for (i = 0; i < found_devices_count; i++ )
	 {
		  if (cmp_bdaddr(connect_addr, found_ble[i].bdAddr)==0)
		  {
			  //sprintf(rssi, "%d", found_ble[i].rssi);
			  itoa(found_ble[i].rssi, rssi, 10);
			  return 1;
		  }
	 }
	 return 0;
}

void initScan ()
{
	found_devices_count=0;
}

// test connessione ble
//ritorno vero se il beacon segnalato sia stato trovato entro il timeout  (invio RSSI individuato).
uint8_t TestConnectBLE(char* mac, char* timeOut,   char* rssi)
{
	//const uint8_t SIZE=10;
	//uint8_t serialCodeRead[50] = {0};

	bleTimeout= atoi(timeOut);

//	addr[5]= (uint8_t) strtol(MAC_BLE, NULL, 16);
//	addr[4]= (uint8_t) strtol(MAC_BLE+3, NULL, 16);
//	addr[3]= (uint8_t) strtol(MAC_BLE+6, NULL, 16);
//	addr[2]= (uint8_t) strtol(MAC_BLE+9, NULL, 16);
//	addr[1]= (uint8_t) strtol(MAC_BLE+12, NULL, 16);
//	addr[0]= (uint8_t) strtol(MAC_BLE+15, NULL, 16);

//	connect_addr.addr[5]= 0x88;
//	connect_addr.addr[4]= 0x6B;
//	connect_addr.addr[3]= 0x0F;
//	connect_addr.addr[2]= 0xF6;
//	connect_addr.addr[1]= 0xC8;
//	connect_addr.addr[0]= 0xB9;

	for (uint8_t i = 0; i < 6; i++)
		connect_addr.addr[5-i]= (uint8_t) strtol(mac+(i*3), NULL, 16);

	//taskENTER_CRITICAL();
	//lancio il task
	initScan();
	BGLIB_DBG_ScanStart();

	//BGLIB_DBG_ScanStart();

	//taskEXIT_CRITICAL();
	//BGLIB_DBG_ScanStart();

	//	 uint32_t start_time= xTaskGetTickCount();
	//	 while( ((xTaskGetTickCount() - start_time) < bleTimeout))
	//	 {
	//		 osDelay(1000);
	//		 osThreadYield();
	//	 }

	//while (1); 		//TO TEST ONLY

	//osDelay(bleTimeout);
	osDelay(3000);

	// verifica ble trovati

	BGLIB_DBG_ScanStop();
	//BGLIB_DeInit();

//	if (!terminatebleTask())
//		return FALSE;

	//trova e ritorna RSSI
	if (trovaBeacon (connect_addr, rssi))
		retval=TRUE;
	else
		retval=FALSE;

	return retval;

}

bool test_switch_off_mode()
{
	HAL_UART_DeInit(&huart5);

	EFLASH_Init();

	uint8_t pWBuffer[1] = {SWITCH_OFF_FLASH_FLAG};
	uint8_t pRBuffer[1] = {0};

	EFLASH_EraseSector(0x00);
	EFLASH_Write(0x00, pWBuffer, sizeof(pWBuffer));
	EFLASH_Read(0x00, pRBuffer, sizeof(pRBuffer));

	/* Configure the system Power */
	CallSystemPower_Config2();

	PWR_systemSwitchOff();

	return false;
}

void PWR_systemSwitchOff(void)
{
    if(!CHRG_IsConnected())
    {
        //CHRG_IntPinConfig(0);

        //MX_GPIO_enableLPCAsEvent();
//        GPIO_InitTypeDef GPIO_InitStruct;
//        HAL_NVIC_DisableIRQ(EXTI0_IRQn);
//        HAL_GPIO_DeInit(LPC_INT_GPIO_Port, LPC_INT_Pin);
//        GPIO_InitStruct.Pin = LPC_INT_Pin;
//        GPIO_InitStruct.Mode = GPIO_MODE_EVT_FALLING;
//        GPIO_InitStruct.Pull = GPIO_NOPULL;
//        HAL_GPIO_Init(LPC_INT_GPIO_Port, &GPIO_InitStruct);
//
//        PCAL6416A_Reset();    // VB: Reset GPIO Expander + LPC_WAKE in stato RESET
//
//        __disable_irq();
//        __disable_fault_irq();
//        HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);


#if SYS_DEBUG_SWITCH_OFF
        HAL_Delay(5000);
#endif
        printf("Go to OFF.\n");

        /*Configure GPIO pin Output Level */
        HAL_GPIO_WritePin(POWER_3V3_ENABLE_GPIO_Port, POWER_3V3_ENABLE_Pin, GPIO_PIN_RESET);

        while(1)
        {

        }
        printf("After OFF - Never shown\n");
    }
    else
    {
        printf("Cannot Switch Off when in charge.\n");
    }
}



