/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <port.h>
#include <config.h>
#include <task_ctrl.h>
#include <task_flush.h>
#include <task_tag.h>
#include "stm32l4xx_hal.h"
#include <tagManager.h>
#include "randomSlotAssignment.h"
#include "stdio.h"
#include "i2c.h"
#include "nxp_pcal6416a.h"
#include "BGLIB_Module.h"
#include "W25Q128FV.h"

#include <test.h>
#include "nxp_pcal6416a.h"
#include "uart_command.h"
#include "inertial_sensing.h"

#include    "stdlib.h"


/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
uint8_t data234567 = 0;
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
CAN_HandleTypeDef hcan1;

//I2C_HandleTypeDef hi2c2;

IWDG_HandleTypeDef hiwdg;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
DMA_HandleTypeDef hdma_spi1_rx;
DMA_HandleTypeDef hdma_spi1_tx;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart4;
UART_HandleTypeDef huart5;
DMA_HandleTypeDef hdma_uart5_tx;

DMA_HandleTypeDef hdma_memtomem_dma1_channel1;
osThreadId defaultTaskHandle;
/* USER CODE BEGIN PV */
uint8_t signalsAlarmArea = 0;
uint8_t enablesignals = 0;

/* All global variables are in the "app" structure */
app_t app;
uint8_t bufferCommand[50];
uint8_t commandLength;
uint8_t cmdID;
uint8_t bufferResp[50];

uint8_t serialCodeR[50];

// HW address del modulo UWB con cui calcolare la distanza
//-1 messaggio Broadcast
uint8_t TGT_ID[50];
uint8_t ID_DW1000[50];
uint8_t DISTANZA_CALCOLATA[50];
uint8_t TO_UWB[10];
int i=0;

//float distCalc;
int32_t distCalc;
uint32_t HW_ID; //TO TEST
uint32_t minDist;
uint32_t maxDist;
uint32_t var;

#define FLOAT_TO_INT(x) ((x)>=0?(int32_t)((x)+0.5):(int32_t)((x)-0.5))

float distCalcF;
float HW_IDF; //TO TEST
float minDistF;
float maxDistF;
float varF;

//BLE
uint8_t MAC_BLE[50];
uint8_t RSSI_BLE[50];
uint8_t TO_BLE[10];
uint8_t serialCodeR[50];

//ACC
char str1[25];
char str2[25];
char str3[25];

int32_t accXmg;
int32_t accYmg;
int32_t accZmg;

ACC_Measure_t acc={0,0,0};

//RELE
char relNum;

//MOTION
char motion=0;

//SWITCHOFF
uint8_t pRBuffer[1] = {0};

#ifdef TAG_PERSONA

//BUTTONS, Charger e Antitamper BLOCCATI
volatile char motion_bt1_pressed=1;
volatile char motion_bt2_pressed=1;

volatile char antitamper_disabled=1;
volatile char charger_disabled=1;


#endif

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_IWDG_Init(void);
static void MX_RTC_Init(void);
static void MX_I2C2_Init(void);
static void MX_SPI1_Init(void);
static void MX_ADC1_Init(void);
static void MX_SPI2_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
void MX_UART5_Init(void);
void MX_UART4_Init(void);

static void MX_CAN1_Init(void);
static void CAN_Config(void);
static void SystemPower_Config(void);
static void SystemPower_Config2(void);

void StartDefaultTask(void const * argument);

static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */
//static void setPWM(TIM_HandleTypeDef, uint32_t, uint16_t, uint16_t);

#ifdef SWODEBUG
int _write(int file, char *ptr, int len)
{
  /* Implement your write code here, this is used by puts and printf for example */
  int i=0;
  for(i=0 ; i<len ; i++)
    ITM_SendChar((*ptr++));
  return len;
}
#endif SWODEBUG


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

static void peripheralReset(void)
{
	SysTick->CTRL ^= SysTick_CTRL_ENABLE_Msk;
	/*
	 * Reset all peripheral after bootloader.
	 *
	 */
	/*__HAL_RCC_AHB1_FORCE_RESET();
	__HAL_RCC_AHB2_FORCE_RESET();
	__HAL_RCC_AHB3_FORCE_RESET();
	__HAL_RCC_APB1_FORCE_RESET();
	__HAL_RCC_APB2_FORCE_RESET();

	__HAL_RCC_AHB1_RELEASE_RESET();
	__HAL_RCC_AHB2_RELEASE_RESET();
	__HAL_RCC_AHB3_RELEASE_RESET();
	__HAL_RCC_APB1_RELEASE_RESET();
	__HAL_RCC_APB2_RELEASE_RESET();
*/
#ifdef DEBUG
	__HAL_DBGMCU_FREEZE_IWDG();
#endif


}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
	//MY_MOD
  /* USER CODE BEGIN 1 */
	peripheralReset();
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  //MX_IWDG_Init();
  MX_RTC_Init();
  //  MX_I2C2_Init(); // TODO: Inizializzato nel file i2c
  MX_SPI1_Init();
  MX_ADC1_Init();
  MX_SPI2_Init();
  MX_TIM1_Init();
#ifndef SWODEBUG
  MX_TIM2_Init();
#endif

  MX_TIM3_Init();
  MX_UART5_Init();
  MX_UART4_Init();

#ifdef TAG_CARRELLO
  MX_CAN1_Init();
  CAN_Config();  	//CONFIG FILTRO RICEZIONE, E AVVIO IL MODULO
#endif

   I2C_Init(I2C_ID_2);

   CHRG_Init(); 					//Init power charger: Legge e setta alcuni registri del BQ24250

   PCAL6416A_Reset();
   PCAL6416A_Init(1);			// configura il chip IO expander abilitando l'INT su LPC-INT
   PWR_CheckExtenderConfig(); 	// configura i pin dell'IO expander

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */

  memset(&app,0,sizeof(app));

  load_bssConfig();                  /**< load the RAM Configuration parameters from NVM block */
  app.pConfig = get_pbssConfig();    /**< app.pConfig is pointed to the RAM (bssConfig) */

  app.xStartTaskEvent = xEventGroupCreate(); /**< xStartTaskEvent indicates which tasks has been started */

  HAL_IWDG_Refresh(&hiwdg);

  port_wakeup_dw1000(DW_A); //wakeup dw1000

  reset_DW1000();

  error_e res = InitTagManager();

  if(res != _NO_ERR)
    error_handler(1, res);

  uint32_t randomSeed = GetTagId();
  InitRandomSlotAssignment(randomSeed);

  //configInit();

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  //HAL_UART_Receive_IT(&huart5, UsartDataUpdate, 1);
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */

  /* FlushTask is always working and flushing the output buffer to uart/usb : 96 + 512bytes of stack */
  osThreadDef(flushTask, FlushTask, osPriorityBelowNormal, 0, 128);
  app.flushTask.Handle = osThreadCreate(osThread(flushTask), NULL);

  /* ctrlTask is always working serving rx from uart/usb it uses 96 + 1024 bytes of stack  */
  //1024B for CTRL task: it needs a lot of memory: it uses malloc(127), sscanf(212bytes)
  osThreadDef(ctrlTask, CtrlTask, osPriorityLow, 0, 512);
  app.ctrlTask.Handle = osThreadCreate(osThread(ctrlTask), NULL);

  osThreadDef(BGLIB_RxHandlerTask, BGLIB_RxHandler, osPriorityNormal, 0, 512);
  app.bleTask.Handle = osThreadCreate(osThread(BGLIB_RxHandlerTask), NULL);

  if( !defaultTaskHandle | !app.flushTask.Handle | !app.ctrlTask.Handle | !app.bleTask.Handle )
  {
	  error_handler(1, _Err_Create_Task_Bad);
  }

  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

void CallSystemClock_Config()
{
	SystemClock_Config();
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  /*RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE
                              |RCC_OSCILLATORTYPE_LSE;*/
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;

  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 18;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  /*PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_UART4
                              |RCC_PERIPHCLK_UART5|RCC_PERIPHCLK_I2C2
                              |RCC_PERIPHCLK_ADC;*/
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_UART4;

  PeriphClkInit.Uart4ClockSelection = RCC_UART4CLKSOURCE_PCLK1;
  PeriphClkInit.Uart5ClockSelection = RCC_UART5CLKSOURCE_PCLK1;
  PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 8;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* DMA1_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
  /* DMA1_Channel3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);
  /* SPI1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SPI1_IRQn, 4, 0);
  HAL_NVIC_EnableIRQ(SPI1_IRQn);
  /* UART4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(UART4_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(UART4_IRQn);
  /* UART5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(UART5_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(UART5_IRQn);
}

//MY_MOD
/**
  * @brief CAN1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN1_Init(void)
{

  /* USER CODE BEGIN CAN1_Init 0 */

  /* USER CODE END CAN1_Init 0 */

  /* USER CODE BEGIN CAN1_Init 1 */

  /* USER CODE END CAN1_Init 1 */
  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 16;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SyncJumpWidth = CAN_SJW_2TQ;
  hcan1.Init.TimeSeg1 = CAN_BS1_13TQ;
  hcan1.Init.TimeSeg2 = CAN_BS2_2TQ;
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = DISABLE;
  hcan1.Init.AutoWakeUp = DISABLE;
  hcan1.Init.AutoRetransmission = DISABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN1_Init 2 */

  /* USER CODE END CAN1_Init 2 */

}

static void CAN_Config(void)
{
  CAN_FilterTypeDef  sFilterConfig;

  /*##-2- Configure the CAN Filter ###########################################*/
  sFilterConfig.FilterBank = 0;
  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilterConfig.FilterIdHigh = 0x0000;
  sFilterConfig.FilterIdLow = 0x0000;
  sFilterConfig.FilterMaskIdHigh = 0x0000;
  sFilterConfig.FilterMaskIdLow = 0x0000;
  sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.SlaveStartFilterBank = 14;

  if (HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig) != HAL_OK)
  {
    /* Filter configuration Error */
    Error_Handler();
  }

}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the ADC multi-mode
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_15;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
//static void MX_I2C2_Init(void)
//{
//
//  /* USER CODE BEGIN I2C2_Init 0 */
////
//  /* USER CODE END I2C2_Init 0 */
//
//  /* USER CODE BEGIN I2C2_Init 1 */
////
//  /* USER CODE END I2C2_Init 1 */
//  hi2c2.Instance = I2C2;
//  hi2c2.Init.Timing = 0x10808DD3;
//  hi2c2.Init.OwnAddress1 = 0;
//  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
//  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
//  hi2c2.Init.OwnAddress2 = 0;
//  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
//  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
//  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
//  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  /** Configure Analogue filter
//  */
//  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  /** Configure Digital filter
//  */
//  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  /* USER CODE BEGIN I2C2_Init 2 */
////
//  /* USER CODE END I2C2_Init 2 */
//
//}

/**
  * @brief IWDG Initialization Function
  * @param None
  * @retval None
  */
static void MX_IWDG_Init(void)
{

  /* USER CODE BEGIN IWDG_Init 0 */

  /* USER CODE END IWDG_Init 0 */

  /* USER CODE BEGIN IWDG_Init 1 */

  /* USER CODE END IWDG_Init 1 */
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_64;
  hiwdg.Init.Window = 4095;
  hiwdg.Init.Reload = 4095;
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN IWDG_Init 2 */

  /* USER CODE END IWDG_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
/*{



  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};


  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 1;
  hrtc.Init.SynchPrediv = 32767;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }


  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_RTCEx_SetWakeUpTimer(&hrtc, 0, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
  {
    Error_Handler();
  }


}*/

{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};
  RTC_AlarmTypeDef sAlarm = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the Alarm A
  */
  sAlarm.AlarmTime.Hours = 0x0;
  sAlarm.AlarmTime.Minutes = 0x0;
  sAlarm.AlarmTime.Seconds = 0x0;
  sAlarm.AlarmTime.SubSeconds = 0x0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_NONE;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 0x1;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the Alarm B
  */
  sAlarm.AlarmDateWeekDay = 0x1;
  sAlarm.Alarm = RTC_ALARM_B;
  /** Enable the WakeUp
  */
  if (HAL_RTCEx_SetWakeUpTimer(&hrtc, 0, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
//static void MX_SPI2_Init(void)
//{
//
//  /* USER CODE BEGIN SPI2_Init 0 */
//
//  /* USER CODE END SPI2_Init 0 */
//
//  /* USER CODE BEGIN SPI2_Init 1 */
//
//  /* USER CODE END SPI2_Init 1 */
//  /* SPI2 parameter configuration*/
//  hspi2.Instance = SPI2;
//  hspi2.Init.Mode = SPI_MODE_MASTER;
//  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
//  hspi2.Init.DataSize = SPI_DATASIZE_4BIT;
//  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
//  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
//  hspi2.Init.NSS = SPI_NSS_HARD_OUTPUT;
//  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
//  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
//  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
//  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
//  hspi2.Init.CRCPolynomial = 7;
//  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
//  hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
//  if (HAL_SPI_Init(&hspi2) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  /* USER CODE BEGIN SPI2_Init 2 */
//
//  /* USER CODE END SPI2_Init 2 */
//
//}

static void MX_SPI2_Init(void)
{
    hspi2.Instance = SPI2;
    hspi2.Init.Mode = SPI_MODE_MASTER;
    hspi2.Init.Direction = SPI_DIRECTION_2LINES;
    hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
    hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
    hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;
    hspi2.Init.NSS = SPI_NSS_SOFT; //SPI_NSS_HARD_OUTPUT;
    hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
    hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
    hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi2.Init.CRCPolynomial = 7;
    hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
    hspi2.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
    if (HAL_SPI_Init(&hspi2) != HAL_OK)
    {
    	Error_Handler();
    }

  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 65535;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 4294967295;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 65535;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief UART4 Initialization Function
  * @param None
  * @retval None
  */
void MX_UART4_Init(void)
{

  /* USER CODE BEGIN UART4_Init 0 */

  /* USER CODE END UART4_Init 0 */

  /* USER CODE BEGIN UART4_Init 1 */

  /* USER CODE END UART4_Init 1 */
  huart4.Instance = UART4;
  huart4.Init.BaudRate = 115200;
  huart4.Init.WordLength = UART_WORDLENGTH_8B;
  huart4.Init.StopBits = UART_STOPBITS_1;
  huart4.Init.Parity = UART_PARITY_NONE;
  huart4.Init.Mode = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl = UART_HWCONTROL_RTS_CTS;
  huart4.Init.OverSampling = UART_OVERSAMPLING_16;
  huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART4_Init 2 */

  /* USER CODE END UART4_Init 2 */

}

/**
  * @brief UART5 Initialization Function
  * @param None
  * @retval None
  */
void MX_UART5_Init(void)
{

  /* USER CODE BEGIN UART5_Init 0 */

  /* USER CODE END UART5_Init 0 */

  /* USER CODE BEGIN UART5_Init 1 */

  /* USER CODE END UART5_Init 1 */
  huart5.Instance = UART5;
  huart5.Init.BaudRate = 115200;
  huart5.Init.WordLength = UART_WORDLENGTH_8B;
  huart5.Init.StopBits = UART_STOPBITS_1;
  huart5.Init.Parity = UART_PARITY_NONE;
  huart5.Init.Mode = UART_MODE_TX_RX;
  huart5.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart5.Init.OverSampling = UART_OVERSAMPLING_16;
  huart5.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart5.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart5) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART5_Init 2 */

  /* USER CODE END UART5_Init 2 */

}

/**
  * Enable DMA controller clock
  * Configure DMA for memory to memory transfers
  *   hdma_memtomem_dma1_channel1
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* Configure DMA request hdma_memtomem_dma1_channel1 on DMA1_Channel1 */
  hdma_memtomem_dma1_channel1.Instance = DMA1_Channel1;
  hdma_memtomem_dma1_channel1.Init.Request = DMA_REQUEST_0;
  hdma_memtomem_dma1_channel1.Init.Direction = DMA_MEMORY_TO_MEMORY;
  hdma_memtomem_dma1_channel1.Init.PeriphInc = DMA_PINC_ENABLE;
  hdma_memtomem_dma1_channel1.Init.MemInc = DMA_MINC_ENABLE;
  hdma_memtomem_dma1_channel1.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  hdma_memtomem_dma1_channel1.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
  hdma_memtomem_dma1_channel1.Init.Mode = DMA_NORMAL;
  hdma_memtomem_dma1_channel1.Init.Priority = DMA_PRIORITY_LOW;
  if (HAL_DMA_Init(&hdma_memtomem_dma1_channel1) != HAL_OK)
  {
    Error_Handler( );
  }

  /* DMA interrupt init */
  /* DMA2_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Channel1_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA2_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LLED_GREEN_Pin|BLE_RST_Pin|LLED_BLUE_Pin
                          |DW1000_WUP_Pin, GPIO_PIN_SET);

#ifdef TAG_CARRELLO
  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, CAN_SHDN_Pin|POWER_5V_ENABLE_Pin, GPIO_PIN_SET); //CAN SHUTDOWN

  /*Configure GPIO pin Output Level */
   HAL_GPIO_WritePin(GPIOA, RELAY_K1_Pin|RELAY_K2_Pin, GPIO_PIN_RESET);
#endif

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, FLASH_CS_Pin|LLED_RED_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, CHGB_TEST_Pin|POWER_SAVE_Pin|LPC_WAKE_Pin, GPIO_PIN_RESET);

#ifdef TAG_PERSONA
  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(POWER_3V3_ENABLE_GPIO_Port, POWER_3V3_ENABLE_Pin, GPIO_PIN_SET);
#endif

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(BLE_IRQ_GPIO_Port, BLE_IRQ_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : LPC_WAKE_Pin LLED_GREEN_Pin BLE_RST_Pin LLED_BLUE_Pin
                           DW1000_WUP_Pin CHGB_TEST_Pin POWER_SAVE_Pin */
  GPIO_InitStruct.Pin = LPC_WAKE_Pin|LLED_GREEN_Pin|BLE_RST_Pin|LLED_BLUE_Pin
                          |DW1000_WUP_Pin|CHGB_TEST_Pin|POWER_SAVE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  //GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);


/*Configure GPIO pin : LPC_INT_Pin */
GPIO_InitStruct.Pin = LPC_INT_Pin;
#ifdef TAG_CARRELLO
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
#endif
#ifdef TAG_PERSONA
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
#endif

  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(LPC_INT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PA1 PA2 PA11 PA12 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);


#ifdef TAG_CARRELLO
  /*Configure GPIO pin : CAN_SHDN_Pin */
  GPIO_InitStruct.Pin = CAN_SHDN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(CAN_SHDN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : POWER_5V_ENABLE_Pin RELAY_K1_Pin RELAY_K2_Pin */
  GPIO_InitStruct.Pin = POWER_5V_ENABLE_Pin|RELAY_K1_Pin|RELAY_K2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
#endif

#ifdef TAG_PERSONA
  /*Configure GPIO pin : POWER_3V3_ENABLE_Pin */
  GPIO_InitStruct.Pin = POWER_3V3_ENABLE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(POWER_3V3_ENABLE_GPIO_Port, &GPIO_InitStruct);
#endif

  /*Configure GPIO pins : DW1000_RST_Pin CHG_STATUS_Pin */
  GPIO_InitStruct.Pin = DW1000_RST_Pin|CHG_STATUS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : DW1000_IRQ_Pin CHG_INT_EXTI8_Pin */
  GPIO_InitStruct.Pin = DW1000_IRQ_Pin|CHG_INT_EXTI8_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PB1 PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : FLASH_CS_Pin */
  GPIO_InitStruct.Pin = FLASH_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(FLASH_CS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : BLE_IRQ_Pin LLED_RED_Pin */
  GPIO_InitStruct.Pin = BLE_IRQ_Pin|LLED_RED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : DW1000_CS_Pin */
	GPIO_InitStruct.Pin = DW1000_CS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(DW1000_CS_GPIO_Port, &GPIO_InitStruct);

	HAL_GPIO_WritePin(DW1000_CS_GPIO_Port, DW1000_CS_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(DW1000_WUP_GPIO_Port, DW1000_WUP_Pin, GPIO_PIN_SET);

}

/* USER CODE BEGIN 4 */
void setPWM(TIM_HandleTypeDef timer, uint32_t channel, uint16_t period,
uint16_t pulse)
{
 HAL_TIM_PWM_Stop(&timer, channel); // stop generation of pwm
 TIM_OC_InitTypeDef sConfigOC;
 timer.Init.Period = period; // set the period duration
 HAL_TIM_PWM_Init(&timer); // reinititialise with new period value
 sConfigOC.OCMode = TIM_OCMODE_PWM1;
 sConfigOC.Pulse = pulse; // set the pulse duration
 sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
 sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
 HAL_TIM_PWM_ConfigChannel(&timer, &sConfigOC, channel);
 HAL_TIM_PWM_Start(&timer, channel); // start pwm generation
}

void stopPWM(TIM_HandleTypeDef timer, uint32_t channel)
{
 HAL_TIM_PWM_Stop(&timer, channel); // stop generation of pwm
}

void setSignmalsAlarm(uint8_t AlarmTipe, uint8_t enable)
{
	signalsAlarmArea = AlarmTipe;
	enablesignals = enable;

}


/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
	uint32_t time_ms;

	/*Init*/
	test_param_init();
	/* USER CODE BEGIN 5 */
	/* Infinite loop */

	openUartCommand();

	//testUartCommand(&huart5);
	strcpy(bufferResp, "U|START\r\n");

	if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
	{
	   /* Transfer error in transmission process */
	   Error_Handler();
	}

#ifdef TAG_PERSONA
	// SWITCH OFF CHECK - START
	EFLASH_Init();
	EFLASH_Read(0, pRBuffer, sizeof(pRBuffer));

	//U|SWITCHOFF - U|START
	//U|CHARGER|OK / U|CHARGER|KO
	if(pRBuffer[0] == SWITCH_OFF_FLASH_FLAG)
	{
		EFLASH_EraseSector(0x00);

		//osDelay(1000);
		time_ms = HAL_GetTick();
		while (((HAL_GetTick() - time_ms) < 5000) && (myCHRG_GetStatus()==0));

		//verifico stato caricatore
		if (myCHRG_GetStatus())
		{
			strcpy(bufferResp, "U|CHARGER|OK\r\n");
			//CHRG_IntPinConfig(1);
		}
		else
			strcpy(bufferResp, "U|CHARGER|KO\r\n");


		if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
		{
		   /* Transfer error in transmission process */
		   Error_Handler();
		}

		PCAL6416A_Reset();
		HAL_Delay(200);
		PCAL6416A_Init(1);

		PWR_CheckExtenderConfig();
		MX_NVIC_Init();
	}
#endif

	cmdID=NOCOMD;

	  for(;;)
	  {
		  if (commandLength=newCommand(bufferCommand))
		  {
			  // arrivato un nuovo comando
			  cmdID=parserCommand(bufferCommand,commandLength);

			  //HAL_IWDG_Refresh(&hiwdg);

			  //task di ricezione ed esecuzione comandi
			  //char cmd=LATLED;
				switch (cmdID)
				{

					//U|UWB|TGT_ID|TO - U|UWB|ID_DW1000|MEDIA|VARIANZA|MIN|MAX / U|UWB|ID_DW1000|-1 / U|UWB|-1|-1
					// todo
					case UWB:
						//printf ("INIT TEST UWB\n ");

						//strcpy(TGT_ID, bufferCommand+4); //posso usare bufferResp
						i=0;

						while (bufferCommand[4+i] != '|')
							i++;
						//i punta |

						strncpy(TGT_ID, bufferCommand+4, i);
						i++;

						strcpy(TO_UWB, bufferCommand+4+i);

						// se distanza calcolata
						distCalcF=test_ultrawideband(TGT_ID, ID_DW1000, TO_UWB);

						distCalc= (int32_t)distCalcF;
						//distCalc = FLOAT_TO_INT(distCalcF);

//						distCalc= 100; 					//TO TEST only
//						HW_ID= GetTagId(); 				//TO TEST only
//						sprintf(ID_DW1000, "%d",HW_ID); //TO TEST only

						switch (distCalc)
						{
							//distanza non calcolata
							case -1:
								//sprintf(bufferResp, "U|UWB|%s|%d\r\n",ID_DW1000, -1);
								strcpy(bufferResp, "U|UWB|");
								strcat(bufferResp,ID_DW1000);
								strcat(bufferResp,"|-1");
								strcat(bufferResp,"|-1");
								strcat(bufferResp,"|-1");
								strcat(bufferResp,"|-1");
							break;

							//No connection
							case -2:
								strcpy(bufferResp, "U|UWB|-1|-1|-1|-1|-1\r\n");
							break;

							//distanza ritornata
							default:
								//sprintf(bufferResp, "U|UWB|%s|%d\r\n",ID_DW1000, distCalc);

								varF=get_variance_distance();
								minDistF=get_min_distance();
								maxDistF=get_max_distance();

								var=(uint32_t)varF;
								minDist= (uint32_t)minDistF;
								maxDist= (uint32_t)maxDistF;

								strcpy(bufferResp, "U|UWB|");
								strcat(bufferResp,ID_DW1000);
								strcat(bufferResp,"|");
								itoa(distCalc, str1, 10);
								strcat(bufferResp,str1);

								strcat(bufferResp,"|");
								itoa(var, str1, 10);
								strcat(bufferResp,str1);

								strcat(bufferResp,"|");
								itoa(minDist, str1, 10);
								strcat(bufferResp,str1);

								strcat(bufferResp,"|");
								itoa(maxDist, str1, 10);
								strcat(bufferResp,str1);

								strcat(bufferResp,"\r\n");

						}

						if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
						{
						  /* Transfer error in transmission process */
						  Error_Handler();
						}

						cmdID=0;

					break;

					//U|FLASH - U|FLASH|OK / U|FLASH|KO
					case FLASH_M:
						//printf ("INIT TEST FLASH\n ");
						if (testFLASH())
						{
							//strcpy(bufferResp, "U|FLASH|OK\r\n");
							memcpy(bufferResp, "U|FLASH|OK\r\n", strlen("U|FLASH|OK\r\n"));
							bufferResp[strlen("U|FLASH|OK\r\n")]='\0';
						}
						else
						{
							//strcpy(bufferResp, "U|FLASH|KO\r\n");
							memcpy(bufferResp, "U|FLASH|KO\r\n", strlen("U|FLASH|KO\r\n"));
							bufferResp[strlen("U|FLASH|KO\r\n")]='\0';
						}

						//strcpy(bufferResp, "U|FLASH|OK\r\n"); //TO TEST-ELIMINARE

						//U|FLASH|OK or U|FLASH|KO
						if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
						{
						  /* Transfer error in transmission process */
						  Error_Handler();
						}

						cmdID=0;
					break;

					//U|ACC - U|ACC|OK|X|Y|Z| / U|ACC|KO
					case ACC:
						//printf ("INIT TEST ACC\n ");
						if (testACC(&acc))
						{

							accXmg=acc.AccX*1000;
							accYmg=acc.AccY*1000;
							accZmg=acc.AccZ*1000;

							//sprintf(bufferResp, "U|ACC|OK|%d|%d|%d\r\n",accXmg, accYmg, accZmg); //fa crepare l'RTOS
//							memcpy(bufferResp, "U|ACC|OK|", strlen("U|ACC|OK|"));
//							bufferResp[strlen("U|ACC|OK|")]='\0';
//
//							gcvt(acc.AccX, 4, str1);
//							strcat(bufferResp,str1);
//
//							gcvt(acc.AccY, 4, str2);
//							strcat(bufferResp,"|");
//							strcat(bufferResp,str2);
//
//							gcvt(acc.AccZ, 4, str3);
//							strcat(bufferResp,"|");
//							strcat(bufferResp,str3);
//
//							strcat(bufferResp,"\r\n");
						}
						else
							strcpy(bufferResp, "U|ACC|KO\r\n");

						//TEST ONLY
//						strcpy(bufferResp, "U|ACC|OK|");
//						gcvt(0.001, 4, str1);
//						strcat(bufferResp,str1);
//
//						gcvt(0.001, 4, str2);
//						strcat(bufferResp,"|");
//						strcat(bufferResp,str2);
//
//						gcvt(1.003, 4, str3);
//						strcat(bufferResp,"|");
//						strcat(bufferResp,str3);
//
//						strcat(bufferResp,"\r\n");

						sprintf(bufferResp, "U|ACC|OK|%d|%d|%d\r\n",accXmg, accYmg, accZmg);// To Test Only:OK
						//sprintf(bufferResp, "U|ACC|OK|%d|%d|%d\r\n",acc.AccX, acc.AccY, acc.AccZ);// To Test Only:OK
						//strcpy(bufferResp, "U|ACC|OK|"); To Test Only: OK

						if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
						{
						  /* Transfer error in transmission process */
						  Error_Handler();
						}

						cmdID=0;

						//HAL_NVIC_SystemReset();
					break;

					//U|BLE|MAC|TO - U|BLE|MAC|OK|RSSI / U|BLE|MAC|KO
					case BLE: 			//TO DO
						//printf ("INIT TEST BLE\n ");

						strncpy(MAC_BLE, bufferCommand+4,17); 	//posso usare bufferResp
						strcpy(TO_BLE, bufferCommand+22); 		//posso usare bufferResp

						if (TestConnectBLE(MAC_BLE,TO_BLE, RSSI_BLE))
						{
							//sprintf(bufferResp, "U|BLE|%s|OK|%s\r\n",MAC_BLE, RSSI_BLE);
							strcpy(bufferResp, "U|BLE|");
							strcat(bufferResp,MAC_BLE);
							strcat(bufferResp,"|OK|");
							strcat(bufferResp,RSSI_BLE);
							strcat(bufferResp,"\r\n");
						}

						else
						{
							//sprintf(bufferResp, "U|BLE|%s|KO\r\n",MAC_BLE);
							strcpy(bufferResp, "U|BLE|");
							strcat(bufferResp,MAC_BLE);
							strcat(bufferResp,"|KO\r\n");
						}

						//to test only
						//sprintf(bufferResp, "U|BLE|%s|OK|%s\r\n",MAC_BLE, "-35");

						if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
						{
							// Transfer error in transmission process
							Error_Handler();
						}

						cmdID=0;
					break;

					//U|SLEEP - U|SLEEP|OK
					case SLEEP:
						if(test_stop_mode()==true)
						{
							strcpy(bufferResp, "U|SLEEP|OK\r\n");

							if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
							{
							   /* Transfer error in transmission process */
							   Error_Handler();
							}
						}

//						else
//							sprintf(bufferResp, "U|SLEEP|KO\r\n");

						//testBUZZER(); 							//FOR TEST
						//testRELE1();  							//FOR TEST
						//sprintf(bufferResp, "U|SLEEP|OK\r\n"); 	//FOR TEST

						cmdID=0;
					break;

					//U|MOTION - U|MOTION|OK / U|MOTION|KO
					//SUBITO DOPO IL MOTION VADO IN SWITCHOFF
					case MOTION:
						//printf ("INIT TEST MOTION\n ");
						if(test_motion() == true)
						{
							strcpy(bufferResp, "U|MOTION|OK\r\n");
							motion=1;
						}
						else
							strcpy(bufferResp, "U|MOTION|KO\r\n");

						if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
						{
						   /* Transfer error in transmission process */
						   Error_Handler();
						}

						//MYMOD
						 //PCAL6416A_Reset();
#ifdef TAG_PERSONA
						//SE IL TEST PRECEDENTE (MOTION è ANDATO A BUON FINE)
						if(motion == 1)
						{
							//osDelay(1000);
							//HAL_UART_DeInit(&huart5);
							test_switch_off_mode();
							motion=0;
						}
#endif
						cmdID=0;
					break;

					// U|SWITCHOFF - U|START
					case SWITCHOFF: 	//TO DO

						//test_switch_off_mode();
						strcpy(bufferResp, "U|START\r\n");

						if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
						{
						   /* Transfer error in transmission process */
						   Error_Handler();
						}
#ifdef TAG_PERSONA
						//abilito il pulsante dx che disattivo a fine test
						motion_bt1_pressed=0;
//						motion_bt2_pressed=0;
//
//						//abilito antitamper che disattivo a fine test
//						antitamper_disabled=0;
//
//						//disabilito il charger (anche se già disattivo)
//						charger_disabled=1;
#endif
						cmdID=0;
					break;

					//U|SERIAL|CODICE_SERIALE - U|SERIAL|CODICE_SERIALE_LETTO
					case SERIAL:
						//printf ("INIT scrittura codice seriale\n ");

						strcpy(serialCodeR, bufferCommand+7); //posso usare bufferResp

						if (writeSerialCode(serialCodeR))
						{
							//sprintf(bufferResp, "U|SERIAL|%s\r\n",serialCodeR);
							strcpy(bufferResp, "U|SERIAL|");
							strcat(bufferResp,serialCodeR);
							strcat(bufferResp,"\r\n");

							if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
							{
							  /* Transfer error in transmission process */
							  Error_Handler();
							}

							//MYMOD
							//HAL_NVIC_SystemReset();
						}

						cmdID=0;


						//MYMOD
						osDelay(2000);
						HAL_NVIC_SystemReset();
					break;


#ifdef TAG_PERSONA
					//U|BUZZER - AZIONE BUZZER
					case BUZZER:
						//printf ("INIT TEST BUZZER\n ");
						testBUZZER();


						//TO TEST-ELIM
	//					strcpy(bufferResp, "U|BUZZER|OK\r\n");
	//
	//					//U|FLASH|OK or U|FLASH|KO
	//					if(HAL_UART_Transmit(&huart5, bufferResp, 12, 1000)!= HAL_OK)
	//					{
	//					  /* Transfer error in transmission process */
	//					  Error_Handler();
	//					}

						cmdID=0;
					break;

					//U|HAPTIC - AZIONE MOTORE
					case HAPTIC:
						//printf ("INIT TEST HAPTIC\n ");
						testHAPTIC();

						//TO TEST-ELIM
	//					strcpy(bufferResp, "U|HAPTIC|OK\r\n");
	//
	//					//U|FLASH|OK or U|FLASH|KO
	//					if(HAL_UART_Transmit(&huart5, bufferResp, 12, 1000)!= HAL_OK)
	//					{
	//					  /* Transfer error in transmission process */
	//					  Error_Handler();
	//					}

						cmdID=0;
					break;

					//U|MAINLED - AZIONE LED
					case MAINLED:
						//printf ("INIT TEST MAINLED\n ");
						testMAINLED();

						//TO TEST - ELIM
						//strcpy(bufferResp, "U|MAINLED|OK\r\n");

						//U|FLASH|OK or U|FLASH|KO
	//					if(HAL_UART_Transmit(&huart5, bufferResp, 12, 1000)!= HAL_OK)
	//					{
	//					  /* Transfer error in transmission process */
	//					  Error_Handler();
	//					}

						cmdID=0;
					break;

					//U|LATLED - AZIONE LED
					case LATLED:
						//printf ("INIT TEST LATLED\n ");
						testLATLED();

						//TO TEST - ELIM
	//					strcpy(bufferResp, "U|LATLED|OK\r\n");
	//
	//					//U|FLASH|OK or U|FLASH|KO
	//					if(HAL_UART_Transmit(&huart5, bufferResp, 12, 1000)!= HAL_OK)
	//					{
	//					  /* Transfer error in transmission process */
	//					  Error_Handler();
	//					}

						cmdID=0;
					break;
				#endif

				#ifdef TAG_CARRELLO

					//U|CAN - U|CAN|OK / U|CAN|KO
					case CAN_BUS:

						//printf ("INIT TEST CAN\n ");
						if (testCAN(&hcan1))
							 //sprintf(bufferResp, "U|CAN|OK\r\n");
							strcpy(bufferResp, "U|CAN|OK\r\n");
						else
							strcpy(bufferResp, "U|CAN|KO\r\n");

						//strcpy(bufferResp, "U|CAN|OK\r\n"); 	//TO TEST-ELIMINARE

						if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
						{
						  /* Transfer error in transmission process */
						  Error_Handler();
						}

						cmdID=0;
					break;

					//U|RELE|1 - U|RELE|2 - AZIONE RELE
					case RELE:
						//printf ("INIT TEST RELE1\n ");
						relNum=bufferCommand[5];
						if (relNum=='1')
							testRELE1();
						else
							if (relNum=='2')
								testRELE2();

						//TO TEST-ELIM
	//					strcpy(bufferResp, "U|R|OK\r\n",serialCodeR);
	//					if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
	//					{
	//					  /* Transfer error in transmission process */
	//					  Error_Handler();
	//					}

						cmdID=0;
					break;
	            #endif

					default:
						if(HAL_UART_Transmit(&huart5, "COMANDO NON RICONOSCIUTO", strlen("COMANDO NON RICONOSCIUTO"), 1000)!= HAL_OK)
						{
						  /* Transfer error in transmission process */
						  Error_Handler();
						}
				}

				//cmd=0;

				//osDelay(1000);
		  }
		  	  osDelay(1000);
		  	  osThreadYield();
	  }
	/* USER CODE END 5 */
}

#ifdef TAG_PERSONA
//extern char button1;
//extern char button2;
void receiveAsyncCommand(uint8_t asyncCmd)
{
	switch (asyncCmd)
	{
		//U|CHARGER|OK / U|CHARGER|KO
		case CHARGER:
			if (charger_disabled==0)
			{
				HAL_Delay(100);

				 uint8_t chargerStatus=myCHRG_GetStatus();

				//verifico status caricatore
				if (chargerStatus==1)
				{
					strcpy(bufferResp, "U|CHARGER|OK\r\n");

					if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
					{
					  /* Transfer error in transmission process */
					  Error_Handler();
					}
				}
				else
				{
					if (chargerStatus==0)
					{
						strcpy(bufferResp, "U|CHARGER|KO\r\n");

						if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
						{
						  /* Transfer error in transmission process */
						  Error_Handler();
						}
					}
				}

				charger_disabled=1;
			}

		break;

		//U|BUTTON|1
		case BUTTON1:
			if(motion_bt1_pressed == 0)
			{
				strcpy(bufferResp, "U|BUTTON|1\r\n");

				if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
				{
				  /* Transfer error in transmission process */
				  Error_Handler();
				}

				//disabilito il pulsante dx e abilito il sx
				motion_bt1_pressed=1;
				motion_bt2_pressed=0;
			}
		break;

		//U|BUTTON|2
		case BUTTON2:
			if(motion_bt2_pressed == 0)
			{
				strcpy(bufferResp, "U|BUTTON|2\r\n");

				if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
				{
				  /* Transfer error in transmission process */
				  Error_Handler();
				}

				//disabilito il pulsante sx e abilito l'antitamper
				motion_bt2_pressed=1;
				antitamper_disabled=0;
			}
		break;

		//U|ANTITAMPER|OK
		case ANTITAMPER:
			//RESETTO BUTTONS
			//			motion_bt1_pressed=0;
			//			motion_bt2_pressed=0;

			if (antitamper_disabled==0)
			{
				HAL_Delay(100);

				strcpy(bufferResp, "U|ANTITAMPER|OK\r\n");

				//U|FLASH|OK or U|FLASH|KO
				if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
				{
				  /* Transfer error in transmission process */
				  Error_Handler();
				}

				//disabilito l'antitamper
				antitamper_disabled=1;
			}

		break;

	}
}
#endif

void stopUWBTask()
{
	taskENTER_CRITICAL();

	HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);

    if(app.uwbTask.Handle)
    {
        //osMutexWait(app.uwbTask.MutexId, osWaitForever);

        osMutexRelease(app.uwbTask.MutexId);
        if(osThreadTerminate(app.uwbTask.Handle) == osOK)
        {
            osMutexDelete(app.uwbTask.MutexId);
            app.uwbTask.Handle = NULL;
        }
        else
        {
            error_handler(1, _ERR_Cannot_Delete_Task);
        }

    }

	disable_dw1000_irq();
	set_dw_spi_slow_rate(DW_A);
	HAL_IWDG_Refresh(&hiwdg);
	dwt_forcetrxoff();
	tag_terminate_tasks();
	taskEXIT_CRITICAL();

}

void UWBTask(void const * argument)
{
  /* USER CODE BEGIN 5 */

	const EventBits_t bitsWaitForA = (Ev_Tag_Task);
	const EventBits_t bitsWaitForAny = (bitsWaitForA | Ev_Stop_All);


	EventBits_t    uxBits;
	uint32_t    chip;

	/* Default task is blinking 3 times on power on and then lets other tasks to start */

	HAL_IWDG_Refresh(&hiwdg);

	for(int i=0; i<6; i++)
	{
		HAL_GPIO_TogglePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin);
		osDelay(250);
		HAL_IWDG_Refresh(&hiwdg);
	}



	    app.mode = mIDLE;

	if (app.pConfig->s.autoStartEn == 1)
	{
		xEventGroupSetBits(app.xStartTaskEvent, Ev_Tag_Task);    /**< activate Tag task */
	}


  /* Infinite loop */
  for(;;)
  {


	  	  uxBits = xEventGroupWaitBits(app.xStartTaskEvent, bitsWaitForAny, pdTRUE, pdFALSE, 200/portTICK_PERIOD_MS );

		  HAL_IWDG_Refresh(&hiwdg);

		  uxBits &= bitsWaitForAny;

		  chip = (uint32_t)(DW_A);

		  /* Event to start/stop task received */
		  /* 1. free the resources: kill all user threads and timers */
		  if(uxBits)
		  {
			  /* Turn all LEDs off on restart of app */
			  HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin , GPIO_PIN_SET);   //indicates Error
			  HAL_GPIO_WritePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin , GPIO_PIN_SET);    //indicates Stationary



			  HAL_IWDG_Refresh(&hiwdg);

			  app.lastErrorCode = _NO_ERR;
			  app.DwCanSleep = 0;

			  {
				  taskENTER_CRITICAL();
				  disable_dw1000_irq();
				  set_dw_spi_slow_rate(DW_A);
				  HAL_IWDG_Refresh(&hiwdg);
				  dwt_forcetrxoff();
				  taskEXIT_CRITICAL();
			  }
			  HAL_IWDG_Refresh(&hiwdg);
			  tag_terminate_tasks();
			  HAL_IWDG_Refresh(&hiwdg);

//			stopPWM(htim1, TIM_CHANNEL_3); 	// Buzzer Off
//			HAL_IWDG_Refresh(&hiwdg);
//			stopPWM(htim3, TIM_CHANNEL_4);	// MOTOR Off
//			HAL_IWDG_Refresh(&hiwdg);
//			stopPWM(htim2, TIM_CHANNEL_2);	// FLED_RED off



		  }

		  HAL_IWDG_Refresh(&hiwdg);
		  osThreadYield();                    //instruct kernel to force switch context that the Idle task can free resources
		  osDelay( 100/portTICK_PERIOD_MS );    //Idle task is freeing resources here
		  HAL_IWDG_Refresh(&hiwdg);

		  taskENTER_CRITICAL();
		  /* 2. Start appropriate RTOS task */
		  switch (uxBits)
		  {
		  case Ev_Tag_Task:
			  app.mode = mTWR;
			  /* execute helper function to setup sub-tasks for Tag process */
			  tag_helper(&chip);
//			  printf("Tag process start\r\n");
			  break;

		  case Ev_Stop_All:
//			  printf("Tag stop\r\n");
			  app.mode = mIDLE;
			  break;

		  default:
			   HAL_IWDG_Refresh(&hiwdg);
			  break;
		  }

//	  if(signalsAlarmArea == 1 && enablesignals == 1)
//	  {
//		  HAL_IWDG_Refresh(&hiwdg);
//		  printf("buzzer on \r\n");
//		  setPWM(htim1, TIM_CHANNEL_3, 32767, 16383); // Buzzer On 2.2KHz
//		enablesignals=0;
//	  }
//	  else if (signalsAlarmArea == 0 && enablesignals == 1)
//	  {
//		  HAL_IWDG_Refresh(&hiwdg);
//		printf("buzzer off \r\n");
//		  setPWM(htim1, TIM_CHANNEL_3, 32767, 0); // Buzzer Off 2.2KHz
//		enablesignals = 0;
//	  }

		  taskEXIT_CRITICAL();
  	  }


  /* USER CODE END 5 */
}

void startUWBTask()
{
	//MX_RTC_Init();

	set_wake_up_counter(WAKE_UP_COUNTER_UWB);
	MX_RTC_Init();


    osThreadDef(uwbTask, UWBTask, osPriorityNormal, 0, 384);
    osMutexDef(uwbMutex);

    app.uwbTask.Handle     = osThreadCreate(osThread(uwbTask), NULL);
    app.uwbTask.MutexId    = osMutexCreate(osMutex(uwbMutex));

	//osThreadDef(uwbTask, UWBTask, osPriorityNormal, 0, 128);
	//uwbTaskHandle = osThreadCreate(osThread(uwbTask), NULL);
}

//void start_rtc_timer()
//{
//	set_wake_up_counter(WAKE_UP_COUNTER_RTC);
//	MX_RTC_Init();
//
//    /*HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);
//
//    if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 5, RTC_WAKEUPCLOCK_CK_SPRE_16BITS) != HAL_OK)
//    {
//      Error_Handler();
//    }*/
//}
//
//
//void stop_rtc_timer()
//{
//	HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);
//}

char createbleTask(void)
{
	taskENTER_CRITICAL();
	  osThreadDef(BGLIB_RxHandlerTask, BGLIB_RxHandler, osPriorityNormal, 0, 512);
	  app.bleTask.Handle = osThreadCreate(osThread(BGLIB_RxHandlerTask), NULL);
	taskEXIT_CRITICAL();

	  if( !app.bleTask.Handle)
		  return 0;
	  return 1;
}
char terminatebleTask(void)
{
	char res=0;

	taskENTER_CRITICAL();
		if(app.bleTask.Handle)
		{
			if(osThreadTerminate(app.bleTask.Handle) == osOK)
			{
				app.bleTask.Handle =NULL;
				res=1;
			}
			else
			{
				//error_handler(1, _ERR_Cannot_Delete_Task);
				return 0;
			}
		}
	taskEXIT_CRITICAL();

	return res;
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM4 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM4) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

void CallSystemPower_Config()
{
	SystemPower_Config();
}

static void SystemPower_Config(void)
{
	//HAL_RTC_DeInit(&hrtc);
  /* Configure RTC */
  hrtc.Instance = RTC;
  /* Set the RTC time base to 1s */
  /* Configure RTC prescaler and RTC data registers as follow:
    - Hour Format = Format 24
    - Asynch Prediv = Value according to source clock
    - Synch Prediv = Value according to source clock
    - OutPut = Output Disable
    - OutPutPolarity = High Polarity
    - OutPutType = Open Drain */
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = RTC_ASYNCH_PREDIV;
  hrtc.Init.SynchPrediv = RTC_SYNCH_PREDIV;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if(HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
}

void CallSystemPower_Config2()
{
	//SWITCH OFF
	SystemPower_Config2();
}

static void SystemPower_Config2(void)
{
//SWITCH OFF


	 //__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	  hrtc.Instance = RTC;

	  hrtc.Init.HourFormat     = RTC_HOURFORMAT_24;
	  hrtc.Init.AsynchPrediv   = RTC_ASYNCH_PREDIV;
	  hrtc.Init.SynchPrediv    = RTC_SYNCH_PREDIV;
	  hrtc.Init.OutPut         = RTC_OUTPUT_ALARMA;
	  hrtc.Init.OutPutPolarity = /*RTC_OUTPUT_POLARITY_LOW*/RTC_OUTPUT_POLARITY_HIGH;
	  hrtc.Init.OutPutType     = RTC_OUTPUT_TYPE_PUSHPULL;

	  if (HAL_RTC_Init(&hrtc) != HAL_OK)
	  {
	    /* Initialization Error */
	    Error_Handler();
	  }

	  //RTC_AlarmConfig();
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
