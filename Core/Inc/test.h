#ifndef __INC_PERIPHERAL_TEST_H
#define __INC_PERIPHERAL_TEST_H

#include <stdbool.h>
#include "port.h"
#include "tag.h"
#include "util.h"
#include "stm32l4xx_hal.h"


#define TAG_PERSONA 1 		//UPA-201014B-P000001
//#define TAG_CARRELLO 1	//UPA-201014B-V000001

//U|BLE|88:6B:0F:F6:C8:B9
//U|UWB|36763|6000

#if TAG_PERSONA + TAG_CARRELLO!= 1
	#error Errore di inclusione in test.h
#endif


#define TEST_ADDRESS (37800)
#define TEST_ADDRESS_FAKE (37666)
#define WAKE_UP_COUNTER_UWB (32767) //100ms
#define WAKE_UP_COUNTER_RTC (327670) //1s
#define SWITCH_OFF_FLASH_FLAG 	(17)

#define ADDRESS_BROADCAST 0

enum CMD
{

		NOCOMD=0,

//		UWB,			//U|UWB|TGT_ID - U|UWB|ID_DW1000|DISTANZA_CALCOLATA / U|UWB|ID_DW1000|-1 / U|UWB|-1|-1

		UWB,			//U|UWB|TGT_ID|TO - U|UWB|ID_DW1000|MEDIA|VARIANZA|MIN|MAX / U|UWB|ID_DW1000|-1 / U|UWB|-1|-1
		FLASH_M, 		//U|FLASH - U|FLASH|OK / U|FLASH|KO
		ACC, 			//U|ACC - U|ACC|OK|X|Y|Z| / U|ACC|KO
		BLE, 			//U|BLE|MAC|TO - U|BLE|MAC|OK|RSSI / U|BLE|MAC|KO
		SLEEP, 			//U|SLEEP - U|SLEEP|OK
		SWITCHOFF, 			//TO DO - U|SWITCHOFF - U|START
		MOTION, 		//U|MOTION - U|MOTION|OK / U|MOTION|KO
		SERIAL, 		//U|SERIAL|CODICE_SERIALE - U|SERIAL|CODICE_SERIALE_LETTO

	#ifdef TAG_PERSONA
		CHARGER,    	//U|CHARGER|OK / U|CHARGER|KO
		BUTTON1,  		//U|BUTTON|1
		BUTTON2,  		//U|BUTTON|2
		ANTITAMPER, 	//U|ANTITAMPER|OK
		BUZZER, 		//U|BUZZER - AZIONE BUZZER
		HAPTIC,			//U|HAPTIC - AZIONE MOTORE
		MAINLED,		//U|MAINLED - AZIONE LED
		LATLED, 		//U|LATLED - AZIONE LED
	#endif

	#ifdef TAG_CARRELLO
		CAN_BUS, 		//U|CAN - U|CAN|OK / U|CAN|KO
		RELE  			//U|RELE|1 - U|RELE|2 - AZIONE RELE
	#endif

};

typedef struct
{
	float uwb_distance; //cm
	bool uwb_test_enable;
	uint16_t uwb_test_address;
	uint32_t wake_up_counter;
	bool uwb_connected;

	uint32_t max_distance;
	uint32_t min_distance;

	//uint32_t mean_distance;
	float mean_distance;

	uint32_t variance_distance;
	uint8_t number_of_measurement;
//	bool motion_bt1_pressed;
//	bool motion_bt2_pressed;

} testParameter;

//typedef struct
//{
//	bd_addr addr;
//	uint32_t rssi;
//
//} bleStruct;

void testRELE1(void);
void testRELE1(void);

void test_param_init();

bool test_charger();

float test_ultrawideband(char* tdt_id, char* my_ID,  char* time_measurement );

bool test_motion();

bool test_switch_off_mode();
void PWR_systemSwitchOff(void);

bool test_stop_mode();

RTC_HandleTypeDef *RTC_GetHandle(void);

float test_ultrawideband_get_distance();
void test_ultrawideband_set_distance(float input);

bool get_uwb_enable_flag();
void set_uwb_enable_flag(bool input);

uint16_t get_uwb_address_target();
void set_uwb_address_target(uint16_t input);

bool get_uwb_connected_flag();
void set_uwb_connected_flag(bool input);

uint32_t get_wake_up_counter();
void set_wake_up_counter(uint32_t input);

uint32_t get_min_distance();
void set_min_distance(uint32_t input);
uint32_t get_max_distance();
void set_max_distance(uint32_t input);

//uint32_t get_mean_distance();
//void set_mean_distance(uint32_t input);
float get_mean_distance();
void set_mean_distance(float input);

uint32_t get_variance_distance();
void set_variance_distance(uint32_t input);
uint8_t get_n_measurement();
void set_n_measurement(uint8_t input);

#ifdef TAG_PERSONA
//bool get_motion_b1_flag();
//void set_motion_b1_flag(bool input);
//bool get_motion_b2_flag();
//void set_motion_b2_flag(bool input);
#endif

#endif /*__INC_PERIPHERAL_TEST_H*/











