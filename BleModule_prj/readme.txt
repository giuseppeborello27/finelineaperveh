ATTENZIONE:
	Per programmare i moduli BLE è necessaria la license key (ref. to http://community.silabs.com/t5/Bluetooth-Wi-Fi-Knowledge-Base/How-to-know-if-BLE112-modules-have-the-license-key-installed/ta-p/147668)
	
	Utilizzando true per il parametro lock_debug in hardware.xml (attualmente impostato su false) si blocca la possibilità di leggere la license key con "Info"
	dal tool BLE SW Update Tool. Questo serve per i moduli che poi andranno in produzione: in questo caso salvare la license key per un'eventuale futura riprogrammazione.
	
Per ottenere la license key è necessario aprire una issue sul sito e inviare il serial number del modulo per cui la si sta richiedendo.



	
	