################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/Decadriver/Src/deca_device.c \
../Drivers/Decadriver/Src/deca_params_init.c 

OBJS += \
./Drivers/Decadriver/Src/deca_device.o \
./Drivers/Decadriver/Src/deca_params_init.o 

C_DEPS += \
./Drivers/Decadriver/Src/deca_device.d \
./Drivers/Decadriver/Src/deca_params_init.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/Decadriver/Src/deca_device.o: ../Drivers/Decadriver/Src/deca_device.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g -DUSE_HAL_DRIVER -DSTM32L476xx -DDEBUG -c -I../Core/Inc -I../Drivers/Decadriver/Inc -I../Ubiquicom/Inc/2Ta -I../Ubiquicom/Inc/TagManager -I../Ubiquicom/Inc -I../Src/inc -I../Drivers/STM32L4xx_HAL_Driver/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Drivers/CMSIS/Device/ST/STM32L4xx/Include -I../Drivers/CMSIS/Include -I../Ubiquicom/Inc/BGLIB -I../Ubiquicom/Inc/FirmwareUpdate -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Decadriver/Src/deca_device.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/Decadriver/Src/deca_params_init.o: ../Drivers/Decadriver/Src/deca_params_init.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g -DUSE_HAL_DRIVER -DSTM32L476xx -DDEBUG -c -I../Core/Inc -I../Drivers/Decadriver/Inc -I../Ubiquicom/Inc/2Ta -I../Ubiquicom/Inc/TagManager -I../Ubiquicom/Inc -I../Src/inc -I../Drivers/STM32L4xx_HAL_Driver/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Drivers/CMSIS/Device/ST/STM32L4xx/Include -I../Drivers/CMSIS/Include -I../Ubiquicom/Inc/BGLIB -I../Ubiquicom/Inc/FirmwareUpdate -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Decadriver/Src/deca_params_init.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

