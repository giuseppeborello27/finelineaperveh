/** \file i2c.h
 ** \brief I2C Management module - header file.
 ** \author Giuseppe Di Martino
 ** \date   14-October-2016
 ** <h2><left>&copy; COPYRIGHT(c) 2017 e-Novia SRL</left></h2>
 ** E-NOVIA CONFIDENTIAL
 ** __________________
 ** 
 ** [2017] e-Novia SRL.
 ** All Rights Reserved.
 ** 
 ** ALL INFORMATION CONTAINED HEREIN WAS PRODUCED BY E-NOVIA SRL AND NO THIRD PARTY MAY CLAIM ANY RIGHT OR PATERNITY ON IT.\n 
 ** THE INTELLECTUAL AND TECHNICAL CONCEPTS CONTAINED HEREIN ARE AND REMAIN
 ** THE PROPERTY OF E-NOVIA SRL AND ARE PROTECTED BY TRADE SECRET OR COPYRIGHT LAW.
 ** REPRODUCTION, DISTRIBUTION OR DISCLOSURE OF THIS MATERIAL TO ANY OTHER PARTY
 ** IS STRICTLY FORBIDDEN UNLESS PRIOR WRITTEN PERMISSION IS OBTAINED FROM E-NOVIA SRL
 */

#ifndef __I2C_H
#define __I2C_H

#include "i2c_cfg.h"
#include "stdbool.h"

typedef enum
{
#ifdef I2C_CFG_ID_1
	I2C_ID_1,
#endif   
#ifdef I2C_CFG_ID_2    
	I2C_ID_2,
#endif
#ifdef I2C_CFG_ID_3    
	I2C_ID_3,
#endif    
    
	I2C_ID_INVALID
}I2C_Id_t;


#define I2C_NUM		I2C_ID_INVALID


void I2C_Init(I2C_Id_t id);

void I2C_DeInit(I2C_Id_t id);

bool I2C_IsReady(I2C_Id_t id);

I2C_HandleTypeDef *I2C_GetHandle(I2C_Id_t id);

void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c);

void HAL_I2C_MspDeInit(I2C_HandleTypeDef* hi2c);

bool I2C_Write(I2C_Id_t id, uint16_t addr, uint8_t data[], uint16_t data_size);

bool I2C_Read(I2C_Id_t id, uint16_t addr, uint8_t data[], uint16_t data_size);








#endif /*__I2C_H*/






