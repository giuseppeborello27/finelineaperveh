/** \file nxp_pcal6416a.h
*	\brief nxp_pcal6416a driver header - Header file
*	\author Lorenzo Gandolfi
*	\date June 03, 2019
*	\copyright Copyright (c) 2019 e-Novia S.p.a. - All Rights Reserved.
* 
* E-NOVIA S.P.A. CONFIDENTIAL
* __________________
* 
* ALL INFORMATION CONTAINED HEREIN WAS PRODUCED BY E-NOVIA S.P.A. AND NO THIRD PARTY MAY CLAIM ANY RIGHT OR PATERNITY ON IT.\n 
* THE INTELLECTUAL AND TECHNICAL CONCEPTS CONTAINED HEREIN ARE AND REMAIN
* THE PROPERTY OF E-NOVIA S.P.A. AND ARE PROTECTED BY TRADE SECRET OR COPYRIGHT LAW.
* REPRODUCTION, DISTRIBUTION OR DISCLOSURE OF THIS MATERIAL TO ANY OTHER PARTY
* IS STRICTLY FORBIDDEN UNLESS PRIOR WRITTEN PERMISSION IS OBTAINED FROM E-NOVIA S.P.A.
*/ 


#ifndef __NXP_PCAL6416A_H
#define __NXP_PCAL6416A_H

//#include "os_api.h"
#include "main.h"
#include <stdbool.h>
#include "nxp_pcal6416a_cfg.h"
#include "nxp_pcal6416a_reg.h"




/**	\defgroup nxp_pcal6416a_MODULE nxp_pcal6416a Module Interface
*   \{
* 	\brief The nxp pcal6416a Module Interface.
*/

/**	\defgroup nxp_pcal6416a_PUBLIC PUBLIC
* 	\brief nxp pcal6416a Public Definitions and Functions.
*	\{
*/
#ifdef PCAL6416A_USE_SET_PP_PENDING
#define PCAL6416A_PORT_PIN_PENDING_MAX_NUM      16U
#endif /* PCAL6416A_USE_SET_PP_PENDING*/

typedef enum
{
    PCAL6416A_DIR_OUT,
    PCAL6416A_DIR_IN
}PCAL6416A_Dir_t;

typedef enum
{
    PCAL6416A_POL_RETAINED,
    PCAL6416A_POL_INVERTED
}PCAL6416A_Pol_t;

typedef enum
{
    PCAL6416A_DRVSTR_025,
	PCAL6416A_DRVSTR_050,
	PCAL6416A_DRVSTR_075,
	PCAL6416A_DRVSTR_100
}PCAL6416A_DrvStr_t;

typedef enum
{
    PCAL6416A_LAT_DISABLED,
    PCAL6416A_LAT_ENABLED
}PCAL6416A_Latch_t;

typedef enum
{
    PCAL6416A_PUL_DISABLED,
    PCAL6416A_PUL_ENABLED
}PCAL6416A_PullEn_t;

typedef enum
{
    PCAL6416A_PUL_DOWN,
    PCAL6416A_PUL_UP
}PCAL6416A_PullSel_t;

typedef enum
{
    PCAL6416A_INTERR_ENABLED,
    PCAL6416A_INTERR_DISABLED
}PCAL6416A_InterrMask_t;

typedef enum
{
    PCAL6416A_PUSHPULL,
    PCAL6416A_OPENDRAIN,
}PCAL6416A_OutPortConfig_t;

#if 0
typedef void (*PCAL6416A_InputCallback_t)(uint8_t input_no, bool input_val);
#else
typedef void (*PCAL6416A_InputCallback_t)(uint16_t int_stat_mask, uint16_t input_mask);
#endif


bool PCAL6416A_Init(PCAL6416A_InputCallback_t cb);

void PCAL6416A_Reset(void);

uint32_t  PCAL6416A_GetRstPinNo(void);

uint32_t  PCAL6416A_GetIntPinNo(void);

#define PCAL6416A_IsInterruptPending()       (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) == GPIO_PIN_SET)

bool PCAL6416A_SetIntAsIrq(void);

bool PCAL6416A_SetIntAsEvent(void);

/** 
*   \brief function to get input value from a specific pin of a port of device
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \param[out] val the value read on the pin
*	\return true means that the read operation was successful, false means that the read operation was not completed
*/
bool PCAL6416A_GetPortPin(uint8_t port, uint8_t pin, bool *val);

/** 
*   \brief function to set output value to a specific pin of a port of device
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \param[in] val the value to write on the pin
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_SetPortPin(uint8_t port, uint8_t pin, bool val);

/** 
*   \brief function to get the polarity inversion set on a port of device
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \param[out] val the polarity inversion value set on the port
*	\return true means that polarity is inverted, false means the polarity is retained
*/
bool PCAL6416A_GetPortPinPol(uint8_t port, uint8_t pin, bool *val);

/** 
*   \brief function to set the polarity inversion to a port of device
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \param[in] polarity the polarity inversion value to set on the port. PCAL6416A_Pol_t struct defines the polarity ammissible values
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_SetPortPinPol(uint8_t port, uint8_t pin, PCAL6416A_Pol_t polarity);

/** 
*   \brief function to set the direction of a specific pin of a device port. A pin can be set as input or output.
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \param[in] dir the direction value to set on the port. PCAL6416A_Dir_t struct defines the ammissible values
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_SetPortPinDir(uint8_t port, uint8_t pin, PCAL6416A_Dir_t dir);


/** 
*   \brief Returns the current direction mask of the module ports.
*   \param[out] dir_mask    I/O direction mask of ports; if bit is 0 direction is OUT, if bit is 1 direction is IN 
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_GetPortsDirConfig(uint16_t *dir_mask);

/** 
*   \brief Returns the current polarity mask of the module ports.
*   \param[out] pol_mask    I/O polarity mask of ports; if bit is 0 direction is OUT, if bit is 1 direction is IN 
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_GetPortsPolConfig(uint16_t *pol_mask);

/** 
*   \brief Returns the current interrupt mask of the module ports.
*   \param[out] int_mask    I/O interrupt mask of ports; if bit is 0 interrupt is enabled, if bit is 1 interrupt is disabled 
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_GetPortsIntConfig(uint16_t *int_mask);

/** 
*   \brief function to set the drive strength of a specific pin of a device port
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \param[in] drvStr the direction value to set on the port. PCAL6416A_DrvStr_t struct defines the ammissible values
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_SetPortPinDrvStr(uint8_t port, uint8_t pin, PCAL6416A_DrvStr_t drvStr);

/** 
*   \brief function to enable and disable input latch of a specific pin of a device port
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \param[in] latchSet the value to enable or disable input latch. PCAL6416A_Latch_t struct provides the ammissible values
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_SetPortPinLatchReg(uint8_t port, uint8_t pin, PCAL6416A_Latch_t latchSet);

/** 
*   \brief function to enable and disable pull up/pull down usage of a specific pin of a device port
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \param[in] pullEnable the value to enable or disable pull up usage. PCAL6416A_PullEn_t struct provides the ammissible values (disabled, enabled)
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_SetPortPinPullEn(uint8_t port, uint8_t pin, PCAL6416A_PullEn_t pullEnable);

/** 
*   \brief function to select if use pull up or pull down of a specific pin of a device port
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \param[in] pull the selection of pull up or pull down. PCAL6416A_PullSel_t struct provides the ammissible values (pull down, pull up)
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_SetPortPinPullSel(uint8_t port, uint8_t pin, PCAL6416A_PullSel_t pull);

/** 
*   \brief function to select if use pull up or pull down of a specific pin of a device port
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \paramo[out] val the selection of pull up or pull down. PCAL6416A_PullSel_t struct provides the ammissible values (pull down, pull up)
*	\return true means that the read operation was successful, false means that the read operation was not completed
*/
bool PCAL6416A_GetPortPinIntMask(uint8_t port, uint8_t pin, bool *val);

/** 
*   \brief function to select the mask to use for enable or disable interrupt related of state change of a specific input pin of a device port
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \param[in] mask the mask for enable or disable interrupt. PCAL6416A_InterrMask_t struct provides the ammissible values (enabled, disabled)
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_SetPortPinIntMask(uint8_t port, uint8_t pin, PCAL6416A_InterrMask_t mask);

/** 
*   \brief function to get the interrupt status related to specific input pin of a device port
*   \param[in] port device port
*   \param[in] pin the pin of the port
*   \param[out] val the interrupt status related to the specified pin and ports. If true the interrupt was asserted, if false no interrupt was asserted
*	\return true means that the read operation was successful, false means that the read operation was not completed
*/
bool PCAL6416A_GetPortPinIntStat(uint8_t port, uint8_t pin, bool *val);

/** 
*   \brief function to select the port configuration of a pin used ad output port. It can be set as push-pull or open-drain I/O stage
*   \param[in] port device port
*   \param[in] cfg the value for output port setting. PCAL6416A_OutPortConfig_t struct provides the ammissible values (push-pull, open-drain)
*	\return true means that the set operation was successful, false means that the set operation was not completed
*/
bool PCAL6416A_SetPortPinOutPortCfg(uint8_t port, PCAL6416A_OutPortConfig_t cfg);



#ifdef PCAL6416A_USE_SET_PP_PENDING
void     PCAL6416A_SetPortPinPending(uint8_t port, uint8_t pin, bool val);
#endif /*PCAL6416A_USE_SET_PP_PENDING*/


void PCAL6416A_CheckInt(void);

uint16_t     PCAL6416A_GetIntStatMask(void);

uint16_t     PCAL6416A_GetInputMask(void);

void PCAL6416A_IntHandler(void);

PCAL6416A_REG_Regs_t* PCAL6416A_UpdateRegs(void);

#ifdef DBG_PCAL6416A
void PCAL6416A_DefaultCallback(uint8_t input_no, bool input_val);
#endif


void PCAL6416A_DebugShowRegisters(void);

/**	\} */ 

/**	\} */ 


#endif /*__NXP_PCAL6416A_H*/


/* EOF */

