/** \file nxp_pcal6416a_reg.h
*	\brief nxp_pcal6416a driver register specifications
*	\author Giuseppe Di Martino, Lorenzo Gandolfi
*	\date June 03, 2019
*	\copyright Copyright (c) 2019 e-Novia S.p.a. - All Rights Reserved.
* 
* E-NOVIA S.P.A. CONFIDENTIAL
* __________________
* 
* ALL INFORMATION CONTAINED HEREIN WAS PRODUCED BY E-NOVIA S.P.A. AND NO THIRD PARTY MAY CLAIM ANY RIGHT OR PATERNITY ON IT.\n 
* THE INTELLECTUAL AND TECHNICAL CONCEPTS CONTAINED HEREIN ARE AND REMAIN
* THE PROPERTY OF E-NOVIA S.P.A. AND ARE PROTECTED BY TRADE SECRET OR COPYRIGHT LAW.
* REPRODUCTION, DISTRIBUTION OR DISCLOSURE OF THIS MATERIAL TO ANY OTHER PARTY
* IS STRICTLY FORBIDDEN UNLESS PRIOR WRITTEN PERMISSION IS OBTAINED FROM E-NOVIA S.P.A.
*/

#ifndef __TI_PCAL6416A_REG_H
#define __TI_PCAL6416A_REG_H

//#include "os_api.h"

#include <stdbool.h>

typedef enum
{
    PCAL6416A_REG_INPUT_ADDR_0		= 0x00U,
    PCAL6416A_REG_INPUT_ADDR_1		= 0x01U,
    PCAL6416A_REG_OUTPUT_ADDR_0		= 0x02U,
    PCAL6416A_REG_OUTPUT_ADDR_1		= 0x03U,
    PCAL6416A_REG_POL_ADDR_0		= 0x04U,
    PCAL6416A_REG_POL_ADDR_1		= 0x05U,
    PCAL6416A_REG_CFG_ADDR_0		= 0x06U,
    PCAL6416A_REG_CFG_ADDR_1		= 0x07U,
	
	PCAL6416A_REG_OUT_DRV_STR_0L	= 0x40U,
    PCAL6416A_REG_OUT_DRV_STR_0H	= 0x41U,
    PCAL6416A_REG_OUT_DRV_STR_1L	= 0x42U,
    PCAL6416A_REG_OUT_DRV_STR_1H	= 0x43U,
	
    PCAL6416A_REG_IN_LATCH_0		= 0x44U,
	PCAL6416A_REG_IN_LATCH_1		= 0x45U,
	
    PCAL6416A_REG_PULL_EN_0			= 0x46U,
    PCAL6416A_REG_PULL_EN_1			= 0x47U,
    PCAL6416A_REG_PULL_SEL_0		= 0x48U,
    PCAL6416A_REG_PULL_SEL_1		= 0x49U,
	
	PCAL6416A_REG_INTERRUPT_MASK_0	= 0x4AU,
    PCAL6416A_REG_INTERRUPT_MASK_1	= 0x4BU,
    PCAL6416A_REG_INTERRUPT_STAT_0	= 0x4CU,
    PCAL6416A_REG_INTERRUPT_STAT_1	= 0x4DU,
	
    PCAL6416A_REG_OUT_PORT_CFG		= 0x4FU
}PCAL6416A_REG_Addr_t;

typedef struct
{
    uint8_t B0:1;
    uint8_t B1:1;
    uint8_t B2:1;
    uint8_t B3:1;
    uint8_t B4:1;
    uint8_t B5:1;
    uint8_t B6:1;
    uint8_t B7:1;
}__attribute__ ((__packed__))PCAL6416A_REG_Bit_t;

typedef struct
{
    uint8_t B01:2;
    uint8_t B23:2;
    uint8_t B45:2;
    uint8_t B67:2;
}__attribute__ ((__packed__))PCAL6416A_REG_BitPair_t;

typedef struct
{
    uint8_t B01:2;
	uint8_t reserved:6;
}__attribute__ ((__packed__))PCAL6416A_REG_BitPtrn3_t;

typedef union 
{
	uint8_t             Port;
	PCAL6416A_REG_Bit_t   Bits;
}__attribute__ ((__packed__))PCAL6416A_REG_Port_t;

typedef union 
{
	uint8_t             	Port;
	PCAL6416A_REG_BitPair_t Bits;
}__attribute__ ((__packed__))PCAL6416A_REG_pair_Port_t;

typedef struct 
{
	PCAL6416A_REG_Port_t P0;
	PCAL6416A_REG_Port_t P1;
}__attribute__ ((__packed__))PCAL6416A_REG_Ports_t;

typedef struct 
{
	PCAL6416A_REG_pair_Port_t P0_L;
	PCAL6416A_REG_pair_Port_t P0_H;
	PCAL6416A_REG_pair_Port_t P1_L;
	PCAL6416A_REG_pair_Port_t P1_H;
}__attribute__ ((__packed__))PCAL6416A_REG_pair_Ports_t;

typedef union 
{
	uint16_t           		AllPorts;
	PCAL6416A_REG_Ports_t    Ports;
}__attribute__ ((__packed__))PCAL6416A_REG_Input_t;

typedef union 
{
	uint16_t           AllPorts;
	PCAL6416A_REG_Ports_t    Ports;
}__attribute__ ((__packed__))PCAL6416A_REG_Output_t;

typedef union 
{
	uint16_t           AllPorts;
	PCAL6416A_REG_Ports_t    Ports;
}__attribute__ ((__packed__))PCAL6416A_REG_Polarity_t;

typedef union 
{
	uint16_t           AllPorts;
	PCAL6416A_REG_Ports_t    Ports;
}__attribute__ ((__packed__))PCAL6416A_REG_Config_t;

typedef union 
{
	uint32_t           AllPorts;
	PCAL6416A_REG_pair_Ports_t    Ports;
}__attribute__ ((__packed__))PCAL6416A_REG_OutDrvStrength_t;

typedef union 
{
	uint16_t           AllPorts;
	PCAL6416A_REG_Ports_t    Ports;
}__attribute__ ((__packed__))PCAL6416A_REG_InLatchReg_t;

typedef union 
{
	uint16_t           AllPorts;
	PCAL6416A_REG_Ports_t    Ports;
}__attribute__ ((__packed__))PCAL6416A_REG_PullUpDwn_t;

typedef union 
{
	uint16_t           AllPorts;
	PCAL6416A_REG_Ports_t    Ports;
}__attribute__ ((__packed__))PCAL6416A_REG_Interrupt_t;

typedef union 
{
	// uint8_t           			AllPorts;
	PCAL6416A_REG_BitPtrn3_t    Port;
}__attribute__ ((__packed__))PCAL6416A_REG_OutPortCfg_t;

typedef struct
{
    PCAL6416A_REG_Input_t Input;
    PCAL6416A_REG_Output_t Output;
    PCAL6416A_REG_Polarity_t Polarity;
    PCAL6416A_REG_Polarity_t Config;
	
	PCAL6416A_REG_OutDrvStrength_t OutDrvStr;
	
	PCAL6416A_REG_InLatchReg_t InLatchReg;
	PCAL6416A_REG_PullUpDwn_t PullUpDwn_EN;
	PCAL6416A_REG_PullUpDwn_t PullUpDwn_SEL;
	
	PCAL6416A_REG_Interrupt_t Interrupt_MASK;
	PCAL6416A_REG_Interrupt_t Interrupt_STATUS;
	
	PCAL6416A_REG_OutPortCfg_t OutPortCfg;
	
}__attribute__ ((__packed__))PCAL6416A_REG_Regs_t;

#define PCAL6416A_REG_INPUT_P0_MASK0


#endif /*__TI_PCAL6416A_REG_H*/









