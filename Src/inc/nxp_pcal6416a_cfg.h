/** \file nxp_pcal6416a_cfg.c
*	\brief nxp_pcal6416a driver configuration
*	\author Giuseppe Di Martino
*	\date June 03, 2019
*	\copyright Copyright (c) 2019 e-Novia S.p.a. - All Rights Reserved.
* 
* E-NOVIA S.P.A. CONFIDENTIAL
* __________________
* 
* ALL INFORMATION CONTAINED HEREIN WAS PRODUCED BY E-NOVIA S.P.A. AND NO THIRD PARTY MAY CLAIM ANY RIGHT OR PATERNITY ON IT.\n 
* THE INTELLECTUAL AND TECHNICAL CONCEPTS CONTAINED HEREIN ARE AND REMAIN
* THE PROPERTY OF E-NOVIA S.P.A. AND ARE PROTECTED BY TRADE SECRET OR COPYRIGHT LAW.
* REPRODUCTION, DISTRIBUTION OR DISCLOSURE OF THIS MATERIAL TO ANY OTHER PARTY
* IS STRICTLY FORBIDDEN UNLESS PRIOR WRITTEN PERMISSION IS OBTAINED FROM E-NOVIA S.P.A.
*/ 

#ifndef __NXP_PCAL6416A_CFG_H
#define __NXP_PCAL6416A_CFG_H

//#include "os_target.h"

#include <stdbool.h>

#ifdef ENABLE_DEBUG
#define DBG_PCAL6416A
#endif

#define PCAL6416A_CFG_I2C_ID                I2C_ID_2

#define PCAL6416A_CFG_DEV_ADDR              (0x20U)

#define PCAL6416A_CFG_INT_MODE_IRQ          GPIO_MODE_IT_RISING

#define PCAL6416A_CFG_INT_MODE_EVT          GPIO_MODE_EVT_RISING //GPIO_MODE_EVT_FALLING

#define PCAL6416A_CFG_INT_EVENT_ID          100//OS_EVENT_ID_PCAL6416A_INT

#define PCAL6416A_CFG_INT_IRQN_TYPE         EXTI0_IRQn
#define PCAL6416A_CFG_INT_IRQ_PRI           5


#define PCAL6416A_CFG_DEFAULT_OUT_REG_0     0x00U
#define PCAL6416A_CFG_DEFAULT_OUT_REG_1     0x00U

#define PCAL6416A_CFG_DEFAULT_POL_REG_0     0x00U
#define PCAL6416A_CFG_DEFAULT_POL_REG_1     0x00U

#define PCAL6416A_CFG_DEFAULT_CFG_REG_0     0x00U
#define PCAL6416A_CFG_DEFAULT_CFG_REG_1     0x00U

//#define PCAL6416A_RESET


/** 
*	\defgroup nxp_pcal6416a_CFG_PORT_PIN UART port pin configuration struct
*   \ingroup nxp_pcal6416a_CFG_MODULE_INOUT
* 	\brief UART port pin configuration struct
*   \{
*/
typedef struct 
{
    GPIO_TypeDef *Port;
    uint32_t    Pin;
    uint32_t    AlternateFunction;
}PCAL6416A_CFG_PortPin_t;

typedef struct 
{
    PCAL6416A_CFG_PortPin_t  Int;
	PCAL6416A_CFG_PortPin_t  Rst;
}PCAL6416A_CFG_InOut_t;

//test
//#define PCAL6416A_CFG_INOUT_INT 	{GPIOA, GPIO_PIN_6,   0}	/**< \brief PCAL6416A int pin configuration*/
//#define PCAL6416A_CFG_INOUT_RST 	{GPIOA, GPIO_PIN_11,   0}	/**< \brief PCAL6416A rst pin configuration*/

//circuito finale
#define PCAL6416A_CFG_INOUT_INT 	{GPIOA, GPIO_PIN_0,   0}	/**< \brief PCAL6416A int pin configuration*/
#define PCAL6416A_CFG_INOUT_RST 	{GPIOC, GPIO_PIN_0,   0}	/**< \brief PCAL6416A rst pin configuration*/

#endif /*__NXP_PCAL6416A_CFG_H*/


