/** \file i2c_cfg.h
 ** \brief I2C Configuration Management Module - header file.
 ** \author Giuseppe Di Martino
 ** \date   21-February-2017
 ** <h2><left>&copy; COPYRIGHT(c) 2017 e-Novia SRL</left></h2>
 ** E-NOVIA CONFIDENTIAL
 ** __________________
 ** 
 ** [2017] e-Novia SRL.
 ** All Rights Reserved.
 ** 
 ** ALL INFORMATION CONTAINED HEREIN WAS PRODUCED BY E-NOVIA SRL AND NO THIRD PARTY MAY CLAIM ANY RIGHT OR PATERNITY ON IT.\n 
 ** THE INTELLECTUAL AND TECHNICAL CONCEPTS CONTAINED HEREIN ARE AND REMAIN
 ** THE PROPERTY OF E-NOVIA SRL AND ARE PROTECTED BY TRADE SECRET OR COPYRIGHT LAW.
 ** REPRODUCTION, DISTRIBUTION OR DISCLOSURE OF THIS MATERIAL TO ANY OTHER PARTY
 ** IS STRICTLY FORBIDDEN UNLESS PRIOR WRITTEN PERMISSION IS OBTAINED FROM E-NOVIA SRL
 */
#ifndef __I2C_CFG_H
#define __I2C_CFG_H

#include "stm32l4xx_hal.h"

//#define I2C_CFG_ID_1
#define I2C_CFG_ID_2
//#define I2C_CFG_ID_3


typedef struct
{
	I2C_TypeDef *Instance;
	uint32_t	Baudrate;
}I2C_CFG_Config_t;


#ifdef I2C_CFG_ID_1
#define I2C_CFG_ID_1_BAUDRATE           0U
#if (I2C_CFG_ID_1_BAUDRATE != 100000) && (I2C_CFG_ID_1_BAUDRATE != 50000) && (I2C_CFG_ID_1_BAUDRATE != 25000)
#error "I2C1: invalid timing configuration"
#endif
#endif 

#ifdef I2C_CFG_ID_2
#define I2C_CFG_ID_2_BAUDRATE           50000U
#if (I2C_CFG_ID_2_BAUDRATE != 100000) && (I2C_CFG_ID_2_BAUDRATE != 50000) && (I2C_CFG_ID_2_BAUDRATE != 25000)
#error "I2C2: invalid timing configuration"
#endif
#endif 

#ifdef I2C_CFG_ID_3
#define I2C_CFG_ID_3_BAUDRATE           0U
#if (I2C_CFG_ID_3_BAUDRATE != 100000) && (I2C_CFG_ID_3_BAUDRATE != 50000) && (I2C_CFG_ID_3_BAUDRATE != 25000)
#error "I2C3: invalid timing configuration"
#endif
#endif 



typedef struct 
{
    GPIO_TypeDef    *Port;
    uint32_t        Pin;
}I2C_CFG_PortPin_t;


typedef struct 
{
    I2C_CFG_PortPin_t  Sda;
    I2C_CFG_PortPin_t  Scl;
}I2C_CFG_InOut_t;


#ifdef I2C_CFG_ID_1
#define I2C_CFG_ID_1_INOUT_SDA      {NULL, 0}
#define I2C_CFG_ID_1_INOUT_SCL      {NULL, 0}
#define I2C_CFG_ID_1_INOUT          {I2C_CFG_ID_1_INOUT_SDA, I2C_CFG_ID_1_INOUT_SCL}
#endif

#ifdef I2C_CFG_ID_2
#define I2C_CFG_ID_2_INOUT_SDA      {GPIOB, GPIO_PIN_11}
#define I2C_CFG_ID_2_INOUT_SCL      {GPIOB, GPIO_PIN_10}
#define I2C_CFG_ID_2_INOUT          {I2C_CFG_ID_2_INOUT_SDA, I2C_CFG_ID_2_INOUT_SCL}
#endif

#ifdef I2C_CFG_ID_3
#define I2C_CFG_ID_3_INOUT_SDA      {NULL, 0}
#define I2C_CFG_ID_3_INOUT_SCL      {NULL, 0}
#define I2C_CFG_ID_3_INOUT          {I2C_CFG_ID_3_INOUT_SDA, I2C_CFG_ID_3_INOUT_SCL}
#endif





#endif /*__I2C_CFG_H*/









