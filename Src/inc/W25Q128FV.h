/**
 * @project       uTag2UWB_Persona
 *
 * @Component     W25Q128FV
 *
 * @file          W25Q128FV.h
 *
 * @brief         This file contains the functions that are used to read and write to the W25Q128FV Flash memory.
 *
 * @author        Simone Bonforti
 *
 * @date          28oct2020
 *
 ***********************************************************************************************************************/
#ifndef FLASH_MEMORY_INCLUDE_W25Q128FV_H_
#define FLASH_MEMORY_INCLUDE_W25Q128FV_H_

#include "main.h"

/* Exported defines */

#define EFLASH_PAGE_SIZE            ( 256 )
#define EFLASH_SECTOR_SIZE          ( 4096 )
#define EFLASH_SECTOR_NUM_X_BLOCK   ( 16 )
#define EFLASH_BLOCK_SIZE           ( EFLASH_SECTOR_SIZE * EFLASH_SECTOR_NUM_X_BLOCK )
#define EFLASH_BLOCK_NUM            ( 256 )
#define EFLASH_SECTOR_NUM           ( EFLASH_SECTOR_NUM_X_BLOCK * EFLASH_BLOCK_NUM )
#define EFLASH_SIZE                 ( EFLASH_SECTOR_SIZE * EFLASH_SECTOR_NUM )
#define EFLASH_PAGE_NUM             ( EFLASH_SIZE / EFLASH_PAGE_SIZE )

/* Exported functions */

boolean_t EFLASH_Init(void);
boolean_t EFLASH_Write(uint32_t address, uint8_t * data, uint32_t data_size);
boolean_t EFLASH_Read(uint32_t address, uint8_t * data, uint32_t data_size);
boolean_t EFLASH_EraseSector(uint16_t sector_num);
boolean_t EFLASH_EraseBlock(uint16_t block_num);
boolean_t EFLASH_EraseChip(void);
boolean_t EFLASH_ProgramPage(uint16_t page_num, uint8_t * data);
boolean_t EFLASH_ReadPage(uint16_t page_num, uint8_t * data);

#endif /* FLASH_MEMORY_INCLUDE_W25Q128FV_H_ */
