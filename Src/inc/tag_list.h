/*! ---------------------------------------------------------------------------
 * @file    tag_list.h
 * @brief
 *
 *
 * @author Decawave Software
 *
 * @attention Copyright 2017 (c) DecaWave Ltd, Dublin, Ireland.
 *            All rights reserved.
 *
 */

#ifndef __INC_TAG_LIST_H__
#define __INC_TAG_LIST_H__ 1

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>

//maximum is limited by NUM_SLOTS; FCONFIG_SIZE and available memory size, see default_config.h
#define MAX_KNOWN_TAG_LIST_SIZE                (1)
#define MAX_DISCOVERED_TAG_LIST_SIZE        (10)

/* Tag list */
typedef enum {
     NOTSPECIFIED = 0,
     IDENTIFIEDPOLL,
     ANONYMOUSPOLL,     
 }pollType;

typedef struct
{
    uint16_t    slot;
    union {
        uint8_t        addrShort[2];
        uint16_t    addr16;
    };
    union    {
        uint8_t        addrLong[8];
        uint64_t     addr64;
    };
    uint16_t    multFast;
    uint16_t    multSlow;
    uint16_t    mode;                           //IMU = bit 0
    uint32_t    lastReceptionTime; // last received tag packet time     
    uint32_t    superFrameNumberOnRegistration; // superframe number when the tag is registered
    uint32_t    nodeSfNumber_of_lastIdentifiedPollResponded; // superframe number of last identified poll reception
    uint32_t    nodeSfNumber_of_lastAnonymousPollResponded; // superframe number of last anonymous poll responded
    uint32_t    nodeSFnumber_of_lastReceivedFinal; 
    uint8_t     lastReceived_PollRNum_InFinal;     
    uint8_t     currentPollrNum; 
    uint32_t    lastSuccessfullyTWRcomputingSuperFrameNumber;       
    pollType    lastReceivedPollType;
    union    {
        uint8_t        req;
        uint8_t        reqUpdatePending : 1;    //request to update Tag's configuration during range phase
    };
}__attribute__((packed))
tag_addr_slot_t;


tag_addr_slot_t *get_tag16_from_knownTagList(uint16_t addr16);
tag_addr_slot_t *get_tag64_from_knownTagList(uint64_t addr64);
tag_addr_slot_t *add_tag_to_knownTagList(uint64_t addr64, uint16_t addr16);
tag_addr_slot_t *add_new_tag_to_knownTagList(uint64_t addr64, uint32_t registrationTime);
tag_addr_slot_t *add_new_tag_to_knownTagList_inCurrentSlot(uint64_t addr64, int16_t currentSlot,uint32_t startSuperFrameTime, uint32_t currentSuperFrameNumber, uint8_t currentPollrNum );

void del_tag64_from_knownTagList(uint64_t addr64);
void del_tag16_from_knownTagList(uint16_t addr16);
uint16_t get_slot_from_currentTime(uint32_t registrationTime,uint32_t startSuperFrameTime);
int      addTagToDList(uint64_t addr64);


uint16_t getDList_size(void);
uint64_t *getDList(void);

void init_knownTagList(void);
tag_addr_slot_t *get_knownTagList(void);
uint16_t get_knownTagList_size(void);
uint32_t get_elapsedTime_from_currentTime(uint32_t currentTime,uint32_t startSuperFrameTime);

void initDList(void);
uint64_t * get_tag64_from_DList(uint64_t addr64);
void deleteDList(void);
#ifdef __cplusplus
}
#endif

#endif /* __INC_TAG_LIST_H_ */
