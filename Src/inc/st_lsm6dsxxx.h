/** \file st_lsm6dsxxx.h
 ** \brief Logical driver for ST-LSM6DS3 and ST-LSM6DS3LTR inertial measurement module - header file.
 ** \authors Giuseppe Di Martino and Luca Bortolotti
 ** \date   07-January-2017
 ** \copyright Copyright (c) 2016 E-Novia Srl. All Rights Reserved.
 */ 

#ifndef __ST_LSM6DSXXX_H
#define	__ST_LSM6DSXXX_H


//#include "os_api.h"
#include "st_lsm6dsxxx_cfg.h"
#include "typeDefs.h"

typedef enum
{
    IMM_MODE_HIGH_PERF,
    //IMM_MODE_LOW_POWER,
    IMM_MODE_NORMAL,
            
    IMM_MODE_INVALID
}IMM_Mode_t;

/**\brief Available ODR when only the accelerometer is activated \ref IMM_REG_CTRL1_XL
 */
typedef enum
{
    IMM_ACC_ODR_POWER_DOWN  = 0U,
    IMM_ACC_ODR_12_5_HZ     = 1U,       /**<\brief BW: 50 Hz (When XL_BW_SCAL_ODR is set to 1, Bandwidth is determined by setting BW_XL[1:0] in CTRL1_XL)*/
    IMM_ACC_ODR_26_HZ       = 2U,       /**<\brief BW: 50 Hz (When XL_BW_SCAL_ODR is set to 1, Bandwidth is determined by setting BW_XL[1:0] in CTRL1_XL)*/ 
    IMM_ACC_ODR_52_HZ       = 3U,       /**<\brief BW: 50 Hz (When XL_BW_SCAL_ODR is set to 1, Bandwidth is determined by setting BW_XL[1:0] in CTRL1_XL)*/
    IMM_ACC_ODR_104_HZ      = 4U,       /**<\brief BW: 50 Hz (When XL_BW_SCAL_ODR is set to 1, Bandwidth is determined by setting BW_XL[1:0] in CTRL1_XL)*/
    IMM_ACC_ODR_208_HZ      = 5U,       /**<\brief BW: 100 Hz (When XL_BW_SCAL_ODR is set to 1, Bandwidth is determined by setting BW_XL[1:0] in CTRL1_XL)*/
    IMM_ACC_ODR_416_HZ      = 6U,       /**<\brief BW: 200 Hz (When XL_BW_SCAL_ODR is set to 1, Bandwidth is determined by setting BW_XL[1:0] in CTRL1_XL)*/
    IMM_ACC_ODR_833_HZ      = 7U,       /**<\brief BW: 400 Hz (When XL_BW_SCAL_ODR is set to 1, Bandwidth is determined by setting BW_XL[1:0] in CTRL1_XL)*/
    IMM_ACC_ODR_1_66_KHZ    = 8U,       /**<\brief BW: 400 Hz (When XL_BW_SCAL_ODR is set to 1, Bandwidth is determined by setting BW_XL[1:0] in CTRL1_XL)*/  
    IMM_ACC_ODR_3_33_KHZ    = 9U,       /**<\brief Filter not used (When XL_BW_SCAL_ODR is set to 1, Bandwidth is determined by setting BW_XL[1:0] in CTRL1_XL)*/    
    IMM_ACC_ODR_6_66_KHZ    = 10U,     /**<\brief Filter not used (When XL_BW_SCAL_ODR is set to 1, Bandwidth is determined by setting BW_XL[1:0] in CTRL1_XL)*/   
            
    IMM_ACC_ODR_IVALID 
}IMM_AccOutputDataRateSel_t;


/**\brief \ref IMM_REG_CTRL1_XL
 */
typedef enum
{
    IMM_ACC_RANGE_02G       = 0U,
    IMM_ACC_RANGE_16G       = 1U,
    IMM_ACC_RANGE_04G       = 2U,
    IMM_ACC_RANGE_8G        = 3U,
    IMM_ACC_RANGE_INVALID   = 4U
}IMM_AccRangeSel_t;

#define IMM_ACC_RANGE_NUM       IMM_ACC_RANGE_INVALID


/**\brief \ref IMM_REG_CTRL1_XL and Table 48 from LSM6DS3 datasheet
 */
typedef enum
{
#if ST_LSM6DS_3H
    IMM_ACC_AAF_BW_400      	= 0U,
    IMM_ACC_AAF_BW_200      	= 1U,
    IMM_ACC_AAF_BW_100      	= 2U,
    IMM_ACC_AAF_BW_50       	= 3U,	
#elif ST_LSM6DS_LTR
	IMM_ACC_AAF_BW_ODR_2		= 0U,
	IMM_ACC_AAF_BW_ODR_4		= 1U,
#endif
	IMM_ACC_AAF_BW_INVALID
}IMM_AccAafBandWidthSel_t;


/**\brief \ref IMM_REG_CTRL8_XL and Tables 68 and 69 from LSM6DS3 datasheet
 */
typedef enum
{
#if ST_LSM6DS_3H
    IMM_ACC_CF_SLOPE_ODR_XL_4   	= 0x00U, /**< Path which uses the SLOPE F. at bauderate of ODR_XL/4 */
    IMM_ACC_CF_HP_ODR_XL_100    	= 0x01U, /**< Path which uses the HPF at baudrate of ORD_XL/100 	*/
    IMM_ACC_CF_HP_ODR_XL_9      	= 0x02U, /**< Path which uses the HPF at baudrate of ORD_XL/9 		*/
    IMM_ACC_CF_HP_ODR_XL_400    	= 0x03U, /**< Path which uses the HPF at baudrate of ORD_XL/400 	*/
	
    IMM_ACC_CF_LPF2_ODR_XL_50    	= 0x10U, /**< Path which uses the LPF2 at bauderate of ODR_XL/50 	*/
    IMM_ACC_CF_LPF2_ODR_XL_100    	= 0x11U, /**< Path which uses the LPF2 at bauderate of ODR_XL/100 	*/
    IMM_ACC_CF_LPF2_ODR_XL_9      	= 0x12U, /**< Path which uses the LPF2 at bauderate of ODR_XL/9 	*/
    IMM_ACC_CF_LPF2_ODR_XL_400    	= 0x13U, /**< Path which uses the LPF2 at bauderate of ODR_XL/400 	*/
	
	IMM_ACC_CF_LPF1				  	= 0x0CU, /**< Bypass path uses only the LPF1 */
	
#elif ST_LSM6DS_LTR
	IMM_ACC_CF_SLOPE_ODR_4   		= 0x00U, /**< Path which uses the SLOPE F. at bauderate of ODR/4 */
    IMM_ACC_CF_HP_ODR_100    		= 0x01U, /**< Path which uses the HPF at baudrate of ORD/100 	 */
    IMM_ACC_CF_HP_ODR_9      		= 0x02U, /**< Path which uses the HPF at baudrate of ORD/9 		 */
    IMM_ACC_CF_HP_ODR_400    		= 0x03U, /**< Path which uses the HPF at baudrate of ORD/400 	 */
	
    IMM_ACC_CF_LPF2_LL_ODR_50    	= 0x10U, /**< Path which uses the LPF2 at bauderate of ODR/50  LOW LATENCY Setted	*/
    IMM_ACC_CF_LPF2_LL_ODR_100    	= 0x11U, /**< Path which uses the LPF2 at bauderate of ODR/100 LOW LATENCY Setted	*/
    IMM_ACC_CF_LPF2_LL_ODR_9      	= 0x12U, /**< Path which uses the LPF2 at bauderate of ODR/9   LOW LATENCY Setted  	*/
    IMM_ACC_CF_LPF2_LL_ODR_400    	= 0x13U, /**< Path which uses the LPF2 at bauderate of ODR/400 LOW LATENCY Setted	*/
	
    IMM_ACC_CF_LPF2_LN_ODR_50    	= 0x1CU, /**< Path which uses the LPF2 at bauderate of ODR/50  LOW NOISE Setted		*/
    IMM_ACC_CF_LPF2_LN_ODR_100    	= 0x1DU, /**< Path which uses the LPF2 at bauderate of ODR/100 LOW NOISE Setted		*/
    IMM_ACC_CF_LPF2_LN_ODR_9      	= 0x1EU, /**< Path which uses the LPF2 at bauderate of ODR/9   LOW NOISE Setted  	*/
    IMM_ACC_CF_LPF2_LN_ODR_400    	= 0x1FU, /**< Path which uses the LPF2 at bauderate of ODR/400 LOW NOISE Setted		*/
	
	IMM_ACC_CF_LPF1_ODR_2		  	= 0x08U, /**< Bypass path uses only the LPF1 with ORD/2 */
	IMM_ACC_CF_LPF1_ODR_4		  	= 0x0CU, /**< Bypass path uses only the LPF1 with ODR/4 */
#endif    
	IMM_ACC_CF_INVALID      
}IMM_AccCompFilterSel_t;



/**\brief Gyroscope ODR configuration setting
 */
typedef enum
{
    IMM_GYRO_ODR_POWER_DOWN  = 0U,
    IMM_GYRO_ODR_12_5_HZ     = 1U,       
    IMM_GYRO_ODR_26_HZ       = 2U,       
    IMM_GYRO_ODR_52_HZ       = 3U,       
    IMM_GYRO_ODR_104_HZ      = 4U,       
    IMM_GYRO_ODR_208_HZ      = 5U,       
    IMM_GYRO_ODR_416_HZ      = 6U,       
    IMM_GYRO_ODR_833_HZ      = 7U,       
    IMM_GYRO_ODR_1_66_KHZ    = 8U,

#if ST_LSM6DS_LTR
	IMM_GYRO_ODR_3_33_KHZ    = 9U,
	IMM_GYRO_ODR_6_66_KHZ    = 10U,
#endif	
            
    IMM_GYRO_ODR_IVALID
}IMM_GyroOutputDataRateSel_t;

/**\brief \ref IMM_REG_CTRL2_G
 */
typedef enum
{
    IMM_GYRO_RANGE_245_DPS   = 0U,
    IMM_GYRO_RANGE_500_DPS   = 1U,
    IMM_GYRO_RANGE_1000_DPS  = 2U,
    IMM_GYRO_RANGE_2000_DPS  = 3U,
    IMM_GYRO_RANGE_125_DPS   = 4U,
	
    IMM_GYRO_RANGE_INVALID
}IMM_GyroRangeSel_t;

#define IMM_GYRO_RANGE_NUM       IMM_GYRO_RANGE_INVALID

typedef enum
{
    IMM_GYRO_ORIENT_XYZ,
    IMM_GYRO_ORIENT_XZY,
    IMM_GYRO_ORIENT_YXZ,
    IMM_GYRO_ORIENT_YZX,
    IMM_GYRO_ORIENT_ZXY,
    IMM_GYRO_ORIENT_ZYX,
    IMM_GYRO_ORIEN_INVALID
}IMM_GyroOrient_t;


typedef enum
{
#if ST_LSM6DS_3H	
    IMM_GYRO_HP_BW_0_0081_HZ    = 0U,
    IMM_GYRO_HP_BW_0_0324_HZ    = 1U,
    IMM_GYRO_HP_BW_2_07_HZ      = 2U,
    IMM_GYRO_HP_BW_16_32_HZ     = 3U,
#elif ST_LSM6DS_LTR
	IMM_GYRO_HP_BW_0_016_HZ     = 0U,
    IMM_GYRO_HP_BW_0_065_HZ     = 1U,
    IMM_GYRO_HP_BW_0_26_HZ      = 2U,
    IMM_GYRO_HP_BW_1_04_HZ      = 3U,
#endif	
    IMM_GYRO_HP_FILTER_OFF      = 0x10U,
	
    IMM_GYRO_HP_INVALID
}IMM_GyroHpFilterSel_t;


typedef struct
{
    int16_t DataX;
    int16_t DataY;
    int16_t DataZ;
    boolean_t    Valid;
}__attribute__ ((__packed__))IMM_GyroData_t;

typedef struct
{
    int16_t DataX;
    int16_t DataY;
    int16_t DataZ;
    boolean_t    Valid;
}__attribute__ ((__packed__))IMM_AccData_t;

typedef struct
{
    int16_t      Data;
    boolean_t    Valid;
}__attribute__ ((__packed__))IMM_TempData_t;

typedef struct
{
    IMM_TempData_t  Temperature;
    IMM_GyroData_t  GyroData;
    IMM_AccData_t   AccData;
}__attribute__ ((__packed__))IMM_MeasureData_t;


/**	\brief This function returns the Device ID
*	\return DeviceID
*
*/
uint8_t IMM_GetID(void);

boolean_t IMM_Init(IMM_AccOutputDataRateSel_t acc_odr, 
            IMM_AccRangeSel_t acc_range,
            IMM_Mode_t acc_mode,
            IMM_GyroOutputDataRateSel_t gyro_odr,
            IMM_GyroRangeSel_t gyro_range,
            IMM_Mode_t gyro_mode);

boolean_t IMM_Reset(void);


boolean_t IMM_SetAccMode(IMM_Mode_t mode);

boolean_t IMM_SetAccOutputDataRate(IMM_AccOutputDataRateSel_t acc_odr);

boolean_t IMM_SetAccFullScale(IMM_AccRangeSel_t acc_fs);

boolean_t IMM_SetAccAafBandwidth(IMM_AccAafBandWidthSel_t acc_bw);

#if ST_LSM6DS_3H
boolean_t IMM_UseAccAafBwForLpf1(boolean_t use_aaf_bw);
#endif

boolean_t IMM_SetAccCompositeFilter(IMM_AccCompFilterSel_t acc_cfs);

//boolean_t IMM_SetAccFilter(IMM_AccFilterSel_t acc_f, IMM_AccHPFilterModeSel_t hpf_mode, IMM_AccCutoffSel_t f_cutoff);

//boolean_t IMM_SetAccAAFBandwidth(boolean_t odr_auto_aaf_bw, IMM_AccAafBandWidthSel_t aaf_bw);

boolean_t IMM_SetAccEnableAxes(boolean_t x_on, boolean_t y_on, boolean_t z_on);

uint32_t  IMM_GetAccSensitivity(IMM_AccRangeSel_t acc_range);


boolean_t IMM_SetGyroMode(IMM_Mode_t mode);

boolean_t IMM_SetGyroOutputDataRate(IMM_GyroOutputDataRateSel_t gyro_odr);

boolean_t IMM_SetGyroFullScale(IMM_GyroRangeSel_t gyro_range);

boolean_t IMM_SetGyroHpFilter(IMM_GyroHpFilterSel_t gyro_hpf);

//boolean_t IMM_SetGyroHPFilterReference(uint8_t ref);

#if ST_LSM6DS_3H
boolean_t IMM_SetGyroEnableAxes(boolean_t pitch_on, boolean_t roll_on, boolean_t yaw_on);

boolean_t IMM_SetGyroSignAndOrientation(boolean_t pitch_neg, boolean_t roll_neg, boolean_t yaw_neg, IMM_GyroOrient_t orient);
#endif

uint32_t  IMM_GetGyroSensitivity(IMM_GyroRangeSel_t gyro_range);



void      IMM_EnableMultipleByteAccess(boolean_t enable);


//boolean_t IMM_SetFifoMode(IMM_FifoMode_t fifo_mode, uint8_t fifo_leve);

//boolean_t IMM_FifoReset(void);



void        IMM_GetMeasureData(IMM_MeasureData_t *measure_data_ptr);

int16_t     IMM_ConvertTemperature(int16_t temp_raw);

//uint32_t    IMM_GetIrqPin(void);

//void        IMM_IrqHandler(void);


//boolean_t   IMU_SelfTestON(void);
//boolean_t   IMU_SelfTestOFF(void);



#endif /*__ST_LSM6DSXXX_H*/

