/*
 * usart.h
 *
 *  Created on: Oct 29, 2020
 *      Author: CCagninelli
 */

#ifndef INC_USART_H_
#define INC_USART_H_
//#include "os_api.h"
#include "main.h"




#define UART5_RX_BUFFER_SIZE    256U 				/* Uart for debug session */
#define UART4_RX_BUFFER_SIZE    (1024U * 1U)		/* Uart for UART session  */
/* USER CODE END Includes */

typedef enum
{
	UART_MODE_NORMAL,
	UART_MODE_LOAD_FW,

	UART_MODE_INVALID
}UART_RxMode_t;


extern UART_HandleTypeDef huart4;
extern UART_HandleTypeDef huart5;
extern uint8_t UART4_RxData;
extern uint8_t UART5_RxData;

extern volatile uint8_t UsartDataUpdate[100];

/* USER CODE BEGIN Private defines */
#define UART4_RX_BUFFER_SIZE    (1024U * 1U)
/* USER CODE END Private defines */

extern void SYS_ErrorHandler(void);

void MX_UART4_Init(void);
void MX_UART5_Init(void);

/* USER CODE BEGIN Prototypes */

/**
*		\brief Function to write uint8_t buffer data to UART3
*
*
**/

void HAL_UART_Send(uint8_t *buffer, uint16_t size);
void HAL_UART_InitRxBuffer(UART_HandleTypeDef *huart);
uint16_t HAL_UART_GetRxBufferAndSize(UART_HandleTypeDef *huart, uint8_t data[]);
void HAL_UART_ResetBuffer(UART_HandleTypeDef *huart);
void UART_SetRxMode(UART_HandleTypeDef *huart, UART_RxMode_t mode);
UART_RxMode_t UART_GetRxMode(UART_HandleTypeDef *huart);
boolean_t HAL_UART_IsLineReady(UART_HandleTypeDef *huart);




#endif /* INC_USART_H_ */
