

#ifndef __CHRG_H
#define __CHRG_H

#include "main.h"
#include <stdbool.h>
/**
 *  \brief Possible statuses of the charger (ref to bq2425x datasheet)
 */
typedef enum
{
    CHRG_STATUS_READY,                  /**< Ready*/
    CHRG_STATUS_CHARGE_IN_PROGRESS,     /**< Charge in progress*/
    CHRG_STATUS_CHARGE_COMPLETE,        /**< Charge done*/
    CHRG_STATUS_FAULT,                  /**< Fault*/
    
    CHRG_STATUS_INVALID
}CHRG_Status_t;

/**
 *  \brief Possible faults of the charger (ref to bq2425x datasheet)
 */
typedef enum
{
    CHRG_FAULT_NONE,                    /**< Normal (no fault)*/
    CHRG_FAULT_INPUT_OVP,               /**< Input OVP*/
    CHRG_FAULT_INPUT_UVLO,              /**< Input Under voltage lockout*/
    CHRG_FAULT_SLEEP,                   /**< Sleep*/
    CHRG_FAULT_BATT_TEMP,               /**< Battery Temperature (TS) Fault*/
    CHRG_FAULT_BATT_OVP,                /**< Battery OVP*/
    CHRG_FAULT_THERMAL_SHUTDOWN,        /**< Thermal Shutdown*/
    CHRG_FAULT_TIME_FAULT,              /**< Timer Fault*/
    CHRG_FAULT_NO_BATT_CONNECTED,       /**< No Battery connected*/
    CHRG_FAULT_ISET_SHORT,              /**< ISET short*/
    CHRG_FAULT_INPUT_LDO_LOW,           /**< Input Fault and Linear DropOut low*/
    
    CHRG_FAULT_INVALID
}CHRG_Fault_t;



/** \addtogroup  BQ24250_APIs
 *  \{
 */

/**
 *  \brief Initialize the BQ24250 Module
 */
void CHRG_Init(void);


void CHRG_MonitorHandler(void);


void CHRG_IntPinConfig(uint8_t mode_int);


/**
 *  \brief Return the status of the charger interrupt signal
 */
bool CHRG_GetIntPinStatus(void);

///**
// ** \brief Enable/Disable the periodical charger monitor
// ** \param[in] enable   If TRUE starts the periodical charger status monitoring; If FALSE stops the periodical charger status monitoring.
// */
//void CHRG_MonitorEnable(boolean_t enable);

void CHRG_StatusRefresh(void);

/**
 *  \brief De-initialize the BQ24250 Module
 */
void CHRG_DeInit(void);

/**
 ** \brief Return the current charger status
 ** \return Current status of the charegr
 */
CHRG_Status_t CHRG_GetStatus(void);

/**
 ** \brief Return the charger current
 ** \return Current current value
 */
uint8_t CHRG_GetChargeCurrent(void);

/**
 ** \brief Put the charger into standby/sleep mode
 ** \return true    If the operation has been successfully completed
 ** \return false   If the operation has not been successfully completed
 */
bool CHRG_SetStandby(bool on);

/**
 ** \brief Return true if is in charging
 ** \return true    If the charge pahse is in progress
 ** \return false   If no charge pahse is in progress
 */
bool CHRG_IsInCharge(void);

/**
 ** \brief Return true if is connected to the charger
 ** \return true    If the charger is connected
 ** \return false   If the charger is not connected
 */
bool CHRG_IsConnected(void);

/**
 ** \brief Set the status change and fault event callbacks
 ** \param[in] status_change_callback   Pointer to the function to be invoked when the charger status changes.
 ** \param[in] fault_callback           Pointer to the function to be invoked when the charger is in faulty state.
 */
void CHRG_SetCallback(void (*status_change_callback)(CHRG_Status_t), void (*fault_callback)(CHRG_Fault_t));


/**
 ** \brief Return the pin related to the INT output of the charger module
 ** \return Pin number related to the INT output of the charger module
 */
uint32_t CHRG_GetPin(void);

/**
 ** \brief Sets the charge current of the charger register #4
 ** \return true    If the operation succeeds
 ** \return false   If the operation fails
 */
bool   CHGR_SetChargeCurrent(uint8_t value);

/**
 ** \brief Return charge current setting of the charger register #4
 ** \return charge current setting
 */
uint8_t     CHGR_GetChargeCurrent(void);

///**
// ** \brief Handler of the INT output of the charger module
// */
//void CHRG_IntHandler(void);
    
/**
  * \}
  */ 
  




#endif /*__CHRG_H*/











