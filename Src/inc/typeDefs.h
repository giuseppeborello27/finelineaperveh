#ifndef __OS_TYPES_H

	#define __OS_TYPES_H

	#include <stdint.h>
	#include <stddef.h>

	#ifndef NULL
	#define NULL				((void *)0)
	#endif /*NULL*/

	#ifndef FALSE
	#define FALSE				0
	#define TRUE				(!FALSE)
	#endif


	#define OUT_t
	#define IN_t



	typedef uint8_t       	uint8;  /*  8 bits */
	typedef uint16_t 		uint16; /* 16 bits */
	typedef uint32_t   		uint32; /* 32 bits */

	typedef int8_t          int8;   /*  8 bits */
	typedef int16_t			int16;  /* 16 bits */
	typedef int32_t			int32;  /* 32 bits */

	typedef volatile int8       vint8;  /*  8 bits */
	typedef volatile int16      vint16; /* 16 bits */
	typedef volatile int32      vint32; /* 32 bits */

	typedef volatile uint8      vuint8;  /*  8 bits */
	typedef volatile uint16     vuint16; /* 16 bits */
	typedef volatile uint32     vuint32; /* 32 bits */

	typedef int					boolean_t;

#endif /*__OS_TYPES_H*/
