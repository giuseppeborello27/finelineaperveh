/**
 * @project       TagPersona
 *
 * @Component     Spi
 *
 * @file          spi.h
 *
 * @brief         Spi functions
 *
 * @author        Author 
 *
 * @date          03nov2020
 *
 ***********************************************************************************************************************/
#ifndef INC_SPI_H_
#define INC_SPI_H_

#include "main.h"

HAL_StatusTypeDef HAL_SPI1_TxRx( GPIO_TypeDef * csPort, uint16_t csPin, uint8_t * pTxData, uint8_t * pRxData, uint16_t Size, uint32_t Timeout );
HAL_StatusTypeDef HAL_SPI2_TxRx( uint8_t * pTxData, uint8_t * pRxData, uint16_t Size, uint32_t Timeout );

#endif /* INC_SPI_H_ */
