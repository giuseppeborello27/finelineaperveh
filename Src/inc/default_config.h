/*
 * @file   default_config.h
 *
 * @brief  default config fie for NVM initialization
 *
 * @author Decawave Software
 *
 * @attention Copyright 2017 (c) DecaWave Ltd, Dublin, Ireland.
 *            All rights reserved.
 *
 */

#ifndef __DEFAULT_CONFIG_H__H__
#define __DEFAULT_CONFIG_H__H__ 1

#ifdef __cplusplus
 extern "C" {
#endif

#include <tag_list.h>
#include <inttypes.h>
#include <deca_device_api.h>

#include "AppConfig.h"

#define ON		1
#define OFF		0

/* UWB config */
#define DEFAULT_CHANNEL             5
#define DEFAULT_PRF                 DWT_PRF_64M
#define DEFAULT_TXPREAMBLENGTH      DWT_PLEN_128
#define DEFAULT_RXPAC               DWT_PAC8
#define DEFAULT_PCODE               9
#define DEFAULT_NSSFD               1
#define DEFAULT_DATARATE            DWT_BR_6M8
#define DEFAULT_PHRMODE             DWT_PHRMODE_STD
#define DEFAULT_SFDTO               (128 + 1 + 16 - 8)

/* run-time config */
#define DEFAULT_UART                0       /**< Do not output to the UART by default */
#define DEFAULT_AUTOS               1       /**< Activate auto start of the Tag top-level application */
#define DEFAULT_REPORT_LEVEL        1       /**< Send TWR & IMU reports back to PC by default */
#define DEFAULT_SMARTTX             0       /**< Do not use Smart Tx on Tag by default */
#define DEFAULT_DEBUG               0       /**< if 1, then the LED_RED used to show an error, if any */
#define DEFAULT_FAULTY_RANGES_NUM   3       /**< after this number of (sequential) ranges faults the tag will return to the discovery phase */
#define DEFAULT_USB2SPI_EMULATE_EVB 0       /**< to emulate EVB USB2SPI string */
#define DEFAULT_ACCEL_STATIONARY_MS 5000    /**< Time of consecutive readings under the threshold needed to consider that the device is stationary */
#define DEFAULT_ACCEL_MOVING_MS     1000    /**< Time of consecutive readings above the threshold needed to consider that the device is moving */
#define ACCEL_NORMALIZED_THRESHOLD  50      /**< Acceleration values difference threshold to consider stationary or moving state.
                                                 This normalized to a resolution in "g" values, we will use a 5% threshold as 1g == 1000 */
#define DEFAULT_ANTD                (16505)	//  Antenna DWM1000

 /* This configures the delay between end of Tag's Blink_TX and Tag start Rx of Ranging Config message
  * Should be the same for Node and Tag
  * */
#define DEFAULT_TAG_BLINK_TX_RC_RX_US   (1000)
#define DEFAULT_RC_RX_TIMEOUT_US        (300)

#define DEFAULT_BUZZER_ALARM			(OFF)
#define DEFAULT_LED_ALARM				(OFF) // VB: Tutto disabilitato per DEMO
#define DEFAULT_MOTOR_ALARM 			(OFF)
/* Below are 2 parameters to be sent to the Tag's in the Ranging Config message.
 * Node uses these parameters to setup its receivers during TWR exchange to a Tag.
 * Tag hardware shall be capable to accept the timings below:
 * */
#define DEFAULT_TAG_POLL_TX_RESPONSE_RX_US  (1000)   /**< 700//1000rough time when Tag shall be ready to receive the node's Response after end of transmission of the Poll.
                                                         This parameter is Node's HW/SW dependent, as the Node's application is not designed to meet shorter timings.
                                                         this works in optimizations -O2 and -O3 **/

#define DEFAULT_TAG_POLL_TX_FINAL_TX_US     (3500)  /**< 1800//3500rough time when Tag shall transmit Final's RMARKER, calculated from Tag's Poll's RMARKER. */
/* EEPROM SIZE used for store configuration */
#define FCONFIG_SIZE            0x100   /**< See NVM/EEPROM linker map and save_bssConfig() */


/* Default configuration initialization */
#define    DEFAULT_CONFIG \
{\
    .s.uartEn                   = DEFAULT_UART, \
    .s.autoStartEn              = DEFAULT_AUTOS, \
    .s.reportLevel              = DEFAULT_REPORT_LEVEL, \
    .s.smartTxEn                = DEFAULT_SMARTTX, \
    .s.debugEn                  = DEFAULT_DEBUG, \
    .s.faultyRanges             = DEFAULT_FAULTY_RANGES_NUM, \
    .s.emuEVB                   = DEFAULT_USB2SPI_EMULATE_EVB, \
    .s.acc_stationary_ms        = DEFAULT_ACCEL_STATIONARY_MS, \
    .s.acc_moving_ms            = DEFAULT_ACCEL_MOVING_MS, \
    .s.acc_threshold            = ACCEL_NORMALIZED_THRESHOLD, \
    \
    .s.ant_rx_a                 = (uint16_t)(/*0.45* */ DEFAULT_ANTD), \
    .s.ant_tx_a                 = (uint16_t)(/*0.55* */ DEFAULT_ANTD), \
    .s.rcDelay_us               = (uint16_t)DEFAULT_TAG_BLINK_TX_RC_RX_US, \
    .s.rcRxTo_us                = (uint16_t)DEFAULT_RC_RX_TIMEOUT_US, \
	\
	.s.buzzerEnabled			= (uint8_t)DEFAULT_BUZZER_ALARM, \
	.s.ledEnabled				= (uint8_t)DEFAULT_LED_ALARM, \
	.s.motorEnabled				= (uint8_t)DEFAULT_MOTOR_ALARM, \
    \
    .s.sfConfig.tag_replyDly_us        = DEFAULT_TAG_POLL_TX_RESPONSE_RX_US, \
    .s.sfConfig.tag_pollTxFinalTx_us   = DEFAULT_TAG_POLL_TX_FINAL_TX_US, \
    \
    .dwt_config.chan            = DEFAULT_CHANNEL, \
    .dwt_config.prf             = DEFAULT_PRF, \
    .dwt_config.txPreambLength  = DEFAULT_TXPREAMBLENGTH, \
    .dwt_config.rxPAC           = DEFAULT_RXPAC, \
    .dwt_config.txCode          = DEFAULT_PCODE, \
    .dwt_config.rxCode          = DEFAULT_PCODE, \
    .dwt_config.nsSFD           = DEFAULT_NSSFD, \
    .dwt_config.dataRate        = DEFAULT_DATARATE, \
    .dwt_config.phrMode         = DEFAULT_PHRMODE, \
    .dwt_config.sfdTO           = DEFAULT_SFDTO,\
	.config.preAlarmThreshold_v		= DEFAULT_PRE_ALRM_THR ,\
	.config.alarmThreshold_v 			= DEFAULT_ALRM_THR ,\
	.config.preAlarmHysteresis_v 		= DEFAULT_PRE_ALRM_HYS,\
	.config.alarmHysteresis_v 		= DEFAULT_ALRM_HYS,\
	.config.timeout_v 				= DEFAULT_TIMEOUT,\
	.config.preAlarmReleActivation_v 	= DEFAULT_PRE_ALRM_REL,\
	.config.alarmReleActivation_v 	= DEFAULT_ALRM_REL,\
	.config.averageWindowSamples_v 	= DEFAULT_AVG_WIN_SAMP,\
	.config.driverExclusionTimeout_v 	= DEFAULT_DRIVE_EXC_TIMEOUT,\
	.config.detectionFrequency_v 		= DEFAULT_DETEC_FREQ\
}

 /*
 	*/


typedef struct
{
    uint16_t    slotPeriod;              /**< Slot period (time for one tag to range) */
    uint16_t    numSlots;                /**< Number of slots used in the SuperFrame */
    uint16_t    sfPeriod_ms;             /**< SuperFrame period in ms */
    uint16_t    tag_replyDly_us;         /**< wait4response delay after end of Poll for the tag, us */
    uint16_t    tag_pollTxFinalTx_us;    /**< PollTxToFinalTx delay, us */

    /* below can be removed if the d2k command, i.e. "add all discovered tags to known list automatically", is not used */
    uint16_t    tag_mFast;              /**< Used to pass a "moving" refresh rate to all tags in the "d2k" command */
    uint16_t    tag_mSlow;              /**< Used to pass a "stationary" refresh rate to all tags in the "d2k" command */
    uint16_t    tag_Mode;               /**< Used to pass a common "mode" parameter to all tags in the "d2k" command */
}sfConfig_t;
/* struct which holding a run-time parameters */
typedef struct {
    uint8_t     uartEn;         /**< activate UART output */
    uint8_t     autoStartEn;    /**< activate auto start */
    uint8_t     reportLevel;    /**< 0: no output 1: JSON output Reports from Tag to PC */
    uint8_t     smartTxEn;      /**< activate Smart Tx TODO: not tested */
    uint8_t     debugEn;        /**< 0: will not stop in error_handler() */
    uint8_t     faultyRanges;   /**< Number of consecutive faulty ranges to consider return back to blinking mode */
    uint8_t     emuEVB;
    uint16_t    acc_stationary_ms;
    uint16_t    acc_moving_ms;
    uint16_t    acc_threshold;
    uint16_t    ant_rx_a;       /**< antenna delay values DW_A */
    uint16_t    ant_tx_a;       /**< antenna delay values DW_A */
    uint16_t    rcDelay_us;     /**< Ranging Config RX on delay: shall be the same to Node */
    uint16_t    rcRxTo_us;      /**< Ranging Config RX timeout: save energy */

    uint8_t 	buzzerEnabled;  /**<  Alarm buzzer */
    uint8_t 	ledEnabled;		/**<  Alarm Led    */
    uint8_t 	motorEnabled;	/**<  Alarm Motor  */

    sfConfig_t  sfConfig;       /**< System configuration: SuperFrame description */
}__attribute__((packed))
run_t;


/* The structure, holding the changeable configuration of the application
 * */

typedef struct {
    run_t           s;                  /**< Run-time parameters */
    dwt_config_t    dwt_config;        /**< Standard Decawave driver config */
    tag_addr_slot_t knownTagList[MAX_KNOWN_TAG_LIST_SIZE];
    uint8_t         free[FCONFIG_SIZE - (sizeof(run_t) + sizeof(dwt_config_t))];
    ConfigParamValues_t   config;
}__attribute__((packed))
param_block_t;



#ifdef __cplusplus
}
#endif

#endif /* __DEFAULT_CONFIG__H__ */
