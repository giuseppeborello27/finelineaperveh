/** \file st_lsm6ds3_cfg.h
 ** \brief Configuration defs for ST-LSM6DS3 and ST-LSM6DS3LTR inertial measurement module - header file.
 ** \authors Giuseppe Di Martino and Luca Bortolotti
 ** \date   07-January-2017
 ** \copyright Copyright (c) 2016 E-Novia Srl. All Rights Reserved.
 *
 *
 *		This driver is able to manage ST-LSM6DS3H and ST-LSM6DSLTR with: 	STM32L476xx (With CUSTOM SPI Api or HAL_SPI Api);
 *																			STM32L152xE (With CUSTOM I2C Api):
 *																			__dsPIC33EP512MC806__
 */

//#include "os_target.h"

#define ST_LSM6DS_3H			0
#define ST_LSM6DS_LTR			1

#if (ST_LSM6DS_3H + ST_LSM6DS_LTR) != 1
	#error "Define the right Module"
#endif

#define IMM_SPI_TIMEOUT				200
//#define IMM_CFG_IRQ_ENABLE








