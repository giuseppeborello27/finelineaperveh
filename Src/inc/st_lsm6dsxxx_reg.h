/** \file st_lsm6ds3_reg.h
 ** \brief Register defs for ST-LSM6DS3 and ST-LSM6DS3LTR inertial measurement module - header file.
 ** \authors Giuseppe Di Martino and Luca Bortolotti
 ** \date   07-January-2017
 ** \copyright Copyright (c) 2016 E-Novia Srl. All Rights Reserved.
 */ 

#ifndef __ST_LSM6DSXXX_REG_H
#define	__ST_LSM6DSXXX_REG_H

#include "st_lsm6dsxxx_cfg.h"

//#include "os_api.h"

/**\brief ST-LSM6DSXXX Resisters Map
 */
typedef enum
{
    IMM_REG_FUNC_CFG_ACCESS         = 0x01U,    /**< \brief Embedded functions configuration register (Type R/W) */
    IMM_REG_SENSOR_SYNC_TIME_FRAME  = 0x04U,    /**< \brief Sensor sync configuration register (Type R/W) */
#if ST_LSM6DS_LTR
	IMM_REG_SENSOR_SYNC_RES_RATIO   = 0x05U,    /**< \brief Sensor sync configuration register (Type R/W) */
#endif
    IMM_REG_FIFO_CTRL_1             = 0x06U,    /**< \brief FIFO configuration register 1 (Type R/W) */
    IMM_REG_FIFO_CTRL_2             = 0x07U,    /**< \brief FIFO configuration register 2 (Type R/W) */
    IMM_REG_FIFO_CTRL_3             = 0x08U,    /**< \brief FIFO configuration register 3 (Type R/W) */      
    IMM_REG_FIFO_CTRL_4             = 0x09U,    /**< \brief FIFO configuration register 4 (Type R/W) */     
    IMM_REG_FIFO_CTRL_5             = 0x0AU,    /**< \brief FIFO configuration register 5 (Type R/W) */
#if ST_LSM6DS_3H	
    IMM_REG_ORIENT_CFG_G            = 0x0BU,    /**< \brief Angular rate sensor sign and orientation register (Type R/W) */
#elif ST_LSM6DS_LTR
	IMM_REG_DRDY_PULSE_CFG_G        = 0x0BU,    /**< \brief Angular rate sensor sign and orientation register (Type R/W) */
#endif
    IMM_REG_INT1_CTRL               = 0x0DU,    /**< \brief INT1 pin control register (Type R/W) */       
    IMM_REG_INT2_CTRL               = 0x0EU,    /**< \brief INT2 pin control register (Type R/W) */       
    IMM_REG_WHO_AM_I                = 0x0FU,    /**< \brief Who_AM_I register. Its value is fixed at 69h (Type R) */
    IMM_REG_CTRL1_XL                = 0x10U,    /**< \brief Accelerometer and gyroscope control registers (Type R/W) */
    IMM_REG_CTRL2_G                 = 0x11U,    /**< \brief Accelerometer and gyroscope control registers (Type R/W) */
    IMM_REG_CTRL3_C                 = 0x12U,    /**< \brief Accelerometer and gyroscope control registers (Type R/W) */
    IMM_REG_CTRL4_C                 = 0x13U,    /**< \brief Accelerometer and gyroscope control registers (Type R/W) */
    IMM_REG_CTRL5_C                 = 0x14U,    /**< \brief Accelerometer and gyroscope control registers (Type R/W) */
    IMM_REG_CTRL6_C                 = 0x15U,    /**< \brief Accelerometer and gyroscope control registers (Type R/W) */
    IMM_REG_CTRL7_G                 = 0x16U,    /**< \brief Accelerometer and gyroscope control registers (Type R/W) */
    IMM_REG_CTRL8_XL                = 0x17U,    /**< \brief Accelerometer and gyroscope control registers (Type R/W) */
    IMM_REG_CTRL9_XL                = 0x18U,    /**< \brief Accelerometer and gyroscope control registers (Type R/W) */
    IMM_REG_CTRL10_C                = 0x19U,    /**< \brief Accelerometer and gyroscope control registers (Type R/W) */
    IMM_REG_MASTER_CONFIG           = 0x1AU,    /**< \brief I2C master configuration register (Type R/W) */
    IMM_REG_WAKE_UP_SRC             = 0x1BU,    /**< \brief Interrupt register (Type R) */
    IMM_REG_TAP_SRC                 = 0x1CU,    /**< \brief Interrupt register (Type R) */        
    IMM_REG_D6D_SRC                 = 0x1DU,    /**< \brief Interrupt register (Type R) */
    IMM_REG_STATUS                  = 0x1EU,    /**< \brief Status data register (Type R) */
    IMM_REG_OUT_TEMP_L              = 0x20U,    /**< \brief Temperature output data register (Type R) */
    IMM_REG_OUT_TEMP_H              = 0x21U,    /**< \brief Temperature output data register (Type R) */
    IMM_REG_OUTX_L_G                = 0x22U,    /**< \brief Gyroscope output register (Type R) */
    IMM_REG_OUTX_H_G                = 0x23U,    /**< \brief Gyroscope output register (Type R) */
    IMM_REG_OUTY_L_G                = 0x24U,    /**< \brief Gyroscope output register (Type R) */
    IMM_REG_OUTY_H_G                = 0x25U,    /**< \brief Gyroscope output register (Type R) */
    IMM_REG_OUTZ_L_G                = 0x26U,    /**< \brief Gyroscope output register (Type R) */
    IMM_REG_OUTZ_H_G                = 0x27U,    /**< \brief Gyroscope output register (Type R) */
    IMM_REG_OUTX_L_XL               = 0x28U,    /**< \brief Accelerometer output register (Type R) */
    IMM_REG_OUTX_H_XL               = 0x29U,    /**< \brief Accelerometer output register (Type R) */
    IMM_REG_OUTY_L_XL               = 0x2AU,    /**< \brief Accelerometer output register (Type R) */
    IMM_REG_OUTY_H_XL               = 0x2BU,    /**< \brief Accelerometer output register (Type R) */
    IMM_REG_OUTZ_L_XL               = 0x2CU,    /**< \brief Accelerometer output register (Type R) */
    IMM_REG_OUTZ_H_XL               = 0x2DU,    /**< \brief Accelerometer output register (Type R) */
    IMM_REG_SENSORHUB1              = 0x2EU,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB2              = 0x2FU,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB3              = 0x30U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB4              = 0x31U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB5              = 0x32U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB6              = 0x33U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB7              = 0x34U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB8              = 0x35U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB9              = 0x36U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB10             = 0x37U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB11             = 0x38U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB12             = 0x39U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_FIFO_STATUS_1           = 0x3AU,    /**< \brief FIFO status register 1 (Type R) */
    IMM_REG_FIFO_STATUS_2           = 0x3BU,    /**< \brief FIFO status register 2 (Type R) */
    IMM_REG_FIFO_STATUS_3           = 0x3CU,    /**< \brief FIFO status register 3 (Type R) */
    IMM_REG_FIFO_STATUS_4           = 0x3DU,    /**< \brief FIFO status register 4 (Type R) */
    IMM_REG_FIFO_DATA_OUT_L         = 0x3EU,    /**< \brief FIFO data output register (Type R) */
    IMM_REG_FIFO_DATA_OUT_H         = 0x3FU,    /**< \brief FIFO data output register (Type R) */
    IMM_REG_TIMESTAMP_0             = 0x40U,    /**< \brief Timestamp output register (Type R) */
    IMM_REG_TIMESTAMP_1             = 0x41U,    /**< \brief Timestamp output register (Type R) */
    IMM_REG_TIMESTAMP_2             = 0x42U,    /**< \brief Timestamp output register (Type R/W) */
    IMM_REG_STEP_TIMESTAMP_L        = 0x49U,    /**< \brief Step counter timestamp register (Type R) */
    IMM_REG_STEP_TIMESTAMP_H        = 0x4AU,    /**< \brief Step counter timestamp register (Type R) */
    IMM_REG_STEP_COUNTER_L          = 0x4BU,    /**< \brief Step counter timestamp register (Type R) */
    IMM_REG_STEP_COUNTER_H          = 0x4CU,    /**< \brief Step counter timestamp register (Type R) */
    IMM_REG_SENSORHUB13             = 0x4DU,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB14             = 0x4EU,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB15             = 0x4FU,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB16             = 0x50U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB17             = 0x51U,    /**< \brief Sensor hub output registers (Type R) */
    IMM_REG_SENSORHUB18             = 0x52U,    /**< \brief Sensor hub output registers (Type R) */ 
#if ST_LSM6DS_3H
    IMM_REG_FUNC_SRC                = 0x53U,    /**< \brief Interrupt registers (Type R) */
#elif ST_LSM6DS_LTR
	IMM_REG_FUNC_SRC1				= 0x53U,	/**< \brief Interrupt registers (Type R) */
	IMM_REG_FUNC_SRC2				= 0x54U,	/**< \brief Interrupt registers (Type R) */
	IMM_REG_WIRST_TILT_IA			= 0x55U,	/**< \brief Interrupt registers (Type R) */
#endif
    IMM_REG_TAP_CFG                 = 0x58U,    /**< \brief Interrupt registers (Type R/W) */
    IMM_REG_TAP_THS_6D              = 0x59U,    /**< \brief Interrupt registers (Type R/W) */
    IMM_REG_INT_DUR2                = 0x5AU,    /**< \brief Interrupt registers (Type R/W) */
    IMM_REG_WAKE_UP_THS             = 0x5BU,    /**< \brief Interrupt registers (Type R/W) */
    IMM_REG_WAKE_UP_DUR             = 0x5CU,    /**< \brief Interrupt registers (Type R/W) */
    IMM_REG_FREE_FALL               = 0x5DU,    /**< \brief Interrupt registers (Type R/W) */
    IMM_REG_MD1_CFG                 = 0x5EU,    /**< \brief Interrupt registers (Type R/W) */
    IMM_REG_MD2_CFG                 = 0x5FU,    /**< \brief Interrupt registers (Type R/W) */
#if ST_LSM6DS_LTR
	IMM_REG_MASTER_CMD_MODE         = 0x60U,    /**< \brief  (Type R/W) */
	IMM_REG_SENS_SYNC_SPI_ERROR_CODE= 0x61U,    /**< \brief  (Type R/W) */
#endif
    IMM_REG_OUT_MAG_RAW_X_L         = 0x66U,    /**< \brief External magnetometer raw data output registers (Type R) */
    IMM_REG_OUT_MAG_RAW_X_H         = 0x67U,    /**< \brief External magnetometer raw data output registers (Type R) */
    IMM_REG_OUT_MAG_RAW_Y_L         = 0x68U,    /**< \brief External magnetometer raw data output registers (Type R) */
    IMM_REG_OUT_MAG_RAW_Y_H         = 0x69U,    /**< \brief External magnetometer raw data output registers (Type R) */
    IMM_REG_OUT_MAG_RAW_Z_L         = 0x6AU,    /**< \brief External magnetometer raw data output registers (Type R) */
    IMM_REG_OUT_MAG_RAW_Z_H         = 0x6BU,    /**< \brief External magnetometer raw data output registers (Type R) */

#if ST_LSM6DS_3H
    IMM_REG_CTRL_SPIAUX             = 0x70U,    /**< \brief OIS data control register (Type R/W) */
#elif ST_LSM6DS_LTR
	IMM_REG_X_OFFS_USR             	= 0x73U,    /**< \brief Accelerometer user offset correction (Type R/W) */
	IMM_REG_Y_OFFS_USR             	= 0x74U,    /**< \brief Accelerometer user offset correction (Type R/W) */
	IMM_REG_Z_OFFS_USR             	= 0x75U,    /**< \brief Accelerometer user offset correction (Type R/W) */
#endif
}IMM_RegisterAddres_t;







/**\brief Angular rate sensor sign and orientation register structure
 */
typedef struct
{
    uint8_t    ORIENT              :3U;    /**<\brief Directional user-orientation selection. Default value: 000.*/
    uint8_t    SIGNZ_G             :1U;    /**<\brief Yaw axis (Z) angular rate sign. Default value: 0*/    
    uint8_t    SIGNY_G             :1U;    /**<\brief Roll axis (Y) angular rate sign. Default value: 0*/    
    uint8_t    SIGNX_G             :1U;    /**<\brief Pitch axis (X) angular rate sign. Default value: 0*/          
    uint8_t    reserved            :2U;
}__attribute__ ((__packed__))IMM_ORIENT_CFG_G_Reg_t;


/**\brief Linear acceleration sensor Control Register 1 structure
 */
typedef struct
{
#if ST_LSM6DS_3H
    uint8_t    BW_XL               :2U;    /**< Anti-aliasing filter bandwidth selection. Default value: 00 (00: 400 Hz; 01: 200 Hz; 10: 100 Hz; 11: 50 Hz)*/
#elif ST_LSM6DS_LTR
	uint8_t    reserved            :1U;
	uint8_t    LPF1_BW_SEL         :1U;    /**< Accelerometer digital LPF (LPF1) bandwidth selection. For bandwidth selection refer to CTRL8_XL (17h)*/
#endif
    uint8_t    FS_XL               :2U;    /**< Accelerometer full-scale selection. Default value: 00.(00: ±2 g; 01: ±16 g; 10: ±4 g; 11: ±8 g)*/
    uint8_t    ODR_XL              :4U;    /**< Output data rate and power mode selection. Default value: 0000.
											* 	
											*	Combination For ST_LSM6DS_3H:
	                                        *
											*	| ODR_XL3 | ODR_XL2 | ODR_XL1 | ODR_XL0 | ODR selection [Hz] whenXL_HM_MODE = 1 | ODR selection [Hz] whenXL_HM_MODE = 0 |
											*	|:-------:|:-------:|:-------:|:-------:|:-------------------------------------:|---------------------------------------|
											*	| 0       | 0       | 0       | 0       | Power-down                            | Power-down                            |
											*	| 0       | 0       | 0       | 1       | 12.5 Hz (low power)                   | 12.5 Hz (high performance)            |
											*	| 0       | 0       | 1       | 0       | 26 Hz (low power)                     | 26 Hz (high performance)              |
											*	| 0       | 0       | 1       | 1       | 52 Hz (low power)                     | 52 Hz (high performance)              |
											*	| 0       | 1       | 0       | 0       | 104 Hz (normal mode)                  | 104 Hz (high performance)             |
											*	| 0       | 1       | 0       | 1       | 208 Hz (normal mode)                  | 208 Hz (high performance)             |
											*	| 0       | 1       | 1       | 0       | 416 Hz (high performance)             | 416 Hz (high performance)             |
											*	| 0       | 1       | 1       | 1       | 833 Hz (high performance)             | 833 Hz (high performance)             |
											*	| 1       | 0       | 0       | 0       | 1.66 kHz (high performance)           | 1.66 kHz (high performance)           |
											*	| 1       | 0       | 0       | 1       | 3.33 kHz (high performance)           | 3.33 kHz (high performance)           |
											*	| 1       | 0       | 1       | 0       | 6.66 kHz (high performance)           | 6.66 kHz (high performance)           |	
	                                        *
											*	Combination For ST_LSM6DS_LTR:
											*	
											*	| ODR_XL3 | ODR_XL2 | ODR_XL1 | ODR_XL0 | ODR selection [Hz] whenXL_HM_MODE = 1 | ODR selection [Hz] whenXL_HM_MODE = 0 |
											*	|:-------:|:-------:|:-------:|:-------:|:-------------------------------------:|---------------------------------------|
											*	| 0       | 0       | 0       | 0       | Power-down                            | Power-down                            |
											*	| 1       | 0       | 1       | 1       | 1.6 Hz (low power only)               | 12.5 Hz (high performance)            |
											*	| 0       | 0       | 0       | 1       | 12.5 Hz (low power)                   | 12.5 Hz (high performance)            |
											*	| 0       | 0       | 1       | 0       | 26 Hz (low power)                     | 26 Hz (high performance)              |
											*	| 0       | 0       | 1       | 1       | 52 Hz (low power)                     | 52 Hz (high performance)              |
											*	| 0       | 1       | 0       | 0       | 104 Hz (normal mode)                  | 104 Hz (high performance)             |
											*	| 0       | 1       | 0       | 1       | 208 Hz (normal mode)                  | 208 Hz (high performance)             |
											*	| 0       | 1       | 1       | 0       | 416 Hz (high performance)             | 416 Hz (high performance)             |
											*	| 0       | 1       | 1       | 1       | 833 Hz (high performance)             | 833 Hz (high performance)             |
											*	| 1       | 0       | 0       | 0       | 1.66 kHz (high performance)           | 1.66 kHz (high performance)           |
											*	| 1       | 0       | 0       | 1       | 3.33 kHz (high performance)           | 3.33 kHz (high performance)           |
											*	| 1       | 0       | 1       | 0       | 6.66 kHz (high performance)           | 6.66 kHz (high performance)           |
											*	| 1       | 1       | x       | x       | Not allowed                           | Not allowed                           |	
	                                        *
											*/
}__attribute__ ((__packed__))IMM_CTRL1_XL_Reg_t;

/**\brief Angular rate sensor Control Register 2 structure
 */
typedef struct
{
    uint8_t    reserved            :1U;
    uint8_t    FS_125              :1U;    /**\brief Gyroscope full-scale at 125 dps. Default value: 0 (0: disabled; 1: enabled)*/    
    uint8_t    FS_G                :2U;    /**\brief Gyroscope full-scale selection. Default value: 00 (00: 245 dps; 01: 500 dps; 10: 1000 dps; 11: 2000 dps)*/
    uint8_t    ODR_G               :4U;    /**\brief Gyroscope output data rate selection. Default value: 0000 (Refer to Table 51)*/
}__attribute__ ((__packed__))IMM_CTRL2_G_Reg_t;


/**\brief Control register 3 structure
 */
typedef struct
{
    uint8_t    SW_RESET            :1U;    /**<\brief Software Reset. Default value: 0. (0: normal mode; 1: reset device) This bit is cleared by hardware after next flash boot*/
    uint8_t    BLE                 :1U;    /**<\brief Big/Little Endian Data Selection. Default value 0. (0: data LSB @ lower address; 1: data MSB @ lower address)*/
    uint8_t    IF_INC              :1U;    /**<\brief Register address automatically incremented during a multiple byte access with a serial interface (I2C or SPI). Default value: 1*/
    uint8_t    SIM                 :1U;    /**<\brief SPI Serial Interface Mode selection. Default value: 0 (4 wire)*/
    uint8_t    PP_OD               :1U;    /**<\brief Push-Pull/Open-Drain selection on INT pad. Default value: 0.(0: push-pull mode; 1: open drain mode)*/
    uint8_t    H_ACTIVE            :1U;    /**<\brief PInterrupt activation level. Default value: 0 (0: interrupt output pads active high; 1: interrupt output pads active low)*/
    uint8_t    BDU                 :1U;    /**<\brief Block Data Update. Default value: 0.(0: continuous update; 1: output registers not updated until MSB and LSB have been read)*/
    uint8_t    BOOT                :1U;    /**<\brief Reboot memory content. Default value: 0. (0: normal mode; 1: reboot memory content(1))*/
}__attribute__ ((__packed__))IMM_CTRL3_C_Reg_t;

/**\brief Control register 4 structure
 */
typedef struct
{
    uint8_t    STOP_ON_FTH         :1U;    /**<\brief Enable FIFO threshold level use. Default value: 0 (0: FIFO depth is not limited; 1: FIFO depth is limited to threshold level) */
    uint8_t    reserved            :1U;    
    uint8_t    I2C_DISABLE         :1U;    /**<\brief Disable I2C interface. Default value: 0 (0: both I2C and SPI enabled; 1: I2C disabled, SPI only) */
    uint8_t    DRDY_MASK           :1U;    /**<\brief Data-ready mask enable. If enabled, when switching from Power-Down to an active mode, 
                                             * the accelerometer and gyroscope data-ready signals are masked until the settling of the sensor 
                                             * filters is completed. Default value: 0 (0: disabled; 1: enabled) 
                                             */
    uint8_t    FIFO_TEMP_ENABLE    :1U;    /**<\brief  Enable temperature data as 4th FIFO data set(3). Default: 0 
                                             * (0: disable temperature data as 4th FIFO data set; 1: enable temperature data as 4th FIFO data set)
                                             */
    uint8_t    INT2_on_INT1        :1U;    /**<\brief  All interrupt signals available on INT1 pad enable. Default value: 0 
                                             * (0: interrupt signals divided between INT1 and INT2 pads; 
                                             *  1: all interrupt signals in logic or on INT1 pad)
                                             */
    uint8_t    SLEEP_G             :1U;    /**<\brief Gyroscope sleep mode enable. Default value: 0 (0: disabled; 1: enabled) */    
    uint8_t    XL_BW_SCAL_ODR      :1U;    /**<\brief Accelerometer bandwidth selection. Default value: 0(
                                             * 0(1): bandwidth determined by ODR selection, refer to Table 48;
                                             * 1(2): bandwidth determined by setting BW_XL[1:0] in CTRL1_XL (10h) register.) 
                                             */
}__attribute__ ((__packed__))IMM_CTRL4_C_Reg_t;

/**\brief Control register 5 structure
 */
typedef struct
{
    uint8_t    ST_XL               :2U;    /**<\brief Linear acceleration sensor self-test enable. Default value: 00 (00: Self-test disabled; Other: refer to Table 60)*/
    uint8_t    ST_G                :2U;    /**<\brief Angular rate sensor self-test enable. Default value: 00 (00: Self-test disabled; Other: refer to Table 59) */
    uint8_t    reserved            :1U;    
    uint8_t    ROUNDING            :3U;    /**<\brief Circular burst-mode (rounding) read from output registers. Default: 000 (000: no rounding; Others: refer to Table 58)*/
}__attribute__ ((__packed__))IMM_CTRL5_C_Reg_t;

/**\brief Angular rate sensor Control Register 6 structure
 */
typedef struct
{
#if ST_LSM6DS_3H
	uint8_t		reserved            :4U;
#elif ST_LSM6DS_LTR
	uint8_t		FTYPE				:2;		/**< Gyroscope's low-pass filter (LPF1) bandwidth selection
											 *  | FTYPE[1:0] | ODR = 800 Hz  | ODR = 1.6 kHz  | ODR = 3.3 kHz  | ODR = 6.6 kHz |
												|:----------:|:-------------:|:--------------:|:--------------:|:-------------:|
												| 00         | 245 Hz        | 315 Hz         | 343 Hz         | 351 Hz        |
												| 01         | 195 Hz        | 224 Hz         | 234 Hz         | 237 Hz        |
												| 10         | 155 Hz        | 168 Hz         | 172 Hz         | 173 Hz        |
												| 11         | 293 Hz        | 505 Hz         | 925 Hz         | 937 Hz        |
											 */
	uint8_t		reserved			:1;
	uint8_t		USR_OFF_W			:1;		/**< Weight of XL user offset bits of registers X_OFS_USR (73h), Y_OFS_USR (74h),
											 * Z_OFS_USR (75h)
											 * 0 = 2-10 g/LSB
											 * 1 = 2-6 g/LSB
											 */
#endif    
	uint8_t    	XL_HM_MODE          :1U;    /**<\brief High-performance operating mode disable for accelerometer(1). Default value: 0 (
                                             * 0: high-performance operating mode enabled;
                                             * 1: high-performance operating mode disabled)
                                             */
    uint8_t    	LVL2_EN             :1U;    /**<\brief Gyroscope level-sensitive latched enable. Default value: 0 (0: level-sensitive latched disabled; 1: level sensitive latched enabled)*/
    uint8_t    	LVLen               :1U;    /**<\brief Gyroscope data level-sensitive trigger enable. Default value: 0 (0: level-sensitive trigger disabled; 1: level sensitive trigger enabled)*/
    uint8_t    	TRIG_EN             :1U;    /**<\brief Gyroscope data edge-sensitive trigger enable. Default value: 0 (0: external trigger disabled; 1: external trigger enabled)*/
}__attribute__ ((__packed__))IMM_CTRL6_C_Reg_t;

/**\brief Angular rate sensor Control Register 7 structure
 */
typedef struct
{
    uint8_t    reserved            :2U;
    uint8_t    ROUNDING_STATUS     :1U;    /**<\brief Source register rounding function enable on STATUS_REG (1Eh), FUNC_SRC (53h) and WAKE_UP_SRC (1Bh) registers. Default value: 0 (
                                             * 0: disabled; 1: enabled)*/
	
#if ST_LSM6DS_3H	
    uint8_t    HP_G_RST            :1U;    /**<\brief Gyro digital HP filter reset. Default: 0 (
                                             * 0: gyro digital HP filter reset OFF; 
                                             * 1: gyro digital HP filter reset ON)
                                             */
	uint8_t    HPCF_G              :2U;    /**<\brief Gyroscope high-pass filter cutoff frequency selection. Default value: 00.Refer to Table 65. */
#elif ST_LSM6DS_LTR
	uint8_t    reserved_3          :1U; 
    uint8_t    HPM_G               :2U;    /**<\brief Gyroscope digital HP filter cutoff selection. Default: 00
											| Value | Cutoff Frequency |
											|-------|------------------|
											| 00    | 16 mHz           |
											| 01    | 65 mHz           |
											| 10    | 260 mHz          |
											| 11    | 1.04 Hz          | */	
#endif
    
    uint8_t    HP_G_EN             :1U;    /**<\brief Gyroscope digital high-pass filter enable. The filter is enabled only if the gyro is in HP mode. Default value: 0 (
                                              * 0: HPF disabled; 1: HPF enabled)*/   
    uint8_t    G_HM_MODE           :1U;    /**<\brief High-performance operating mode disable for gyroscope(1). Default: 0 (
                                             * 0: high-performance operating mode enabled;
                                             * 1: high-performance operating mode disabled)*/
}__attribute__ ((__packed__))IMM_CTRL7_G_Reg_t;

/**\brief Linear acceleration sensor Control Register 8 structure
 */
typedef struct
{
    uint8_t    	LOW_PASS_ON_6D      :1U;    /**< Accelerometer low-pass filter LPF2 selection. */
    uint8_t    	reserved_1          :1U;    
    uint8_t    	HP_SLOPE_XL_EN      :1U;    /**< Accelerometer slope filter / high-pass filter selection */
    
#if ST_LSM6DS_3H	
	uint8_t    	reserved_34         :2U;  
#elif ST_LSM6DS_LTR
	uint8_t	   	INPUT_COMPOSITE		:1U;	/**< Composite filter input selection. Default: 0
											 * (0: ODR/2 low pass filtered sent to composite filter (default)
											 * 1: ODR/4 low pass filtered sent to composite filter) */	
	uint8_t	   	HP_REF_MODE			:1U;	/**< Enable HP filter reference mode. Default value: 0 (0: disabled; 1: enabled) */
#endif
	
    uint8_t    	HPCF_XL             :2U;    /**< Accelerometer slope filter and high-pass filter configuration and cutoff setting. Refer to Tables 68 and 69 for ST_LSM6DS_3H and table 72 for ST_LSM6DS_LTR. 
                                             * It is also used to select the cutoff frequency of the LPF2 filter, as shown in Table 69 */
    uint8_t    	LPF2_XL_EN          :1U;    /**< Accelerometer low-pass filter LPF2 selection*/
}__attribute__ ((__packed__))IMM_CTRL8_XL_Reg_t;

/**\brief Linear acceleration sensor Control Register 9 structure
 */
typedef struct
{
    uint8_t    	reserved_01         :2U;    
    uint8_t    	SOFT_EN             :1U;    /**<\briefEnable soft-iron correction algorithm for magnetometer(1). Default value: 0 (
                                             * 0: soft-iron correction algorithm disabled;
                                             * 1: soft-iron correction algorithm disabled)*/
#if ST_LSM6DS_3H
    uint8_t    	XEN_XL              :1U;    /**<\brief Accelerometer X-axis output enable. Default value: 1 (0: X-axis output disabled; 1: Z-axis output enabled).*/
    uint8_t    	YEN_XL              :1U;    /**<\brief Accelerometer Y-axis output enable. Default value: 1 (0: Y-axis output disabled; 1: Y-axis output enabled).*/
    uint8_t    	ZEN_XL              :1U;    /**<\brief Accelerometer Z-axis output enable. Default value: 1 (0: Z-axis output disabled; 1: Z-axis output enabled). */
    uint8_t    	reserved_67         :2U;
#elif ST_LSM6DS_LTR
	uint8_t		reserved_3			:1U;
	uint8_t		DEN_XL_G			:1U;	/**< DEN stamping sensor selection. Default value: 0
											 * (0: DEN pin info stamped in the gyroscope axis selected by bits [7:5];
											 * 1: DEN pin info stamped in the accelerometer axis selected by bits [7:5]) */
	uint8_t		DEN_X				:1U;	/**< DEN value stored in LSB of X-axis. Default value: 1 (0: DEN not stored in X-axis LSB; 1: DEN stored in X-axis LSB) */
	uint8_t		DEN_Y				:1U;	/**< DEN value stored in LSB of Y-axis. Default value: 1 (0: DEN not stored in Y-axis LSB; 1: DEN stored in Y-axis LSB) */
	uint8_t		DEN_Z				:1U;	/**< DEN value stored in LSB of Z-axis. Default value: 1 (0: DEN not stored in Z-axis LSB; 1: DEN stored in Z-axis LSB) */
#endif
}__attribute__ ((__packed__))IMM_CTRL9_XL_Reg_t;

/**\brief Linear acceleration sensor Control Register 10 structure
 */
typedef struct
{
    uint8_t    SIGN_MOTION_EN      :1U;    /**<\brief Enable significant motion function. Default value: 0 (0: disabled; 1: enabled)*/
    uint8_t    PEDO_RST_STEP       :1U;    /**<\brief Reset pedometer step counter. Default value: 0 (0: disabled; 1: enabled)*/
    uint8_t    FUNC_EN             :1U;    /**<\brief Enable embedded functionalities (pedometer, tilt, significant motion, sensor hub and ironing) 
                                             * and accelerometer HP and LPF2 filters (refer to Figure 6). Default value: 0 (
                                             * 0: disable functionalities of embedded functions and accelerometer filters;
                                             * 1: enable functionalities of embedded functions and accelerometer filters)*/
#if ST_LSM6DS_3H	
    uint8_t    XEN_G               :1U;    /**<\brief Gyroscope pitch axis (X) output enable. Default value: 1 (0: X-axis output disabled; 1: X-axis output enabled)*/
    uint8_t    YEN_G               :1U;    /**<\brief Gyroscope roll axis (Y) output enable. Default value: 1 (0: Y-axis output disabled; 1: Y axis output enabled)*/
    uint8_t    ZEN_G               :1U;    /**<\brief Gyroscope yaw axis (Z) output enable. Default value: 1 (0: Z-axis output disabled; 1: Z-axis output enabled)*/
    uint8_t    reserved            :2U;    /**<\brief */
#elif ST_LSM6DS_LTR
    uint8_t    TILT_EN             :1U;    /**< Enable tilt calculation. This is effective if the FUNC_EN bit is set to '1' */
    uint8_t    PEDO_EN             :1U;    /**< Enable pedometer algorithm(1). Default value: 0 (0: pedometer algorithm disabled; 1: pedometer algorithm enabled)  This is effective if the FUNC_EN bit is set to '1' */
    uint8_t    TIMER_EN            :1U;    /**< Enable timestamp count. The count is saved in TIMESTAMP0_REG (40h), TIMESTAMP1_REG (41h) and TIMESTAMP2_REG (42h). Default: 0
												(0: timestamp count disabled; 1: timestamp count enabled) */
    uint8_t    reserved            :1U;    
	uint8_t    WRIST_TILT_EN       :1U;    /**< Enable wrist tilt algorithm(1)(2). Default value: 0 (0: wrist tilt algorithm disabled;
												1: wrist tilt algorithm enabled) */
#endif	
}__attribute__ ((__packed__))IMM_CTRL10_C_Reg_t;

/**\brief Angular rate sensor Control Register 6 structure
 */
typedef struct
{
    uint8_t    XL_DA               :1U;       /**\brief Accelerometer new data available*/
    uint8_t    G_DA                :1U;       /**\brief Gyro new data available*/
    uint8_t    TDA                 :1U;       /**\brief Temperature sensor new data available.*/    
    uint8_t    EV_BOOT             :1U;       /**\brief Boot running flag signal.*/   
    uint8_t    reserved            :4U;
}__attribute__ ((__packed__))IMM_STATUS_REG_Reg_t;


#if ST_LSM6DS_LTR
/**\brief Angular rate sensor Control Register 6 structure
 */
typedef struct
{
    uint8_t    OFS_USR;        				/**\brief Accelerometer new data available*/
}__attribute__ ((__packed__))IMM_OFS_USR_Reg_t;



#endif





#endif /*__ST_LSM6DSXXX_REG_H*/









