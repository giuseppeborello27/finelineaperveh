/*
 * @file  port.h
 * @brief port headers file to STM32F303 Anch board for Rose demo
 * @author Decawave Software
 *
 * @attention Copyright 2016 (c) Decawave Ltd, Dublin, Ireland.
 *            All rights reserved.
 *
 * */

#ifndef __PORT__H__
#define __PORT__H__ 1

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

//#include "usbd_cdc.h"
#include "deca_device_api.h"

#include "default_config.h"

#include "cmsis_os.h"

#include "error.h"

#include "circ_buf.h"

//-----------------------------------------------------------------------------
bool pushButton;


 /* SPI/USART/USB buffers */
 #define UART_RX_BUF_SIZE   0x100           /**< Read buffer for UART reception, shall be 1<<X */
 #define USB_RX_BUF_SIZE    0x100           /**< Read buffer for USB reception, shall be 1<<X */
 #define COM_RX_BUF_SIZE    USB_RX_BUF_SIZE /**< Communication RX buffer size */

#if (COM_RX_BUF_SIZE < CDC_DATA_FS_MAX_PACKET_SIZE)
#error "COM_RX_BUF_SIZE should be longer than CDC_DATA_FS_MAX_PACKET_SIZE"
#endif


/* events to start/stop tasks : event group */
enum{
    Ev_Tag_Task     	= 0x08,
    Ev_Tcfm_A_Task      = 0x40,
    Ev_Tcwm_A_Task      = 0x100,
    Ev_Usb2spi_A_Task   = 0x400,
    Ev_Stop_All         = 0x1000
};




//-----------------------------------------------------------------------------
// common macros

#define DW_NSS_Pin          GPIO_PIN_4
#define DW_NSS_GPIO_Port    GPIOA

#ifndef SWAP
#define SWAP(a,b) {a^=b;b^=a;a^=b;}
#endif /* SWAP */

#ifndef MIN
#define MIN(a,b)    (((a) < (b)) ? (a) : (b))
#endif /* MIN */

#ifndef MAX
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#endif /* MAX */


#ifndef TRUE
#define TRUE  true
#endif /* TRUE */

#ifndef FALSE
#define FALSE  false
#endif /* FALSE */


#define MASK_40BIT		(0x00FFFFFFFFFF)  // DW1000 counter is 40 bits
#define MASK_TXDTS      (0x00FFFFFFFE00)  //The TX timestamp will snap to 8 ns resolution - mask lower 9 bits.


typedef enum
{
    PWR_ID_EXT_NOT_USED_0_0,
	PWR_ID_EXT_CLR_NEW_BATT,
	PWR_ID_EXT_PWR_ON_NEW_BATT,
	PWR_ID_EXT_MOTION_RESET,
	PWR_ID_EXT_MOTION_FLAG,
	PWR_ID_EXT_MOTION_L_TRIGGER,
	PWR_ID_EXT_MOTION_ENABLE,
	PWR_ID_EXT_MOTION_H_TRIGGER,

	PWR_ID_EXT_ANTI_TAMPER,
	PWR_ID_EXT_BUT_1,
    PWR_ID_EXT_BUT_2,
    PWR_ID_EXT_NOT_USED_1_3,
	PWR_ID_EXT_RTC_ALM,
	PWR_ID_EXT_IMU_INT1,
	PWR_ID_EXT_POW_ON_CHG_IN,
    PWR_ID_EXT_IMU_INT2,

    PWR_ID_EXT_INVALID
}PWR_ControlExtId_t;

/**
*	\enum PWR_Status_t
*	\brief The type which defines the Power Status of the device
*/
typedef enum
{
	PWR_STATUS_ON,		/**< Device On */
	PWR_STATUS_OFF,		/**< Device Off */
	PWR_STATUS_SLEEP,	/**< Device in Stop Mode */

	PWR_STATUS_INVALID
}PWR_Status_t;
#define PWR_CONTROL_NUM_EXT	    PWR_ID_EXT_INVALID	/**< Number of Power Controls */

typedef void (*PWR_interruptCallback)(uint16_t intOccourred, PWR_Status_t status);

//-----------------------------------------------------------------------------
//

typedef enum {
    DW_A,
    DW_B
}dw_name_e;


/* description of spi interface to DW1000 chip */
typedef struct
{
    SPI_HandleTypeDef       *phspi;
    uint32_t                prescaler_slow;
    uint32_t                prescaler_fast;
    DMA_TypeDef             *dmaRx;
    uint32_t                channelRx;
    DMA_TypeDef             *dmaTx;
    uint32_t                channelTx;
    uint32_t                csPin;
    GPIO_TypeDef            *csPort;
    __IO HAL_LockTypeDef    Lock;
    __IO uint32_t           TxComplete;
    __IO uint32_t           RxComplete;
    uint8_t                 *pBuf;
}spi_handle_t;


/* description of connection to the DW1000 chip */
typedef struct
{
    uint16_t        irqPin;
    GPIO_TypeDef    *irqPort;
    IRQn_Type       irqN;
    uint16_t        rstPin;
    GPIO_TypeDef    *rstPort;
    IRQn_Type    	rstIrqN;
    uint16_t        wakeUpPin;
    GPIO_TypeDef    *wakeUpPort;
    spi_handle_t    *pSpi;
}dw_t;


/* System mode of operation. used to
 *
 * 1. indicate in which mode of operation system is running
 * 2. configure the access rights to command handler in control mode
 * */
typedef enum {
    mANY = 0,   /**< Used only for Commands: indicates the command can be executed in any modes below */
    mIDLE,   	/**< IDLE mode */
    mTWR,       /**< TWR (active) mode */
    mUSB2SPI,   /**< USB2SPI mode */
    mTCWM,      /**< Transmit Continuous Wave Mode mode */
    mTCFM       /**< Transmit Continuous Frame Mode mode */
}mode_e;


/* Application tasks handles & corresponded signals structure
 * */
typedef struct
{
    osThreadId	Handle;
    osMutexId   MutexId;
    int32_t     Signal;
}task_signal_t;


/* Application's
 * Global parameters structure
 * */
typedef struct
{
    param_block_t   *pConfig;       /**< Current configuration */
    mode_e          mode;           /**< saves current "mode" of operation */
    int             lastErrorCode;  /**< Saves the error code in the error_handler() function */
    int             maxMsgLen;      /**< See the longest string size to optimize the MAX_STR_SIZE */
    volatile int    DwCanSleep;     /**< When 1, app can put DW1000 chip to deep sleep mode */

    /* USB_VBUS driver */
   enum {
       USB_DISCONNECTED,
       USB_PLUGGED,
       USB_CONNECTED,
       USB_CONFIGURED,
       USB_UNPLUGGED
   }
   usbState;                            /**< USB connect state */


    struct
    {
        uint8_t  tmpRx;
        uint16_t head;
        uint16_t tail;
        uint8_t  buf[UART_RX_BUF_SIZE];
    }uartRx;                            /**< circular buffer RX from USART */

    struct
    {
        uint16_t head;
        uint16_t tail;
        uint8_t  buf[USB_RX_BUF_SIZE];
    }usbRx;                             /**< circular buffer RX from USB */

    uint16_t    local_buff_length;              /**< from usb_uart_rx parser to application */
    uint8_t     local_buff[COM_RX_BUF_SIZE];    /**< for RX from USB/USART */


    /* Tasks section */
    EventGroupHandle_t xStartTaskEvent;     /**< This event group will pass activation to tasks to start */

    //defaultTask is always running and is not accepting signal, but Event

    task_signal_t   ctrlTask;           /* Tag/Node : accept usb/uart */
    task_signal_t   flushTask;          /* Tag/Node */

    /* app task for TAG mode */
    task_signal_t   imuTask;            /* Tag/Node */
    task_signal_t   rxTask;             /* Tag/Node */
    task_signal_t   blinkTask;          /* Tag only */
    task_signal_t   blinkTmr;           /* Tag only: SW timer */
    task_signal_t   pollTask;           /* Tag only */
    task_signal_t	actionTask;
    /* app tasks for special modes */
    task_signal_t   usb2spiTask;        /* usb2spi mode */
    task_signal_t   tcfmTask;           /* tcfm mode */
    task_signal_t   tcwmTask;           /* tcwm mode */

    //MY_MOD
    task_signal_t    bleTask;
    task_signal_t	testTask;
    task_signal_t	uwbTask;

}__attribute__((packed))
app_t;


/****************************************************************************//**
 *
 * */
extern app_t app;

extern RTC_HandleTypeDef hrtc;
extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern DMA_HandleTypeDef hdma_spi1_rx;
extern DMA_HandleTypeDef hdma_spi1_tx;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern DMA_HandleTypeDef hdma_usart5_rx;
extern DMA_HandleTypeDef hdma_usart5_tx;
extern DMA_HandleTypeDef hdma_memtomem_dma1_channel1;
//extern USBD_HandleTypeDef hUsbDeviceFS;
extern IWDG_HandleTypeDef hiwdg;

extern const dw_t *pDwA;
extern const dw_t dw_chip_A;

/****************************************************************************//**
 * port functions prototypes
 *
 * */
void port_wakeup_dw1000(dw_name_e);
error_e port_wakeup_dw1000_fast(void);

/* application section */
void error_handler(int block, error_e err);

/* SPI/USART/USB section */
void   port_uart_rx_init(void);

/* spi is a part of port.h */
void wakeup_dw1000(dw_name_e chip);
void set_dw_spi_slow_rate(dw_name_e);    /**< slowrate freq ~2.25MHz */
void set_dw_spi_fast_rate(dw_name_e);    /**< fastrate freq ~18MHz */
spi_handle_t * get_spi_handler(void);


///* UWB control section */
void reset_DW1000(void);
void init_dw1000_irq(void);
void enable_dw1000_irq(void);
void disable_dw1000_irq(void);

void port_stop_all_UWB(void);
void port_init_dw1000_irq(void);

/* mutex's */
#define deca_mutex_on   decamutexon
#define deca_mutex_off  decamutexoff

decaIrqStatus_t deca_mutex_on(void);
void deca_mutex_off(decaIrqStatus_t s);


/* Time section */
void start_timer(volatile uint32_t * p_timestamp);
bool check_timer(uint32_t timestamp, uint32_t time);
void Sleep( volatile uint32_t );

//for deca_sleep()
#define portGetTickCount()    HAL_GetTick()


/* GPIO section */
void port_disable_wake_init_dw(void);
void PWR_interruptCheck(PWR_Status_t status, uint16_t int_stat_mask, uint16_t input_stat_mask);
bool PWR_CheckExtenderConfig(void);
void disableAllInterrupts(void);
bool PWR_isBtn1Pressed(void);
#ifdef __cplusplus
}
#endif

#endif /* __PORT__H__ */
