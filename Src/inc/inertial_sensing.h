/** \file inertial_sensing.h
 ** \brief Inertial Sensing Measurement module - header file.
 ** \authors Giuseppe Di Martino
 ** \date   01-December-2016
 ** \copyright Copyright (c) 2016 E-Novia Srl. All Rights Reserved.
 */ 

#ifndef __ISM_H
#define __ISM_H

//#include "os_api.h"
#include "typeDefs.h"

#define ISM_G_TO_MS2           9.807F
#define ISM_DEG_TO_RAD         0.0174533F

typedef struct
{  
    /*m per s2*/
    float AccX;
    float AccY;
    float AccZ;
    /*rad per s*/
    float GyroX;
    float GyroY;
    float GyroZ;
    /* �C */
    float Temp;

}ISM_Measure_t;


typedef struct
{
    float AccX;
    float AccY;
    float AccZ;

}ACC_Measure_t;

boolean_t   ISM_Init(void);

void        ISM_DeInit(void);

boolean_t   ISM_IsReady(void);

void        ISM_GetLastMeasure(ISM_Measure_t *measure);









#endif /*__ISM_H*/


