/*!----------------------------------------------------------------------------
 * @file    task_tag.c
 * @brief   DecaWave Application Layer
 *          RTOS tag implementation
 *
 * @attention
 *
 * Copyright 2016-2017 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author
 *
 * DecaWave
 */

#include <stdio.h>
#include <util.h>
#include <circ_buf.h>
#include <task_tag.h>
#include "2ta.h"
#include "frameConfiguration.h"
#include "randomSlotAssignment.h"
#include "superFrameCounter.h"
#include "main.h" // solo per LLED
#include "event_groups.h"
#include "test.h"
//#include <testDefines.h>
//-----------------------------------------------------------------------------

#define BLINK_PERIOD_MS            (500)    /* range init phase - blink sends period, ms */
#define CONTACT_TIME  15

#define DANGERAREA  8	// area di pericolo in metri
#define ALARMZONE	4	//zona di allarme in metri
#define PERIODALARM 8	//periodo di intermittenza segnalazione

#define LONG_PRESS_THRESHOLD 50 // 5sec
#define MAX_DISTANCE 		 10.0f // 10m


bool dangerZone = false;
bool receptionPoll = false;
uint8_t approachTime = 0;
uint8_t withdrawalTime = 0;
uint8_t nonReceptionTimeout = 0;
uint8_t statusAlarmZone = 0;
uint8_t statusDangerArea = 0;
uint8_t countTwr = 0;
uint8_t timeoutAction = 0;
uint8_t output = 0;
uint8_t timePressButton = 0;
uint8_t timePressButton_d = 0;
float sumDistance_m = 0;
float averageDistance_m = 1000; //Default value of distance

uint8_t intermittentPeriod = 0;
bool statusAction = true;



extern void send_to_pc_tag_location(twr_res_ext_t*);
extern float dwt_getrangebias(uint8 chan, float range, uint8 prf);
//-----------------------------------------------------------------------------

/* @brief    The software timer.
 *  It is signalling to the TwrBlinkTask to transmit a blink.
 *  Will be stopped on reception of Ranging Config response from a Node
 * */
//static void
//BlinkTimer_cb(void const *arg)
//{
//    if(app.blinkTask.Handle) //RTOS : blinkTask can be not started yet
//    {
//        if(osSignalSet(app.blinkTask.Handle, app.blinkTask.Signal) != osOK)
//        {
//            error_handler(1, _Err_Signal_Bad);
//        }
//    }
//    UNUSED(arg);
//}
//
//
///*
// * @brief
// *     The thread is initiating the transmission of the blink
// *     on reception of app.blinkTask.Signal
// *
// * */
//
//
//static void
//BlinkTask(void const * arg)
//{
//    twr_info_t     *p;
//
//    do{
//        osDelay(100);
//    }while(!(p = getTwrInfoPtr()));    //wait for initialisation of pTwrInfo
//
//
//    do {
//        osMutexRelease(app.blinkTask.MutexId);
//
//        osSignalWait(app.blinkTask.Signal, osWaitForever);
//
//        osMutexWait(app.blinkTask.MutexId, 0);    //we do not want the task can be deleted in the middle of operation
//
//        taskENTER_CRITICAL();
//        tag_wakeup_dw1000_blink_poll(p);
//        taskEXIT_CRITICAL();
//
//        initiator_send_blink(p);
//
//    }while(1);
//
//    UNUSED(arg);
//}

/*
 * @brief
 *     The thread is initiating the TWR sequence
 *  on reception of .signal.twrTxPoll
 *
 * */

void TagPollTask(void const * arg)
{

    const frameConfig * frameConfiguration = GetFrameConfiguration();

    targetsContainer targetsContainer = Init2Ta(frameConfiguration);
    randomSlotsResult randomTau_ns;
    int32_t nextWakeUpPeriod_ns;


    initDList();

    dwt_entersleep();		//Enable Sleep

    uint8_t poll_task_cycle_counter = 0;

    do {

      uint32_t tStartFrame = getCurrentTime_ticks();

      randomTau_ns = GetTwoRandomPollsTimes();

      nextWakeUpPeriod_ns = randomTau_ns.first_of_two_tau_ns;

      Twota_waitFor(false, nextWakeUpPeriod_ns);
//      HAL_GPIO_TogglePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin);

      targetsContainer.firstTargetInfo.targetInfoId = get_uwb_address_target();

      // first poll
      Twr_with_target(&targetsContainer.firstTargetInfo);
      timeoutAction--;
      // WaitForFinishingComunication();
      // nextWakeUpPeriod_ns = 5000000; // 5ms
      // Twota_waitFor(false, nextWakeUpPeriod_ns);
//      Sleep(10);

      //  second poll
//      Twr_with_target(&targetsContainer.firstTargetInfo);
      // WaitForFinishingComunication();
      //  nextWakeUpPeriod_ns = 1000000; // 1ms
      // Twota_waitFor(false, nextWakeUpPeriod_ns);
      osDelay(5);


       // third poll
//      Twr_with_target(&targetsContainer.firstTargetInfo);
//      osDelay(1);


      //  // fourth poll
      // Twr_with_target(&targetsContainer.firstTargetInfo);
//       osDelay(1);

      //  // fourth poll
      // Twr_with_target(&targetsContainer.firstTargetInfo);
      //  osSignalWait(2, osWaitForever);
      //  WaitForFinishingComunication();
      //  nextWakeUpPeriod_ns = 1000000;
      // Twota_waitFor(false, nextWakeUpPeriod_ns);

      //  Twr_with_target(&targetsContainer.firstTargetInfo);
      //  WaitForFinishingComunication();

//      HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin,GPIO_PIN_SET);

      //init_knownTagList();
      // printf("dange%d \r\n",dangerZone);
      // printf("appr %d \r\n",approachTime);


      // printf("approachTime %d \r\n",approachTime);
      // printf("withdrawalTime %d \r\n",withdrawalTime);


      dangerZone = false;
//      deleteDList();

//      if (!receptionPoll)
//      nonReceptionTimeout = updateNonReceptionTimeout(nonReceptionTimeout);
//
//      receptionPoll = false;
//
//      // int32_t nextWakeUpPeriod_ns = 100000000;
      nextWakeUpPeriod_ns = getRemainingSuperFrameTime(tStartFrame);
//
//
//      printf("REM %d \r\n",nextWakeUpPeriod_ns);
//       printf("c \r\n");


      Twota_waitFor(true, nextWakeUpPeriod_ns);



      //printf("nextWakeUpPeriod_ns after:%d\r\n",nextWakeUpPeriod_ns);

      // count++;
      // if (count >= 1200) {
      //   while(1) {
      //     __NOP();
      //   }
      // }

      /**DRIVER_EXCLUSION**/
      //Mode different from normal mode?

    	osThreadYield();
    }while(1);

    UNUSED(arg);
}


/* @brief DW1000 RX : RTOS implementation
 *
 * */
static void
TagRxTask(void const * arg)
{
    error_e     ret;
    uint16_t    head, tail;

    twr_info_t  *ptwrInfo;

    do{
    	osDelay(100);
    }while(!(ptwrInfo = getTwrInfoPtr()));    //wait for initialisation of pTwrInfo

    int size = sizeof(ptwrInfo->rxPcktBuf.buf) / sizeof(ptwrInfo->rxPcktBuf.buf[0]);
#ifdef P2P_FUNCTION
//    taskENTER_CRITICAL();
//    /* Clear reception timeout to start next ranging process. */
//    dwt_setrxtimeout(0);
//    /* Activate reception immediately. */
//    dwt_rxenable(DWT_START_RX_IMMEDIATE);
//    taskEXIT_CRITICAL();
#endif P2P_FUNCTION
    do {
        osMutexRelease(app.rxTask.MutexId);
        osSignalWait(app.rxTask.Signal, osWaitForever);
        osMutexWait(app.rxTask.MutexId, 0);

        taskENTER_CRITICAL();
        head = ptwrInfo->rxPcktBuf.head;
        tail = ptwrInfo->rxPcktBuf.tail;
        taskEXIT_CRITICAL();

        /* We are using circular buffer + Signal to safely deliver packets from ISR to APP */
        if(CIRC_CNT(head,tail,size) > 0)
        {
        	set_uwb_connected_flag(true);
            rx_pckt_t       *prxPckt    = &ptwrInfo->rxPcktBuf.buf[tail];
            twr_res_ext_t   p;
            twr_res_p2p_t res;



            ret = twr_initiator_algorithm_rx(prxPckt, ptwrInfo); /**< Run bare twr_initiator_algorithm */
            //if(ret != _Err_Not_Twr_Frame)
            //{


//            if (ret !=  _Err_Not_Twr_Frame)
//			{
				//     ptwrInfo->faultyRangesCnt = 0;

				//     p.addr          = AR2U16(ptwrInfo->env.tagAddr);
				//     p.node_addr     = AR2U16(ptwrInfo->env.nodeAddr);
				//     p.rNum          = prxPckt->msg.respExtMsg.resp.rNum;
				//     p.x_cm          = (int16_t)AR2U16(prxPckt->msg.respExtMsg.resp.x_cm);
				//     p.y_cm          = (int16_t)AR2U16(prxPckt->msg.respExtMsg.resp.y_cm);
				//     p.clkOffset_pphm= (int16_t)AR2U16(prxPckt->msg.respExtMsg.resp.clkOffset_pphm); //Crystal Clock offset value reported back from the Node

					  {
							//XTAL trimming will be performed after sending of Final.
						  /* Instead of using a clkOffset_pphm from Response, which calculated by Node based on distances measurements,
						   * the more precise and direct method of adjusting clock offset using
						   * carrier integrator counter value will be used.*/
						  float co_ppm;
						  co_ppm= dwt_readcarrierintegrator() * (FREQ_OFFSET_MULTIPLIER * HERTZ_TO_PPM_MULTIPLIER_CHAN_5 );
						  ptwrInfo->clkOffset_pphm = (int)(co_ppm * 100); //save the offset value for future apply after Final message been sent
					  }

				// }
				//printf("ret: %d, \t x:%d\r\n",ret,p.x_cm);

				taskENTER_CRITICAL();
				tail = (tail + 1) & (size-1);
				ptwrInfo->rxPcktBuf.tail = tail;
				taskEXIT_CRITICAL();


				if((app.pConfig->s.reportLevel) && (ret == _No_Err_Final))
				{
	//            	printf("final \r\n ");
					int32_t tofi;
					int64_t     Rb, Da, Ra, Db ;

					final_accel_t    *pFinalMsg;

					pFinalMsg = &prxPckt->msg.finalMsg.final;

					uint64_t    tagPollTxTime;
					uint64_t    tagRespRxTime;
					uint64_t    tagFinalTxTime;

					uint64_t    nodeRespTxTime;
					uint64_t    nodePollRxTime;
					uint64_t    nodeFinalRxTime;

					float    RaRbxDaDb = 0;
					float    RbyDb = 0;
					float    RayDa = 0;

					TS2U64_MEMCPY(tagPollTxTime, pFinalMsg->pollTx_ts);
					TS2U64_MEMCPY(tagRespRxTime, pFinalMsg->responseRx_ts);
					TS2U64_MEMCPY(tagFinalTxTime, pFinalMsg->finalTx_ts);
					// printf("tagPollTxTime %d \r\n",(int32_t)tagPollTxTime);
					// printf("tagRespRxTime %d \r\n",(int32_t)tagRespRxTime);
					// printf("tagFinalTxTime %d \r\n",(int32_t)tagFinalTxTime);
					TS2U64_MEMCPY(nodePollRxTime, prxPckt->nodePollRx_ts);
					TS2U64_MEMCPY(nodeRespTxTime, prxPckt->nodeRespTx_ts);
					TS2U64_MEMCPY(nodeFinalRxTime, prxPckt->rxTimeStamp);
					// printf("nodePollRxTime %d \r\n",(int32_t)nodePollRxTime);
					// printf("nodeRespTxTime %d \r\n",(int32_t)nodeRespTxTime);
					// printf("nodeFinalRxTime %d \r\n",(int32_t)nodeFinalRxTime);

					Ra = (int64_t)((tagRespRxTime - tagPollTxTime) & MASK_40BIT);
					Db = (int64_t)((nodeRespTxTime - nodePollRxTime) & MASK_40BIT);

					Rb = (int64_t)((nodeFinalRxTime - nodeRespTxTime) & MASK_40BIT);
					Da = (int64_t)((tagFinalTxTime - tagRespRxTime) & MASK_40BIT);

					RaRbxDaDb = (((float)Ra))*(((float)Rb)) - (((float)Da))*(((float)Db));

					RbyDb = ((float)Rb + (float)Db);

					RayDa = ((float)Ra + (float)Da);

					tofi = (int32) ( RaRbxDaDb/(RbyDb + RayDa) );
					if(tofi <0)
					{
					  tofi = -tofi;
					}
					float range_m  = (float)tofi* (SPEED_OF_LIGHT/499.2e6f/128.0f);

					float dist_to_correct = range_m;
					dist_to_correct /= 1.51f;
					range_m = range_m - dwt_getrangebias(5, dist_to_correct, DWT_PRF_64M);

					if(range_m < MAX_DISTANCE)
					{

						uint16_t dist_cm = range_m*100.0;
						//printf("range %d \r\n",(uint8_t)dist_cm);
						res.addrTag = AR2U16(ptwrInfo->env.nodeAddr);
						//res.addrTag = ptwrInfo->pDestTag->addr16;
						res.range_cm = dist_cm;
						res.rNum = ptwrInfo->rangeNum;
					  //  printf ("dist %d \r\n",dist_cm);

						//if we are not in exclusion mode then evaluate danger zone

						send_to_pc_tag_location(&res);
						nonReceptionTimeout = 0;
						receptionPoll = true;

					}
					else
					{
					  ret =  _Err_Range_Calculation;
					}

				}



				/* ready to serve next raw reception */
				if( (ret != _NO_ERR) )
				{
					/* If the Node is performing a Tx, then the receiver will be enabled
					 * after the Tx automatically and twr_responder_algorithm_rx reports "_NO_ERR".
					 *
					 * Otherwise always re-enable the receiver : unknown frame, not expected tag,
					 * final message, _Err_DelayedTX_Late, etc.
					 * */
	#ifdef P2P_FUNCTION
	//                taskENTER_CRITICAL();
	//                /* Clear reception timeout to start next ranging process. */
	//                dwt_setrxtimeout(0);
	//                /* Activate reception immediately. */
	//                dwt_rxenable(DWT_START_RX_IMMEDIATE);
	//                taskEXIT_CRITICAL();
	#endif P2P_FUNCTION
				}


				 /* Report previous range/coordinates/offset back to UART/USB */
				 if((app.pConfig->s.reportLevel) && (ret == _No_Err_Response))
				 {
					 resp_tag_t   *pRespMsg;
					 pRespMsg = &prxPckt->msg.respMsg.resp;

					 uint64_t    poll_tx_ts; //poll_tx_ts
					 //GB
					 //uint32_t    resp_rx_ts; //resp_rx_ts
					 uint64_t    resp_rx_ts; //resp_rx_ts
					 //GB/

					 uint64_t    resp_tx_ts; //resp_tx_ts
					 uint64_t    poll_rx_ts; //poll_rx_ts

					 int64_t rtd_init, rtd_resp;

					 float distance_m;
					 float tof;
					 float clockOffsetRatio ;

					 /* Read carrier integrator value and calculate clock offset ratio. See NOTE 11 below. */
					 clockOffsetRatio = dwt_readcarrierintegrator() * (FREQ_OFFSET_MULTIPLIER * HERTZ_TO_PPM_MULTIPLIER_CHAN_5 / 1.0e6) ;


					 /* Retrieve poll transmission and response reception timestamps. See NOTE 9 below. */

					 TS2U64_MEMCPY(poll_tx_ts, ptwrInfo->pollTx_ts);
					 TS2U64_MEMCPY(resp_rx_ts, ptwrInfo->tagRespRx_ts);

					 TS2U64_MEMCPY(poll_rx_ts, pRespMsg->pollRx_ts);
					 TS2U64_MEMCPY(resp_tx_ts, pRespMsg->responseTx_ts);


					 /* Compute time of flight and distance, using clock offset ratio to correct for differing local and remote clock rates */
					 rtd_init = resp_rx_ts - poll_tx_ts;
	//            	 printf("rtd_init %d \r\n",(int32_t)rtd_init);
					 rtd_resp = (uint32_t)resp_tx_ts - (uint32_t)poll_rx_ts;
	//            	 printf("rtd_resp %d \r\n",(int32_t)rtd_resp);

					 tof = ((rtd_init - rtd_resp * (1 - clockOffsetRatio)) / 2.0) * DWT_TIME_UNITS;


					 if(tof <0)
					 {
					   tof = -tof;
					 }

					 distance_m = tof* (SPEED_OF_LIGHT);

					 float dist_to_correct = distance_m;
	////
					 distance_m = distance_m - dwt_getrangebias(5, dist_to_correct, DWT_PRF_64M);





	//            	 uint16_t dist_cm = distance_m*100.0;





					  if((distance_m < MAX_DISTANCE)) //Distanza valida
					  {

						 if((distance_m * 100) > get_max_distance())
						 {
							 set_max_distance(distance_m*100);
						 }

						 if((distance_m * 100) < get_min_distance())
						 {
							 set_min_distance(distance_m*100);
						 }

						 set_n_measurement(get_n_measurement() + 1);

						 set_mean_distance(get_mean_distance() + distance_m * 100);

						 sumDistance_m = sumDistance_m + distance_m;

						 countTwr++;
						 timeoutAction = 4;

//						 if(countTwr == 10)
//						 {
//							 averageDistance_m = sumDistance_m/countTwr;
//							 countTwr = 0;
//							 sumDistance_m = 0;
//							 uint16_t averagedist_cm = averageDistance_m*100.0;
//							 //printf("averagedist_cm %d \r\n",(uint16_t)averagedist_cm);
//							 test_ultrawideband_set_distance(averagedist_cm);
//						 }





					  }

	//                 send_to_pc_tag_location(&p);
				 }
//			 }
        }

        osThreadYield();
    }while(1);

    UNUSED(arg);
}


/* @brief Setup TWR tasks and timers for discovery phase.
 *         - blinking timer
 *         - blinking task
 *          - twr polling task
 *         - rx task
 * Only setup, do not start.
 * */
static void tag_setup_tasks(void)
{


    /* This will be the main poll thread for the Tag after discovery completed
     * Do not reduce the stack size for this thread.
     * */
    osThreadDef(pollTask, TagPollTask, osPriorityNormal, 0, 384);
    osMutexDef(pollMutex);

    /* rxThread is passing signal from RX IRQ to an actual two-way ranging algorithm.
     * It awaiting of Rx Signal from RX IRQ ISR and decides what to do next in TWR exchange process.
     * Do not reduce the stack size for this thread.
     * */
    osThreadDef(rxTask, TagRxTask, osPriorityRealtime, 0, 512);
    osMutexDef(rxMutex);

    app.pollTask.Handle     = osThreadCreate(osThread(pollTask), NULL);
    app.pollTask.MutexId    = osMutexCreate(osMutex(pollMutex));

    app.rxTask.Handle       = osThreadCreate(osThread(rxTask), NULL);
    app.rxTask.MutexId      = osMutexCreate(osMutex(rxMutex));

    if( \
    	(app.pollTask.Handle == NULL)   ||\
        (app.rxTask.Handle == NULL)   /*||\
        (app.imuTask.Handle == NULL)*/
        )
    {
        error_handler(1, _Err_Create_Task_Bad);
    }
}

//-----------------------------------------------------------------------------

/* @brief
 *      Stop and delete blink timer and blink timer Task
 * */
void blink_timer_delete(void)
{
    taskENTER_CRITICAL();

    if(app.blinkTmr.Handle)
    {
        osTimerStop(app.blinkTmr.Handle);

        if (osTimerDelete(app.blinkTmr.Handle) == osOK)
        {
            app.blinkTmr.Handle = NULL;
        }
        else
        {
            error_handler(1, _ERR_Cannot_Delete_Timer);
        }
    }

    taskEXIT_CRITICAL();
}

void blink_task_delete(void)
{
    if(app.blinkTask.Handle)
    {
        osMutexWait(app.blinkTask.MutexId, osWaitForever);
        taskENTER_CRITICAL();
        osMutexRelease(app.blinkTask.MutexId);
        if(osThreadTerminate(app.blinkTask.Handle) == osOK)
        {
            osMutexDelete(app.blinkTask.MutexId);
            app.blinkTask.Handle = NULL;
        }
        else
        {
            error_handler(1, _ERR_Cannot_Delete_Task);
        }
        taskEXIT_CRITICAL();
    }
}

//-----------------------------------------------------------------------------

/* @brief
 *      Kill all task and timers related to TWR if any
 *      DW1000's RX and IRQ shall be switched off before task termination,
 *      that IRQ will not produce unexpected Signal
 * */
void tag_terminate_tasks(void)
{

    if(app.imuTask.Handle)
    {
        //osMutexWait(app.imuTask.MutexId, osWaitForever);
        taskENTER_CRITICAL();
        osMutexRelease(app.imuTask.MutexId);
        if(osThreadTerminate(app.imuTask.Handle) == osOK)
        {
            osMutexDelete(app.imuTask.MutexId);
            app.imuTask.Handle = NULL;
        }
        else
        {
            error_handler(1, _ERR_Cannot_Delete_Task);
        }
        taskEXIT_CRITICAL();
    }

    if(app.rxTask.Handle)
    {
        //osMutexWait(app.rxTask.MutexId, osWaitForever);
        taskENTER_CRITICAL();
        osMutexRelease(app.rxTask.MutexId);
        if(osThreadTerminate(app.rxTask.Handle) == osOK)
        {
            osMutexDelete(app.rxTask.MutexId);
            app.rxTask.Handle = NULL;
        }
        else
        {
            error_handler(1, _ERR_Cannot_Delete_Task);
        }
        taskEXIT_CRITICAL();
    }

    if(app.pollTask.Handle)
    {
        //osMutexWait(app.pollTask.MutexId, osWaitForever);
        taskENTER_CRITICAL();
        osMutexRelease(app.pollTask.MutexId);
        if(osThreadTerminate(app.pollTask.Handle) == osOK)
        {
            osMutexDelete(app.pollTask.MutexId);
            app.pollTask.Handle = NULL;
        }
        else
        {
            error_handler(1, _ERR_Cannot_Delete_Task);
        }
        taskEXIT_CRITICAL();
    }
   
    //tag_process_terminate();    //de-allocate Tag RAM Resources
}


/* @fn         tag_helper
 * @brief     this is a service function which starts the Tag
 * top-level application
 *
 * */
void tag_helper(void const *argument)
{
    error_e     tmp;

    port_disable_wake_init_dw();

    taskENTER_CRITICAL();    /**< When the app will setup RTOS tasks, then if task has a higher priority,
                                 the kernel will start it immediately, thus we need to stop the scheduler.*/

    /* "RTOS-independent" part : initialisation of two-way ranging process */
    tmp = tag_process_init();

    if( tmp != _NO_ERR)
    {
        error_handler(1, tmp);
    }

    tag_setup_tasks();        /**< "RTOS-based" : setup all RTOS tasks. */

    HAL_Delay(100); //Start Task

    tag_process_start();



    taskEXIT_CRITICAL();    /**< all RTOS tasks can be scheduled */

}


