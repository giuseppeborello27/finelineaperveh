/** \file i2c.c
 ** \brief I2C Management module - source file.
 ** \author Giuseppe Di Martino
 ** \date   14-October-2016
 ** <h2><left>&copy; COPYRIGHT(c) 2017 e-Novia SRL</left></h2>
 ** E-NOVIA CONFIDENTIAL
 ** __________________
 ** 
 ** [2017] e-Novia SRL.
 ** All Rights Reserved.
 ** 
 ** ALL INFORMATION CONTAINED HEREIN WAS PRODUCED BY E-NOVIA SRL AND NO THIRD PARTY MAY CLAIM ANY RIGHT OR PATERNITY ON IT.\n 
 ** THE INTELLECTUAL AND TECHNICAL CONCEPTS CONTAINED HEREIN ARE AND REMAIN
 ** THE PROPERTY OF E-NOVIA SRL AND ARE PROTECTED BY TRADE SECRET OR COPYRIGHT LAW.
 ** REPRODUCTION, DISTRIBUTION OR DISCLOSURE OF THIS MATERIAL TO ANY OTHER PARTY
 ** IS STRICTLY FORBIDDEN UNLESS PRIOR WRITTEN PERMISSION IS OBTAINED FROM E-NOVIA SRL
 */

#include "i2c.h"

#ifdef STM32L476xx
#define I2C_TIMING_REG_CFG_100K             0x10909CECU
#define I2C_TIMING_REG_CFG_50K              0x30408CFFU
#define I2C_TIMING_REG_CFG_25K              0x90103EFFU
#endif


static I2C_HandleTypeDef I2C_Handle[I2C_NUM];

typedef struct
{
	I2C_TypeDef     *Instance;
	uint32_t	    Frequency;
    I2C_CFG_InOut_t InOut;
}I2C_Config_t;

static const I2C_Config_t I2C_Config[I2C_NUM] = 
{
#ifdef I2C_CFG_ID_1    
	{I2C1, I2C_CFG_ID_1_BAUDRATE, I2C_CFG_ID_1_INOUT},
#endif
#ifdef I2C_CFG_ID_2    
	{I2C2, I2C_CFG_ID_2_BAUDRATE, I2C_CFG_ID_2_INOUT},
#endif
#ifdef I2C_CFG_ID_3    
	{I2C3, I2C_CFG_ID_3_BAUDRATE, I2C_CFG_ID_3_INOUT},
#endif    
};


static bool I2C_Ready[I2C_NUM] =
{
#ifdef I2C_CFG_ID_1
    false,
#endif
#ifdef I2C_CFG_ID_2    
    false,
#endif
#ifdef I2C_CFG_ID_3      
    false
#endif    
};


static void I2C_PinSetup(I2C_Id_t id);
static void I2C_PinUnSetup(I2C_Id_t id);


void I2C_Init(I2C_Id_t id)
{
	if(id < I2C_ID_INVALID)
	{
        I2C_Handle[id].Instance = I2C_Config[id].Instance;
        I2C_Handle[id].Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
        I2C_Handle[id].Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
        I2C_Handle[id].Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
        I2C_Handle[id].Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
        I2C_Handle[id].Init.OwnAddress1 = 0;
		I2C_Handle[id].Init.OwnAddress2 = 0;
        
#ifdef STM32L152xE    
        
		I2C_Handle[id].Init.ClockSpeed = I2C_Config[id].Frequency;
		I2C_Handle[id].Init.DutyCycle = I2C_DUTYCYCLE_2;
        I2C_Ready[id] = (HAL_I2C_Init(&I2C_Handle[id]) != HAL_OK);
        
#elif defined(STM32L476xx)     
        
        if(I2C_Config[id].Frequency == 100000U)
        {
            I2C_Handle[id].Init.Timing = I2C_TIMING_REG_CFG_100K;
            I2C_Ready[id] = true;
        }
        else if(I2C_Config[id].Frequency == 50000U)
        {
            I2C_Handle[id].Init.Timing = I2C_TIMING_REG_CFG_50K;
            I2C_Ready[id] = true;
        }
        else if(I2C_Config[id].Frequency == 25000U)
        {
            I2C_Handle[id].Init.Timing = I2C_TIMING_REG_CFG_25K;
            I2C_Ready[id] = true;
        }
        else
        {
            I2C_Ready[id] = false;
        }
        
        I2C_Handle[id].Init.OwnAddress2Masks = I2C_OA2_NOMASK;
        I2C_Ready[id] = I2C_Ready[id] && (HAL_I2C_Init(&I2C_Handle[id]) == HAL_OK);

        /**Configure Analogue filter 
        */
        I2C_Ready[id] = I2C_Ready[id] && (HAL_I2CEx_ConfigAnalogFilter(&I2C_Handle[id], I2C_ANALOGFILTER_ENABLE) == HAL_OK);        
#endif                
	}
}


void I2C_DeInit(I2C_Id_t id)
{
	HAL_I2C_DeInit(&I2C_Handle[id]);
}


bool I2C_IsReady(I2C_Id_t id)
{
    return I2C_Ready[id];
}


I2C_HandleTypeDef *I2C_GetHandle(I2C_Id_t id)
{
    return &I2C_Handle[id];
}


void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c)
{
#ifdef I2C_CFG_ID_1
	if(hi2c->Instance==I2C1)
	{
		I2C_PinSetup(I2C_ID_1);
		/* Peripheral clock enable */
        __HAL_RCC_I2C1_CLK_ENABLE();
	}
#endif
#ifdef I2C_CFG_ID_2
	if(hi2c->Instance==I2C2)
	{
		I2C_PinSetup(I2C_ID_2);
		/* Peripheral clock enable */
		__HAL_RCC_I2C2_CLK_ENABLE();
	}
#endif
#ifdef I2C_CFG_ID_3
	if(hi2c->Instance==I2C3)
	{
		I2C_PinSetup(I2C_ID_3);
		/* Peripheral clock enable */
		__HAL_RCC_I2C3_CLK_ENABLE();
	}
#endif
}


void HAL_I2C_MspDeInit(I2C_HandleTypeDef* hi2c)
{
#ifdef I2C_CFG_ID_1
	if(hi2c->Instance==I2C1)
	{
		__HAL_RCC_I2C1_CLK_DISABLE();
		I2C_PinUnSetup(I2C_ID_1);

	}
#endif
#ifdef I2C_CFG_ID_2
	if(hi2c->Instance==I2C2)
	{
		__HAL_RCC_I2C2_CLK_DISABLE();
		I2C_PinUnSetup(I2C_ID_2);
	}
#endif
#ifdef I2C_CFG_ID_3
    if(hi2c->Instance==I2C3)
	{
		__HAL_RCC_I2C3_CLK_DISABLE();
		I2C_PinUnSetup(I2C_ID_3);
	}
#endif
}

bool I2C_Write(I2C_Id_t id, uint16_t addr, uint8_t data[], uint16_t data_size)
{
	HAL_StatusTypeDef res = HAL_ERROR;
	if((id < I2C_NUM) && data && data_size)
	{
//		OS_SYNCHRONIZE( /*GDM72*/
			res = HAL_I2C_Master_Transmit(&I2C_Handle[id], addr, data, data_size, 500U);
//		)
	}
	return (res == HAL_OK);
}


bool I2C_Read(I2C_Id_t id, uint16_t addr, uint8_t data[], uint16_t data_size)
{
	HAL_StatusTypeDef res = HAL_ERROR;
	if((id < I2C_NUM) && data && data_size)
	{
//		OS_SYNCHRONIZE( /*GDM72*/
			res = HAL_I2C_Master_Receive(&I2C_Handle[id], addr, data, data_size, 500U);
//		)
	}
	return (res == HAL_OK);
}


static void I2C_PinSetup(I2C_Id_t id)
{
    if(id < I2C_NUM)
    {
        GPIO_InitTypeDef GPIO_InitStruct;
        
#ifdef I2C_CFG_ID_1 
        if(id == I2C_ID_1)
        {
            GPIO_InitStruct.Alternate =  GPIO_AF4_I2C1;
        }
#endif   
#ifdef I2C_CFG_ID_2        
        if(id == I2C_ID_2)
        {
            GPIO_InitStruct.Alternate =  GPIO_AF4_I2C2;
        }
#endif  
#ifdef I2C_CFG_ID_3        
        if(id == I2C_ID_3)
        {
            GPIO_InitStruct.Alternate =  GPIO_AF4_I2C3;
        }
#endif           
         
        GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
       
        GPIO_InitStruct.Pin = I2C_Config[id].InOut.Sda.Pin;
        HAL_GPIO_Init(I2C_Config[id].InOut.Sda.Port, &GPIO_InitStruct);
        
        GPIO_InitStruct.Pin = I2C_Config[id].InOut.Scl.Pin;
        HAL_GPIO_Init(I2C_Config[id].InOut.Scl.Port, &GPIO_InitStruct);
    }
}


static void I2C_PinUnSetup(I2C_Id_t id)
{
    if(id < I2C_NUM)
    {
        HAL_GPIO_DeInit(I2C_Config[id].InOut.Scl.Port, I2C_Config[id].InOut.Scl.Pin);
        HAL_GPIO_DeInit(I2C_Config[id].InOut.Sda.Port, I2C_Config[id].InOut.Sda.Pin);
    }	
}



