/*
 * @file portDW.c
 *
 * @brief  Application platform-dependent functions for Decawave application are collected here
 * @author Decawave
 *
 * @attention Copyright 2016 (c) DecaWave Ltd, Dublin, Ireland.
 *            All rights reserved.
 *
 */
#include <port.h>
#include "main.h"
#include "nxp_pcal6416a.h"
#include "test.h"
#include "uart_command.h"

#define USART_RX_DMA_IRQn    DMA1_Channel5_IRQn

/**/
#define LED_ERROR_Port  LLED_RED_GPIO_Port
#define LED_ERROR_Pin   LLED_RED_Pin

/**/
static volatile uint32_t signalResetDone;    /**< this is bare-metal signal */

#define PWR_CONTROL_DIR_CONFIG_MASK  0xFFB5U        /**< Default I/O extender direction configuration */

#define PWR_CONTROL_POL_CONFIG_MASK  0xA700U        /**< Default I/O extender polarity configuration */


#if PWR_ENABLE_INT_ON_MOT_L
#define PWR_CONTROL_INT_CONFIG_MASK  0x88CBU        /**< Default I/O extender interrupt configuration */
#endif /*PWR_ENABLE_INT_ON_MOT_L*/

#if PWR_ENABLE_INT_ON_MOT_H
#define PWR_CONTROL_INT_CONFIG_MASK  0x886BU        /**< Default I/O extender interrupt configuration */
#endif /*PWR_ENABLE_INT_ON_MOT_H*/

#if PWR_ENABLE_INT_ON_BOTH_MOT_L_AND_H
#define PWR_CONTROL_INT_CONFIG_MASK  0x884BU        /**< Default I/O extender interrupt configuration */
#endif /*PWR_ENABLE_INT_ON_BOTH_MOT_L_AND_H*/

#if (PWR_ENABLE_INT_ON_MOT_L + PWR_ENABLE_INT_ON_MOT_H + PWR_ENABLE_INT_ON_BOTH_MOT_L_AND_H) == 0
#define PWR_CONTROL_INT_CONFIG_MASK  0x88EBU        /**< Default I/O extender interrupt configuration */
#endif /*(PWR_ENABLE_INT_ON_MOT_L + PWR_ENABLE_INT_ON_MOT_H + PWR_ENABLE_INT_ON_BOTH_MOT_L_AND_H)*/

static bool PWR_Initialized  = false;  /*GDM*/
static PWR_interruptCallback manageInterruptsCallback = NULL;

typedef struct
{
	uint8_t        			Port;		// 0 - 1 Port
    uint8_t	    			Pin;		// 0 - 7 Pin
	PCAL6416A_Dir_t 		Direction;	//Direction
	PCAL6416A_Pol_t			Polarity;	//Polarity
    PCAL6416A_PullEn_t		PullEn;		//Pull Enable
	PCAL6416A_PullSel_t		PullSel;	//Pull Selection
	PCAL6416A_InterrMask_t	IntEn;		//Interrupt Enable
    PCAL6416A_Latch_t       LatEn;      //Latch Enable
}PWR_ExtPortControl_t;


static const PWR_ExtPortControl_t PWR_ExtPortControl[PWR_CONTROL_NUM_EXT]  =
{
	{0, 0, PCAL6416A_DIR_IN,  PCAL6416A_POL_RETAINED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_NOT_USED_0_0))      >> PWR_ID_EXT_NOT_USED_0_0),     PCAL6416A_LAT_DISABLED},    /**< PWR_ID_EXT_NOT_USED_0_0 */
	{0, 1, PCAL6416A_DIR_OUT, PCAL6416A_POL_RETAINED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_CLR_NEW_BATT))      >> PWR_ID_EXT_CLR_NEW_BATT),     PCAL6416A_LAT_DISABLED},    /**< PWR_ID_EXT_CLR_NEW_BATT */
	{0, 2, PCAL6416A_DIR_IN,  PCAL6416A_POL_RETAINED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_PWR_ON_NEW_BATT))   >> PWR_ID_EXT_PWR_ON_NEW_BATT),  PCAL6416A_LAT_ENABLED},     /**< PWR_ID_EXT_PWR_ON_NEW_BATT */
	{0, 3, PCAL6416A_DIR_OUT, PCAL6416A_POL_RETAINED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_MOTION_RESET))      >> PWR_ID_EXT_MOTION_RESET),     PCAL6416A_LAT_DISABLED},    /**< PWR_ID_EXT_MOTION_RESET */
	{0, 4, PCAL6416A_DIR_IN,  PCAL6416A_POL_RETAINED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_MOTION_FLAG))       >> PWR_ID_EXT_MOTION_FLAG),      PCAL6416A_LAT_DISABLED},    /**< PWR_ID_EXT_MOTION_FLAG */
	{0, 5, PCAL6416A_DIR_IN,  PCAL6416A_POL_RETAINED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_MOTION_L_TRIGGER) ) >> PWR_ID_EXT_MOTION_L_TRIGGER), PCAL6416A_LAT_DISABLED},    /**< PWR_ID_EXT_MOTION_L_TRIGGER */
	{0, 6, PCAL6416A_DIR_OUT, PCAL6416A_POL_RETAINED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_MOTION_ENABLE))     >> PWR_ID_EXT_MOTION_ENABLE),    PCAL6416A_LAT_DISABLED},    /**< PWR_ID_EXT_MOTION_ENABLE */
	{0, 7, PCAL6416A_DIR_IN,  PCAL6416A_POL_RETAINED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_MOTION_H_TRIGGER))  >> PWR_ID_EXT_MOTION_H_TRIGGER), PCAL6416A_LAT_DISABLED},    /**< PWR_ID_EXT_MOTION_H_TRIGGER */

	{1, 0, PCAL6416A_DIR_IN,  PCAL6416A_POL_INVERTED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_ANTI_TAMPER))       >> PWR_ID_EXT_ANTI_TAMPER),      PCAL6416A_LAT_ENABLED},     /**< PWR_ID_EXT_ANTI_TAMPER */
	{1, 1, PCAL6416A_DIR_IN,  PCAL6416A_POL_INVERTED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_BUT_1))             >> PWR_ID_EXT_BUT_1),            PCAL6416A_LAT_ENABLED},     /**< PWR_ID_EXT_BUT_1 */
	{1, 2, PCAL6416A_DIR_IN,  PCAL6416A_POL_INVERTED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_BUT_2))             >> PWR_ID_EXT_BUT_2),            PCAL6416A_LAT_ENABLED},     /**< PWR_ID_EXT_BUT_2 */
	{1, 3, PCAL6416A_DIR_IN,  PCAL6416A_POL_RETAINED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_NOT_USED_1_3))      >> PWR_ID_EXT_NOT_USED_1_3),     PCAL6416A_LAT_DISABLED},    /**< PWR_ID_EXT_NOT_USED_1_3 */
	{1, 4, PCAL6416A_DIR_IN,  PCAL6416A_POL_RETAINED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_RTC_ALM))           >> PWR_ID_EXT_RTC_ALM),          PCAL6416A_LAT_ENABLED},     /**< PWR_ID_EXT_RTC_ALM */
	{1, 5, PCAL6416A_DIR_IN,  PCAL6416A_POL_INVERTED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_IMU_INT1))          >> PWR_ID_EXT_IMU_INT1),         PCAL6416A_LAT_ENABLED},     /**< PWR_ID_EXT_IMU_INT1 */
	{1, 6, PCAL6416A_DIR_IN,  PCAL6416A_POL_RETAINED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_POW_ON_CHG_IN))     >> PWR_ID_EXT_POW_ON_CHG_IN),    PCAL6416A_LAT_ENABLED},     /**< PWR_ID_EXT_POW_ON_CHG_IN */
	{1, 7, PCAL6416A_DIR_IN,  PCAL6416A_POL_INVERTED, PCAL6416A_PUL_DISABLED, PCAL6416A_PUL_DOWN, (PCAL6416A_InterrMask_t)((PWR_CONTROL_INT_CONFIG_MASK & (1U << PWR_ID_EXT_IMU_INT2))          >> PWR_ID_EXT_IMU_INT2),         PCAL6416A_LAT_ENABLED},     /**< PWR_ID_EXT_IMU_INT2 */
};

typedef struct
{
    const uint16_t  DirMask;
    const uint16_t  PolMask;
    uint16_t        IntMask;
}PWR_ExtPortConfig_t;

static PWR_ExtPortConfig_t  PWR_ExtPortConfig =
{
    (uint16_t)PWR_CONTROL_DIR_CONFIG_MASK,
    (uint16_t)PWR_CONTROL_POL_CONFIG_MASK,
    (uint16_t)PWR_CONTROL_INT_CONFIG_MASK
};
//-----------------------------------------------------------------------------
/*
 * The standard malloc/free used in sscanf/sprintf
 * we want them to be replaced with FreeRTOS's implementation
 * */
//_PTR _calloc_r(struct _reent *re, size_t num, size_t size) {
//    return pvPortMalloc(num*size);
//}
//
//_PTR _malloc_r(struct _reent *re, size_t size) {
//    return pvPortMalloc(size);
//}
//
//_VOID _free_r(struct _reent *re, _PTR ptr) {
//    vPortFree(ptr);
//}


/****************************************************************************//**
 *
 *                                 UART RX section
 *
 *******************************************************************************/

/* @fn      port_uart_rx_init()
 * @brief   UART needs to know how many character needs to be received.
 * 			As this is unknown, we set of reception of single character.
 *        	see HAL_UART_RxCpltCallback for RX callback operation.
 * */
//void port_uart_rx_init(void)
//{
//    app.uartRx.head = app.uartRx.tail = 0;
//
//	if (huart5.gState == HAL_UART_STATE_BUSY_TX_RX)
//    {
//		huart5.State = HAL_UART_STATE_BUSY_TX;
//    }
//    else
//    {
//      huart5.State = HAL_UART_STATE_READY;
//    }
//
//    /* RX uses UART_IRQ (IT) mode; TX will use DMA_IRQ+UART_IRQ */
//    if (HAL_UART_Receive_IT(&huart5, &app.uartRx.tmpRx, 1) != HAL_OK)
//    {
//        error_handler(0, _ERR_UART_RX);
//    }
//}
//
//
///* @fn      HAL_UART_RxCpltCallback
// * 			ISR level function
// * @brief   on reception of UART RX char save it to the buffer.
// * 			UART_RX is working in IT mode, but UART_TX is working with DMA mode
// * */
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
//{
//	uint8_t	data;
//
//	data = app.uartRx.tmpRx;
//
//	/* ReStart USART RX listening for next char */
//	if (HAL_UART_Receive_IT(&huart5, &app.uartRx.tmpRx, 1) != HAL_OK)
//    {
//        error_handler(0, _ERR_UART_RxCplt);
//    }
//
//	/* Add received data to the uart Rx circular buffer */
//    if(CIRC_SPACE( app.uartRx.head, app.uartRx.tail, sizeof(app.uartRx.buf)) > 0)
//    {
//		app.uartRx.buf[app.uartRx.head] = data;
//		app.uartRx.head = (app.uartRx.head + 1) & (sizeof(app.uartRx.buf)-1);
//    }
//    else
//    {
//        error_handler(0, _ERR_UART_RxCplt_Overflow);
//    }
//
//	if(app.ctrlTask.Handle)                                   	//RTOS : ctrlTask could be not started yet
//	{
//		osSignalSet(app.ctrlTask.Handle, app.ctrlTask.Signal);  //signal to the ctrl thread : UART data ready
//	}
//
//}
//
//
///* @fn        HAL_UART_TxCpltCallback
// * @brief      on complete of UART TX release
// * */
//void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
//{
//    /* Check if a tx/rx Process is ongoing or not */
//    if(huart->State == HAL_UART_STATE_BUSY_TX_RX)
//    {
//      huart->State = HAL_UART_STATE_BUSY_RX;
//    }
//    else
//    {
//      huart->State = HAL_UART_STATE_READY;
//    }
//}


/****************************************************************************//**
 *
 *                                 USB RX section
 *
 *******************************************************************************/
/* nothing */

/****************************************************************************//**
 *
 */

/* @fn        error_handler(block, code)
 * @brief     visually indicates something went wrong
 * @parm    if block is 1 then block execution
 * */
void error_handler(int block, error_e err)
{
    app.lastErrorCode = err;

    if(app.pConfig->s.debugEn)
    {
        if(block)
        {
            /* Flash Error Led*/
            while(block)
            {
                for(int i = err; i>0; i--)
                {
                    HAL_IWDG_Refresh(&hiwdg);
                    HAL_GPIO_WritePin(LED_ERROR_Port, LED_ERROR_Pin , GPIO_PIN_SET);
                    Sleep(250);
//                    HAL_GPIO_WritePin(LED_ERROR_Port, LED_ERROR_Pin , GPIO_PIN_RESET);
                    Sleep(250);
                }
                HAL_IWDG_Refresh(&hiwdg);
                Sleep(5000);
                HAL_IWDG_Refresh(&hiwdg);
                Sleep(5000);
                HAL_IWDG_Refresh(&hiwdg);
            }
        }
    }
}



/****************************************************************************//**
 *
 *                                 IRQ section
 *
 *******************************************************************************/

/* @brief     manually configuring and enabling of EXTI priority
 * */
void init_dw1000_irq(void)
{
    /* EXTI15_10_IRQn interrupt configuration for DW_A DW_B */
    HAL_NVIC_SetPriority(DW1000_IRQ_IRQn,configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY, 0);
    enable_dw1000_irq();
}

/* @fn        process_dwRSTn_irq
 * @brief    call-back to signal to the APP that the DW_RESET pin is went high
 *
 * */
static void dw_rst_pin_irq_cb(void)
{
	signalResetDone = 1;
}


void enable_dw1000_irq(void)
{
    HAL_NVIC_EnableIRQ(DW1000_IRQ_IRQn);
}


__INLINE void disable_dw1000_irq(void)
{
    HAL_NVIC_DisableIRQ(DW1000_IRQ_IRQn);
}


__STATIC_INLINE int
check_IRQ_enabled(IRQn_Type IRQn)
{
    return ((   NVIC->ISER[((uint32_t)(IRQn) >> 5)]
             & (uint32_t)0x01 << (IRQn & (uint8_t)0x1F)  ) ? 1 : 0) ;
}

/* @fn         HAL_GPIO_EXTI_Callback
 * @brief      EXTI line detection callback from HAL layer
 * @param      GPIO_Pin: Specifies the port pin connected to corresponding EXTI line.
 */


extern uint8_t bufferResp[50];
extern UART_HandleTypeDef huart5;
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    switch (GPIO_Pin)
    {
        case DW1000_RST_Pin :
        { /* bare-metal signal */
            dw_rst_pin_irq_cb();
            break;
        }

        case DW1000_IRQ_Pin :
        {
            set_dw_spi_fast_rate(DW_A);

            while(HAL_GPIO_ReadPin(DW1000_IRQ_GPIO_Port, DW1000_IRQ_Pin) == GPIO_PIN_SET)
            {
                dwt_isr();
            }

            if(app.DwCanSleep)
            {
                dwt_entersleep();
            }

            break;
        }
        case LPC_INT_Pin:

			if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) == GPIO_PIN_SET)
			{

//				HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin, GPIO_PIN_RESET);
//				HAL_Delay(500);
//				HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin, GPIO_PIN_SET);

	//			INTCTL_EnterCritical();
	//			OS_EventSet(OS_EVENT_ID_PCAL6416A_INT);
				PCAL6416A_IntHandler();
			}

			break;

        default:
            break;
    }
}

/* @fn deca_mutex_on()
 * @brief     Disables IRQ from DW IRQ lines
 *             Returns the IRQ state before disable, this value is used to re-enable in decamutexoff call
 *             This is called at the beginning of a critical section
 */
decaIrqStatus_t deca_mutex_on(void)
{
    decaIrqStatus_t s  ;

    s = check_IRQ_enabled(DW1000_IRQ_IRQn);

    if(s)
    {
        HAL_NVIC_DisableIRQ(DW1000_IRQ_IRQn); //disable the external interrupt line
        __ASM volatile ("dsb 0xF":::"memory");    //CMSIS __DSB;
        __ASM volatile ("isb 0xF":::"memory");    //CMSIS __ISB;
    }

    return s ;   // return state before disable, value is used to re-enable in decamutexoff call
}

/* @fn deca_mutex_off(s)
 *
 * @brief restores IRQ enable state as saved by decamutexon
 *           This is called at the end of a critical section
 */
void deca_mutex_off(decaIrqStatus_t s)
{
    if(s)
    {
        HAL_NVIC_EnableIRQ(DW1000_IRQ_IRQn);
    }
}



/****************************************************************************//**
 *
 *                                 END OF IRQ section
 *
 *******************************************************************************/

/* @fn        reset_DW1000
 * @brief    DW_RESET pin on DW1000 has 2 functions
 *             In general it is output, but it also can be used to reset the digital
 *             part of DW1000 by driving this pin low.
 *             Note, the DW_RESET pin should not be driven high externally.
 * */
void reset_DW1000(void)
{
    GPIO_InitTypeDef     GPIO_InitStruct;

    GPIO_InitStruct.Pin = DW1000_RST_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(DW1000_RST_GPIO_Port, &GPIO_InitStruct);

    HAL_GPIO_WritePin(DW1000_RST_GPIO_Port, DW1000_RST_Pin, RESET);

    Sleep(1);

    GPIO_InitStruct.Pin = DW1000_RST_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(DW1000_RST_GPIO_Port, &GPIO_InitStruct);
}

/* @fn        port_wakeup_dw1000
 * @brief    "slow" waking up of DW1000 using DW_CS only
 * */
void port_wakeup_dw1000(dw_name_e chip)
{
    wakeup_dw1000(chip);
}


/* @fn        setup_DW1000_rst_pin_irq
 * @brief    setup the DW_RESET pin mode
 *             0 - output Open collector mode
 *             !0 - input mode with connected EXTI0 IRQ
 * */
static void setup_DW1000_rst_pin_irq(int enable)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    if(enable)
    {
        /* Enable Reset Pin for use as IRQ for interrupt
         * this will be used to determine the chip in the Idle mode
         * */
        GPIO_InitStruct.Pin  = DW1000_RST_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
        HAL_GPIO_Init(DW1000_RST_GPIO_Port, &GPIO_InitStruct);

        HAL_NVIC_SetPriority(EXTI3_IRQn,4, 0); //below RTOS priority
        HAL_NVIC_EnableIRQ(EXTI3_IRQn);


    }
    else
    {
        HAL_NVIC_DisableIRQ(EXTI3_IRQn);

        //put the pin back to tri-state ... as analog input or input floating
        GPIO_InitStruct.Pin  = DW1000_RST_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(DW1000_RST_GPIO_Port, &GPIO_InitStruct);
		HAL_GPIO_WritePin(DW1000_RST_GPIO_Port, DW1000_RST_Pin, GPIO_PIN_SET);

    }
}


/* @fn      usleep
 * @brief precise usleep() delay
 * */
#pragma GCC optimize ("O0")
void usleep(__useconds_t usec)
{
    int i;
#pragma GCC ivdep
    for(i=0;i<usec;i++)
    {
        __NOP();
        __NOP();
        __NOP();
        __NOP();
        __NOP();
        __NOP();
    }
}


#define WAKEUP_TMR_MS    (3)
/* @fn      port_wakeup_dw1000_fast
 * @brief   waking up of DW1000 using DW_CS and DW_RESET pins.
 *          The DW_RESET signalling that the DW1000 is in the INIT state.
 *          the total fast wakeup takes ~2.2ms and depends on crystal startup time
 * */
error_e port_wakeup_dw1000_fast(void)
{
    uint32_t x = 0;
    uint32_t timestamp = portGetTickCount();    //protection
    error_e  ret = _Err_Timeout;

    setup_DW1000_rst_pin_irq(0);    //disable RSTn IRQ
    signalResetDone = 0;            //signalResetDone connected to the process_dwRSTn_irq() callback
    setup_DW1000_rst_pin_irq(1);    //enable RSTn Rising IRQ

    spi_handle_t *pSpi = get_spi_handler();

    //set CS pins = 0
    HAL_GPIO_WritePin(pSpi->csPort, pSpi->csPin, GPIO_PIN_RESET);

    //need to poll to check when the DW1000 is in the IDLE. The CPLL interrupt is not reliable.
    //when RSTn goes high the DW1000 is in INIT, it will enter IDLE after PLL lock (in 5 us)
    while((HAL_GetTick() - timestamp) < WAKEUP_TMR_MS)
    {
        x++;     //when DW1000 will switch to an IDLE state RSTn pin will be high
        if(signalResetDone == 1)
        {
            ret = _NO_ERR;
            break;
        }

    }
//    printf("x %d \r\n",x);
//    printf("signalResetDone %d \r\n",signalResetDone);
    setup_DW1000_rst_pin_irq(0);         //disable RSTn IRQ

    //set CS pins = 1
    HAL_GPIO_WritePin(pSpi->csPort, pSpi->csPin, GPIO_PIN_SET);

    //it takes ~35us in total for the DW1000 to lock the PLL, download AON and go to IDLE state
    //usleep(35);

    //MYMOD:BUG DECAWAVE
    usleep(1000);

    //TODO: TO TEST ONLY
    //uint32 status = dwt_read32bitreg(0x0F); // Read status register low 32bits

    return ret;
}


/* @fn         port_stop_all_UWB(s)
 *
 * @brief     stop UWB activity
 */
void port_stop_all_UWB(void)
{
    decaIrqStatus_t s = deca_mutex_on();

    set_dw_spi_slow_rate(DW_A);

    dwt_forcetrxoff();

    dwt_setinterrupt(DWT_INT_TFRS | DWT_INT_RFCG | DWT_INT_ARFE | DWT_INT_RFSL |\
                       DWT_INT_SFDT | DWT_INT_RPHE | DWT_INT_RFCE | DWT_INT_RFTO | DWT_INT_RXPTO, 0);

    dwt_softreset();

    dwt_setcallbacks(NULL, NULL,  NULL, NULL);

    deca_mutex_off(s);
}


/**
 *  @brief     Bare-metal level
 *          initialise master/slave DW1000 (check if can talk to device and wake up and reset)
 */
static error_e
port_init_device(dw_name_e device)
{
    set_dw_spi_slow_rate(device);

    //this is called here to wake up the device (i.e. if it was in sleep mode before the restart)
    uint32   devID0 = dwt_readdevid() ;

    if(DWT_DEVICE_ID != devID0) //if the read of device ID fails, the DW1000 could be asleep
    {
        port_wakeup_dw1000(device);

        devID0 = dwt_readdevid();
        // SPI not working or Unsupported Device ID
        if(DWT_DEVICE_ID != devID0)
            return(_ERR_DEVID);
    }
    //clear the sleep bit in case it is set - so that after the hard reset below the DW does not go into sleep
    dwt_softreset();

    return (_NO_ERR);
}

/*
 * @brief this function
 * 1. disables irq from DW1000 chip
 * 2. sets the default DW1000 chip structures pointer pDwA
 * 3. executes a slow wakeup of DW1000
 * 4. sets slow SPI speed to DW1000
 * 5. read devID
 * 6. on successfull reading of devID executes the dwt_softreset()
 * */
void port_disable_wake_init_dw(void)
{
    disable_dw1000_irq();             /**< disable NVIC IRQ until we configure the device */

    taskENTER_CRITICAL();

    pDwA = &dw_chip_A;

    //this is called here to wake up the device (i.e. if it was in sleep mode before the restart)
    port_wakeup_dw1000(DW_A);

    if( (port_init_device(DW_A) != _NO_ERR))
    {
        error_handler(1,  _ERR_INIT);
    }

    taskEXIT_CRITICAL();
}
/* @fn         start_timer(uint32 *p_timestamp)
 * @brief     save system timestamp (in CLOCKS_PER_SEC)
 * @parm     p_timestamp pointer on current system timestamp
 */
void start_timer(volatile uint32_t * p_timestamp)
{
    *p_timestamp = HAL_GetTick();
}


/* @fn         check_timer(uint32 timestamp , uint32 time)
 * @brief     check if time from current timestamp over expectation
 * @param     [in] timestamp - current timestamp
 * @param     [in] time - time expectation (in CLOCKS_PER_SEC)
 * @return     true - time is over
 *             false - time is not over yet
 */
bool check_timer(uint32_t timestamp, uint32_t time)
{
    uint32_t time_passing;
    uint32_t temp_tick_time = HAL_GetTick();

    if (temp_tick_time >= timestamp)
    {
        time_passing = temp_tick_time - timestamp;
    }
    else
    {
        time_passing = 0xffffffffUL - timestamp + temp_tick_time;
    }

    if (time_passing >= time)
    {
        return (true);
    }

    return (false);
}


/* @brief    Sleep
 *             -DDEBUG defined in Makefile prevents __WFI
 */
void Sleep( volatile uint32_t dwMs )
{
    uint32_t dwStart ;
    start_timer(&dwStart);
    while(check_timer(dwStart,dwMs)==false)
    {
        #ifndef DEBUG
//        __WFI();
        #endif
    }
}



/**/

#if (configCHECK_FOR_STACK_OVERFLOW > 0)

void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName )
{
    error_handler(1, _ERR_Stack_Overflow);
}
#endif

static void PWR_resetNewBatteryFlag(void)
{

    PCAL6416A_SetPortPin(PWR_ExtPortControl[PWR_ID_EXT_CLR_NEW_BATT].Port, PWR_ExtPortControl[PWR_ID_EXT_CLR_NEW_BATT].Pin, TRUE);
    PCAL6416A_SetPortPin(PWR_ExtPortControl[PWR_ID_EXT_CLR_NEW_BATT].Port, PWR_ExtPortControl[PWR_ID_EXT_CLR_NEW_BATT].Pin, FALSE);

}

bool PWR_CheckExtenderConfig(void)
{
    bool retval = false;
    uint8_t i;
    uint16_t ext_dir_config = 0U, ext_int_config = 0U, ext_pol_config = 0U;

//    DBG_INFO("ICER: %08X", NVIC->ICER[(((uint32_t)(int32_t)PCAL6416A_CFG_INT_IRQN_TYPE) >> 5UL)]);

    (void)PCAL6416A_GetPortsDirConfig(&ext_dir_config);
    (void)PCAL6416A_GetPortsPolConfig(&ext_pol_config);
    (void)PCAL6416A_GetPortsIntConfig(&ext_int_config);

//    DBG_INFO("Extender dir config: 0x%04X", ext_dir_config);
//    DBG_INFO("Extender pol config: 0x%04X", ext_pol_config);
//    DBG_INFO("Extender int config: 0x%04X", ext_int_config);

    if((PWR_ExtPortConfig.DirMask != ext_dir_config) ||
        (PWR_ExtPortConfig.IntMask != ext_int_config) ||
        (PWR_ExtPortConfig.PolMask != ext_pol_config))
    {
//        printf("Configuring extender... \r\n");

        for(i = 0U; i < PWR_CONTROL_NUM_EXT; i++)
        {
            if(i == PWR_ID_EXT_CLR_NEW_BATT)
            {
                PCAL6416A_SetPortPin(PWR_ExtPortControl[i].Port, PWR_ExtPortControl[i].Pin, FALSE);
            }

            PCAL6416A_SetPortPinLatchReg(PWR_ExtPortControl[i].Port,PWR_ExtPortControl[i].Pin,PWR_ExtPortControl[i].LatEn);
            PCAL6416A_SetPortPinDir(PWR_ExtPortControl[i].Port, PWR_ExtPortControl[i].Pin, PWR_ExtPortControl[i].Direction);
            PCAL6416A_SetPortPinPol(PWR_ExtPortControl[i].Port, PWR_ExtPortControl[i].Pin, PWR_ExtPortControl[i].Polarity);
            PCAL6416A_SetPortPinPullEn(PWR_ExtPortControl[i].Port, PWR_ExtPortControl[i].Pin, PWR_ExtPortControl[i].PullEn);
            PCAL6416A_SetPortPinPullSel(PWR_ExtPortControl[i].Port, PWR_ExtPortControl[i].Pin, PWR_ExtPortControl[i].PullSel);
            PCAL6416A_SetPortPinIntMask(PWR_ExtPortControl[i].Port, PWR_ExtPortControl[i].Pin, PWR_ExtPortControl[i].IntEn);

            if(PWR_ExtPortControl[i].IntEn)
            {
                PWR_ExtPortConfig.IntMask |= (uint16_t)(1U << i);
            }
            else
            {
                PWR_ExtPortConfig.IntMask &= (uint16_t)~(1U << i);
            }
        }
    }
    else
    {
        retval = true;
//        printf("Extender config OK \r\n");
    }

    PWR_Initialized = true;
    return retval;
}

bool PWR_isChargerOn(void)
{
    bool retval;

    PCAL6416A_GetPortPin(PWR_ExtPortControl[PWR_ID_EXT_POW_ON_CHG_IN].Port, PWR_ExtPortControl[PWR_ID_EXT_POW_ON_CHG_IN].Pin, (bool*)&retval);

    return retval;

}

//volatile char button1=0;
//volatile char button2=0;
void PWR_interruptCheck(PWR_Status_t status, uint16_t int_stat_mask, uint16_t input_stat_mask)
{


//	strcpy(bufferResp, "PWR_interruptCheck");
//
//	if(HAL_UART_Transmit(&huart5, bufferResp, strlen(bufferResp), 1000)!= HAL_OK)
//	{
//	   /* Transfer error in transmission process */
//	   Error_Handler();
//	}

    if(PWR_Initialized /*&& !interruptProcessed*/) /*GDM*/
    {
//
//        /* Set the callback to manage interrupts */
//        if(manageInterruptsCallback == NULL)
//        {
////            manageInterruptsCallback = PWR_IntDefCb;
//        }
//
//        if(int_stat_mask & (1U << PWR_ID_EXT_MOTION_FLAG))
//        {
//        	printf("PWR_ID_EXT_MOTION_FLAG \r\n");
////            PWR_motionFlagEnableInterrupt(false);
////            callbackParam |= PWR_INT_MASK_MOTION_FLAG;
//        }

#ifdef TAG_PERSONA
        if(int_stat_mask & (1U << PWR_ID_EXT_ANTI_TAMPER))
        {
        	//printf("PWR_ID_EXT_ANTI_TAMPER \r\n");
//          callbackParam |= PWR_INT_MASK_ANTI_TAMPER;

        	//HAL_Delay(200);
        	receiveAsyncCommand (ANTITAMPER);
        }


        if(int_stat_mask & (1U << PWR_ID_EXT_BUT_1))
        {

//        	if (button1==0)
//        	{
//        		button1=1;
        		receiveAsyncCommand (BUTTON1);
//        	}


//        	if (pushButton == false)
//        	{
//
////			  dwt_entersleep(); // sleep DWM1000
//			  pushButton = true;
//        	}
        	//callbackParam |= PWR_INT_MASK_BTN_1;
        }

        if(int_stat_mask & (1U << PWR_ID_EXT_BUT_2))
        {
        	//HAL_Delay(200);
//        	if (button2==0)
//        	{
//        		button2=1;
        		receiveAsyncCommand (BUTTON2);
//        	}

        	//printf("PWR_ID_EXT_BUT_2 \r\n");
//            callbackParam |= PWR_INT_MASK_BTN_2;
        }
#endif

//        if(int_stat_mask & (1U << PWR_ID_EXT_RTC_ALM))
//        {
//        	printf("PWR_ID_EXT_RTC_ALM \r\n");
////          callbackParam |= PWR_INT_MASK_RTC_ALARM;
//        }
//
//        if(int_stat_mask & (1U << PWR_ID_EXT_IMU_INT1))
//        {
//        	printf("PWR_ID_EXT_IMU_INT1 \r\n");
////          callbackParam |= PWR_INT_MASK_IMU_1;
//        }
//
//        if(int_stat_mask & (1U << PWR_ID_EXT_IMU_INT2))
//        {
//        	printf("PWR_ID_EXT_IMU_INT2 \r\n");
////          callbackParam |= PWR_INT_MASK_IMU_2;
//        }
//
//        if(int_stat_mask & (1U << PWR_ID_EXT_MOTION_L_TRIGGER))
//        {
//        	printf("PWR_ID_EXT_MOTION_L_TRIGGER \r\n");
////          callbackParam |= PWR_INT_MASK_MOT_L;
//        }
//
//        if(int_stat_mask & (1U << PWR_ID_EXT_MOTION_H_TRIGGER))
//        {
//        	printf("PWR_ID_EXT_MOTION_H_TRIGGER \r\n");
////          callbackParam |= PWR_INT_MASK_MOT_H;
//        }
//
//
#ifdef TAG_PERSONA
        /* Check New Battery Event */
        if(int_stat_mask & (1U << PWR_ID_EXT_PWR_ON_NEW_BATT))
        {
//        	printf("PWR_ID_EXT_PWR_ON_NEW_BATT \r\n");
//            callbackParam |= PWR_INT_MASK_NEW_BATTERY;

            PWR_resetNewBatteryFlag();
        }
        if(int_stat_mask & (1U << PWR_ID_EXT_POW_ON_CHG_IN))
        {

        	receiveAsyncCommand (CHARGER);

//        	if (app.mode == mIDLE && PWR_isChargerOn())
//        	{
//
////        	printf("PWR_ID_EXT_POW_ON_CHG_IN \r\n");
//        	pushButton = false;
//
//        	if(app.ctrlTask.Handle)                                   	//RTOS : ctrlTask could be not started yet
//        		{
//        			osSignalSet(app.ctrlTask.Handle, app.ctrlTask.Signal);  //signal to the ctrl thread : UART data ready
//        		}
//        	stopPWM(htim1, TIM_CHANNEL_3); 	// Buzzer Off
//			stopPWM(htim3, TIM_CHANNEL_4);	// MOTOR Off
//			stopPWM(htim2, TIM_CHANNEL_2);	// FLED_RED off
//        	while(1); // faccio intervenire il watchdog per resettare il sistema
//        	vEventGroupSetBitsCallback( app.xStartTaskEvent, Ev_Tag_Task );
//        	command_start_app();
//        	}

	//            callbackParam |= PWR_INT_MASK_NEW_BATTERY;
         }
#endif
//        /* Check Low Power Inactivity */
//        if(LPM_inactivityFlagGet())
//        {
////            LPM_inactivityFlagReset();
////            callbackParam |= PWR_INT_MASK_LPI;
//        }
//
//        /* Check UAT Alarm Flag */
//        if(UAT_flagAlarmGet())
//        {
//            UAT_flagAlarmReset();
//            callbackParam |= PWR_INT_MASK_UAT;
//        }
//
//        if (manageInterruptsCallback && callbackParam != 0)
//        {
//            callbackStatus = status;
//            OS_EventSet(OS_EVENT_ID_PWR_IRQ_CHECK_CALLBACK);
//        }
    }
}

bool PWR_isBtn1Pressed(void)
{
    bool retval;

    PCAL6416A_GetPortPin(PWR_ExtPortControl[PWR_ID_EXT_BUT_1].Port, PWR_ExtPortControl[PWR_ID_EXT_BUT_1].Pin, (bool*)&retval);

    return retval;

}



//void PWR_motionFlagEnable(bool enable)
//{
//
//    if(enable)
//	{
//		PWR_motionFlagReset();
//	}
//
//    return PCAL6416A_SetPortPin(PWR_ExtPortControl[PWR_ID_EXT_MOTION_ENABLE].Port,
//        PWR_ExtPortControl[PWR_ID_EXT_MOTION_ENABLE].Pin,
//        enable);
//
//}

void PWR_motionFlagEnable(bool enable)
{

    if(enable)
	{
		//reset
	    PCAL6416A_SetPortPin(PWR_ExtPortControl[PWR_ID_EXT_MOTION_RESET].Port,
	        PWR_ExtPortControl[PWR_ID_EXT_MOTION_RESET].Pin,
	        true);

	    osDelay(100);

	    PCAL6416A_SetPortPin(PWR_ExtPortControl[PWR_ID_EXT_MOTION_RESET].Port,
	        PWR_ExtPortControl[PWR_ID_EXT_MOTION_RESET].Pin,
	        false);
	}

    PCAL6416A_SetPortPin(PWR_ExtPortControl[PWR_ID_EXT_MOTION_ENABLE].Port,
        PWR_ExtPortControl[PWR_ID_EXT_MOTION_ENABLE].Pin,
        enable);

}

void PWR_motionFlagEnableInterrupt(bool enable)
{
	/* Disable - Enable Interrupt */

    if(!enable)
    {
        PWR_ExtPortConfig.IntMask |= (uint16_t)(1U << PWR_ID_EXT_MOTION_FLAG);
    }
    else
    {
        PWR_ExtPortConfig.IntMask &= (uint16_t)~(1U << PWR_ID_EXT_MOTION_FLAG);
    }

    	PCAL6416A_SetPortPinIntMask(PWR_ExtPortControl[PWR_ID_EXT_MOTION_FLAG].Port,
        PWR_ExtPortControl[PWR_ID_EXT_MOTION_FLAG].Pin,
        (enable ? PCAL6416A_INTERR_ENABLED : PCAL6416A_INTERR_DISABLED));

}

//void PWR_motionFlagEnableInterrupt(bool enable)
//{
//	/* Disable - Enable Interrupt */
//
//    if(!enable)
//    {
//        PWR_ExtPortConfig.IntMask |= (uint16_t)(1U << PWR_ID_EXT_MOTION_FLAG);
//    }
//    else
//    {
//        PWR_ExtPortConfig.IntMask &= (uint16_t)~(1U << PWR_ID_EXT_MOTION_FLAG);
//    }
//
//    return PCAL6416A_SetPortPinIntMask(PWR_ExtPortControl[PWR_ID_EXT_MOTION_FLAG].Port,
//        PWR_ExtPortControl[PWR_ID_EXT_MOTION_FLAG].Pin,
//        (enable ? PCAL6416A_INTERR_ENABLED : PCAL6416A_INTERR_DISABLED));
//
//}


void enableMotion(bool input)
{

	PWR_motionFlagEnable(input);
	PWR_motionFlagEnableInterrupt(input);

}

bool readMotionFlag()
{
    bool retval;

    PCAL6416A_GetPortPin(PWR_ExtPortControl[PWR_ID_EXT_MOTION_FLAG].Port, PWR_ExtPortControl[PWR_ID_EXT_MOTION_FLAG].Pin, (bool*)&retval);

    return retval;
}


void disableAllInterrupts(void) {
//        	DBG_INFO_INTERRUPT("DISABLING INTERRUPTs");



	PWR_motionFlagEnable(false);

	WR_motionFlagEnableInterrupt(false);


	PWR_intEnableBtn1(false);


	PWR_intEnableBtn2(false);


	enableUAT(false);

}

