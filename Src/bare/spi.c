/**
 * @project       TagPersona
 *
 * @Component     Spi
 *
 * @file          spi.c
 *
 * @brief         Spi functions
 *
 * @author        Author 
 *
 * @date          03nov2020
 *
 ***********************************************************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "spi.h"

extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern DMA_HandleTypeDef hdma_spi1_rx;
extern DMA_HandleTypeDef hdma_spi1_tx;

/*!
 * @name HAL_SPI1_TxRx
 * @brief This function is used to communicate through the SPI 1 peripheral.
 * @param csPort Pointer to the chip select pin port.
 * @param csPin Chip select pin number.
 * @param pTxData Pointer to the transmit buffer.
 * @param pRxData Pointer to the receive buffer.
 * @param Size The size of the receive/transmit process.
 * @param Timeout The timeout value for the communication process.
 * @retval - HAL_OK if everything went well.
 *         - HAL_TIMEOUT if the communication process has exeeded the timeout value.
 *         - HAL_ERROR if there an error has occurred during the communication process or
 *                     if either Tx/Rx buffer are pointers to NULL.
 */
HAL_StatusTypeDef HAL_SPI1_TxRx( GPIO_TypeDef * csPort, uint16_t csPin, uint8_t * pTxData, uint8_t * pRxData, uint16_t Size, uint32_t Timeout )
{
   HAL_StatusTypeDef res = HAL_OK;
   uint32_t tick_start = 0;

   if( ( NULL != pTxData ) || (  NULL != pRxData) )
   {
      tick_start = HAL_GetTick();

      HAL_GPIO_WritePin(csPort, csPin, GPIO_PIN_RESET);

      while( HAL_SPI_STATE_READY != HAL_SPI_GetState(&hspi1) )
      {
         if( 100 < (HAL_GetTick() - tick_start) )
         {
            res = HAL_TIMEOUT;
            break;
         }
      }

      if( HAL_OK == res )
      {
         if( NULL != pRxData )
         {
            memset(pRxData, 0x00, Size);
         }

         if( ( NULL != pTxData ) && ( NULL != pRxData ) )
         {
             HAL_SPIEx_FlushRxFifo(&hspi1);
             res = HAL_SPI_TransmitReceive(&hspi1, pTxData, pRxData, Size, Timeout);
         }
         else if( NULL != pTxData )
         {
             res = HAL_SPI_Transmit(&hspi1, pTxData, Size, Timeout);
         }
         else if( NULL != pRxData )
         {
             memset(pRxData, 0x00, Size);
             HAL_SPIEx_FlushRxFifo(&hspi1);
             res = HAL_SPI_Receive(&hspi1, pRxData, Size, Timeout);
         }

         HAL_GPIO_WritePin(csPort, csPin, GPIO_PIN_SET);

         tick_start = HAL_GetTick();

         while( HAL_SPI_STATE_READY != HAL_SPI_GetState(&hspi1) )
         {
            if( 100 < (HAL_GetTick() - tick_start) )
            {
               break;
            }
         }
      }
   }
   else
   {
      res = HAL_ERROR;
   }

   return res;
}


/*!
 * @name HAL_SPI2_TxRx
 * @brief This function is used to communicate through the SPI 2 peripheral.
 * @param pTxData Pointer to the transmit buffer.
 * @param pRxData Pointer to the receive buffer.
 * @param Size The size of the receive/transmit process.
 * @param Timeout The timeout value for the communication process.
 * @retval - HAL_OK if everything went well.
 *         - HAL_TIMEOUT if the communication process has exeeded the timeout value.
 *         - HAL_ERROR if there an error has occurred during the communication process or
 *                     if either Tx/Rx buffer are pointers to NULL.
 */
HAL_StatusTypeDef HAL_SPI2_TxRx( uint8_t * pTxData, uint8_t * pRxData, uint16_t Size, uint32_t Timeout )
{
   HAL_StatusTypeDef res = HAL_OK;
   uint32_t tick_start = 0;

   if( ( NULL != pTxData ) || (  NULL != pRxData) )
   {
      tick_start = HAL_GetTick();

      HAL_GPIO_WritePin(IMU_CS_SPI2_GPIO_Port, IMU_CS_SPI2_Pin, GPIO_PIN_RESET);

      while( HAL_SPI_STATE_READY != HAL_SPI_GetState(&hspi1) )
      {
         if( 100 < (HAL_GetTick() - tick_start) )
         {
            res = HAL_TIMEOUT;
            break;
         }
      }

      if( HAL_OK == res )
      {
         if( NULL != pRxData )
         {
            memset(pRxData, 0x00, Size);
         }

         if( ( NULL != pTxData ) && ( NULL != pRxData ) )
         {
             HAL_SPIEx_FlushRxFifo(&hspi2);
             res = HAL_SPI_TransmitReceive(&hspi2, pTxData, pRxData, Size, Timeout);
         }
         else if( NULL != pTxData )
         {
             res = HAL_SPI_Transmit(&hspi2, pTxData, Size, Timeout);
         }
         else if( NULL != pRxData )
         {
             memset(pRxData, 0x00, Size);
             HAL_SPIEx_FlushRxFifo(&hspi2);
             res = HAL_SPI_Receive(&hspi2, pRxData, Size, Timeout);
         }

         HAL_GPIO_WritePin(IMU_CS_SPI2_GPIO_Port, IMU_CS_SPI2_Pin, GPIO_PIN_SET);

         tick_start = HAL_GetTick();

         while( HAL_SPI_STATE_READY != HAL_SPI_GetState(&hspi2) )
         {
            if( 100 < (HAL_GetTick() - tick_start) )
            {
               break;
            }
         }
      }
   }
   else
   {
      res = HAL_ERROR;
   }

   return res;
}
