/**
 * @project       uTag2UWB_Persona
 *
 * @Component     W25Q128FV
 *
 * @file          W25Q128FV.c
 *
 * @brief         This file contains the functions that are used to read and write to the W25Q128FV external flash memory.
 *
 * @author        Simone Bonforti
 *
 * @date          28oct2020
 *
 ***********************************************************************************************************************/

#include "W25Q128FV.h"

#include "spi.h"

/* Defines */

#define EFLASH_DEFAULT_TIMEOUT                   ( 500 )
#define EFLASH_ERASE_CHIP_DEFAULT_TIMEOUT        ( 200000 )
#define EFLASH_ERASE_BLOCK_DEFAULT_TIMEOUT       ( 2000 )
#define EFLASH_ERASE_SECTOR_DEFAULT_TIMEOUT      ( 400 )

#define EFLASH_CMD_FIRST                         EFLASH_CMD_WRITE_ENABLE
#define EFLASH_CMD_NUM                           EFLASH_CMD_INVALID
#define EFLASH_CMD_CHECK(cmd)                    ( ( ( cmd ) >= EFLASH_CMD_FIRST ) && ( ( cmd ) < EFLASH_CMD_NUM ) )

/* Private type definition */

typedef struct
{
    uint8_t BUSY  :1U;        /**< @brief Erase/Write In Progress */
    uint8_t WEL   :1U;        /**< @brief WrieEnable Latch */
    uint8_t BP    :3U;        /**< @brief Block Protect Bits */
    uint8_t TBP   :1U;        /**< @brief Top/Bottom Protect Bit */
    uint8_t SECP  :1U;        /**< @brief Sector Protect Bit */
    uint8_t SRP0  :1U;        /**< @brief Status Register Protect 0 */
}__attribute__((packed))EFLASH_StatusReg_t;


typedef enum
{
    EFLASH_CMD_WRITE_ENABLE = 0,
    EFLASH_CMD_VOLAT_SR_WRITE_ENABLE = 1,
    EFLASH_CMD_WRITE_DISABLE = 2,
    EFLASH_CMD_READ_STAT_REG = 3,
    EFLASH_CMD_WRITE_STAT_REG = 4,
    EFLASH_CMD_CHIP_ERASE = 5,
    EFLASH_CMD_POWER_DOWN = 6,
    EFLASH_CMD_RELEASE_POWER_DOWN = 7,
    EFLASH_CMD_MANUFACT_DEVICE_ID = 8,
    EFLASH_CMD_JEDEC_ID = 9,
    EFLASH_CMD_GLOBAL_BLOCK_LOCK = 10,
    EFLASH_CMD_GLOBAL_BLOCK_UNLOCK = 11,
    EFLASH_CMD_RESET_DEVICE = 12,
    EFLASH_CMD_READ_UNIQUE_ID = 13,
    EFLASH_CMD_PAGE_PROGRAM = 14,
    EFLASH_CMD_SECTOR_ERASE = 15,
    EFLASH_CMD_BLOCK_ERASE = 16,
    EFLASH_CMD_READ_DATA = 17,
    EFLASH_CMD_INVALID = 18
}EFLASH_CmdIndex_t;

/* Exported Variables */

extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern DMA_HandleTypeDef hdma_spi1_rx;
extern DMA_HandleTypeDef hdma_spi1_tx;

/* Private variables */

static const uint8_t EFLASH_Cmd[EFLASH_CMD_NUM] =
{
    0x06,      /* EFLASH_CMD_WRITE_ENABLE */
    0x50,      /* EFLASH_CMD_VOLAT_SR_WRITE_ENABLE */
    0x04,      /* EFLASH_CMD_WRITE_DISABLE */
    0x05,      /* EFLASH_CMD_READ_STAT_REG */
    0x01,      /* EFLASH_CMD_WRITE_STAT_REG */
    0x60,      /* EFLASH_CMD_CHIP_ERASE */
    0xB9,      /* EFLASH_CMD_POWER_DOWN */
    0xAB,      /* EFLASH_CMD_RELEASE_POWER_DOWN */
    0x90,      /* EFLASH_CMD_MANUFACT_DEVICE_ID */
    0x9F,      /* EFLASH_CMD_JEDEC_ID */
    0x7E,      /* EFLASH_CMD_GLOBAL_BLOCK_LOCK */
    0x98,      /* EFLASH_CMD_GLOBAL_BLOCK_UNLOCK */
    0x99,      /* EFLASH_CMD_RESET_DEVICE */
    0x4B,      /* EFLASH_CMD_READ_UNIQUE_ID */
    0x02,      /* EFLASH_CMD_PAGE_PROGRAM */
    0x20,      /* EFLASH_CMD_SECTOR_ERASE */
    0x52,      /* EFLASH_CMD_BLOCK_ERASE */
    0x03       /* EFLASH_CMD_READ_DATA */
};

/* Private function definitions */

static void EFLASH_PhysicalInit(void);
static boolean_t EFLASH_IsReady(uint32_t timeout_ms);
static uint8_t EFLASH_ReadStatusRegister(void);
static boolean_t EFLASH_WriteStatusRegister(uint8_t val);
static void EFLASH_WriteEnable(void);
static void EFLASH_WriteDisable(void);

/* Functions implementation */

/*!
 * @name EFLASH_PhysicalInit
 * @brief This function initialize the chip select pin for the SPI communication.
 * @param none.
 * @retval none.
 */
static void EFLASH_PhysicalInit(void)
{
   GPIO_InitTypeDef GPIO_InitStruct;

   GPIO_InitStruct.Pin = FLASH_CS_Pin;
   GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
   GPIO_InitStruct.Pull = GPIO_NOPULL;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
   HAL_GPIO_Init(FLASH_CS_GPIO_Port, &GPIO_InitStruct);
}


/*!
 * @name EFLASH_IsReady
 * @brief This function check if the flash memory is ready.
 * @param timeout_ms Time out value in milliseconds.
 * @retval - TRUE if the EFLASH memory is ready.
 *         - FALSE if the EFLASH memory is busy.
 */
boolean_t EFLASH_IsReady(uint32_t timeout_ms)
{
   EFLASH_StatusReg_t status;
   uint8_t status_byte;
   uint32_t tick_start = HAL_GetTick();

   do{
      status_byte = EFLASH_ReadStatusRegister();
      status = *( EFLASH_StatusReg_t * ) &status_byte;

      if( ( timeout_ms < ( HAL_GetTick() - tick_start ) && ( status.BUSY ) ) )
      {
         break;
      }

   }while( status.BUSY );

   return ( 0 == status.BUSY );
}


/*!
 * @name EFLASH_ReadStatusRegister
 * @brief This function read the status register of the flash memory.
 * @param none.
 * @retval The status register value.
 */
static uint8_t EFLASH_ReadStatusRegister(void)
{
   uint8_t tmp_cmd[ 2 ] = { EFLASH_Cmd[ EFLASH_CMD_READ_STAT_REG ], 0x00 };
   uint8_t tmp_res[ 2 ] = { 0x00 };

   HAL_SPI1_TxRx(FLASH_CS_GPIO_Port, FLASH_CS_Pin, tmp_cmd, tmp_res, sizeof(tmp_cmd), EFLASH_DEFAULT_TIMEOUT);

   return tmp_res[ 1 ];
}


/*!
 * @name EFLASH_WriteStatusRegister
 * @brief This function write the status register of the flash memory.
 * @param val The value to write in the status register.
 * @retval - TRUE if the value read of status register is correct.
 *         - FALSE if the value read of status register did not match.
 */
static boolean_t EFLASH_WriteStatusRegister(uint8_t val)
{
   uint8_t statusReg = 0;
   uint8_t tmp_cmd[ 2 ] = { EFLASH_Cmd[ EFLASH_CMD_WRITE_STAT_REG ], 0x00 };

   tmp_cmd[ 1 ] = val;

   HAL_SPI1_TxRx(FLASH_CS_GPIO_Port, FLASH_CS_Pin, tmp_cmd, NULL, sizeof(tmp_cmd), EFLASH_DEFAULT_TIMEOUT);

   statusReg = EFLASH_ReadStatusRegister();

   return (statusReg == val);
}


/*!
 * @name EFLASH_WriteEnable
 * @brief This function remove the write protection of the flash memory.
 * @param none.
 * @retval none.
 */
static void EFLASH_WriteEnable(void)
{
   HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, GPIO_PIN_RESET);
   HAL_SPI_Transmit(&hspi1, ( uint8_t * ) &EFLASH_Cmd[ EFLASH_CMD_WRITE_ENABLE ], sizeof(uint8_t), EFLASH_DEFAULT_TIMEOUT);
   HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, GPIO_PIN_SET);
}


/*!
 * @name EFLASH_WriteDisable
 * @brief This function enable the write protection of the flash memory.
 * @param none.
 * @retval none.
 */
static void  EFLASH_WriteDisable(void)
{
   HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, GPIO_PIN_RESET);
   HAL_SPI_Transmit(&hspi1, ( uint8_t * ) &EFLASH_Cmd[ EFLASH_CMD_WRITE_DISABLE ], sizeof(uint8_t), EFLASH_DEFAULT_TIMEOUT);
   HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, GPIO_PIN_SET);
}


/*!
 * @name EFLASH_Init
 * @brief This function initialize the flash memory communication and read the manufacturer id as a read test.
 * @param none.
 * @retval - TRUE if the communication works.
 *         - FALSE if the communication does not work (either timeout or error).
 */
boolean_t EFLASH_Init(void)
{
   HAL_StatusTypeDef res = HAL_OK;
   uint8_t tmp_cmd[] = { EFLASH_Cmd[ EFLASH_CMD_MANUFACT_DEVICE_ID ], 0x00, 0x00, 0x00, 0x00, 0x00 };
   uint8_t tmp_res[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

   EFLASH_PhysicalInit();

   res = HAL_SPI1_TxRx(FLASH_CS_GPIO_Port, FLASH_CS_Pin, tmp_cmd, tmp_res, sizeof(tmp_cmd), EFLASH_DEFAULT_TIMEOUT);

   return (res == HAL_OK);
}


/*!
 * @name EFLASH_Write
 * @brief This function is used to write data in the flash memory in a specific address.
 * @param address The address where the data needs to be written.
 * @param data Pointer to the data buffer.
 * @param data_size Size of the data buffer.
 * @retval - TRUE if the write process has finished correctly.
 *         - FALSE if the write process has finished with an error (either timeout or error).
 */
boolean_t EFLASH_Write(uint32_t address, uint8_t * data, uint32_t data_size)
{
   boolean_t retval = FALSE;

   if( ( address < ( EFLASH_SIZE - data_size ) ) && ( NULL != data ) && ( 0 != data_size ) )
   {
      EFLASH_StatusReg_t status;
      uint8_t status_byte;
      uint8_t command[] = { EFLASH_Cmd[ EFLASH_CMD_PAGE_PROGRAM ], ( ( address & 0xFF0000 ) >> 16 ), ( ( address & 0xFF00 ) ) >> 8, ( address & 0xFF ) };
      uint32_t tick_start = HAL_GetTick();

      if( EFLASH_IsReady(EFLASH_DEFAULT_TIMEOUT) )
      {
         EFLASH_WriteEnable();

         do{
            status_byte = EFLASH_ReadStatusRegister();
            status = *( EFLASH_StatusReg_t * ) &status_byte;

            if( ( EFLASH_DEFAULT_TIMEOUT < ( HAL_GetTick() - tick_start ) ) && !( status.WEL ) )
            {
              break;
            }

         }while(!status.WEL);

         if( 1 == status.WEL )
         {
            HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, GPIO_PIN_RESET);

            retval = ( HAL_SPI_Transmit(&hspi1, command, sizeof(command), EFLASH_DEFAULT_TIMEOUT) == HAL_OK) ;
            retval = ( retval && ( HAL_SPI_Transmit(&hspi1, data, data_size, EFLASH_DEFAULT_TIMEOUT) == HAL_OK ) );
            HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, GPIO_PIN_SET);
         }
         EFLASH_WriteDisable();
         retval = ( retval && EFLASH_IsReady(10) );
      }
   }

   return retval;
}


/*!
 * @name EFLASH_Read
 * @brief This function is used to read data in the flash memory from a specific address.
 * @param address The address of the data stored on the flash memory.
 * @param data Pointer a buffer used to store the data.
 * @param data_size Size of the data buffer.
 * @retval - TRUE if the read process has finished correctly.
 *         - FALSE if the read process has finished with an error (either timeout or error).
 */
boolean_t EFLASH_Read(uint32_t address, uint8_t * data, uint32_t data_size)
{
    boolean_t retval = FALSE;
    uint8_t command[] = { EFLASH_Cmd[ EFLASH_CMD_READ_DATA ], ( ( address & 0xFF0000 ) >> 16 ), ( ( address & 0xFF00 ) ) >> 8, ( address & 0xFF ) };

    if( ( address < ( EFLASH_SIZE - data_size ) ) && ( NULL != data ) && ( 0 != data_size ) )
    {
        if(EFLASH_IsReady(EFLASH_DEFAULT_TIMEOUT))
        {
            HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, GPIO_PIN_RESET);
            retval = ( HAL_SPI_Transmit(&hspi1, command, sizeof(command), EFLASH_DEFAULT_TIMEOUT) == HAL_OK );
            HAL_SPIEx_FlushRxFifo(&hspi1);
            retval = ( retval && ( HAL_SPI_Receive(&hspi1, data, data_size, EFLASH_DEFAULT_TIMEOUT) == HAL_OK ) );
            HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, GPIO_PIN_SET);
        }
    }
    return retval;
}


/*!
 * @name EFLASH_EraseSector
 * @brief This function is used to erase a sector of the flash memory.
 * @param sector_num The sector number.
 * @retval - TRUE if the erase process has finished correctly.
 *         - FALSE if the erase process has finished with an error (either timeout or error).
 */
boolean_t EFLASH_EraseSector(uint16_t sector_num)
{
   boolean_t retval = FALSE;
   EFLASH_StatusReg_t status;
   uint8_t status_byte;
   uint32_t address = ( EFLASH_SECTOR_SIZE * sector_num );
   uint8_t command[]   = { EFLASH_Cmd[ EFLASH_CMD_SECTOR_ERASE ], ( ( address & 0xFF0000 ) >> 16 ), ( ( address & 0xFF00 ) ) >> 8, ( address & 0xFF ) };

   if( sector_num < EFLASH_SECTOR_NUM )
   {
     if( EFLASH_IsReady(EFLASH_DEFAULT_TIMEOUT) )
     {
         EFLASH_WriteEnable();

         status_byte = EFLASH_ReadStatusRegister();
         status = *( EFLASH_StatusReg_t * ) &status_byte;

         if( 1 == status.WEL )
         {
             retval = ( HAL_SPI1_TxRx(FLASH_CS_GPIO_Port, FLASH_CS_Pin, command, NULL, sizeof(command), EFLASH_DEFAULT_TIMEOUT) == HAL_OK );
         }
         EFLASH_WriteDisable();
         retval = ( ( retval ) && ( EFLASH_IsReady(EFLASH_ERASE_SECTOR_DEFAULT_TIMEOUT) ) );
     }
   }
   return retval;
}


/*!
 * @name EFLASH_EraseBlock
 * @brief This function is used to erase a block of the flash memory.
 * @param block_num The block number.
 * @retval - TRUE if the erase process has finished correctly.
 *         - FALSE if the erase process has finished with an error (either timeout or error).
 */
boolean_t EFLASH_EraseBlock(uint16_t block_num)
{
   boolean_t retval = FALSE;
   EFLASH_StatusReg_t status;
   uint8_t status_byte;
   uint32_t address = ( EFLASH_BLOCK_SIZE * block_num );
   uint8_t command[]   = { EFLASH_Cmd[ EFLASH_CMD_BLOCK_ERASE ], ( ( address & 0xFF0000 ) >> 16 ), ( ( address & 0xFF00 ) ) >> 8, ( address & 0xFF ) };

   if( block_num < EFLASH_BLOCK_NUM )
   {
      if( EFLASH_IsReady(EFLASH_DEFAULT_TIMEOUT) )
      {
         EFLASH_WriteEnable();

         status_byte = EFLASH_ReadStatusRegister();
         status = *( EFLASH_StatusReg_t * ) &status_byte;

         if( 1 == status.WEL )
         {
            retval = ( HAL_SPI1_TxRx(FLASH_CS_GPIO_Port, FLASH_CS_Pin, command, NULL, sizeof(command), EFLASH_DEFAULT_TIMEOUT) == HAL_OK );
         }

         EFLASH_WriteDisable();
         retval = ( ( retval ) && ( EFLASH_IsReady(EFLASH_ERASE_BLOCK_DEFAULT_TIMEOUT) ) );
      }
   }
   return retval;
}


/*!
 * @name EFLASH_EraseChip
 * @brief This function is used to erase the entire flash memory.
 * @param none.
 * @retval - TRUE if the erase process has finished correctly.
 *         - FALSE if the erase process has finished with an error (either timeout or error).
 */
boolean_t EFLASH_EraseChip(void)
{
   boolean_t retval = FALSE;
   EFLASH_StatusReg_t status;
   uint8_t status_byte;

   if( EFLASH_IsReady(EFLASH_DEFAULT_TIMEOUT) )
   {
      EFLASH_WriteEnable();

      status_byte = EFLASH_ReadStatusRegister();
      status = *( EFLASH_StatusReg_t * ) &status_byte;

      if( 1 == status.WEL )
      {
         retval = ( HAL_SPI1_TxRx(FLASH_CS_GPIO_Port, FLASH_CS_Pin, ( uint8_t * )& EFLASH_Cmd[ EFLASH_CMD_CHIP_ERASE ], NULL, sizeof(uint8_t), EFLASH_DEFAULT_TIMEOUT) == HAL_OK );
      }

      EFLASH_WriteDisable();
      retval = ( ( retval ) && ( EFLASH_IsReady(EFLASH_ERASE_CHIP_DEFAULT_TIMEOUT) ) );
   }

   return retval;
}


/*!
 * @name EFLASH_ProgramPage
 * @brief This function is used to write an entire page in flash memory.
 * @param page_num The page number.
 * @param data Pointer to the data buffer.
 * @retval - TRUE if the write process has finished correctly.
 *         - FALSE if the write process has finished with an error (either timeout or error).
 */
boolean_t EFLASH_ProgramPage(uint16_t page_num, uint8_t * data)
{
    return ( ( EFLASH_Write((EFLASH_PAGE_SIZE * page_num), data, EFLASH_PAGE_SIZE) ) && ( NULL != data ));
}


/*!
 * @name EFLASH_ReadPage
 * @brief This function is used to read an entire page in flash memory.
 * @param page_num The page number.
 * @param data Pointer a buffer used to store the data.
 * @retval - TRUE if the read process has finished correctly.
 *         - FALSE if the read process has finished with an error (either timeout or error).
 */
boolean_t EFLASH_ReadPage(uint16_t page_num, uint8_t * data)
{
    return ( ( EFLASH_Read((EFLASH_PAGE_SIZE * page_num), data, EFLASH_PAGE_SIZE) ) && ( NULL != data ) );
}
