/*
 * @file    tag_list.c
 * @brief    functions to manage
 *
 * @author Decawave Software
 *
 * @attention Copyright 2017 (c) DecaWave Ltd, Dublin, Ireland.
 *            All rights reserved.
 *
 */


#include "port.h"
#include "tag_list.h"

#include <tag.h> //todo: this uses only WKUP_RESOLUTION_NS

// ----------------------------------------------------------------------------
// There 2 Tag's lists in the Node:
// Discovered Tags List (DList) and Known Tags List (KList)
//
// DList[MAX_DISCOVERED_TAG_LIST_SIZE] is temporary list for harvesting of tags from the air.
// KList[MAX_KNOWN_TAG_LIST_SIZE] is permanent list and a part of NVM.
//
static uint64_t        DList[MAX_DISCOVERED_TAG_LIST_SIZE];


/* array implementation of knownTagList: array is faster than linked list and it easier for "del"
 *
 * init_knownTagList
 * get_knownTagList
 * get_knownTagList_Size
 * get_tag64_from_knownTagList
 * get_tag16_from_knownTagList
 * get_free_slot_from_knownTagList
 * add_tag_to_knownTagList
 * del_tag16_from_knownTagList
 * del_tag64_from_knownTagList
 *
 * For PC<->NODE communication protocol see cmd_fn.c
 *
 * PC->NODE
 * "getKList" : Node returns the knownTagList (long string)."<SLOT16> <ADDR16> <ADDR64>"
 * "getDList" : Node returns the knownTagList (long string)."<SLOT16> <ADDR16> <ADDR64>"
 * "addtag <ADDR64> <ADDR16> <some params>"
 *
 * NODE->PC
 * JSON format strings:
 * "JSxxxx{"New Tag": <ADDR64>"}
 * "JSxxxx{"TWR": <ADDR16> ....}"
 *
 * */

/* @brief
 * @return the pointer to the first element of knownTagList
 * */
tag_addr_slot_t *get_knownTagList(void)
{
    return (app.pConfig->knownTagList);
}

/* @brief
 *         knownTagList can have gaps in the middle
 * @return the numeber of elements in the knownTagList
 * */
uint16_t get_knownTagList_size(void)
{
    uint16_t         size = 0;
    tag_addr_slot_t *klist = get_knownTagList();

    //KList can be with gaps, so need to scan it whole
    for(int i = 0; i< MAX_KNOWN_TAG_LIST_SIZE; i++)
    {
        if(klist->slot != (uint16_t)(0))
        {
            size++;
        }

        klist++;
    }

    return (size);
}

/*
 * */
void init_knownTagList(void)
{
    memset(app.pConfig->knownTagList, 0, sizeof(app.pConfig->knownTagList));
}

/* brief
 * returns the address of the known tag if it is in known_list;
 * otherwise returns NULL.
 *
 * */
tag_addr_slot_t *
get_tag64_from_knownTagList(uint64_t addr64)
{
    int i;
    int size = sizeof(app.pConfig->knownTagList)/sizeof(app.pConfig->knownTagList[0]);

    tag_addr_slot_t *klist = get_knownTagList();

    for(i = 0; i < size; i++)
    {
        if(klist[i].addr64 == addr64 && (klist[i].slot != 0))
        {
            return &klist[i];
        }
    }
    return NULL;
}

/*
 * */
tag_addr_slot_t *
get_tag16_from_knownTagList(uint16_t addr16)
{
    int i;
    int size = sizeof(app.pConfig->knownTagList)/sizeof(app.pConfig->knownTagList[0]);

    tag_addr_slot_t *klist = get_knownTagList();

    for(i = 0; i < size; i++)
    {
        if((klist[i].addr16 == addr16) && (klist[i].slot != 0))
        {
            return &klist[i];
        }
    }
    return NULL;
}

/*
 * return [1..(size+1)] for free slot
 *
 * return 0 if there no free slots found
 *
 * */
uint16_t get_free_slot_from_knownTagList(void)
{
    int16_t i;
    int size = sizeof(app.pConfig->knownTagList)/sizeof(app.pConfig->knownTagList[0]);

    tag_addr_slot_t *klist = get_knownTagList();

    for(i = 0; i < size; i++)
    {
        if(klist[i].slot == (uint16_t)(0))
        {
            return (i+1);
        }
    }
    return (0);
}


/*
 * @brief Checks and adds the tag to the list, if it's not in the list yet.
 *
 *     returns the pointer to the tag in the list;
 *     returns NULL if the list is full;
 *
 * */
tag_addr_slot_t *add_tag_to_knownTagList(uint64_t addr64, uint16 addr16)
{
    int16_t slot;
    tag_addr_slot_t *p, *klist;

    int size = sizeof(app.pConfig->knownTagList)/sizeof(app.pConfig->knownTagList[0]);

    p = get_tag64_from_knownTagList(addr64);

    if (!p)
    {
        klist = get_knownTagList();

        slot = get_free_slot_from_knownTagList();

        if (slot > 0 && slot <= size)
        {
            /* Duplicate 16-bit addresses are not allowed
             * use any next available address which is not
             * in known addresses space instead
             * */
            while(get_tag16_from_knownTagList(addr16))
            {
                addr16++;
            }

            /* add tag to KList */
            p = &klist[slot-1];     // slot=[1..MAX_KNOWN_TAG_LIST_SIZE]
            p->slot   = slot;       // klist[0].slot = 1; klist[1].slot = 2, etc.
            p->addr16 = addr16;
            p->addr64 = addr64;

            /* use default parameters for all tags : used in automatic adding of all tags : "d2k" command */
            p->multFast = app.pConfig->s.sfConfig.tag_mFast;
            p->multSlow = app.pConfig->s.sfConfig.tag_mSlow;
            p->mode     = app.pConfig->s.sfConfig.tag_Mode;
        }
    }

    return (p);
}
uint16_t get_slot_from_currentTime(uint32_t registrationTime,uint32_t startSuperFrameTime) {
  int32_t elapsedTimeFromSuperFrameStart_ticks = startSuperFrameTime-registrationTime; // downcounter

    if (elapsedTimeFromSuperFrameStart_ticks < 0)
    {
        elapsedTimeFromSuperFrameStart_ticks += 32768;
    }

  uint16_t numSlots = app.pConfig->s.sfConfig.numSlots;
  uint32_t slotPeriod_ns = app.pConfig->s.sfConfig.slotPeriod*1000000;
    
  uint16_t slotNumber = 0;
  uint32_t elapsedTimeFromSuperFrameStart_ns =(WKUP_RESOLUTION_NS*elapsedTimeFromSuperFrameStart_ticks);  
  slotNumber = ((elapsedTimeFromSuperFrameStart_ns)%slotPeriod_ns)%numSlots +1;

  return slotNumber;
}

uint32_t get_elapsedTime_from_currentTime(uint32_t currentTime,uint32_t startSuperFrameTime) {
   int    tmp;

    tmp = startSuperFrameTime - currentTime;

    if (tmp < 0)
    {
        tmp += 32768;
    }

    tmp = (int)((tmp * WKUP_RESOLUTION_NS) );//- (1e6f * slot * pTwr->pSfConfig->slotPeriod));
    //tmp /= 1e3f; // us

    return tmp;
}

static uint16_t lastAssignedSlot = 0;
static uint16_t findNextAvailableSlotNumber(tag_addr_slot_t *klist) {
    int kListSize = sizeof(app.pConfig->knownTagList)/sizeof(app.pConfig->knownTagList[0]);
    
    uint32_t oldestTagSuccessfullyCompletingTWR = 0;
    uint16_t slotNumber = 1; // default slot
    for(size_t i1 = 0; i1 < kListSize; i1++)
    {      
      if (klist[i1].addr64 == 0) {
        slotNumber = i1+1;
        break;
      }
      else if( (klist[i1].lastSuccessfullyTWRcomputingSuperFrameNumber < 
          klist[oldestTagSuccessfullyCompletingTWR].lastSuccessfullyTWRcomputingSuperFrameNumber)
      ) {
        oldestTagSuccessfullyCompletingTWR = i1;        
        slotNumber = i1+1;    
      }              
    }       
    if(slotNumber) {
      if(slotNumber == lastAssignedSlot) {
        slotNumber= (slotNumber+1)%(kListSize);                
        if (slotNumber == 0) {
          slotNumber = 20;
        }
      }
      lastAssignedSlot = slotNumber;  
    }       
    
    
    
    // todo: a slot should be assigned for at least 5 ms 

    return slotNumber;
}
//todo: remove 64bit addressing in favour of 16bit addr.
tag_addr_slot_t *add_new_tag_to_knownTagList_inCurrentSlot(uint64_t addr64, int16_t currentSlot ,uint32_t startSuperFrameTime, uint32_t currentSuperFrameNumber,\
uint8_t currentPollrNum)
{
    int16_t slot;
    tag_addr_slot_t *p, *klist;
    uint16_t addr16 = (uint16_t)addr64;
    
    int size = sizeof(app.pConfig->knownTagList)/sizeof(app.pConfig->knownTagList[0]);

    p = get_tag64_from_knownTagList(addr64);

    if (!p)
    {
        klist = get_knownTagList();
        
        //todo: if there a tag already  registered and active in this slot, change slot. Pay attention to tags in same slot
        //slot = GetAvailableTagSlot();
        //slot = findNextAvailableSlotNumber(klist);
        slot = currentSlot;
        // slot = get_slot_from_currentTime(registrationTime,startSuperFrameTime);
        // //printf("addr: %d\r\n",(int)addr64);
        // //slot = (int16_t) addr64;
        // //printf("slot: %d, addr: %d\r\n", slot, addr64);
        if (slot > 0 && slot <= size)
        {           
            /* add tag to KList */
            p = &klist[slot-1];     // slot=[1..MAX_KNOWN_TAG_LIST_SIZE]
            p->slot   = slot;       // klist[0].slot = 1; klist[1].slot = 2, etc.
            p->addr16 = addr16;
            p->addr64 = addr64;

            /* use default parameters for all tags : used in automatic adding of all tags : "d2k" command */
            p->multFast = app.pConfig->s.sfConfig.tag_mFast;
            p->multSlow = app.pConfig->s.sfConfig.tag_mSlow;
            p->mode     = app.pConfig->s.sfConfig.tag_Mode;
            //p->lastReceptionTime = registrationTime;
            p->nodeSfNumber_of_lastIdentifiedPollResponded = 0;
            p->nodeSfNumber_of_lastAnonymousPollResponded = currentSuperFrameNumber;
            p->superFrameNumberOnRegistration = currentSuperFrameNumber;
            
            p->lastReceived_PollRNum_InFinal = 0;            
            p->nodeSFnumber_of_lastReceivedFinal = 0;
            p->currentPollrNum = currentPollrNum;
            p->lastSuccessfullyTWRcomputingSuperFrameNumber = 0;  
        
            p->lastReceivedPollType = ANONYMOUSPOLL;
            p->req = 0;
        }
    }

    return (p);
}

tag_addr_slot_t *add_new_tag_to_knownTagList(uint64_t addr64, uint32_t registrationTime)
{
    int16_t slot;
    tag_addr_slot_t *p, *klist;
    uint16 addr16 = 1;
    
    int size = sizeof(app.pConfig->knownTagList)/sizeof(app.pConfig->knownTagList[0]);

    p = get_tag64_from_knownTagList(addr64);

    if (!p)
    {
        klist = get_knownTagList();

        slot = get_free_slot_from_knownTagList();

        if (slot > 0 && slot <= size)
        {
            /* Duplicate 16-bit addresses are not allowed
             * use any next available address which is not
             * in known addresses space instead
             * */
            while(get_tag16_from_knownTagList(addr16))
            {
                addr16++;
            }

            /* add tag to KList */
            p = &klist[slot-1];     // slot=[1..MAX_KNOWN_TAG_LIST_SIZE]
            p->slot   = slot;       // klist[0].slot = 1; klist[1].slot = 2, etc.
            p->addr16 = addr16;
            p->addr64 = addr64;

            /* use default parameters for all tags : used in automatic adding of all tags : "d2k" command */
            p->multFast = app.pConfig->s.sfConfig.tag_mFast;
            p->multSlow = app.pConfig->s.sfConfig.tag_mSlow;
            p->mode     = app.pConfig->s.sfConfig.tag_Mode;
            p->lastReceptionTime = registrationTime;
        }
    }

    return (p);
}

/*
 * */
void del_tag16_from_knownTagList(uint16_t addr16)
{
    tag_addr_slot_t *p;

    p = get_tag16_from_knownTagList(addr16);

    if (p)
    {
        memset(p, 0, sizeof(tag_addr_slot_t));
    }
}


/*
 * */
void del_tag64_from_knownTagList(uint64_t addr64)
{
    tag_addr_slot_t * p;

    p = get_tag64_from_knownTagList(addr64);

    if (p)
    {
        memset(p, 0, sizeof(tag_addr_slot_t));
    }
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
//for harvesting Tags offline : we have limited space : use DList
//
uint16_t getDList_size(void)
{
    uint16_t    len = 0;
    for(int i = 0; i < sizeof(DList)/sizeof(DList[0]); i++)
    {
        if(!DList[i])
        {
            break;
        }
        len++;
    }
    return (len);
}


uint64_t * getDList(void)
{
    return (DList);
}

uint64_t * get_tag64_from_DList(uint64_t addr64)
{
    int i;
    int size = sizeof(DList)/sizeof(DList[0]);

    //uint64_t *Dlist = getDList();

    for(i = 0; i < size; i++)
    {
        if(DList[i] == addr64)
        {
            return DList[i];
        }
    }
    return NULL;
}
/* @brief
 * return 1 : tag successfully added to discovered tag list / no space left in discovered tag list
 * return 0 : already in the list
 * */
int addTagToDList(uint64_t addr64)
{
    int i, ret = 1;

    for(i = 0; i < sizeof(DList)/sizeof(DList[0]); i++)
    {
        if(DList[i] == addr64)
        {
            ret = 0;
            break;
        }

        if(!DList[i])
        {
            DList[i] = addr64;
            ret = 1;
            break;
        }
    }
    return(ret);
}

void initDList(void)
{
    memset(DList, 0, sizeof(DList));
}

void deleteDList(void)
{
     //uint64_t *Dlist = getDList();
    //  for(int i = 0; i < sizeof(DList)/sizeof(DList[0]); i++)
    // {
    //     printf("dlist %d \r\n", (uint16_t)DList[i]);
    // }

    memset(DList, 0, sizeof(DList));

    // for(int j = 0; j < sizeof(DList)/sizeof(DList[0]); j++)
    // {
    //     printf("dlist1 %d \r\n", (uint16_t)DList[j]);
    // }
}

