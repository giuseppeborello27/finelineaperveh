/**
  ******************************************************************************
  * @file    UART/UART_HyperTerminal_TxPolling_RxIT/Src/main.c
  * @author  MCD Application Team
  * @brief   This sample code shows how to use UART HAL and LL APIs to transmit
  *          data in polling mode while receiving data in Interrupt mode, by mixing 
  *          use of LL and HAL APIs;
  *          HAL driver is used to perform UART configuration, 
  *          then TX transfer procedure is based on HAL APIs use (polling)
  *               RX transfer procedure is based on LL APIs use (IT)
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "uart_command.h"
#include "main.h"
#include "test.h"

/* Buffer used for transmission */
uint8_t aTxStartMessage[] = "U|START\r\n";
uint8_t ubSizeToSend = sizeof(aTxStartMessage);

/* Buffer used for reception */
uint8_t aRXBufferA[RX_BUFFER_SIZE];
uint8_t aRXBufferB[RX_BUFFER_SIZE];
__IO uint32_t uwNbReceivedChars = 0;
__IO uint32_t uwBufferReadyIndication = 0;
__IO uint32_t inizioComando = 0;
__IO uint32_t lunghezzaComando = 0;
uint8_t *pBufferReadyForUser;
uint8_t *pBufferReadyForReception;



//Configure UART peripheral for reception process (using LL) ##########*/
void openUartCommand()
{
	pBufferReadyForReception = aRXBufferA;
	pBufferReadyForUser      = aRXBufferB;
	uwNbReceivedChars = 0;
	uwBufferReadyIndication = 0;

	/* Enable RXNE and Error interrupts */
	//LL_UART_EnableIT_RXNE(huart->Instance);
	//LL_UART_EnableIT_ERROR(huart->Instance);

	LL_USART_EnableIT_RXNE(UARTx);
	LL_USART_EnableIT_ERROR(UARTx);
}


void testUartCommand(UART_HandleTypeDef* huart)
{
	if(HAL_UART_Transmit(huart, (uint8_t*)aTxStartMessage, ubSizeToSend, 1000)!= HAL_OK)
	{
	  /* Transfer error in transmission process */
	  Error_Handler();
	}
}


// se nuovo comando (ritorna lunghezza comando)
uint8_t newCommand (uint8_t* pBufferReady)
{
	if (uwBufferReadyIndication)
	{
		/* Reset indication */
		uwBufferReadyIndication = 0;
		for (int i=0;i<lunghezzaComando-1;i++)
		{
			pBufferReady[i]=pBufferReadyForUser[i];
		}
		pBufferReady[lunghezzaComando-1]='\0';

		//Toggle LLED_BLUE
		HAL_GPIO_TogglePin(GPIOC, LLED_BLUE_Pin);

		return lunghezzaComando-1;
	}
	else
		return 0;
}

// ritorna cmdID
uint8_t parserCommand(uint8_t* pBufferReady, uint8_t length)
{

	if (memcmp(pBufferReady, "UWB", strlen("UWB")-1)==0)
				return UWB;

	if (memcmp(pBufferReady, "FLASH", strlen("FLASH")-1)==0)
			return FLASH_M;

	if (memcmp(pBufferReady, "ACC", strlen("ACC")-1)==0)
			return ACC;

	if (memcmp(pBufferReady, "BLE", strlen("BLE")-1)==0)
					return BLE;

	if (memcmp(pBufferReady, "SLEEP", strlen("SLEEP")-1)==0)
					return SLEEP;

	if (memcmp(pBufferReady, "SWITCHOFF", strlen("SWITCHOFF")-1)==0)
						return SWITCHOFF;

	if (memcmp(pBufferReady, "MOTION", strlen("MOTION")-1)==0)
							return MOTION;

	if (memcmp(pBufferReady, "SERIAL", strlen("SERIAL")-1)==0)
				return SERIAL;

#ifdef TAG_PERSONA
	if (memcmp(pBufferReady, "LATLED", strlen("LATLED")-1)==0)
		return LATLED;

	if (memcmp(pBufferReady, "MAINLED", strlen("LATLED")-1)==0)
		return MAINLED;

	if (memcmp(pBufferReady, "BUZZER", strlen("BUZZER")-1)==0)
		return BUZZER;

	if (memcmp(pBufferReady, "HAPTIC", strlen("HAPTIC")-1)==0)
		return HAPTIC;
#endif

#ifdef TAG_CARRELLO

	if (memcmp(pBufferReady, "CAN", strlen("CAN")-1)==0)
				return CAN_BUS;

	if (memcmp(pBufferReady, "RELE", strlen("RELE")-1)==0)
			return RELE;
#endif

	return NOCOMD;

}

/**
  * @brief  Rx Transfer completed callback
  * @note   This example shows a simple way to report end of IT Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
//ritorna la DIM del comando
void UART_CharReception_Callback(void)
{
	uint8_t *ptemp;
	//char prec,succ;

  /* Read Received character. RXNE flag is cleared by reading of RDR register */
  pBufferReadyForReception[uwNbReceivedChars] = LL_USART_ReceiveData8(UARTx);

  //
  if (inizioComando==0)
  {
	  if (pBufferReadyForReception[0]=='U')
	  {
	  	  uwNbReceivedChars++;

	  	  if (uwNbReceivedChars>1)
	  		  if (pBufferReadyForReception[1]=='|')
	  		  {
	  			  inizioComando=1;
	  			  uwNbReceivedChars=0;
	  		  }
	   }
  }

  //inizio comando corretto (inizio_comando=1)
  else
  {
	  if (pBufferReadyForReception[uwNbReceivedChars]=='\n')
	  {
		  if (pBufferReadyForReception[uwNbReceivedChars-1]=='\r')
		  {
			  /* Set Buffer swap indication */
			  uwBufferReadyIndication = 1;

			  /* Swap buffers for next bytes to be received */
			 ptemp = pBufferReadyForUser;
			 pBufferReadyForUser = pBufferReadyForReception;
			 pBufferReadyForReception = ptemp;
			 lunghezzaComando=uwNbReceivedChars;
			 uwNbReceivedChars = 0;
			 inizioComando=0;
			 return;
		  }
	  }
	  uwNbReceivedChars++;

	  /* Checks if Buffer full indication has to be set */
	   if (uwNbReceivedChars >= RX_BUFFER_SIZE )
		   Error_Handler();
  }

}

/**
  * @brief  UART error callbacks
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
void UART_Error_Callback(void)
{
  __IO uint32_t isr_reg;

  /* Disable UARTx_IRQn */
  NVIC_DisableIRQ(UARTx_IRQn);
  
  /* Error handling example :
    - Read USART ISR register to identify flag that leads to IT raising
    - Perform corresponding error handling treatment according to flag
  */
  isr_reg = LL_USART_ReadReg(UARTx, ISR);
  if (isr_reg & LL_USART_ISR_NE)
  {
    /* Turn LED R on: Transfer error in reception/transmission process */
	 HAL_GPIO_WritePin(GPIOC, LLED_RED_Pin, GPIO_PIN_RESET);
  }
  else
  {
    /* Turn LED R on: Transfer error in reception/transmission process */
	 HAL_GPIO_WritePin(GPIOC, LLED_RED_Pin, GPIO_PIN_RESET);
  }

  // noise detection flag
  //ISR_NE: writing 1 to the NECF bit in the USART_ICR register
  if (isr_reg & LL_USART_ISR_NE)
  {
	  SET_BIT(UARTx->ICR, USART_ICR_NECF);
  }

  //Error Frame
  //ISR_FE: To clear write 1 to the FECF bit in the USART_ICR register
  if (isr_reg & LL_USART_ISR_FE)
  {
	  SET_BIT(UARTx->ICR, USART_ICR_FECF);
  }

  //ISR_CMF: To clear write 1 to the CMCF bit in the USART_ICR register.
  if (isr_reg & LL_USART_ISR_CMF)
  {
  	  SET_BIT(UARTx->ICR, USART_ICR_CMCF);
  }


  //ISR_EOBF: To clear write 1 to the EOBCF bit in the USART_ICR register.
  if (isr_reg & LL_USART_ISR_EOBF)
  {
 	  //LL_USART_WriteReg(UARTx, ICR, )
 	   SET_BIT(UARTx->ICR, USART_ICR_EOBCF);
  }

  NVIC_EnableIRQ(UARTx_IRQn);

//  if (HAL_UART_DeInit(&huart5) != HAL_OK)
//  {
//     Error_Handler();
//  }
//
//  if (HAL_UART_Init(&huart5) != HAL_OK)
//  {
//     Error_Handler();
//  }

}


/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
