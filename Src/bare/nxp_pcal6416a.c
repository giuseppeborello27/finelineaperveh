/** \file nxp_pcal6416a.c
*	\brief nxp_pcal6416a driver - Source file
*	\author Lorenzo Gandolfi
*	\date June 03, 2019
*	\copyright Copyright (c) 2019 e-Novia S.p.a. - All Rights Reserved.
* 
* E-NOVIA S.P.A. CONFIDENTIAL
* __________________
* 
* ALL INFORMATION CONTAINED HEREIN WAS PRODUCED BY E-NOVIA S.P.A. AND NO THIRD PARTY MAY CLAIM ANY RIGHT OR PATERNITY ON IT.\n 
* THE INTELLECTUAL AND TECHNICAL CONCEPTS CONTAINED HEREIN ARE AND REMAIN
* THE PROPERTY OF E-NOVIA S.P.A. AND ARE PROTECTED BY TRADE SECRET OR COPYRIGHT LAW.
* REPRODUCTION, DISTRIBUTION OR DISCLOSURE OF THIS MATERIAL TO ANY OTHER PARTY
* IS STRICTLY FORBIDDEN UNLESS PRIOR WRITTEN PERMISSION IS OBTAINED FROM E-NOVIA S.P.A.
*/ 

#include <string.h>
#include "i2c.h"
#include "port.h"
#include "nxp_pcal6416a.h"

#ifdef DBG_PCAL6416A
#include "debug_log.h"
#endif


/**	\defgroup nxp_pcal6416a_PRIVATE PRIVATE
*	\ingroup nxp_pcal6416a_MODULE
* 	\brief nxp pcal6416a Private Definitions and Functions.
*	\{
*/

#define PCAL6416A_RESET_NO

#define PCAL6416A_READ_WRITE_REG_MASK	0x01U
#define PCAL6416A_WRITE_ADDR			((PCAL6416A_CFG_DEV_ADDR) << 1U)
#define PCAL6416A_READ_ADDR				(((PCAL6416A_CFG_DEV_ADDR) << 1U) | (PCAL6416A_READ_WRITE_REG_MASK))

//#define RESET_PIN_DELAY					2 //millisecondi da attendere per imporre un reset all'extender
#define RESET_PIN_DELAY					    100 //millisecondi da attendere per imporre un reset all'extender

static const PCAL6416A_CFG_InOut_t PCAL6416A_CFG_InOut = 
{
    PCAL6416A_CFG_INOUT_INT,
	PCAL6416A_CFG_INOUT_RST
};

#ifdef PCAL6416A_USE_SET_PP_PENDING
typedef struct
{
    uint8_t     Port;
    uint8_t     Pin;
    bool   Status;
}PCAL6416A_PortPinPending_t;


static PCAL6416A_PortPinPending_t   PCAL6416A_PortPinPending[PCAL6416A_PORT_PIN_PENDING_MAX_NUM] = 
{
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
    {0xFFU,0xFFU,FALSE},
};
#endif /* PCAL6416A_USE_SET_PP_PENDING*/


/*static*/ PCAL6416A_REG_Regs_t PCAL6416A_REG_Regs = { 0 };


static PCAL6416A_InputCallback_t PCAL6416A_InputCallback = NULL;

static bool    PCAL6416A_PhyInit(void);

static bool    PCAL6416A_PhyInitInt(uint32_t mode);

static bool    PCAL6416A_ReadReg(PCAL6416A_REG_Addr_t addr, PCAL6416A_REG_Regs_t *regs);

static bool    PCAL6416A_WriteReg(PCAL6416A_REG_Addr_t addr, PCAL6416A_REG_Regs_t *regs);

/** 
*   \brief function to obtain a reference to the correct register in the struct that hold the register's PCAL6416A values
*   \param[in] addr a pointer to a valid address contained in a PCAL6416A_REG_Regs_t struct
*   \param[in] reg_ref a pointer to a pointer to the struct where find the values of registers of PCAL6416A
*   \param[out] reg_ptr a pointer to a pointer to uint8_t where set found register's address
*/
static void PCAL6416A_SetRegisterPointer(PCAL6416A_REG_Addr_t *addr, PCAL6416A_REG_Regs_t **reg_ref, uint8_t **reg_ptr);

#ifdef DBG_PCAL6416A
static void         PCAL6416A_ShowRegisters(void);
#else
#define             PCAL6416A_ShowRegisters()
#endif


/** \} */


/**	\addtogroup nxp_pcal6416a_PUBLIC
*	\{
*/


bool PCAL6416A_Init(PCAL6416A_InputCallback_t cb)
{
	bool retval = PCAL6416A_PhyInit();
	
#ifdef PCAL6416A_RESET    
	/*Reset of PCAL6416A*/
	PCAL6416A_Reset();
#endif    
	
    /*Read all regs*/
//    memset((void*)&PCAL6416A_REG_Regs, 0, sizeof(PCAL6416A_REG_Regs_t));
//    retval = retval && PCAL6416A_UpdateRegs();
    
#if 0    
    PCAL6416A_ShowRegisters();
#endif
    
//    if(NULL!=cb)
//	{
//		PCAL6416A_InputCallback = cb;
//	}
//
//    OS_EventCreate(PCAL6416A_CFG_INT_EVENT_ID, PCAL6416A_IntHandler);
//
//    return retval;
}

/** 
*   \brief function to reset the configuration of PCAL6416A to defauult values
*/
void PCAL6416A_Reset(void)
{
	HAL_GPIO_WritePin(PCAL6416A_CFG_InOut.Rst.Port,PCAL6416A_CFG_InOut.Rst.Pin,GPIO_PIN_RESET);

	//OS_Sleep(RESET_PIN_DELAY);
	HAL_Delay(RESET_PIN_DELAY);

	HAL_GPIO_WritePin(PCAL6416A_CFG_InOut.Rst.Port,PCAL6416A_CFG_InOut.Rst.Pin,GPIO_PIN_SET);

	//OS_Sleep(RESET_PIN_DELAY);
	HAL_Delay(RESET_PIN_DELAY);

	HAL_GPIO_WritePin(PCAL6416A_CFG_InOut.Rst.Port,PCAL6416A_CFG_InOut.Rst.Pin,GPIO_PIN_RESET);
}


PCAL6416A_REG_Regs_t* PCAL6416A_GetReadParamsStruct(void)
{
    return &PCAL6416A_REG_Regs;
}



uint32_t PCAL6416A_GetRstPinNo(void)
{
    return PCAL6416A_CFG_InOut.Rst.Pin;
}

uint32_t PCAL6416A_GetIntPinNo(void)
{
    return PCAL6416A_CFG_InOut.Int.Pin;
}

bool PCAL6416A_SetIntAsIrq(void)
{
    return PCAL6416A_PhyInitInt(PCAL6416A_CFG_INT_MODE_IRQ);
}

bool PCAL6416A_SetIntAsEvent(void)
{
    return PCAL6416A_PhyInitInt(PCAL6416A_CFG_INT_MODE_EVT);
}

bool PCAL6416A_GetPortPin(uint8_t port, uint8_t pin, bool *val)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U) && val)
    {
        if(PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_INPUT_ADDR_0: PCAL6416A_REG_INPUT_ADDR_1), &PCAL6416A_REG_Regs))
        {            
            *val = (PCAL6416A_REG_Regs.Input.AllPorts & ((uint16_t)1U << (pin + (8U * port)))) != 0U;
            
            retval = true;
        }
    }

    return retval;
}

bool PCAL6416A_SetPortPin(uint8_t port, uint8_t pin, bool val)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U))
    {
        if(PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_OUTPUT_ADDR_0: PCAL6416A_REG_OUTPUT_ADDR_1), &PCAL6416A_REG_Regs))
        {
            if(!(PCAL6416A_REG_Regs.Config.AllPorts & ((uint16_t)1U << (pin + (8U * port)))))
            {
                PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_OUTPUT_ADDR_0: PCAL6416A_REG_OUTPUT_ADDR_1), &PCAL6416A_REG_Regs);
                
                if(val)
                {
                    PCAL6416A_REG_Regs.Output.AllPorts |= ((uint16_t)1U << (pin + (8U * port)));
                }
                else
                {
                    PCAL6416A_REG_Regs.Output.AllPorts &= (uint16_t)~((uint16_t)1U << (pin + (8U * port)));
                }
                
                
                
                retval = PCAL6416A_WriteReg(((!port) ? PCAL6416A_REG_OUTPUT_ADDR_0: PCAL6416A_REG_OUTPUT_ADDR_1), &PCAL6416A_REG_Regs);
            }
        }
    }
    
    return retval;
}

bool PCAL6416A_GetPortPinPol(uint8_t port, uint8_t pin, bool *val)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U) && val)
    {
        if(PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_POL_ADDR_0: PCAL6416A_REG_POL_ADDR_1), &PCAL6416A_REG_Regs))
        {            
            *val = (PCAL6416A_REG_Regs.Polarity.AllPorts & ((uint16_t)1U << (pin + (8U * port)))) != 0U;
            
            retval = true;
        }
    }

    return retval;
}

bool PCAL6416A_SetPortPinPol(uint8_t port, uint8_t pin, PCAL6416A_Pol_t polarity)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U))
    {
        polarity &= 0x01U;
        PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_POL_ADDR_0: PCAL6416A_REG_POL_ADDR_1), &PCAL6416A_REG_Regs);
        if(polarity)
        {
            PCAL6416A_REG_Regs.Polarity.AllPorts |= ((uint16_t)1U << (pin + (8U * port)));
        }
        else
        {
            PCAL6416A_REG_Regs.Polarity.AllPorts &= (uint16_t)~((uint16_t)1U << (pin + (8U * port)));
        }
        
        retval = PCAL6416A_WriteReg(((!port) ? PCAL6416A_REG_POL_ADDR_0: PCAL6416A_REG_POL_ADDR_1), &PCAL6416A_REG_Regs);
    }
    
    return retval;
}

bool PCAL6416A_SetPortPinDir(uint8_t port, uint8_t pin, PCAL6416A_Dir_t dir)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U))
    {
        dir &= 0x01U;
        PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_CFG_ADDR_0: PCAL6416A_REG_CFG_ADDR_1), &PCAL6416A_REG_Regs);
        if(dir)
        {
            PCAL6416A_REG_Regs.Config.AllPorts |= ((uint16_t)1U << (pin + (8U * port)));
        }
        else
        {
            PCAL6416A_REG_Regs.Config.AllPorts &= (uint16_t)~((uint16_t)1U << (pin + (8U * port)));
        }
        
        retval = PCAL6416A_WriteReg(((!port) ? PCAL6416A_REG_CFG_ADDR_0: PCAL6416A_REG_CFG_ADDR_1), &PCAL6416A_REG_Regs);
    }
    
    return retval;
}

bool PCAL6416A_GetPortsDirConfig(uint16_t *dir_mask)
{
	bool retval = false;
    
    if(dir_mask)
    {
        retval = PCAL6416A_ReadReg(PCAL6416A_REG_CFG_ADDR_0, &PCAL6416A_REG_Regs);
        retval = retval && PCAL6416A_ReadReg(PCAL6416A_REG_CFG_ADDR_1, &PCAL6416A_REG_Regs);
        
        *dir_mask = PCAL6416A_REG_Regs.Config.AllPorts;
    }
    
    return retval;
}

bool PCAL6416A_GetPortsPolConfig(uint16_t *pol_mask)
{
	bool retval = false;
    
    if(pol_mask)
    {
        retval = PCAL6416A_ReadReg(PCAL6416A_REG_POL_ADDR_0, &PCAL6416A_REG_Regs);
        retval = retval && PCAL6416A_ReadReg(PCAL6416A_REG_POL_ADDR_1, &PCAL6416A_REG_Regs);
        
        *pol_mask = PCAL6416A_REG_Regs.Polarity.AllPorts;
    }
    
    return retval;
}


bool PCAL6416A_GetPortsIntConfig(uint16_t *int_mask)
{
	bool retval = false;
    
    if(int_mask)
    {
        retval = PCAL6416A_ReadReg(PCAL6416A_REG_INTERRUPT_MASK_0, &PCAL6416A_REG_Regs);
        retval = retval && PCAL6416A_ReadReg(PCAL6416A_REG_INTERRUPT_MASK_1, &PCAL6416A_REG_Regs);
        
        *int_mask = PCAL6416A_REG_Regs.Interrupt_MASK.AllPorts;
    }
    
    return retval;
}


bool PCAL6416A_SetPortPinDrvStr(uint8_t port, uint8_t pin, PCAL6416A_DrvStr_t drvStr)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U))
    {
        drvStr &= 0x03U;
		
		uint32_t bit_0 = drvStr & 0x01;
		uint32_t bit_1 = drvStr & 0x02;
        
        if(pin<4)
		{
            PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_OUT_DRV_STR_0L: PCAL6416A_REG_OUT_DRV_STR_1L), &PCAL6416A_REG_Regs);
		}
		else
		{
            PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_OUT_DRV_STR_0H: PCAL6416A_REG_OUT_DRV_STR_1H), &PCAL6416A_REG_Regs);
		}
		
		if(bit_0)
			PCAL6416A_REG_Regs.OutDrvStr.AllPorts |= (bit_0 << (2U * pin + (16U * port)));
		else
			PCAL6416A_REG_Regs.OutDrvStr.AllPorts &= (uint32_t)~(bit_0 << (2U * pin + (16U * port)));
		
		if(bit_1)
			PCAL6416A_REG_Regs.OutDrvStr.AllPorts |= (bit_1 << (2U * pin + (16U * port)));
		else
			PCAL6416A_REG_Regs.OutDrvStr.AllPorts &= (uint32_t)~(bit_1 << (2U * pin + (16U * port)));
		
		if(pin<4)
		{
			retval = PCAL6416A_WriteReg(((!port) ? PCAL6416A_REG_OUT_DRV_STR_0L: PCAL6416A_REG_OUT_DRV_STR_1L), &PCAL6416A_REG_Regs);
		}
		else
		{
			retval = PCAL6416A_WriteReg(((!port) ? PCAL6416A_REG_OUT_DRV_STR_0H: PCAL6416A_REG_OUT_DRV_STR_1H), &PCAL6416A_REG_Regs);
		}
    }
    
    return retval;
}

bool PCAL6416A_SetPortPinLatchReg(uint8_t port, uint8_t pin, PCAL6416A_Latch_t latchSet)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U))
    {
        latchSet &= 0x01U;
        PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_IN_LATCH_0: PCAL6416A_REG_IN_LATCH_1), &PCAL6416A_REG_Regs);
        if(latchSet)
        {
            PCAL6416A_REG_Regs.InLatchReg.AllPorts |= ((uint16_t)1U << (pin + (8U * port)));
        }
        else
        {
            PCAL6416A_REG_Regs.InLatchReg.AllPorts &= (uint16_t)~((uint16_t)1U << (pin + (8U * port)));
        }
        
        retval = PCAL6416A_WriteReg(((!port) ? PCAL6416A_REG_IN_LATCH_0: PCAL6416A_REG_IN_LATCH_1), &PCAL6416A_REG_Regs);
    }
    
    return retval;
}

bool PCAL6416A_SetPortPinPullEn(uint8_t port, uint8_t pin, PCAL6416A_PullEn_t pullEnable)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U))
    {
        pullEnable &= 0x01U;
        PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_PULL_EN_0: PCAL6416A_REG_PULL_EN_1), &PCAL6416A_REG_Regs);
        if(pullEnable)
        {
            PCAL6416A_REG_Regs.PullUpDwn_EN.AllPorts |= ((uint16_t)1U << (pin + (8U * port)));
        }
        else
        {
            PCAL6416A_REG_Regs.PullUpDwn_EN.AllPorts &= (uint16_t)~((uint16_t)1U << (pin + (8U * port)));
        }
        
        retval = PCAL6416A_WriteReg(((!port) ? PCAL6416A_REG_PULL_EN_0: PCAL6416A_REG_PULL_EN_1), &PCAL6416A_REG_Regs);
    }
    
    return retval;
}

bool PCAL6416A_SetPortPinPullSel(uint8_t port, uint8_t pin, PCAL6416A_PullSel_t pull)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U))
    {
        pull &= 0x01U;
        PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_PULL_SEL_0: PCAL6416A_REG_PULL_SEL_1), &PCAL6416A_REG_Regs);
        if(pull)
        {
            PCAL6416A_REG_Regs.PullUpDwn_SEL.AllPorts |= ((uint16_t)1U << (pin + (8U * port)));
        }
        else
        {
            PCAL6416A_REG_Regs.PullUpDwn_SEL.AllPorts &= (uint16_t)~((uint16_t)1U << (pin + (8U * port)));
        }
        
        retval = PCAL6416A_WriteReg(((!port) ? PCAL6416A_REG_PULL_SEL_0: PCAL6416A_REG_PULL_SEL_1), &PCAL6416A_REG_Regs);
    }
    
    return retval;
}

bool PCAL6416A_GetPortPinIntMask(uint8_t port, uint8_t pin, bool *val)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U) && val)
    {
        if(PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_INTERRUPT_MASK_0: PCAL6416A_REG_INTERRUPT_MASK_1), &PCAL6416A_REG_Regs))
        {            
            *val = (PCAL6416A_REG_Regs.Interrupt_MASK.AllPorts & ((uint16_t)1U << (pin + (8U * port)))) != 0U;
            
            retval = true;
        }
    }

    return retval;
}

bool PCAL6416A_SetPortPinIntMask(uint8_t port, uint8_t pin, PCAL6416A_InterrMask_t mask)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U))
    {		
        mask &= 0x01U;
        PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_INTERRUPT_MASK_0: PCAL6416A_REG_INTERRUPT_MASK_1), &PCAL6416A_REG_Regs);
        if(mask)
        {
            PCAL6416A_REG_Regs.Interrupt_MASK.AllPorts |= ((uint16_t)1U << (pin + (8U * port)));
        }
        else
        {
            PCAL6416A_REG_Regs.Interrupt_MASK.AllPorts &= (uint16_t)~((uint16_t)1U << (pin + (8U * port)));
        }
        
        retval = PCAL6416A_WriteReg(((!port) ? PCAL6416A_REG_INTERRUPT_MASK_0: PCAL6416A_REG_INTERRUPT_MASK_1), &PCAL6416A_REG_Regs);
    }

#ifdef DBG_PCAL6416A
//	PCAL6416A_UpdateRegs();
//	PCAL6416A_ShowRegisters();
#endif			
    
    return retval;
}

bool PCAL6416A_GetPortPinIntStat(uint8_t port, uint8_t pin, bool *val)
{
	bool retval = false;
    
    if((port < 2U) && (pin < 8U) && val)
    {
        if(PCAL6416A_ReadReg(((!port) ? PCAL6416A_REG_INTERRUPT_STAT_0: PCAL6416A_REG_INTERRUPT_STAT_1), &PCAL6416A_REG_Regs))
        {            
            *val = (PCAL6416A_REG_Regs.Interrupt_STATUS.AllPorts & ((uint16_t)1U << (pin + (8U * port)))) != 0U;
			
            retval = true;
        }
    }
	
#ifdef DBG_PCAL6416A
//	PCAL6416A_UpdateRegs();
//	PCAL6416A_ShowRegisters();
#endif			

    return retval;
}

bool PCAL6416A_SetPortPinOutPortCfg(uint8_t port, PCAL6416A_OutPortConfig_t cfg)
{
	bool retval = false;
    
    if(port < 2U)
    {
        cfg &= 0x01U;
        PCAL6416A_ReadReg(PCAL6416A_REG_OUT_PORT_CFG, &PCAL6416A_REG_Regs);
        if(cfg)
        {
            PCAL6416A_REG_Regs.OutPortCfg.Port.B01 |= ((uint16_t)1U << port);
        }
        else
        {
            PCAL6416A_REG_Regs.OutPortCfg.Port.B01 &= (uint16_t)~((uint16_t)1U << port);
        }
 
        retval = PCAL6416A_WriteReg(PCAL6416A_REG_OUT_PORT_CFG, &PCAL6416A_REG_Regs);
    }
    
    return retval;
}

#ifdef PCAL6416A_USE_SET_PP_PENDING
void PCAL6416A_SetPortPinPending(uint8_t port, uint8_t pin, bool val)
{
    if((port < 2U) && (pin < 8U))
    {
        PCAL6416A_PortPinPending[pin + (port * 8U)].Port = port;
        PCAL6416A_PortPinPending[pin + (port * 8U)].Pin = pin;
        PCAL6416A_PortPinPending[pin + (port * 8U)].Status = val;
    }
}
#endif /*PCAL6416A_USE_SET_PP_PENDING*/


//void PCAL6416A_CheckInt(void)
//{
//    PCAL6416A_IntHandler();
//}


PCAL6416A_REG_Regs_t* PCAL6416A_UpdateRegs(void)
{
    PCAL6416A_REG_Addr_t reg_addr;
    
	for(reg_addr = PCAL6416A_REG_INTERRUPT_STAT_0; (reg_addr <= PCAL6416A_REG_OUT_PORT_CFG); reg_addr++)
    {
        PCAL6416A_ReadReg(reg_addr, &PCAL6416A_REG_Regs);
    }
	
	
    for(reg_addr = PCAL6416A_REG_INPUT_ADDR_0; (reg_addr <= PCAL6416A_REG_CFG_ADDR_1); reg_addr++)
    {
        PCAL6416A_ReadReg(reg_addr, &PCAL6416A_REG_Regs);
    }
	for(reg_addr = PCAL6416A_REG_OUT_DRV_STR_0L; (reg_addr < PCAL6416A_REG_INTERRUPT_STAT_0); reg_addr++)
//	for(reg_addr = PCAL6416A_REG_OUT_DRV_STR_0L; (reg_addr <= PCAL6416A_REG_OUT_PORT_CFG); reg_addr++)
    {
        PCAL6416A_ReadReg(reg_addr, &PCAL6416A_REG_Regs);
    }
    
    //PCAL6416A_ShowRegisters();
    
    return &PCAL6416A_REG_Regs;
}

void PCAL6416A_DebugShowRegisters(void)
{
    PCAL6416A_ShowRegisters();
}

/** \} */


/** \addtogroup nxp_pcal6416a_PRIVATE
*	\{
*/


#ifdef DBG_PCAL6416A
void PCAL6416A_DefaultCallback(uint8_t input_no, bool input_val)
{
	DBG_INFO("INTERRUPT - pin:%i val:%i", input_no, input_val);	
}
#endif

static bool PCAL6416A_PhyInit(void)
{
	bool retval = false;
	GPIO_InitTypeDef GPIO_InitStruct_Rst;
    
    I2C_Init(PCAL6416A_CFG_I2C_ID);
    
    retval = PCAL6416A_SetIntAsIrq();
    
    
	HAL_GPIO_DeInit(PCAL6416A_CFG_InOut.Rst.Port, PCAL6416A_CFG_InOut.Rst.Pin);
	
	//reset pin configuration
	HAL_GPIO_WritePin(PCAL6416A_CFG_InOut.Rst.Port,PCAL6416A_CFG_InOut.Rst.Pin,GPIO_PIN_RESET);//this way the pin will be set immediately thanks from the value in the register
	GPIO_InitStruct_Rst.Pin =  PCAL6416A_CFG_InOut.Rst.Pin;
    GPIO_InitStruct_Rst.Mode = 	GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct_Rst.Pull = 	GPIO_NOPULL;
    GPIO_InitStruct_Rst.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(PCAL6416A_CFG_InOut.Rst.Port, &GPIO_InitStruct_Rst);
    
    return retval;
}

static bool PCAL6416A_PhyInitInt(uint32_t mode)
{
    GPIO_InitTypeDef GPIO_InitStruct_Int;
	HAL_GPIO_DeInit(PCAL6416A_CFG_InOut.Int.Port, PCAL6416A_CFG_InOut.Int.Pin);
	GPIO_InitStruct_Int.Pin =  PCAL6416A_CFG_InOut.Int.Pin;
    GPIO_InitStruct_Int.Mode = 	mode;
    GPIO_InitStruct_Int.Pull = 	GPIO_NOPULL;
    GPIO_InitStruct_Int.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(PCAL6416A_CFG_InOut.Int.Port, &GPIO_InitStruct_Int);
	
    if(mode == PCAL6416A_CFG_INT_MODE_IRQ)
    {
        HAL_NVIC_SetPriority(PCAL6416A_CFG_INT_IRQN_TYPE, PCAL6416A_CFG_INT_IRQ_PRI, 0);
        HAL_NVIC_EnableIRQ(PCAL6416A_CFG_INT_IRQN_TYPE);
    }
    else if(mode == PCAL6416A_CFG_INT_MODE_EVT)
    {
        HAL_NVIC_DisableIRQ(PCAL6416A_CFG_INT_IRQN_TYPE);
    }
    
    return true;
}

#ifdef TAG2_UWB
#include "deca_device_api.h"
#endif /*TAG2_UWB*/

static bool  PCAL6416A_WriteReg(PCAL6416A_REG_Addr_t addr, PCAL6416A_REG_Regs_t *reg_ref)
{
	bool retval = false;
    
    if(reg_ref)
    {
		uint8_t *reg_ptr;
        
		PCAL6416A_SetRegisterPointer(&addr,&reg_ref,&reg_ptr);        
		
        if(reg_ptr != NULL)
        {   
#ifdef TAG2_UWB     
         decaIrqStatus_t  stat ;
         stat = decamutexon();
#endif  /*TAG2_UWB*/              
            uint8_t buff[2U] = {addr, *reg_ptr};
            
            retval = I2C_Write(PCAL6416A_CFG_I2C_ID, PCAL6416A_WRITE_ADDR, buff, sizeof(buff));
#ifdef TAG2_UWB  
           decamutexoff(stat);
#endif  /*TAG2_UWB*/            
        }
    }
    
#ifdef DBG_PCAL6416A
//	PCAL6416A_UpdateRegs();
//	PCAL6416A_ShowRegisters();
#endif	
    return retval;
}



static bool  PCAL6416A_ReadReg(PCAL6416A_REG_Addr_t addr, PCAL6416A_REG_Regs_t *reg_ref)
{
	bool retval = false;
    
    if(reg_ref)
    {
		uint8_t *reg_ptr;
		PCAL6416A_SetRegisterPointer(&addr,&reg_ref,&reg_ptr);		
        
        if(reg_ptr != NULL)
        {
#ifdef TAG2_UWB     
         decaIrqStatus_t  stat ;
         stat = decamutexon();
#endif  /*TAG2_UWB*/            
			retval = I2C_Write(PCAL6416A_CFG_I2C_ID, PCAL6416A_WRITE_ADDR, (uint8_t*) &addr, sizeof(uint8_t)) &&
				I2C_Read(PCAL6416A_CFG_I2C_ID, PCAL6416A_READ_ADDR, (uint8_t*) reg_ptr, sizeof(uint8_t));
#ifdef TAG2_UWB  
           decamutexoff(stat);
#endif  /*TAG2_UWB*/
        }
    }
    
    return retval;
}

static void PCAL6416A_SetRegisterPointer(PCAL6416A_REG_Addr_t *addr, PCAL6416A_REG_Regs_t **reg_ref, uint8_t **reg_ptr)
{
	switch((uint8_t)(*addr))
	{
		case PCAL6416A_REG_INPUT_ADDR_0:
			*reg_ptr = &((*reg_ref)->Input.Ports.P0.Port);
			break;
		case PCAL6416A_REG_INPUT_ADDR_1:
			*reg_ptr = &((*reg_ref)->Input.Ports.P1.Port);
			break;
		case PCAL6416A_REG_OUTPUT_ADDR_0:
			*reg_ptr = &((*reg_ref)->Output.Ports.P0.Port);
			break;
		case PCAL6416A_REG_OUTPUT_ADDR_1:
			*reg_ptr = &((*reg_ref)->Output.Ports.P1.Port);
			break;
		case PCAL6416A_REG_POL_ADDR_0:
			*reg_ptr = &((*reg_ref)->Polarity.Ports.P0.Port);
			break;
		case PCAL6416A_REG_POL_ADDR_1:
			*reg_ptr = &((*reg_ref)->Polarity.Ports.P1.Port);
			break;
		case PCAL6416A_REG_CFG_ADDR_0:
			*reg_ptr = &((*reg_ref)->Config.Ports.P0.Port);
			break;
		case PCAL6416A_REG_CFG_ADDR_1:
			*reg_ptr = &((*reg_ref)->Config.Ports.P1.Port);
			break;
		case PCAL6416A_REG_OUT_DRV_STR_0L:
			*reg_ptr = &((*reg_ref)->OutDrvStr.Ports.P0_L.Port);
			break;
		case PCAL6416A_REG_OUT_DRV_STR_0H:
			*reg_ptr = &((*reg_ref)->OutDrvStr.Ports.P0_H.Port);
			break;
		case PCAL6416A_REG_OUT_DRV_STR_1L:
			*reg_ptr = &((*reg_ref)->OutDrvStr.Ports.P1_L.Port);
			break;
		case PCAL6416A_REG_OUT_DRV_STR_1H:
			*reg_ptr = &((*reg_ref)->OutDrvStr.Ports.P1_H.Port);
			break;
		case PCAL6416A_REG_IN_LATCH_0:
			*reg_ptr = &((*reg_ref)->InLatchReg.Ports.P0.Port);
			break;
		case PCAL6416A_REG_IN_LATCH_1:
			*reg_ptr = &((*reg_ref)->InLatchReg.Ports.P1.Port);
			break;
		case PCAL6416A_REG_PULL_EN_0:
			*reg_ptr = &((*reg_ref)->PullUpDwn_EN.Ports.P0.Port);
			break;
		case PCAL6416A_REG_PULL_EN_1:
			*reg_ptr = &((*reg_ref)->PullUpDwn_EN.Ports.P1.Port);	
			break;
		case PCAL6416A_REG_PULL_SEL_0:
			*reg_ptr = &((*reg_ref)->PullUpDwn_SEL.Ports.P0.Port);
			break;
		case PCAL6416A_REG_PULL_SEL_1:
			*reg_ptr = &((*reg_ref)->PullUpDwn_SEL.Ports.P1.Port);
			break;
		case PCAL6416A_REG_INTERRUPT_MASK_0:
			*reg_ptr = &((*reg_ref)->Interrupt_MASK.Ports.P0.Port);
			break;
		case PCAL6416A_REG_INTERRUPT_MASK_1:
			*reg_ptr = &((*reg_ref)->Interrupt_MASK.Ports.P1.Port);
			break;
		case PCAL6416A_REG_INTERRUPT_STAT_0:
			*reg_ptr = &((*reg_ref)->Interrupt_STATUS.Ports.P0.Port);
			break;
		case PCAL6416A_REG_INTERRUPT_STAT_1:
			*reg_ptr = &((*reg_ref)->Interrupt_STATUS.Ports.P1.Port);
			break;
		case PCAL6416A_REG_OUT_PORT_CFG:
		{
			static uint8_t b01;
			b01 = (*reg_ref)->OutPortCfg.Port.B01;
			*reg_ptr = &(b01);
		}
			break;
		default:
			break;
	}
}


uint16_t     PCAL6416A_GetIntStatMask(void)
{
    uint16_t retval = 0U;
    PCAL6416A_REG_Regs_t regs;  
    
    if(PCAL6416A_ReadReg(PCAL6416A_REG_INTERRUPT_STAT_0, &regs) && PCAL6416A_ReadReg(PCAL6416A_REG_INTERRUPT_STAT_1, &regs))
    {
        retval = regs.Interrupt_STATUS.AllPorts;
    }
    
    return retval;
}

uint16_t     PCAL6416A_GetInputMask(void)
{
    uint16_t retval = 0U;
    PCAL6416A_REG_Regs_t regs;  
    
    if(PCAL6416A_ReadReg(PCAL6416A_REG_INPUT_ADDR_0, &regs) && PCAL6416A_ReadReg(PCAL6416A_REG_INPUT_ADDR_1, &regs))
    { 
        retval = regs.Input.AllPorts;
    }
    
    return retval;    
}

extern uint8_t bufferResp[50];
extern UART_HandleTypeDef huart5;
void         PCAL6416A_IntHandler(void)
{	
    PCAL6416A_REG_Regs_t regs;   

	if(PCAL6416A_ReadReg(PCAL6416A_REG_INTERRUPT_STAT_0, &regs) && PCAL6416A_ReadReg(PCAL6416A_REG_INTERRUPT_STAT_1, &regs) && 
        PCAL6416A_ReadReg(PCAL6416A_REG_INPUT_ADDR_0, &regs) && PCAL6416A_ReadReg(PCAL6416A_REG_INPUT_ADDR_1, &regs))
    {

        uint16_t int_stat_mask = regs.Interrupt_STATUS.AllPorts;
        uint16_t input_stat_mask = regs.Input.AllPorts;

#if 0        
        for(i = 0U; i < 16U; i++)
        {
            if(changed_mask & (1U << i))
            {
                /*callback*/
                if(PCAL6416A_InputCallback)
                {
                    PCAL6416A_InputCallback(i, (new_val_mask & (1U << i)) != 0U);
                }
            }
        }
#else
//        if(PCAL6416A_InputCallback)
//        {
        	PWR_interruptCheck(PWR_STATUS_ON/*status*/, int_stat_mask, input_stat_mask);
//            PCAL6416A_InputCallback(int_stat_mask, input_stat_mask);
//        }
#endif        
        PCAL6416A_REG_Regs.Input.AllPorts = regs.Input.AllPorts;
        
//        INTCTL_ExitCritical();
    }
	//MYMOD
	else
	{
		 //PCAL6416A_Reset();
	}
    
#ifdef PCAL6416A_USE_SET_PP_PENDING    
    {
        uint8_t i = 0U;
        for(i = 0U; i < PCAL6416A_PORT_PIN_PENDING_MAX_NUM; i++)
        {
            if((PCAL6416A_PortPinPending[i].Port != 0xFFU) && (PCAL6416A_PortPinPending[i].Pin != 0xFFU))
            {
                PCAL6416A_SetPortPin(PCAL6416A_PortPinPending[i].Port, PCAL6416A_PortPinPending[i].Pin, PCAL6416A_PortPinPending[i].Status);
                PCAL6416A_PortPinPending[i].Port = 0xFFU;
                PCAL6416A_PortPinPending[i].Pin = 0xFFU;
            }
        }
    }
#endif /*PCAL6416A_USE_SET_PP_PENDING*/    
}


#ifdef DBG_PCAL6416A
static void         PCAL6416A_ShowRegisters(void)
{
    DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_INPUT_ADDR_0, PCAL6416A_REG_Regs.Input.Ports.P0.Port);
    DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_INPUT_ADDR_1, PCAL6416A_REG_Regs.Input.Ports.P1.Port);
    DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_OUTPUT_ADDR_0, PCAL6416A_REG_Regs.Output.Ports.P0.Port);
    DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_OUTPUT_ADDR_1, PCAL6416A_REG_Regs.Output.Ports.P1.Port);
    DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_POL_ADDR_0, PCAL6416A_REG_Regs.Polarity.Ports.P0.Port);
    DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_POL_ADDR_1, PCAL6416A_REG_Regs.Polarity.Ports.P1.Port);
    DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_CFG_ADDR_0, PCAL6416A_REG_Regs.Config.Ports.P0.Port);
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_CFG_ADDR_1, PCAL6416A_REG_Regs.Config.Ports.P1.Port);
	
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_OUT_DRV_STR_0L, PCAL6416A_REG_Regs.OutDrvStr.Ports.P0_L.Port);
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_OUT_DRV_STR_0H, PCAL6416A_REG_Regs.OutDrvStr.Ports.P0_H.Port);
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_OUT_DRV_STR_1L, PCAL6416A_REG_Regs.OutDrvStr.Ports.P0_L.Port);
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_OUT_DRV_STR_1H, PCAL6416A_REG_Regs.OutDrvStr.Ports.P1_H.Port);
	
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_IN_LATCH_0, PCAL6416A_REG_Regs.InLatchReg.Ports.P0.Port);
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_IN_LATCH_1, PCAL6416A_REG_Regs.InLatchReg.Ports.P1.Port);
	
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_PULL_EN_0, PCAL6416A_REG_Regs.PullUpDwn_EN.Ports.P0.Port);
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_PULL_EN_1, PCAL6416A_REG_Regs.PullUpDwn_EN.Ports.P1.Port);
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_PULL_SEL_0, PCAL6416A_REG_Regs.PullUpDwn_SEL.Ports.P0.Port);
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_PULL_SEL_1, PCAL6416A_REG_Regs.PullUpDwn_SEL.Ports.P1.Port);
	
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_INTERRUPT_MASK_0, PCAL6416A_REG_Regs.Interrupt_MASK.Ports.P0.Port);
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_INTERRUPT_MASK_1, PCAL6416A_REG_Regs.Interrupt_MASK.Ports.P1.Port);
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_INTERRUPT_STAT_0, PCAL6416A_REG_Regs.Interrupt_STATUS.Ports.P0.Port);
	DBG_INFO("Reg address: 0x%02X val = 0x%02X", PCAL6416A_REG_INTERRUPT_STAT_1, PCAL6416A_REG_Regs.Interrupt_STATUS.Ports.P1.Port);
	
	DBG_INFO("Reg address: 0x%02X val = 0x%02X\r\n", PCAL6416A_REG_OUT_PORT_CFG, PCAL6416A_REG_Regs.OutPortCfg.Port);
}
#endif


/**	\} */


/* EOF */

