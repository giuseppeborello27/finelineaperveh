/*
 * usart.c
 *
 *  Created on: Oct 29, 2020
 *      Author: CCagninelli
 */

#include <stdint.h>
#include <string.h>
#include "main.h"
#include "usart.h"
#include "cmsis_os.h"
#include "fwRecord.h"

uint8_t dummy = 1;

typedef struct
{
    uint8_t     Data[UART4_RX_BUFFER_SIZE];
    uint16_t    Pos;
    uint8_t     LineReady;
}UART4_RxBuffer_t;

UART4_RxBuffer_t    UART4_RxBuffer =
{
    {0U},
    0U,
    0U
};

typedef struct
{
    uint8_t     Data[UART5_RX_BUFFER_SIZE];
    uint16_t    Pos;
    uint8_t     LineReady;
}UART5_RxBuffer_t;

UART5_RxBuffer_t    UART5_RxBuffer =
{
    {0U},
    0U,
    0U
};

uint8_t UART4_RxData;
uint8_t UART5_RxData;

volatile uint8_t UsartDataUpdate[100];

static volatile UART_RxMode_t UART_RxMode  = UART_MODE_NORMAL;


void HAL_UART_Send(uint8_t *buffer, uint16_t size)
{
	HAL_UART_Transmit(&huart5, buffer, size, 500);
}

#include "sched.h"

#ifndef USE_OLD_BLE_IF
#include "BGLIB_Module.h"
#endif

extern uint8_t bglib_rx_byte;

extern osSemaphoreId OS_EventID_BGLIB_Handler;
extern osSemaphoreId OS_EventID_BGLIB_FWUP_Handler;

volatile uint16_t counter = 0;
volatile uint16_t lineCounterCol = 0;


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    if(huart == &huart4)
    {
		bglib_rx_byte = UART4_RxData;

		if( OS_EventID_BGLIB_Handler )
      {
         osSemaphoreRelease(OS_EventID_BGLIB_Handler);
      }
    }
    else if(huart == &huart5)
    {
       if(0x3A == UsartDataUpdate[counter])
       {
          lineCounterCol++;
          if( ( OS_EventID_BGLIB_FWUP_Handler ) && ( counter > 1 ) )
          {
             UsartDataUpdate[counter] = 0;
             UsartDataUpdate[counter - 1] = '\n';
             osSemaphoreRelease(OS_EventID_BGLIB_FWUP_Handler);
             UsartDataUpdate[0] = ':';
             counter = 1;
          }
       }
       else
       {
          counter = (counter + 1) % 100;
       }

       HAL_UART_Receive_IT(&huart5, &UsartDataUpdate[counter], 1);



//        if(UART_RxMode == UART_MODE_NORMAL)
//        {
//        	//##CC Controllare su FreeRTOS   if(SCHED_GetMode() == SCHED_MODE_CMD)
//        	/* Integrare con macchina a stati nostra */
//        	if(1)
//            {
//
//                    if(UART5_RxBuffer.Pos + 2U < UART5_RX_BUFFER_SIZE)
//                    {
//                        UART5_RxBuffer.LineReady = FALSE;
//
//                        //##CC Controllare su FreeRTOS OS_EventClear(OS_EVENT_ID_CMD_RX_READY);
//
//
//                        if((UART5_RxData == '\n') || (UART5_RxData == 0) || (UART5_RxData == '\r'))
//                        {
//                            if(UART5_RxBuffer.Pos)
//                            {
//                                //UART5_RxBuffer.Data[UART5_RxBuffer.Pos++] = UART5_RxData;
//                                UART5_RxBuffer.Data[UART5_RxBuffer.Pos++] = 0U;
//                                UART5_RxBuffer.LineReady = TRUE;
//
//                                //##CC Controllare su FreeRTOS OS_EventSet(OS_EVENT_ID_CMD_RX_READY);
//
//                            }
//                        }
//                        else
//                        {
//                            UART5_RxBuffer.Data[UART5_RxBuffer.Pos++] = UART5_RxData;
//                            if(UART5_RxBuffer.Pos + 2U >= UART5_RX_BUFFER_SIZE)
//                            {
//                                UART5_RxBuffer.Data[UART5_RxBuffer.Pos++] = 0U;
//                                UART5_RxBuffer.LineReady = TRUE;
//                                //##CC Controllare su FreeRTOS  OS_EventSet(OS_EVENT_ID_CMD_RX_READY);
//
//                            }
//                        }
//                    }
////                )
//            }
//            else
//            {
//                if(UART5_RxData == 0x03U)
//                {
//                	//##CC Controllare su FreeRTOS   SCHED_SetMode(SCHED_MODE_CMD);
//                }
//            }
//        }
//        else if(UART_RxMode == UART_MODE_LOAD_FW)
//		{
//			UART5_RxBuffer.Data[UART5_RxBuffer.Pos++] = UART5_RxData;
//
//			if((UART5_RxData == 0) || (UART5_RxData == '\r'))
//			{
//                UART5_RxBuffer.LineReady = TRUE;
//                //##CC Controllare su FreeRTOS     OS_EventSet(OS_EVENT_ID_FW_LOAD);
//                /* Semaforo task in sostituzione degli EVENTSET */
//
//
////				if(FWREC_SetRecordBlock((char*)UART5_RxBuffer.Data, UART5_RxBuffer.Pos))
////				{
////					if(strstr((const char*)UART5_RxBuffer.Data, ":00000001FF"))
////					{
////						UART_RxMode = UART_MODE_NORMAL;
////					}
////				}
////				else
////				{
////					UART_RxMode = UART_MODE_NORMAL;
////				}
////
////				HAL_UART_ResetBuffer(huart);
//			}
//		}


            HAL_UART_Receive_IT(huart, &UART5_RxData, sizeof(uint8_t));

    }
}


uint16_t HAL_UART_GetRxBufferAndSize(UART_HandleTypeDef *huart, uint8_t data[])
{
    uint16_t buff_size = 0U;
//    OS_SYNCHRONIZE(   /*GDM72*/
        if((huart == &huart4) && data && UART4_RxBuffer.LineReady)
        {
           HAL_NVIC_DisableIRQ(UART4_IRQn);

            UART4_RxBuffer.Data[UART4_RxBuffer.Pos++] = 0U;
            memcpy(data, UART4_RxBuffer.Data, UART4_RxBuffer.Pos);
            buff_size = UART4_RxBuffer.Pos;
            UART4_RxBuffer.Pos = 0U;
            UART4_RxBuffer.LineReady = 0U;

            HAL_NVIC_EnableIRQ(UART4_IRQn);
        }
        else if((huart == &huart5) && data && UART5_RxBuffer.LineReady)
        {
        	//##CC Controllare su FreeRTOS  OS_SYNCHRONIZE(HAL_NVIC_DisableIRQ(UART5_IRQn);)    /*GDM72*/
            if(UART_RxMode == UART_MODE_NORMAL)
            {
                UART5_RxBuffer.Data[UART5_RxBuffer.Pos++] = 0U;
            }
            memcpy(data, UART5_RxBuffer.Data, UART5_RxBuffer.Pos);
            buff_size = UART5_RxBuffer.Pos;
            UART5_RxBuffer.Pos = 0U;
            UART5_RxBuffer.LineReady = 0U;
            //##CC Controllare su FreeRTOS OS_SYNCHRONIZE(HAL_NVIC_EnableIRQ(UART5_IRQn);)     /*GDM72*/
        }
//    )
    return buff_size;
}

void HAL_UART_ResetBuffer(UART_HandleTypeDef *huart)
{
//    OS_SYNCHRONIZE(   /*GDM72*/
        if(huart == &huart4)
        {
            HAL_NVIC_DisableIRQ(UART4_IRQn);

            UART4_RxBuffer.Data[UART4_RxBuffer.Pos] = 0U;
            UART4_RxBuffer.Pos = 0U;
            UART4_RxBuffer.LineReady = 0U;

            HAL_NVIC_EnableIRQ(UART4_IRQn);
        }
        else if(huart == &huart5)
        {
        	//##CC Controllare su FreeRTOS   OS_SYNCHRONIZE(HAL_NVIC_DisableIRQ(UART5_IRQn);)    /*GDM72*/
            UART5_RxBuffer.Data[UART5_RxBuffer.Pos] = 0U;
            UART5_RxBuffer.Pos = 0U;
            UART5_RxBuffer.LineReady = 0U;
            //##CC Controllare su FreeRTOS   OS_SYNCHRONIZE(HAL_NVIC_EnableIRQ(UART5_IRQn);)     /*GDM72*/
        }
//    )
}




void UART_SetRxMode(UART_HandleTypeDef *huart, UART_RxMode_t mode)
{
    if((huart == &huart5) && (mode < UART_MODE_INVALID))
    {
        UART_RxMode = mode;
		if(UART_RxMode == UART_MODE_LOAD_FW)
		{
			HAL_UART_ResetBuffer(huart);
		}
    }
}

UART_RxMode_t UART_GetRxMode(UART_HandleTypeDef *huart)
{
    UART_RxMode_t retval = UART_MODE_INVALID;
    if(huart == &huart5)
    {
        retval = UART_RxMode;
    }
    return retval;
}

boolean_t HAL_UART_IsLineReady(UART_HandleTypeDef *huart)
{
    boolean_t retval = FALSE;
    if(huart == &huart4)
    {
       retval = UART4_RxBuffer.LineReady;
    }
    else if(huart == &huart5)
    {
    	//##CC Controllare su FreeRTOS OS_SYNCHRONIZE(
    	//##CC Controllare su FreeRTOS    retval = UART5_RxBuffer.LineReady;
    	//##CC Controllare su FreeRTOS)
    }
    return retval;
}

void HAL_UART_InitRxBuffer(UART_HandleTypeDef *huart)
{
    if(huart == &huart4)
    {
        memset(&UART4_RxBuffer, 0U, sizeof(UART4_RxBuffer));
        HAL_UART_Receive_IT(huart, &UART4_RxData, sizeof(uint8_t));
    }
    else if(huart == &huart5)
    {
        memset(&UART5_RxBuffer, 0U, sizeof(UART5_RxBuffer));
        HAL_UART_Receive_IT(huart, &UART5_RxData, sizeof(uint8_t));
    }
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
 if(huart == &huart4)
    {
        HAL_UART_Receive_IT(huart, &UART4_RxData, sizeof(uint8_t));
    }
    else if(huart == &huart5)
    {

        HAL_UART_Receive_IT(huart, &UART5_RxData, sizeof(uint8_t));
    }
}
