
/** \file st_lsm6ds3.c
 ** \brief Logical driver for ST-LSM6DS3 and ST-LSM6DS3LTR inertial measurement module - source file.
 ** \authors Giuseppe Di Martino and Luca Bortolotti
 ** \date   07-January-2017
 ** \copyright Copyright (c) 2016 E-Novia Srl. All Rights Reserved.
 */

#include <stddef.h>
#include <string.h>
//#include "debug_log.h"
//#include "os_target.h"
#include "spi.h"
//#include "sys.h"
#include "st_lsm6dsxxx_cfg.h"
#include "st_lsm6dsxxx_reg.h"
#include "st_lsm6dsxxx.h"

#define IMM_ST_LDM6Dxxx_ID           0x6AU

#define IMM_READ_WRITE_REG_MASK     0x80U
#define IMM_REG_MASK                0x7FU
#define IMM_WRITE_REG(x)            (x)
#define IMM_READ_REG(x)             ((x) | IMM_READ_WRITE_REG_MASK)  

#define IMM_REG_BIT_SET          1U
#define IMM_REG_BIT_RESET        0U

#define IMM_VALIDATE(a1, a2, g1, g2)    (((a1) < IMM_ACC_ODR_IVALID) &&\
                                        ((a2) < IMM_ACC_RANGE_INVALID) &&\
                                        ((g1) < IMM_GYRO_ODR_IVALID) &&\
                                        ((g2) < IMM_GYRO_RANGE_INVALID))

//#define IMM_WAIT(ms)                   	OS_Sleep(ms);

#define IMM_DEVICE_ACC_ACTIVE_MASK       		(1U << 0U)
#define IMM_DEVICE_GYRO_ACTIVE_MASK         	(1U << 1U)

#define IMM_DEVICE_ACC_SET_ACTIVE(x, on)    ((x) = ((on) ? ((uint8_t)(x) | IMM_DEVICE_ACC_ACTIVE_MASK) : ((uint8_t)(x) & (uint8_t)~IMM_DEVICE_ACC_ACTIVE_MASK)))
#define IMM_DEVICE_GYRO_SET_ACTIVE(x, on)   ((x) = ((on) ? ((uint8_t)(x) | IMM_DEVICE_GYRO_ACTIVE_MASK) : ((uint8_t)(x) & (uint8_t)~IMM_DEVICE_GYRO_ACTIVE_MASK)))

#define IMM_DEVICE_ACC_IS_ACTIVE(x)         (((uint8_t)(x) & IMM_DEVICE_ACC_ACTIVE_MASK)
#define IMM_DEVICE_GYRO_IS_ACTIVE(x)        (((uint8_t)(x) & IMM_DEVICE_GYRO_ACTIVE_MASK)

#define IMM_TEMPERATURE_OUTPUT_OFFSET           25  
#define IMM_COMPOSITE_FILTER_TYPE_SEL_MASK      0xF0U

#define IMM_AUTO_DATA_BUFF_LEN					15U


static const IMM_CTRL3_C_Reg_t  IMM_CTRL3_C_RegCfg = 
{
    IMM_REG_BIT_SET,            /*Software reset on*/
    IMM_REG_BIT_RESET,          /*data LSB @ lower address*/
    IMM_REG_BIT_SET,            /*Register address automatically incremented during a multiple byte access*/
    IMM_REG_BIT_RESET,          /*SPI Serial Interface Mode selected*/
    IMM_REG_BIT_RESET,          /*push-pull mode selected*/
    IMM_REG_BIT_RESET,          /*interrupt output pads active high*/ 
    IMM_REG_BIT_RESET,          /*continuous update;*/
    IMM_REG_BIT_RESET           /*normal mode (no reboot triggered)*/
};

static uint8_t  IMM_DeviceActiveMask = 0U;
static uint8_t  IMM_OutDataBuff[IMM_AUTO_DATA_BUFF_LEN] = {0U};

static void     IMM_CheckDataReady(IMM_STATUS_REG_Reg_t *status);
static void     IMM_PhysicalInit(void);
static uint8_t  IMM_ReadRegister(IMM_RegisterAddres_t reg_addr);
static void     IMM_WriteRegister(IMM_RegisterAddres_t reg_addr, uint8_t data);
static void     IMM_SetBitRegister(IMM_RegisterAddres_t reg_addr, uint8_t bit_no);
static void     IMM_ClearBitRegister(IMM_RegisterAddres_t reg_addr, uint8_t bit_no);

static const uint32_t   IMM_AccSensitivity[IMM_ACC_RANGE_NUM] = 
{
    61U,
    488U,
    122U,
    244U
};

static const uint32_t   IMM_GyroSensitivity[IMM_GYRO_RANGE_NUM] = 
{
    8750U,
    1750U,
    35000U,
    70000U,
    4375U
};

uint8_t IMM_GetID(void)
{
	return IMM_ReadRegister(IMM_REG_WHO_AM_I);
}

boolean_t IMM_Init(IMM_AccOutputDataRateSel_t acc_odr, 
            IMM_AccRangeSel_t acc_range,
            IMM_Mode_t acc_mode,
            IMM_GyroOutputDataRateSel_t gyro_odr,
            IMM_GyroRangeSel_t gyro_range,
            IMM_Mode_t gyro_mode)
{
    boolean_t result = FALSE;
    
    IMM_PhysicalInit();
    
//    __HAL_I2C_GET_FLAG(I2C_GetHandle(I2C_ID_1), I2C_FLAG_BUSY);
    result = IMM_Reset();
    
    if(result && (IMM_GetID() == (uint8_t)IMM_ST_LDM6Dxxx_ID))
    {
		uint32_t tick_start;
		
        //DBG_INFO("[IMM] ST-LSM6DS3 device found");
        volatile uint8_t byte_reg = 0U;
        volatile IMM_STATUS_REG_Reg_t status_reg;
        
        IMM_WriteRegister(IMM_REG_CTRL3_C, *(uint8_t*)&IMM_CTRL3_C_RegCfg);
                
        result = IMM_Reset();
        
        if(result)
        {
            tick_start = HAL_GetTick();
            do
            {
                //IMM_WAIT(10);
            	//osDelay (10);
                HAL_Delay(10);
                
                byte_reg = IMM_ReadRegister(IMM_REG_STATUS);
                status_reg = *(IMM_STATUS_REG_Reg_t*)&byte_reg;
            }while(status_reg.EV_BOOT && (HAL_GetTick() - tick_start) < 500U);
                        
            result = IMM_SetAccMode(acc_mode);
            result = result && IMM_SetAccOutputDataRate(acc_odr);
            result = result && IMM_SetAccFullScale(acc_range);
            result = result && IMM_SetGyroMode(gyro_mode);
            result = result && IMM_SetGyroOutputDataRate(gyro_odr);
            result = result && IMM_SetGyroFullScale(gyro_range);
        }
    }
    
#ifdef IMM_CFG_IRQ_ENABLE    
    OS_EventCreate(OS_EVENT_ID_IMM_IRQ, NULL);
#endif    
	
	if(result)
    {
        /* TEST READ REG */
        volatile uint8_t byte_reg = IMM_ReadRegister(IMM_REG_X_OFFS_USR);
        volatile IMM_OFS_USR_Reg_t ofsx, ofsy, ofsz;
        
        ofsx = *(IMM_OFS_USR_Reg_t*)&byte_reg;
        
        byte_reg = IMM_ReadRegister(IMM_REG_Y_OFFS_USR);
        ofsy = *(IMM_OFS_USR_Reg_t*)&byte_reg;
        
        byte_reg = IMM_ReadRegister(IMM_REG_Z_OFFS_USR);
        ofsz = *(IMM_OFS_USR_Reg_t*)&byte_reg;
    }
    return result;
}

boolean_t IMM_Reset(void)
{
    uint8_t byte_reg = 0U;
    IMM_CTRL3_C_Reg_t ctrl3_reg;
    uint32_t tick_start = HAL_GetTick();
    
    IMM_SetBitRegister(IMM_REG_CTRL3_C, 0U);
    
    do
    {
        //IMM_WAIT(100);
    	//osDelay (100);
    	HAL_Delay(100);

        byte_reg = IMM_ReadRegister(IMM_REG_CTRL3_C);
        ctrl3_reg = *(IMM_CTRL3_C_Reg_t*)&byte_reg;
    }while(ctrl3_reg.SW_RESET && ((HAL_GetTick() - tick_start) < 500U));
    
    return (ctrl3_reg.SW_RESET == 0U);
}

boolean_t IMM_SetAccMode(IMM_Mode_t mode)
{
    boolean_t result = FALSE;
    if(mode < IMM_MODE_INVALID)
    {
        uint8_t byte_reg = 0U;
        IMM_CTRL6_C_Reg_t reg;
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL6_C);
        reg = *(IMM_CTRL6_C_Reg_t*)&byte_reg;
        reg.XL_HM_MODE = (mode == IMM_MODE_HIGH_PERF) ? 0U : 1U;
        IMM_WriteRegister(IMM_REG_CTRL6_C, *(uint8_t*)&reg);
        
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL6_C);
        result = (byte_reg == *(uint8_t*)&reg);
    }
    return result;
}

boolean_t IMM_SetAccOutputDataRate(IMM_AccOutputDataRateSel_t acc_odr)
{
    boolean_t result = FALSE;
    
    if(acc_odr < IMM_ACC_ODR_IVALID)
    {
        volatile uint8_t byte_reg = 0U;
        volatile IMM_CTRL1_XL_Reg_t reg;
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL1_XL);
        reg = *(IMM_CTRL1_XL_Reg_t*)&byte_reg;
        reg.ODR_XL = acc_odr;
        IMM_WriteRegister(IMM_REG_CTRL1_XL, *(uint8_t*)&reg);
        
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL1_XL);
        result = (byte_reg == *(uint8_t*)&reg);
        
        IMM_DEVICE_ACC_SET_ACTIVE(IMM_DeviceActiveMask, (result && (acc_odr != IMM_ACC_ODR_POWER_DOWN)));
    }
    
    return result;
}

boolean_t IMM_SetAccFullScale(IMM_AccRangeSel_t acc_fs)
{
    boolean_t result = FALSE;
    
    if(acc_fs < IMM_ACC_RANGE_INVALID)
    {
        uint8_t byte_reg = 0U;
        IMM_CTRL1_XL_Reg_t reg;
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL1_XL);
        reg = *(IMM_CTRL1_XL_Reg_t*)&byte_reg;
        reg.FS_XL = acc_fs;
        IMM_WriteRegister(IMM_REG_CTRL1_XL, *(uint8_t*)&reg);
        
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL1_XL);
        result = (byte_reg == *(uint8_t*)&reg);
    }
    return result;
}

boolean_t IMM_SetAccAafBandwidth(IMM_AccAafBandWidthSel_t acc_bw)
{
    boolean_t result = FALSE;
    
    if(acc_bw < IMM_ACC_AAF_BW_INVALID)
    {
        uint8_t byte_reg = 0U;
        IMM_CTRL1_XL_Reg_t reg;
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL1_XL);
        reg = *(IMM_CTRL1_XL_Reg_t*)&byte_reg;		
#if ST_LSM6DS_3H
        reg.BW_XL = acc_bw;
#elif ST_LSM6DS_LTR
		reg.LPF1_BW_SEL = acc_bw;
#endif        
		IMM_WriteRegister(IMM_REG_CTRL1_XL, *(uint8_t*)&reg);
        
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL1_XL);
        result = (byte_reg == *(uint8_t*)&reg);
    }
    return result;
}

#if ST_LSM6DS_3H
boolean_t IMM_UseAccAafBwForLpf1(boolean_t use_aaf_bw)
{
    boolean_t result = FALSE;
    
    if(use_aaf_bw < IMM_ACC_AAF_BW_INVALID)
    {
        uint8_t byte_reg = 0U;
        IMM_CTRL4_C_Reg_t reg;
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL4_C);
        reg = *(IMM_CTRL4_C_Reg_t*)&byte_reg;
        reg.XL_BW_SCAL_ODR = (use_aaf_bw ? 1U : 0U);
        IMM_WriteRegister(IMM_REG_CTRL4_C, *(uint8_t*)&reg);
        
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL4_C);
        result = (byte_reg == *(uint8_t*)&reg);
    }
    return result;
}
#endif

boolean_t IMM_SetAccCompositeFilter(IMM_AccCompFilterSel_t acc_cfs)
{
    boolean_t result = FALSE;
    
    if(acc_cfs < IMM_ACC_CF_INVALID)
    {
        uint8_t byte_reg = 0U;
        IMM_CTRL8_XL_Reg_t reg;
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL8_XL);
        reg = *(IMM_CTRL8_XL_Reg_t*)&byte_reg;
        
#if ST_LSM6DS_3H		
		if((uint8_t)acc_cfs & IMM_ACC_CF_LPF1)
		{
			/* Bypass Filter */
			reg.LPF2_XL_EN = 1U;
			reg.HP_SLOPE_XL_EN = 0U;
		}
		else
		{
			if((uint8_t)acc_cfs & (uint8_t)IMM_COMPOSITE_FILTER_TYPE_SEL_MASK)
			{
				/*LP filter selection*/         
				reg.LPF2_XL_EN = 1U;
				reg.HP_SLOPE_XL_EN = 1U;
			}
			else
			{
				/*HP filter selection*/                        
				reg.LPF2_XL_EN = 0U;
				reg.HP_SLOPE_XL_EN = 1U;
			}
		
			acc_cfs &= (uint8_t)~IMM_COMPOSITE_FILTER_TYPE_SEL_MASK;
			reg.HPCF_XL = (acc_cfs & 0x03U);
		}
#elif ST_LSM6DS_LTR
		
		
		if(acc_cfs & (uint8_t)IMM_ACC_CF_LPF2_LL_ODR_50)
		{
			/* LPF 2 */
			reg.HP_SLOPE_XL_EN = 0;
			reg.LPF2_XL_EN = 1;			
			reg.HPCF_XL = (acc_cfs & 0x03U);
			
			if(acc_cfs & 0x0C)
			{
				/* Low Noise */
				reg.INPUT_COMPOSITE = 1;
			}
			else
			{
				/* Low Latency */
				reg.INPUT_COMPOSITE = 0;
			}			
		}
		else if(acc_cfs & 0x08)
		{
			/* LPF1 (or Bypass) */
			reg.HP_SLOPE_XL_EN = 0;
			reg.LPF2_XL_EN = 0;
			
			uint8_t byte_reg_ctrl1_xl = 0U;
			IMM_CTRL1_XL_Reg_t reg_ctrl1_xl;
			byte_reg_ctrl1_xl = IMM_ReadRegister(IMM_REG_CTRL1_XL);
			reg_ctrl1_xl = *(IMM_CTRL1_XL_Reg_t*)&byte_reg_ctrl1_xl;
			
			/* Set ODR/2 or ODR/4 */
			reg_ctrl1_xl.LPF1_BW_SEL = (acc_cfs & 0x04);
			
			IMM_WriteRegister(IMM_REG_CTRL1_XL, *(uint8_t*)&reg_ctrl1_xl);
        
			byte_reg_ctrl1_xl = IMM_ReadRegister(IMM_REG_CTRL1_XL);
			result = (byte_reg_ctrl1_xl == *(uint8_t*)&reg_ctrl1_xl);
			
		}
		else
		{
			/* SLOPE or HP */
			reg.HP_SLOPE_XL_EN = 1;
			reg.HPCF_XL = (acc_cfs & 0x03U);
			reg.INPUT_COMPOSITE = 0;			
		}

#endif
		
        IMM_WriteRegister(IMM_REG_CTRL8_XL, *(uint8_t*)&reg);
        
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL8_XL);
        result = (byte_reg == *(uint8_t*)&reg);
		
    }

    return result;
}

boolean_t IMM_SetAccEnableAxes(boolean_t x_on, boolean_t y_on, boolean_t z_on)
{
    boolean_t result = FALSE;
    
    uint8_t byte_reg = 0U;
    IMM_CTRL9_XL_Reg_t ctrl9_reg;
    
    byte_reg = IMM_ReadRegister(IMM_REG_CTRL9_XL);
    ctrl9_reg = *(IMM_CTRL9_XL_Reg_t*)&byte_reg;

#if ST_LSM6DS_3H    
    ctrl9_reg.XEN_XL = (x_on ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);
    ctrl9_reg.YEN_XL = (y_on ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);
    ctrl9_reg.ZEN_XL = (z_on ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);
#elif ST_LSM6DS_LTR
    ctrl9_reg.DEN_X = (x_on ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);
    ctrl9_reg.DEN_Y = (y_on ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);
    ctrl9_reg.DEN_Z = (z_on ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);	
#endif
    
    IMM_WriteRegister(IMM_REG_CTRL9_XL, *(uint8_t*)&ctrl9_reg);
    /*check*/
    byte_reg = IMM_ReadRegister(IMM_REG_CTRL9_XL);
    result = (byte_reg == *(uint8_t*)&ctrl9_reg); 
            
    return result;
}

uint32_t  IMM_GetAccSensitivity(IMM_AccRangeSel_t acc_range)
{
    uint32_t retval = 0U;
    
    if(acc_range < IMM_ACC_RANGE_NUM)
    {
        retval = IMM_AccSensitivity[acc_range];
    }
    
    return retval;
}

boolean_t IMM_SetGyroMode(IMM_Mode_t mode)
{
    boolean_t result = FALSE;
    if(mode < IMM_MODE_INVALID)
    {
        uint8_t byte_reg = 0U;
        IMM_CTRL7_G_Reg_t reg;
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL7_G);
        reg = *(IMM_CTRL7_G_Reg_t*)&byte_reg;
		
        reg.G_HM_MODE = (mode == IMM_MODE_HIGH_PERF) ? 0U : 1U;
        
		IMM_WriteRegister(IMM_REG_CTRL7_G, *(uint8_t*)&reg);
        
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL7_G);
        result = (byte_reg == *(uint8_t*)&reg);        
    }
    return result;    
}

boolean_t IMM_SetGyroOutputDataRate(IMM_GyroOutputDataRateSel_t gyro_odr)
{
    boolean_t result = FALSE;
    
    if(gyro_odr < IMM_GYRO_ODR_IVALID)
    {
        uint8_t byte_reg = 0U;
        IMM_CTRL2_G_Reg_t reg;
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL2_G);
        reg = *(IMM_CTRL2_G_Reg_t*)&byte_reg;

        reg.ODR_G = gyro_odr;

        IMM_WriteRegister(IMM_REG_CTRL2_G, *(uint8_t*)&reg);
        
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL2_G);
        result = (byte_reg == *(uint8_t*)&reg);
        
        IMM_DEVICE_GYRO_SET_ACTIVE(IMM_DeviceActiveMask, (result && (gyro_odr != IMM_GYRO_ODR_POWER_DOWN)));
    }
    
    return result;
}

boolean_t IMM_SetGyroFullScale(IMM_GyroRangeSel_t gyro_range)
{
    boolean_t result = FALSE;
    
    if(gyro_range < IMM_GYRO_RANGE_INVALID)
    {
        uint8_t byte_reg = 0U;
        IMM_CTRL2_G_Reg_t reg;
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL2_G);
        reg = *(IMM_CTRL2_G_Reg_t*)&byte_reg;
        
        if(gyro_range != IMM_GYRO_RANGE_125_DPS)
        {
            reg.FS_G = gyro_range;
            reg.FS_125 = IMM_REG_BIT_RESET;
        }
        else
        {
            reg.FS_125 = IMM_REG_BIT_SET;
        }

        IMM_WriteRegister(IMM_REG_CTRL2_G, *(uint8_t*)&reg);
        
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL2_G);
        result = (byte_reg == *(uint8_t*)&reg);
    }
    
    return result;
}

boolean_t IMM_SetGyroHpFilter(IMM_GyroHpFilterSel_t gyro_hpf)
{
    boolean_t result = FALSE;
    if(gyro_hpf < IMM_GYRO_HP_INVALID)
    {
        uint8_t byte_reg = 0U;
        IMM_CTRL7_G_Reg_t reg;
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL7_G);
        reg = *(IMM_CTRL7_G_Reg_t*)&byte_reg;
        
        if(gyro_hpf == IMM_GYRO_HP_FILTER_OFF)
        {
            reg.HP_G_EN = 0U;
        }
        else
        {
            reg.HP_G_EN = 1U;
#if ST_LSM6DS_3H			
            reg.HPCF_G = (gyro_hpf & 0x03U);
#elif ST_LSM6DS_LTR
			reg.HPM_G = (gyro_hpf & 0x03U);
#endif			
        }
        
        IMM_WriteRegister(IMM_REG_CTRL7_G, *(uint8_t*)&reg);
        
        byte_reg = IMM_ReadRegister(IMM_REG_CTRL7_G);
        result = (byte_reg == *(uint8_t*)&reg);
    }
    return result;
}

#if ST_LSM6DS_3H
boolean_t IMM_SetGyroEnableAxes(boolean_t pitch_on, boolean_t roll_on, boolean_t yaw_on)
{
    boolean_t result = FALSE;
    
    uint8_t byte_reg = 0U;
    IMM_CTRL10_C_Reg_t ctrl10_reg;
    
    byte_reg = IMM_ReadRegister(IMM_REG_CTRL10_C);
    ctrl10_reg = *(IMM_CTRL10_C_Reg_t*)&byte_reg;
    
    ctrl10_reg.XEN_G = (pitch_on ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);
    ctrl10_reg.YEN_G = (roll_on ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);
    ctrl10_reg.ZEN_G = (yaw_on ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);
    
    IMM_WriteRegister(IMM_REG_CTRL10_C, *(uint8_t*)&ctrl10_reg);
    /*check*/
    byte_reg = IMM_ReadRegister(IMM_REG_CTRL10_C);
    result = (byte_reg == *(uint8_t*)&ctrl10_reg);
    
    return result;
}

boolean_t IMM_SetGyroSignAndOrientation(boolean_t pitch_neg, boolean_t roll_neg, boolean_t yaw_neg, IMM_GyroOrient_t orient)
{
    boolean_t result = FALSE;
    uint8_t byte_reg = 0U;
    IMM_ORIENT_CFG_G_Reg_t orient_reg;
	
    byte_reg = IMM_ReadRegister(IMM_REG_ORIENT_CFG_G);
    
	orient_reg = *(IMM_ORIENT_CFG_G_Reg_t*)&byte_reg;
    
    orient_reg.SIGNX_G = (pitch_neg ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);
    orient_reg.SIGNY_G = (roll_neg ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);
    orient_reg.SIGNZ_G = (yaw_neg ? IMM_REG_BIT_SET : IMM_REG_BIT_RESET);
    
    if(orient < IMM_GYRO_ORIEN_INVALID)
    {
        orient_reg.ORIENT = orient;
    }
    
    IMM_WriteRegister(IMM_REG_ORIENT_CFG_G, *(uint8_t*)&orient_reg);
    /*check*/
    byte_reg = IMM_ReadRegister(IMM_REG_ORIENT_CFG_G);
    result = (byte_reg == *(uint8_t*)&orient_reg);
	
    return result;    
}
#endif

uint32_t  IMM_GetGyroSensitivity(IMM_GyroRangeSel_t gyro_range)
{
    uint32_t retval = 0U;
    if(gyro_range < IMM_GYRO_RANGE_NUM)
    {
        retval = IMM_GyroSensitivity[gyro_range];
    }
    return retval;
}

void     IMM_EnableMultipleByteAccess(boolean_t enable)
{
    if(enable)
    {
        IMM_SetBitRegister(IMM_REG_CTRL3_C, 2U);
    }
    else
    {
        IMM_ClearBitRegister(IMM_REG_CTRL3_C, 2U);
    }
}

void IMM_GetMeasureData(IMM_MeasureData_t *measure_data_ptr)
{
    if(measure_data_ptr)
    {
        IMM_STATUS_REG_Reg_t status;
        uint8_t start_reg_to_read = 0U;

        IMM_CheckDataReady(&status);
 
        memset(measure_data_ptr, 0, sizeof(IMM_MeasureData_t));
        
        start_reg_to_read = IMM_READ_REG(IMM_REG_OUT_TEMP_L);
        
        /*
        if(status.TDA)
        {
            buff_len += 2U;
            start_reg_to_read = IMM_READ_REG(IMM_REG_OUT_TEMP_L);
        }
        if(status.G_DA)
        {
            buff_len += 6U;
            if(!status.TDA)
            {
                start_reg_to_read = IMM_READ_REG(IMM_REG_OUTX_L_G);
            }
        }
        if(status.XL_DA)
        {
            buff_len += 6U;
            if(!status.TDA && !status.G_DA)
            {
                start_reg_to_read = IMM_READ_REG(IMM_REG_OUTX_L_XL);
            }
        }
         * */
        
        //if(buff_len)
        {
            uint8_t pos = 0U;
			
			uint8_t write[IMM_AUTO_DATA_BUFF_LEN] = {0U};
			write[0] = start_reg_to_read;			

			HAL_SPI2_TxRx(write, IMM_OutDataBuff, IMM_AUTO_DATA_BUFF_LEN, IMM_SPI_TIMEOUT);
			pos++;
            
            if(status.TDA)
            {
                measure_data_ptr->Temperature.Data = IMM_OutDataBuff[pos++];
                measure_data_ptr->Temperature.Data |= (IMM_OutDataBuff[pos++] << 8U);
                measure_data_ptr->Temperature.Valid = 1U;
            }
            else
            {
                pos += 2U;
            }
            measure_data_ptr->GyroData.Valid = 0U;
            if(status.G_DA)
            {
                measure_data_ptr->GyroData.DataX = IMM_OutDataBuff[pos++];
                measure_data_ptr->GyroData.DataX |= (IMM_OutDataBuff[pos++] << 8U);

                measure_data_ptr->GyroData.DataY = IMM_OutDataBuff[pos++];
                measure_data_ptr->GyroData.DataY |= (IMM_OutDataBuff[pos++] << 8U);

                measure_data_ptr->GyroData.DataZ = IMM_OutDataBuff[pos++];
                measure_data_ptr->GyroData.DataZ |= (IMM_OutDataBuff[pos++] << 8U);
                measure_data_ptr->GyroData.Valid = 1U;
            }
            else
            {
                pos += 6U;
            }
            measure_data_ptr->AccData.Valid = 0U;
            if(status.XL_DA)
            {
                measure_data_ptr->AccData.DataX = IMM_OutDataBuff[pos++];
                measure_data_ptr->AccData.DataX |= (IMM_OutDataBuff[pos++] << 8U);

                measure_data_ptr->AccData.DataY = IMM_OutDataBuff[pos++];
                measure_data_ptr->AccData.DataY |= (IMM_OutDataBuff[pos++] << 8U);

                measure_data_ptr->AccData.DataZ = IMM_OutDataBuff[pos++];
                measure_data_ptr->AccData.DataZ |= (IMM_OutDataBuff[pos++] << 8U);
                measure_data_ptr->AccData.Valid = 1U;
            }
        }     
    } 
}

int16_t IMM_ConvertTemperature(int16_t temp_raw)
{
    int16_t temp = (IMM_TEMPERATURE_OUTPUT_OFFSET * 16);
    
    temp += temp_raw;
    temp = temp / 16;
    
    return temp;
}

//uint32_t IMM_GetIrqPin(void)
//{
//    return IMM_IRQ_PIN;
//}

//void IMM_IrqHandler(void)
//{
//    if(HAL_GPIO_ReadPin(IMM_IRQ_PORT, IMM_IRQ_PIN) == GPIO_PIN_SET)
//    {
//        OS_EventSet(OS_EVENT_ID_IMM_IRQ);
//    }
//}

static void IMM_CheckDataReady(IMM_STATUS_REG_Reg_t *status)
{
    if(status)
    {
        uint8_t byte_reg;
        byte_reg = IMM_ReadRegister(IMM_REG_STATUS);
        *status = *(IMM_STATUS_REG_Reg_t*)&byte_reg;
    }
}

static void IMM_PhysicalInit(void)
{
     GPIO_InitTypeDef GPIO_InitStruct;

     /*Configure GPIO pin : PtPin */
     GPIO_InitStruct.Pin = IMU_CS_SPI2_Pin;
     GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
     GPIO_InitStruct.Pull = GPIO_NOPULL;
     GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
     HAL_GPIO_Init(IMU_CS_SPI2_GPIO_Port, &GPIO_InitStruct);
}

static uint8_t  IMM_ReadRegister(IMM_RegisterAddres_t reg_addr)
{
    uint8_t res = 0U;

	uint8_t buff[2U] = {IMM_READ_REG(reg_addr), 0U};
	uint8_t resp[2U] = {0U};

	HAL_SPI2_TxRx(buff, resp, sizeof(resp), IMM_SPI_TIMEOUT);

	res = resp[1U];

    return res;
}

static void  IMM_WriteRegister(IMM_RegisterAddres_t reg_addr, uint8_t data)
{
	uint8_t buff[2U] = {IMM_WRITE_REG(reg_addr), data};

	HAL_SPI2_TxRx(buff, NULL, sizeof(buff), IMM_SPI_TIMEOUT);

}

static void  IMM_SetBitRegister(IMM_RegisterAddres_t reg_addr, uint8_t bit_no)
{
    if(bit_no < 8U)
    {
        uint8_t reg_val = IMM_ReadRegister(reg_addr);
    
        reg_val |= (1U << bit_no);

        IMM_WriteRegister(reg_addr, reg_val);
    }
}

static void  IMM_ClearBitRegister(IMM_RegisterAddres_t reg_addr, uint8_t bit_no)
{
    if(bit_no < 8U)
    {
        uint8_t reg_val = IMM_ReadRegister(reg_addr);
    
        reg_val &= (uint8_t)~(1U << bit_no);

        IMM_WriteRegister(reg_addr, reg_val);
    }
}

boolean_t IMU_SelfTestON(void)
{
    boolean_t result = FALSE;
    uint8_t byte_reg = 0U;
    IMM_CTRL5_C_Reg_t reg;
        
    byte_reg = IMM_ReadRegister(IMM_REG_CTRL5_C);
    reg = *(IMM_CTRL5_C_Reg_t*)&byte_reg;
    
    // Set positive sign self tests
    reg.ST_XL = IMM_REG_BIT_SET;
    reg.ST_G = IMM_REG_BIT_SET;
    

    IMM_WriteRegister(IMM_REG_CTRL5_C, *(uint8_t*)&reg);

    byte_reg = IMM_ReadRegister(IMM_REG_CTRL5_C);
    result = (byte_reg == *(uint8_t*)&reg);
    
    return result;
}

boolean_t IMU_SelfTestOFF(void)
{
    boolean_t result = FALSE;
    uint8_t byte_reg = 0U;
    IMM_CTRL5_C_Reg_t reg;
        
    byte_reg = IMM_ReadRegister(IMM_REG_CTRL5_C);
    reg = *(IMM_CTRL5_C_Reg_t*)&byte_reg;
    
    // Set positive sign self tests
    reg.ST_XL = IMM_REG_BIT_RESET;
    reg.ST_G = IMM_REG_BIT_RESET;

    IMM_WriteRegister(IMM_REG_CTRL5_C, *(uint8_t*)&reg);

    byte_reg = IMM_ReadRegister(IMM_REG_CTRL5_C);
    result = (byte_reg == *(uint8_t*)&reg);
    
    return result;
}
