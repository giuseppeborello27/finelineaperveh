/** \file inertial_sensing.c
 ** \brief Inertial Sensing Measurement module - source file.
 ** \authors Giuseppe Di Martino
 ** \date   01-December-2016
 ** \copyright Copyright (c) 2016 E-Novia Srl. All Rights Reserved.
 */
#include <stdio.h>
#include <string.h>
//#include "os_target.h"
#include "st_lsm6dsxxx.h"
//#include "debug_log.h"

#include "inertial_sensing.h"


typedef struct
{
    IMM_AccOutputDataRateSel_t  AccDataRate;
    IMM_AccRangeSel_t           AccRange;
    float                       AccSensitivity;
    IMM_GyroOutputDataRateSel_t GyroDataRate;
    IMM_GyroRangeSel_t          GyroRange;
    float                       GyroSensitivity;
}ISM_Config_t;

static boolean_t ISM_Ready = FALSE;

static ISM_Config_t ISM_ConfigInit = 
{
    IMM_ACC_ODR_416_HZ,
    IMM_ACC_RANGE_16G,
    0.0,
    IMM_GYRO_ODR_416_HZ,
    IMM_GYRO_RANGE_245_DPS,
    0.0
};

static ISM_Config_t ISM_ConfigDeInit = 
{
    IMM_ACC_ODR_POWER_DOWN,
    IMM_ACC_RANGE_16G,
    0.0,
    IMM_GYRO_ODR_POWER_DOWN,
    IMM_GYRO_RANGE_245_DPS,
    0.0
};

static ISM_Measure_t ISM_Measure; 

static boolean_t ISM_RefreshSensitivity(void);

static void ISM_MeasureHandle(void);    

boolean_t ISM_Init(void)
{
    ISM_Ready = IMM_Init(ISM_ConfigInit.AccDataRate, 
                        ISM_ConfigInit.AccRange, 
                        IMM_MODE_HIGH_PERF, 
                        ISM_ConfigInit.GyroDataRate,
                        ISM_ConfigInit.GyroRange,
                        IMM_MODE_HIGH_PERF);
    ISM_Ready = ISM_Ready && IMM_SetAccEnableAxes(TRUE, TRUE, TRUE);
#if ST_LSM6DS_3H    
	ISM_Ready = ISM_Ready && IMM_SetGyroEnableAxes(TRUE, TRUE, TRUE);
#endif
    
    ISM_Ready = ISM_Ready && ISM_RefreshSensitivity();

    //DBG_INFO( "[ISM] Init status: %u", ISM_Ready);

    return ISM_Ready;
}

void        ISM_DeInit(void)
{
	ISM_Ready = FALSE;
	
	IMM_SetAccEnableAxes(FALSE, FALSE, FALSE);
#if ST_LSM6DS_3H    
	IMM_SetGyroEnableAxes(FALSE, FALSE, FALSE);
#endif
	
    IMM_Init(	ISM_ConfigDeInit.AccDataRate, 
				ISM_ConfigDeInit.AccRange, 
				IMM_MODE_NORMAL, 
				ISM_ConfigDeInit.GyroDataRate,
				ISM_ConfigDeInit.GyroRange,
				IMM_MODE_NORMAL);	
	
}

boolean_t ISM_IsReady(void)
{
    return ISM_Ready;
}

void ISM_GetLastMeasure(ISM_Measure_t *measure)
{
    if(measure)
    {
		ISM_MeasureHandle();
        memcpy(measure, &ISM_Measure, sizeof(ISM_Measure));
    }
}

static void ISM_MeasureHandle(void)
{
    if(ISM_Ready)
    {
        IMM_MeasureData_t measure;
        
        IMM_GetMeasureData(&measure);
               
        if(measure.AccData.Valid)
        {
            ISM_Measure.AccX = ISM_ConfigInit.AccSensitivity * measure.AccData.DataX;
            ISM_Measure.AccY = ISM_ConfigInit.AccSensitivity * measure.AccData.DataY;
            ISM_Measure.AccZ = ISM_ConfigInit.AccSensitivity * measure.AccData.DataZ;              
        }
        
        if(measure.GyroData.Valid)
        {
            ISM_Measure.GyroX = ISM_ConfigInit.GyroSensitivity * measure.GyroData.DataX;
            ISM_Measure.GyroY = ISM_ConfigInit.GyroSensitivity * measure.GyroData.DataY;
            ISM_Measure.GyroZ = ISM_ConfigInit.GyroSensitivity * measure.GyroData.DataZ;         
        }
        
        if(measure.Temperature.Valid)
        {
#if ST_LSM6DS_3H
			ISM_Measure.Temp = 25 + ((float)measure.Temperature.Data) * 0.0625F;
#elif ST_LSM6DS_LTR
			ISM_Measure.Temp = 25 + ((float)measure.Temperature.Data) * 0.00390625F;
#endif
        }
        
    }
    
//    if(OS_EventCheck(OS_EVENT_ID_IMM_IRQ))
//    {
//        OS_EventClear(OS_EVENT_ID_IMM_IRQ);
//        /*Do something*/
//        
//        
//    }
}

static boolean_t ISM_RefreshSensitivity(void)
{
    if(ISM_Ready)
    {
        static volatile float val;
        //ISM_Config.AccSensitivity =  (float)(((double)IMM_GetAccSensitivity(ISM_Config.AccRange) / 1000) * ISM_G_TO_MS2);
        val = IMM_GetAccSensitivity(ISM_ConfigInit.AccRange);
        val = val / 1000;
        ISM_ConfigInit.AccSensitivity =  val * ISM_G_TO_MS2;
        ISM_ConfigInit.AccSensitivity =  (ISM_ConfigInit.AccSensitivity / 1000);
        
        //ISM_Config.GyroSensitivity = ((float)IMM_GetGyroSensitivity(ISM_Config.GyroRange) / 1000) * ISM_DEG_TO_RAD;
        val = IMM_GetGyroSensitivity(ISM_ConfigInit.GyroRange);
        val = val / 1000;
        ISM_ConfigInit.GyroSensitivity = val * ISM_DEG_TO_RAD;
        ISM_ConfigInit.GyroSensitivity = (ISM_ConfigInit.GyroSensitivity / 1000);          
    }
    
    return (ISM_Ready && (ISM_ConfigInit.AccSensitivity) && (ISM_ConfigInit.GyroSensitivity));
}
