

#include "BQ24250.h"
#include "main.h"
#include "i2c.h"

#define CHRG_I2C_ID                 I2C_ID_2


#define CHRG_DEVICE_ADDR            0x6AU

#define CHRG_READ_WRITE_REG_MASK    0x01U
#define CHRG_WRITE_ADDR             ((CHRG_DEVICE_ADDR) << 1U)
#define CHRG_READ_ADDR              (((CHRG_DEVICE_ADDR) << 1U) | (CHRG_READ_WRITE_REG_MASK))

#define CHRG_REG_ADDR_MAX           4U
#define CHRG_REG_NUM                (CHRG_REG_ADDR_MAX + 1U)

/*Status/Control Register*/
typedef struct
{
    uint8_t     FAULT           :4U;
    uint8_t     STAT            :2U;
    uint8_t     WD_EN           :1U;
    uint8_t     WD_FAULT        :1U;
}__attribute__ ((__packed__))CHRG_Reg1_t;
static CHRG_Reg1_t  CHRG_Reg1;

/*Control Register*/
typedef struct
{
    uint8_t     HZ_MODE         :1U;
    uint8_t     CE              :1U;
    uint8_t     EN_TERM         :1U;
    uint8_t     EN_STAT         :1U;
    uint8_t     I_IN_LIMIT      :3U;
    uint8_t     RESET           :1U;
}__attribute__ ((__packed__))CHRG_Reg2_t;
static CHRG_Reg2_t  CHRG_Reg2;

/*Control/Battery Voltage Register*/
typedef struct
{
    uint8_t     USB_DET         :2U;
    uint8_t     VBAT_REG        :6U;
}__attribute__ ((__packed__))CHRG_Reg3_t;
static CHRG_Reg3_t  CHRG_Reg3;

/*Vender/Part/Revision Register*/
typedef struct
{
    uint8_t     I_TERM          :3U;
    uint8_t     I_CHG           :5U;
}__attribute__ ((__packed__))CHRG_Reg4_t;
static CHRG_Reg4_t  CHRG_Reg4;

/*Battery Termination/Fast Charge Current Register*/
typedef struct
{
    uint8_t     V_INDPM         :3U;
    uint8_t     CE_STATUS       :1U;
    uint8_t     DPDM_EN         :1U;
    uint8_t     LOW_CHG         :1U;
    uint8_t     LOOP_STATUS     :2U;
}__attribute__ ((__packed__))CHRG_Reg5_t;
static CHRG_Reg5_t  CHRG_Reg5;

static const char *statusStr[CHRG_STATUS_INVALID] =
{
	"READY",
	"CHARGE IN PROGRESS",
	"CHARGE COMPLETE",
	"FAULT"
};

static const char *faultStr[CHRG_FAULT_INVALID] =
{
	"NONE",
	"INPUT OVP",
	"INPUT UVL",
	"SLEEP",
	"BATT TEMP",
	"BATT OVP",
	"THERMAL_SHUTDOWN",
	"TIME FAULT",
	"NO BATT CONNECTED",
	"ISET SHORT",
	"INPUT LDO LOW"
};


static uint8_t *const CHRG_Regs[CHRG_REG_NUM] =
{
    (uint8_t*)&CHRG_Reg1,
    (uint8_t*)&CHRG_Reg2,
    (uint8_t*)&CHRG_Reg3,
    (uint8_t*)&CHRG_Reg4,
    (uint8_t*)&CHRG_Reg5
};


static void (*CHRG_StatusChangeCallback)(CHRG_Status_t status) = NULL;
static void (*CHRG_FaultCallback)(CHRG_Fault_t fault) = NULL;

/** \addtogroup  BQ24250_private_func
 *  \{
 */
//static void CHRG_MonitorHandler(void);
static uint8_t CHGR_ReadRegister(uint8_t reg_addr);
static bool CHGR_WriteRegister(uint8_t reg_addr, uint8_t val);
static bool CHRG_IsConnectedHw(void);
static bool CHRG_IsConnectedSw(void);

bool   CHGR_SetInLimCurrent(uint8_t value)
{
    bool retval = false;
    uint8_t reg_byte = 0U;
    CHRG_Reg2_t reg2;
    
    reg_byte = CHGR_ReadRegister(1U);
    reg2 = *(CHRG_Reg2_t*)&reg_byte;
    reg2.I_IN_LIMIT = (0x07U & value);
    reg2.RESET = 0;
    retval = CHGR_WriteRegister(1U, *(uint8_t*)&reg2);
    reg_byte = CHGR_ReadRegister(1U);
    
    retval = retval && (reg_byte == *(uint8_t*)&reg2);
    
    return retval;
}


/** \defgroup BQ24250_APIs	BQ24250 Module API Functions
 *  \brief  BQ24250 Module API Functions
 *  \{
 */
void CHRG_Init(void)
{
    uint8_t reg = 0U;

    CHRG_IntPinConfig(0);
    
    for(reg = 0U; reg < CHRG_REG_NUM; reg++)
    {
        *CHRG_Regs[reg] = CHGR_ReadRegister(reg);
    }
    

    
//    CHGR_SetChargeCurrent(31);
//    CHGR_SetInLimCurrent(0x02);
    CHGR_SetChargeCurrent(0x00);
    
//    OS_TimerCreate(OS_TIMER_ID_CHARGER_MONITOR, 200U, TIMER_CONTINUOUS_MODE, CHRG_MonitorHandler);
//    OS_EventCreate(OS_EVENT_ID_CHARGER_INT_HANDLER, CHRG_IntHandler);
//    OS_EventCreate(OS_EVENT_ID_CHARGER_CHARGE_START, NULL);
}



void CHRG_IntPinConfig(uint8_t mode_int)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    
    HAL_GPIO_DeInit(CHG_INT_EXTI8_GPIO_Port, CHG_INT_EXTI8_Pin); /*GDM72*/
    
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Pin = CHG_INT_EXTI8_Pin;
    if(mode_int)
    {
        GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    }
    else
    {
        HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
        GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING_FALLING;
    }
    HAL_GPIO_Init(CHG_INT_EXTI8_GPIO_Port, &GPIO_InitStruct);
    
    if(mode_int)
    {
        HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
    }
}

bool CHRG_GetIntPinStatus(void)
{
    return (HAL_GPIO_ReadPin(CHG_INT_EXTI8_GPIO_Port, CHG_INT_EXTI8_Pin) == GPIO_PIN_SET);
}

void CHRG_DeInit(void)
{
    HAL_GPIO_DeInit(CHG_INT_EXTI8_GPIO_Port, CHG_INT_EXTI8_Pin);
//    OS_TimerRemove(OS_TIMER_ID_CHARGER_MONITOR);
}

CHRG_Status_t CHRG_GetStatus(void)
{    
    return (CHRG_Status_t)CHRG_Reg1.STAT;
}

uint8_t CHRG_GetChargeCurrent(void)
{
    return CHRG_Reg4.I_CHG;
}


bool CHRG_SetStandby(bool on)
{
    *(uint8_t*)&CHRG_Reg2 = CHGR_ReadRegister(1U);
    
    CHRG_Reg2.HZ_MODE = (on ? 1U : 0U);
    
    CHGR_WriteRegister(1U, *(uint8_t*)&CHRG_Reg2);
    *(uint8_t*)&CHRG_Reg2 = CHGR_ReadRegister(1U);
    
    return (CHRG_Reg2.HZ_MODE == (on ? 1U : 0U));
}

bool CHRG_IsInCharge(void)
{
    CHRG_Reg1_t reg1;
    *(uint8_t*)(&reg1) = CHGR_ReadRegister(0U);  
   return (reg1.STAT == CHRG_STATUS_CHARGE_IN_PROGRESS);
}

bool CHRG_IsConnected(void)
{
	return (/*CHRG_IsConnectedSw() &&*/ CHRG_IsConnectedHw()!= 0);
}

void CHRG_SetCallback(void (*status_change_callback)(CHRG_Status_t), void (*fault_callback)(CHRG_Fault_t))
{
    CHRG_StatusChangeCallback = status_change_callback;
    CHRG_FaultCallback = fault_callback;
}

uint32_t CHRG_GetPin(void)
{
    return CHG_INT_EXTI8_Pin;
}



bool    CHGR_SetChargeCurrent(uint8_t value)
{
    bool retval = false;
    uint8_t reg_byte = 0U;
    CHRG_Reg4_t reg4;
    
    reg_byte = CHGR_ReadRegister(3U);
    reg4 = *(CHRG_Reg4_t*)&reg_byte;
    reg4.I_CHG = (0x1FU & value);
    retval = CHGR_WriteRegister(3U, *(uint8_t*)&reg4);
    reg_byte = CHGR_ReadRegister(3U);
    
    retval = retval && (reg_byte == *(uint8_t*)&reg4);

    return retval;
}


uint8_t CHGR_GetChargeCurrent(void)
{
    uint8_t reg_byte = 0U;
    CHRG_Reg4_t reg4;
    reg_byte = CHGR_ReadRegister(3U);
    reg4 = *(CHRG_Reg4_t*)&reg_byte;
    return reg4.I_CHG;
}

//void CHRG_IntHandler(void)
//{    
//	DBG_HIDEBUG("Call PWR_interruptCheck from Charger Interrupt.");
//	PWR_interruptCheck(PWR_STATUS_ON);
//    HAL_NVIC_EnableIRQ(EXTI0_IRQn);
//    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
//}



/**
  * \}
  */ 

/** \defgroup BQ24250_private_func BQ24250 Module Private Functions
 *  \brief  BQ24250 Module Private Functions
 *  \{
 */
/**
 ** \brief Update the charger register 0
 */
/*static*/ void CHRG_MonitorHandler(void)
{
    CHRG_Reg1_t reg1;
    CHRG_Reg4_t reg4;
    *(uint8_t*)(&reg1) = CHGR_ReadRegister(0U);
    *(uint8_t*)(&reg4) = CHGR_ReadRegister(3U);
    
    if(reg1.STAT != CHRG_Reg1.STAT)
    {
        
        if(CHRG_StatusChangeCallback)
        {
            CHRG_StatusChangeCallback((CHRG_Status_t)reg1.STAT);
        }
        
        if(reg1.STAT == CHRG_STATUS_FAULT)
        {
            if(CHRG_FaultCallback)
            {
                CHRG_FaultCallback((CHRG_Fault_t)reg1.FAULT);
            }
        }
        DBG_INFO("reg1.STAT: %hhu, CHRG_Reg1.STAT: %hhu", reg1.STAT, CHRG_Reg1.STAT);
        DBG_INFO("reg1.FAULT: %hhu", reg1.FAULT);
    }
    
    DBG_HIDEBUG("reg1.STAT: %hhu, CHRG_Reg1.STAT: %hhu", reg1.STAT, CHRG_Reg1.STAT);

    *(uint8_t*)(&CHRG_Reg1) = *(uint8_t*)(&reg1);
    *(uint8_t*)(&CHRG_Reg4) = *(uint8_t*)(&reg4);
}

//leggo i bit di stato del registro 0
//se FAULT ritorno 0
uint8_t myCHRG_GetStatus(void)
{
    CHRG_Reg1_t reg1;

    *(uint8_t*)(&reg1) = CHGR_ReadRegister(0U);

    // se non è in Fault
	if(reg1.STAT != CHRG_STATUS_FAULT)
		return 1;
	else
		return 0;

    /***********PROVA***************************************/
//    do
//    {
//    	*(uint8_t*)(&reg1) = CHGR_ReadRegister(0U);
//
//    } while(reg1.FAULT==10);
//
//    if(reg1.FAULT == 0 && reg1.STAT==1)
//    	return 1;
//    else
//    	return 0;
    /***********PROVA***************************************/
}

void CHRG_StatusRefresh(void)
{
    CHRG_Reg1_t reg1;
    CHRG_Reg4_t reg4;
    *(uint8_t*)(&reg1) = CHGR_ReadRegister(0U);
    *(uint8_t*)(&reg4) = CHGR_ReadRegister(3U);
    
    //if(reg1.STAT != CHRG_Reg1.STAT)
    {
        
        if(CHRG_StatusChangeCallback)
        {
            CHRG_StatusChangeCallback((CHRG_Status_t)reg1.STAT);
        }
        
        if(reg1.STAT == CHRG_STATUS_FAULT)
        {
            if(CHRG_FaultCallback)
            {
                CHRG_FaultCallback((CHRG_Fault_t)reg1.FAULT);
            }
        }
        DBG_INFO("reg1.STAT: %hhu, CHRG_Reg1.STAT: %hhu", reg1.STAT, CHRG_Reg1.STAT);
        DBG_INFO("reg1.FAULT: %hhu", reg1.FAULT);
    }
    
    DBG_HIDEBUG("reg1.STAT: %hhu, CHRG_Reg1.STAT: %hhu", reg1.STAT, CHRG_Reg1.STAT);

    *(uint8_t*)(&CHRG_Reg1) = *(uint8_t*)(&reg1);
    *(uint8_t*)(&CHRG_Reg4) = *(uint8_t*)(&reg4);
}



/**
 ** \brief Read the register related to the specified address
 ** \param[in] reg_addr Register address to read
 ** \return The current register value as a byte
 */
static uint8_t      CHGR_ReadRegister(uint8_t reg_addr)
{
    uint8_t reg_val = 0U;
    
    if(reg_addr <= CHRG_REG_ADDR_MAX)
    {
//        OS_SYNCHRONIZE(   /*GDM72*/
            I2C_Write(CHRG_I2C_ID, CHRG_WRITE_ADDR, (uint8_t*)&reg_addr, 1U);
            I2C_Read(CHRG_I2C_ID, CHRG_READ_ADDR, &reg_val, 1U);
//        )
    }
    
    return reg_val;
}

/**
 ** \brief Write the register related to the specified address
 ** \param[in] reg_addr Register address to read
 ** \param[in] val      Value to write
 ** \return true If the operation has been successfully completed
 ** \return false If the operation has not been successfully completed
 */
static bool    CHGR_WriteRegister(uint8_t reg_addr, uint8_t val)
{
    bool retval = false;
    if(reg_addr <= CHRG_REG_ADDR_MAX)
    {
        uint8_t buff[2U] = {0U};
        buff[0U] = reg_addr;
        buff[1U] = val;
//        OS_SYNCHRONIZE(   /*GDM72*/
            retval = I2C_Write(CHRG_I2C_ID, CHRG_WRITE_ADDR, buff, sizeof(buff));
//        )
    }
    return retval;
}

/**
 ** \brief Return true if the charger is connected (It checks the hardware connection to the charger)
 ** \return true    If the charger is connected
 ** \return false   If the charger isn't connected
 */
static bool CHRG_IsConnectedHw(void)
{

#ifdef USE_OLD_PORT_EXT_IF

#ifdef PROTO_B
	uint16_t gpmr = 0x00;
	ENOVIA_STMPE1600_readGPMRRegisters(&gpmr);
	
    if(gpmr & ENOVIA_STMPE1600_GPIO_14_MASK)
    {
        DBG_HIDEBUG("CHRG_IsConnectedHw");
    }
    
	return (gpmr & ENOVIA_STMPE1600_GPIO_14_MASK);
#else
	return TRUE;
#endif /*PROTO_B*/

#else
    bool retval = true;

    PCAL6416A_GetPortPin(1U, 6U, (bool*)&retval);

    return retval;
#endif /*USE_OLD_PORT_EXT_IF*/
}

static int8_t     statPrev = -1;

/**
 ** \brief Return true if the charger is connected (It checks the status of the charger module)
 ** \return true    If the charger is connected
 ** \return false   If the charger isn't connected
 */
static bool CHRG_IsConnectedSw(void)
{
    bool nConn;
    
	CHRG_Reg1_t reg1;
	*(uint8_t*)(&reg1) = CHGR_ReadRegister(0U);
    GPIO_PinState pins[2U] = {HAL_GPIO_ReadPin(CHG_INT_EXTI8_GPIO_Port, CHG_INT_EXTI8_Pin), HAL_GPIO_ReadPin(CHG_STATUS_GPIO_Port, CHG_STATUS_Pin)};
	
	DBG_INFO("INT PIN: %d \t STATUS PIN: %d \t STATUS: %s [%s]", pins[0U],
																	pins[1U],
																	statusStr[reg1.STAT], faultStr[reg1.FAULT]);


    if(reg1.STAT == CHRG_STATUS_CHARGE_COMPLETE)
    {
        if(pins[0U] == GPIO_PIN_RESET)
        {
            nConn = false;
        }
        else if(reg1.FAULT != CHRG_FAULT_TIME_FAULT)
        {
            nConn = (pins[0U] == GPIO_PIN_SET);
        }
    }
    else if((reg1.STAT == CHRG_STATUS_FAULT) && (reg1.FAULT == CHRG_FAULT_NO_BATT_CONNECTED))
    {
        nConn = true;
    }
    else
    {
        nConn = 	(reg1.STAT == CHRG_STATUS_FAULT && (reg1.FAULT == CHRG_FAULT_INPUT_LDO_LOW || reg1.FAULT == CHRG_FAULT_SLEEP)) ||
                        (reg1.STAT == CHRG_STATUS_READY && statPrev != CHRG_STATUS_CHARGE_IN_PROGRESS);
    }
	
	statPrev = reg1.STAT;
	
    return (!nConn);
    
//	if(nConn)
//	{
//		return false;
//	}		
//	else
//	{
//		return true;
//	}
}



#ifdef DBG_CHRG
void CHRG_MonitorHandlerDbg(void)
{
	CHRG_Reg1_t reg1;
    *(uint8_t*)(&reg1) = CHGR_ReadRegister(0U);
	
	DBG_INFO("INT PIN: %d \t STATUS PIN: %d \t STATUS: %s [%s]", 	HAL_GPIO_ReadPin(CHRG_INT_PORT, CHRG_INT_PIN),
																	HAL_GPIO_ReadPin(CHG_STATUS_GPIO_Port, CHG_STATUS_Pin),
																	statusStr[reg1.STAT], faultStr[reg1.FAULT]);
}
/**
 ** \brief Show the charger status
 ** \param[in] status Status of the charger ( \ref CHRG_Status_t) 
 */
void CHRG_ShowStatus(CHRG_Status_t status)
{

    DBG_INFO("[CHRG] Charger status: %hhu", status);

}

/**
 ** \brief Show the charger fault
 ** \param[in] fault Fault of the charger ( \ref CHRG_Fault_t) 
 */
void CHRG_ShowFault(CHRG_Fault_t fault)
{
    DBG_INFO("[CHRG] Charger fault: %hhu", fault);      
}
#endif

/**
  * \}
  */ 
