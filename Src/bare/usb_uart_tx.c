/*
 * @file      flush_task.c
 * @brief
 *
 * @author Decawave Software
 *
 * @attention Copyright 2017 (c) DecaWave Ltd, Dublin, Ireland.
 *            All rights reserved.
 *
 */
//#include "usbd_cdc_if.h"
#include "circ_buf.h"
#include "usb_uart_rx.h"

#include "usb_uart_tx.h"

#include "port.h"

//-----------------------------------------------------------------------------
//     USB/UART report section
//
// Currently implemented using statically allocated Report.buf which shall be big enough for
// Usb2Spi top-level application, i.e. >4KB.
// The next available is 8K.

//the size is such long because of usb2spi top-level application
#define REPORT_BUFSIZE          0x2000  /**< the size of report buffer, must be 1<<N, i.e. 0x800,0x1000 etc*/
#define USB_UART_TX_TIMEOUT_MS  10      /**< the timeout in ms to output 64 bytes of data on 115200 bitrate */

static struct
{
    HAL_LockTypeDef    Lock;
    struct
    {
        uint16_t   head;
        uint16_t   tail;
        uint8_t    buf[REPORT_BUFSIZE];    /**< Large USB/UART circular Tx buffer */
    }
    Report;                    /**< circular report buffer, data to transmit */
}
txHandle =
{
    .Lock          = HAL_UNLOCKED,
    .Report.head = 0,
    .Report.tail = 0
};


//-----------------------------------------------------------------------------
// Implementation

int reset_report_buf(void)
{
    __HAL_LOCK(&txHandle);
    txHandle.Report.head = txHandle.Report.tail = 0;
    __HAL_UNLOCK(&txHandle);
    return  _NO_ERR;
}

/* @fn      copy_tx_msg()
 * @brief   put message to circular report buffer
 *          it will be transmitted in background ASAP from flushing thread
 * @return  HAL_BUSY - can not do it now, wait for release
 *          HAL_ERROR- buffer overflow
 *          HAL_OK   - scheduled for transmission
 * */
__STATIC_INLINE
error_e copy_tx_msg(uint8_t *str, int len)
{
    error_e     ret = _NO_ERR;
    uint16_t    head, tail;
    uint16_t    size = sizeof(txHandle.Report.buf) / sizeof(txHandle.Report.buf[0]);

    /* add TX msg to circular buffer and increase circular buffer length */

    __HAL_LOCK(&txHandle); //"return HAL_BUSY;" if locked
    head = txHandle.Report.head;
    tail = txHandle.Report.tail;

    if (CIRC_SPACE(head, tail, size) > len)
    {
        while (len > 0)
        {
            txHandle.Report.buf[head] = *(str++);
            head = (head + 1) & (size - 1);
            len--;
        }

        txHandle.Report.head = head;
    }
    else
    {
        /* if packet can not fit, setup TX Buffer overflow ERROR and exit */
        error_handler(0, _Err_TxBuf_Overflow);
        ret = _Err_TxBuf_Overflow;
    }

    __HAL_UNLOCK(&txHandle);
    return ret;
}

/* @fn      port_tx_msg()
 * @brief   wrap for copy_tx_msg()
 *          Puts message to circular report buffer
 *          and instructs output task to send it
 *
 * @return  see copy_tx_msg()
 * */
error_e port_tx_msg(uint8_t* str, int len)
{
    error_e ret;

    if(app.maxMsgLen<len)
    {
        app.maxMsgLen=len;
    }

    ret = copy_tx_msg(str, len);

    if(app.flushTask.Handle ) //RTOS : usbFlushTask can be not started yet
    {
        osSignalSet(app.flushTask.Handle, app.flushTask.Signal);
    }

    return (ret);
}


//-----------------------------------------------------------------------------
//     USB/UART report : platform - dependent section


/* @fn      flush_report_buff()
 * @brief   FLUSH should have higher priority than port_tx_msg()
 *          This shall be called periodically from process, which can not be locked,
 *          i.e. from independent high priority thread / timer etc.
 * */
extern void reset_FlushTask(void);

error_e flush_report_buf(void)
{
//    USBD_CDC_HandleTypeDef *hcdc =
//            (USBD_CDC_HandleTypeDef*) hUsbDeviceFS.pClassData;	//	TODO: utilizzare la UART del nuovo HW
//
//    uint8_t ubuf[CDC_DATA_FS_MAX_PACKET_SIZE]; /**< linear buffer, to transmit next chunk of data */ //TODO: utilizzare la UART del nuovo HW
//    uint16_t    size = sizeof(txHandle.Report.buf) / sizeof(txHandle.Report.buf[0]);
//    uint16_t    chunk;
//    uint32_t    tmr;
//
//
//    __HAL_LOCK(&txHandle); //"return HAL_BUSY;" if locked
//    uint16_t head = txHandle.Report.head;
//    uint16_t tail = txHandle.Report.tail;
//
//    uint16_t len = CIRC_CNT(head, tail, size);
//
//    start_timer(&tmr);
//
//    while(len > 0)
//    {
//        if (check_timer(tmr, USB_UART_TX_TIMEOUT_MS))
//        {
//            break;
//        }
//
//        /*    check UART status - ready to TX */
//        if((HAL_UART_GetState(&huart1) != HAL_UART_STATE_BUSY_RX) &&\
//           (HAL_UART_GetState(&huart1) != HAL_UART_STATE_READY) &&\
//           (app.pConfig->s.uartEn == 1))
//        {
//            continue; /**< UART is slow, but it will never fail to transmit/stall : wait to complete */
//        }
//
//        /*    check USB status - ready to TX */
////        if ((hUsbDeviceFS.dev_state == USBD_STATE_CONFIGURED)
////                && (hcdc->TxState != 0) && (app.usbState == USB_CONFIGURED)) 		TODO: utilizzare la UART del nuovo HW
////        {
////            break;  /**< USB should not be busy. If it's busy, we should let it output data */
////        }
//
//        start_timer(&tmr);
//
//        /* copy MAX allowed length from circular buffer to linear buffer */
//        chunk = MIN(sizeof(ubuf), len);
//
//        for (int i = 0; i < chunk; i++)
//        {
//            ubuf[i] = txHandle.Report.buf[tail];
//            tail = (tail + 1) & (size - 1);
//        }
//
//        len -= chunk;
//
//        txHandle.Report.tail = tail;
//
//        /* setup UART DMA transfer */
//        if (app.pConfig->s.uartEn == 1)
//        {
//            if (HAL_UART_Transmit_DMA(&huart1, ubuf, chunk) != HAL_OK)
//            {
//                error_handler(0, _ERR_UART_TX); /**< indicate UART transmit error */
//                break;
//            }
//        }
//
//        if((hUsbDeviceFS.dev_state == USBD_STATE_CONFIGURED) && (app.usbState == USB_CONFIGURED))
//        {
//            /* setup USB IT transfer */
//            if (CDC_Transmit_FS(ubuf, chunk) != USBD_OK)
//            {
//                error_handler(0, _Err_Usb_Tx); /**< indicate USB transmit error */
//                break;
//            }
//        }
//    }
//
//    __HAL_UNLOCK(&txHandle);
//    return _NO_ERR;
}

//      END OF Report section

