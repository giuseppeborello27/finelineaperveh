/**
 * @project       Project
 *
 * @Component     Component
 *
 * @file          File
 *
 * @brief         Brief
 *
 * @author        Author 
 *
 * @date          Date
 *
 * @copyright     Copyright 2019 Kalpa All rights reserved.
 *
 *                This file is copyrighted and the property of Kalpa. It contains confidential and proprietary
 *                information and may not be distributed or reproduced in whole or in part without express written
 *                permission from Kalpa.
 *                Any copies of this file (in whole or in part) made by any method must also include a copy of this legend.
 *
 * Developed by:  Kalpa
 ***********************************************************************************************************************/
#ifndef INC_APPCONFIG_H_
#define INC_APPCONFIG_H_

#include "stdio.h"
#include "stdint.h"
#include "stdbool.h"
#include "BLE_Protocol.h"

#define FIX_ENDIANNESS(x)     ( ( ( x << 8 ) & 0xFF00 ) | ( ( x >> 8 ) & 0xFF ) )

#define CONFIG_PARAM_VERSION        (1)
#define CONFIG_PARAM_SIZE           ( 66 )//(sizeof())

/* BLE custom config Definition */
#define DEFAULT_PRE_ALRM_THR        (800)
#define DEFAULT_ALRM_THR            (1200)
#define DEFAULT_PRE_ALRM_HYS        (100)
#define DEFAULT_ALRM_HYS            (100)
#define DEFAULT_TIMEOUT             (5)
#define DEFAULT_PRE_ALRM_REL        (1)      //Boolean
#define DEFAULT_ALRM_REL            (1)      //Boolean
#define DEFAULT_AVG_WIN_SAMP        (10)
#define DEFAULT_DRIVE_EXC_TIMEOUT   (1000)
#define DEFAULT_DETEC_FREQ          (1000)


typedef enum
{
   ParamNone = 0,
   ParamPreAlarmThreshold = 1,
   ParamAlarmThreshold = 2,
   ParamPreAlarmHysteresis = 3,
   ParamAlarmHysteresis = 4,
   ParamTimeout = 5,
   ParamPreAlarmReleActivation = 6,
   ParamAlarmReleActivation = 7,
   ParamAverageWindowSamples = 8,
   ParamDriverExclusionTimeout = 9,
   ParamDetectionFrequency = 10,
   MAX_ParamID
}__attribute__ ((__packed__))ConfigParamId_t;

typedef enum
{
   typeU8 = 0,
   typeU16,
   typeU32,
   typeS8,
   typeS16,
   typeS32,
   typeString,
   typeMax
}__attribute__ ((__packed__))ConfigParamType_e;

typedef struct
{
      uint16_t preAlarmThreshold_v;
      uint16_t alarmThreshold_v;
      uint16_t preAlarmHysteresis_v;
      uint16_t alarmHysteresis_v;
      uint16_t timeout_v;
      bool preAlarmReleActivation_v;
      bool alarmReleActivation_v;
      uint16_t averageWindowSamples_v;
      uint16_t driverExclusionTimeout_v;
      uint16_t detectionFrequency_v;
}ConfigParamValues_t;


typedef struct
{
      uint16_t version;
      uint32_t size;
      uint16_t maxParam;
}ConfigParamHeader_t;


typedef struct
{
      ConfigParamId_t id;
      ConfigParamType_e type;
      uint16_t size;
}ConfigParamItemHeader_t;


typedef struct
{
      ConfigParamItemHeader_t header;
      uint8_t data;
}typedefU8_t;


typedef struct
{
      ConfigParamItemHeader_t header;
      uint16_t data;
}typedefU16_t;


typedef struct
{
      ConfigParamItemHeader_t header;
      uint32_t data;
}typedefU32_t;


typedef struct
{
      ConfigParamItemHeader_t header;
      int8_t data;
}typedefS8_t;

typedef struct
{
      ConfigParamItemHeader_t header;
      int16_t data;
}typedefS16_t;

typedef struct
{
      ConfigParamItemHeader_t header;
      int32_t data;
}typedefS32_t;


typedef union
{
      ConfigParamItemHeader_t header;
      typedefU8_t typeU8;
      typedefU16_t typeU16;
      typedefU32_t typeU32;
      typedefS8_t typeS8;
      typedefS16_t typeS16;
      typedefS32_t typeS32;
      uint8_t rawData[4];
}ConfigParamItem_u;


typedef struct
{
      ConfigParamHeader_t confHeader;
      ConfigParamItem_u preAlarmThreshold;
      ConfigParamItem_u alarmThreshold;
      ConfigParamItem_u preAlarmHysteresis;
      ConfigParamItem_u alarmHysteresis;
      ConfigParamItem_u timeout;
      ConfigParamItem_u preAlarmReleActivation;
      ConfigParamItem_u alarmReleActivation;
      ConfigParamItem_u averageWindowSamples;
      ConfigParamItem_u driverExclusionTimeout;
      ConfigParamItem_u detectionFrequency;
}ConfigParam_t;


void configManager(uint8_t * pBuffer, uint8_t size, ConfigParamId_t id, paramAction_e action);

ConfigParam_t * getConfigParam(void);
uint8_t * getSerialNumber(void);
uint16_t getPreAlarmThreshold(void);
uint16_t getAlarmThreshold(void);
uint16_t getPreAlarmHysteresis(void);
uint16_t getAlarmHysteresis(void);
uint16_t getTimeout(void);
uint16_t getPreAlarmReleActivation(void);
uint16_t getAlarmReleActivation(void);
uint16_t getAverageWindowSamples(void);
uint16_t getDriverExclusionTimeout(void);
uint16_t getDetectionFrequency(void);


void setPreAlarmThreshold(uint16_t value);
void setAlarmThreshold(uint16_t value);
void setPreAlarmHysteresis(uint16_t value);
void setAlarmHysteresis(uint16_t value);
void setTimeout(uint16_t value);
void setPreAlarmReleActivation(uint16_t value);
void setAlarmReleActivation(uint16_t value);
void setAverageWindowSamples(uint16_t value);
void setDriverExclusionTimeout(uint16_t value);
void setDetectionFrequency(uint16_t value);

void configInit(void);
void restoreConfig(void);
ConfigParamItem_u buildConfigItem(ConfigParamId_t id, ConfigParamType_e type, uint16_t size, uint8_t * data, uint32_t * buildSize);
void restoreConfigDefault(ConfigParam_t * cfg);
void writeConfigIntoFlash(ConfigParam_t * cfg);

#endif /* INC_APPCONFIG_H_ */
