/** 
*	\file BGLIB_Module.h
*	\brief [Description] - Header File.
*	\author Luca Bortolotti
*	\date   16-May-2017
*	\copyright Copyright (c) 2017 E-Novia Srl. All Rights Reserved.
* 
* E-NOVIA CONFIDENTIAL
* __________________
* 
* ALL INFORMATION CONTAINED HEREIN WAS PRODUCED BY E-NOVIA SRL AND NO THIRD PARTY MAY CLAIM ANY RIGHT OR PATERNITY ON IT.\n 
* THE INTELLECTUAL AND TECHNICAL CONCEPTS CONTAINED HEREIN ARE AND REMAIN
* THE PROPERTY OF E-NOVIA SRL AND ARE PROTECTED BY TRADE SECRET OR COPYRIGHT LAW.
* REPRODUCTION, DISTRIBUTION OR DISCLOSURE OF THIS MATERIAL TO ANY OTHER PARTY
* IS STRICTLY FORBIDDEN UNLESS PRIOR WRITTEN PERMISSION IS OBTAINED FROM E-NOVIA SRL
*/ 

#ifndef USE_OLD_BLE_IF

#ifndef __BGLIB_MODULE_H_
#define __BGLIB_MODULE_H_

#include <stdint.h>
#include "BGLIB_Module_Cfg.h"
#include "BLG_UART_Cfg.h"

/** 
*	\defgroup BGLIB_Module_MODULE BGLIB Module Module Interface
* 	\brief The BGLIB Module Module Interface.
*   \{
*/

/** 
*	\defgroup BGLIB_Module_PUBLIC PUBLIC
* 	\brief BGLIB Module Public Definitions and Functions.
*	\{
*/

#ifdef ENABLE_DEBUG
#define BLE_DBG						    /**< Change this to avoid testing BLE functions */
#endif
 

/**
*	\brief Function to Init the BG-Libraries
*	
*	This function Initialize the BLE Module and wake it up from sleep.
*	After this the user will receive the hardware_io_port_status callback (Referr to the BGLIB Drivers Manuals).
*	
*	<strong> Usage: </strong>
*	- Implements the needed callback functions into BGLIB drivers commands.c file 
*	- Start calling this function
*	- Wait for the hardware_io_port_status callback
*	- Now user can use defined commands into cmd_def.h (Referr to the BGLIB Drivers Manuals)
*
*   \warning Call this function only with the BLE module programmed with the provided project.
*/
void BGLIB_Init(void);

/**
*	\brief Function to De-Init the BG-Libraries
*/
void BGLIB_DeInit(void);

/**
*	\brief Function to Reset the BLE module
*/
void BGLIB_ResetModule(void);
    
/**
*	\brief Function called when a Byte is ready on UART
*	\param[in] data rxByte
*/
void BGLIB_RxCallback(uint8_t rxByte);

//#ifdef BLE_DBG

void BGLIB_DBG_ScanStart(void);

void BGLIB_DBG_ScanStop(void);

/**
*	\brief Function to Start advertising through BLE.
*   \warning This is only a testing purpose function.
*/
void BGLIB_DBG_AdvertisingStart(void);
/**
*	\brief Function to Stop advertising through BLE
*   \warning This is only a testing purpose function.
*/
void BGLIB_DBG_AdvertisingStop(void);
//#endif

void BGLIB_RxHandler(void * arguments);
/**
*	\}
*/ 

/**
*	\}
*/ 

#endif /*__BGLIB_MODULE_H_ */

#endif /*USE_OLD_BLE_IF*/

/* EOF */
