
#ifndef APITYPES_H_
#define APITYPES_H_

#include "main.h"

//typedef unsigned char  uint8;
//typedef unsigned short uint16;
//typedef signed short   int16;
//typedef unsigned long  uint32;
//typedef signed char    int8;

typedef struct bd_addr_t
{
    uint8_t addr[6];

}__attribute__ ((__packed__)) bd_addr;

typedef bd_addr hwaddr;
typedef struct
{
	uint8_t len;
	uint8_t data[];
}__attribute__ ((__packed__)) uint8array;

typedef struct
{
	uint8_t len;
    int8_t data[];
}string;

//MYMOD
typedef struct
{
	bd_addr bdAddr;
    int8_t rssi;

} bleStruct;
//MYMOD_END


#endif
