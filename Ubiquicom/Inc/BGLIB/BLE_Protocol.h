/*
 * BLE_Protocol.h
 *
 *  Created on: Nov 4, 2020
 *      Author: CCagninelli
 */

#ifndef SRC_BGLIB_BLE_PROTOCOL_H_
#define SRC_BGLIB_BLE_PROTOCOL_H_



#include "W25Q128FV.h"
#include "cmd_def.h"



#define REQUEST_PARAMETER_HANDLE 12
#define PARAMETER_DATA_HANDLE    15

#define REQUEST_UPDATE_FW_HANDLE 20
#define UPDATE_FW_HANDLE         23

#define NOTIFY_ATTRIBUTE_ALL     0xFF

#define BLE_PACKET_MAX_SIZE	 	 0x17
#define BLE_RETURN_MAX_SIZE      0x17
#define MESSAGE_SIZE			 4
#define CRC_SIZE 				 0
#define CALCULATE_MSG_SIZE(value) (value + MESSAGE_SIZE + CRC_SIZE)

typedef enum
{
	read = 0,
	write,
	copy
}paramAction_e;

typedef enum
{
	INVALID = 0,
	VALID
}bleMsgIntegrity_e;

typedef enum
{
	cmdParamReadRequest = 2,
	cmdParamReadResponse,
	cmdParamWriteRequest,
	cmdParamWriteResponse,
	cmdSaveFlashRequest,
	cmdSaveFlashResponse,
	cmdAbortSavingRequest,
	cmdAbortSavingResponse,
	cmdStartUpgradeFwReq,
	cmdStartUpgradeFwResp,
	cmdChunkFWFileReq,
	cmdChunkFWFileResp,
	cmdEndUpgradeFwReq,
	cmdEndUpgradeFwResp,

	MAX_cmd,
	errorParam = 0xF0,
	errorFWUpgrade,
	MAX_error
}cmdType_e;



#define CMD_REQUEST_PARAM        0x10
#define CMD_UPDATE_PARAM         0x11
#define CMD_SAVE_PARAM			 0X12
#define CMD_UPGARDE_FIRMWARE     0x13
#define CMD_CONF_SAVE_PARAM		 0xC0
#define CMD_CONF_ABORT_PARAM	 0xC1



#define CMD_LEN_IDX			 	 0x00
#define CMD_REQ_IDX				 0x01
#define CMD_TYPE_IDX	 		 0x02
#define CMD_PARAM_IDX	 		 0x03
#define CMD_FIRST_BYTE_IDX	     0x04



bleMsgIntegrity_e BLE_verifyPacket(const struct ble_msg_attributes_value_evt_t *msg);
void BLE_buildPacket( uint8_t *pMsgToBLE, uint8_t *pMsgLen, uint8_t *pMsgData , cmdType_e cmdType, uint8_t *pMsgPayload, uint8_t msgSize);

#endif /* SRC_BGLIB_BLE_PROTOCOL_H_ */
