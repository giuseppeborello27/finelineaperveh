/**
 * @project       uTag2UWB_Persona
 *
 * @Component     Firmware backup
 *
 * @file          fwBackup.h
 *
 * @brief         Brief
 *
 * @author        Author 
 *
 * @date          04nov2020
 *
 ***********************************************************************************************************************/
#ifndef INC_FIRMWAREUPDATE_FWBACKUP_H_
#define INC_FIRMWAREUPDATE_FWBACKUP_H_

#include "info.h"

/**
 ** \brief Internally decides if a backup of the current firmware must be stored into the reserved external flash memory.
 ** \remark For further details please refer to <a href='../../UB-EFDM-01 (External Flash Data Map).xlsx'>UB-EFDM-01 (External Flash Data Map).xlsx</a>.
 */
void     FWBKP_Handler(void);

#ifdef APPLICATION_BOOTLOADER
/** \defgroup FWBKP_APIs   Firmware Backup Module APIs (application+bootloader section)
 *  \brief  Firmware Backup Module APIs (application+bootloader section)
 *  \{
 */
/**
 ** \brief Perform the first backup of the current firmware into the reserved external flash memory.
 ** \remark Relevant only to the application+bootloader configuration <a href='../../UB-EFDM-01 (External Flash Data Map).xlsx'>UB-EFDM-01 (External Flash Data Map).xlsx</a>.
 */
boolean_t   FWBKP_ApplicationFirstBackup(void);
/**
  * \}
  */
#endif

#endif /* INC_FIRMWAREUPDATE_FWBACKUP_H_ */
