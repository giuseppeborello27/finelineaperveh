/**
 * @project       uTag2UWB_Persona
 *
 * @Component     Firmware record
 *
 * @file          fwRecord.h
 *
 * @brief         This file contains the functions used to create records for the firmware update.
 *
 * @author        Author 
 *
 * @date          04nov2020
 *
 ***********************************************************************************************************************/
#ifndef INC_FIRMWAREUPDATE_FWRECORD_H_
#define INC_FIRMWAREUPDATE_FWRECORD_H_

#include "main.h"

/* Defines */
#define FWREC_APPLICATION_START_ADDR            ( 0x8000000U + ( VECT_TAB_OFFSET ) )

#define FWREC_APPLICATION_END_ADDR              ( 0x8080000U )

/**
 ** \brief Hex file record data size.
 */
#define FWREC_DATA_SIZE                16U

/**
 ** \brief Data offset position within the hex record.
 */
#define FWREC_FW_DATA_OFFSET           4U

/**
 ** \brief New firmware available key code.
 */
#define FWREC_NEW_FW_AVAILABLE_KEY        0x55AA6996U

/**
 ** \brief New firmware available key code offset position within the FW Key data group sector.
 */
#define FWREC_NEW_FW_AVAILABLE_KEY_OFFSET 0U

/**
 ** \brief New firmware record number info offset position within the FW Key data group sector.
 */
#define FWREC_NEW_FW_RECORD_NUM_OFFSET    (FWREC_NEW_FW_AVAILABLE_KEY_OFFSET + (2U * FWREC_FW_DATA_OFFSET))

/**
 ** \brief New firmware key code mask.
 */
#define FWREC_FW_PROGRAMMED_KEY_MASK        0xFFFF0000U

/**
 ** \brief Programmed firmware key code.
 */
#define FWREC_FW_PROGRAMMED_KEY             0x18180000U

/**
 ** \brief Backup firmware pending request key code mask.
 */
#define FWREC_FW_BACKUP_REQ_KEY_MASK        0x0000FFFFU

/**
 ** \brief Backup firmware pending request key code.
 */
#define FWREC_FW_BACKUP_REQ_KEY             0x0000ACACU

/**
 ** \brief Backup firmware available key code.
 */
#define FWREC_BKP_FW_AVAILABLE_KEY        0xABCD8421U

/**
 ** \brief Backup firmware available info offset position within the FW Key data group sector.
 */
#define FWREC_BKP_FW_AVAILABLE_KEY_OFFSET (FWREC_NEW_FW_RECORD_NUM_OFFSET + FWREC_FW_DATA_OFFSET + 4U)

/**
 ** \brief Backup firmware record number info offset position within the FW Key data group sector.
 */
#define FWREC_BKP_FW_RECORD_NUM_OFFSET    (FWREC_BKP_FW_AVAILABLE_KEY_OFFSET + (2U * FWREC_FW_DATA_OFFSET))

/**
 ** \brief Backup firmware start address within the external flash memory.
 */
#define FWREC_BKPUPFW_START_ADDR       (EFP_FW_DATA_START_ADDR + (EFP_FW_DATA_SIZE >> 1U))


/**
 ** \brief Data struct holding the key codes relevant to the firmware key data group.
 */
 typedef struct
{
   uint32_t    NewFwAvailableKey[2U];
   uint32_t NewFwRecordNum;
    uint32_t   NewFwProgramRetryNum;
   uint32_t    BkpFwAvailableKey[2U];
   uint32_t BkpFwRecordNum;
}FWREC_Key_t;


/**
 ** \brief Types of record relevant to the <a href="http://www.keil.com/support/docs/1584/">INTEL HEX FILE FORMAT</a>
 */
 typedef enum
 {
    FWREC_TYPE_DATA           = 0,    /**< \brief Data record type */
    FWREC_TYPE_EOF            = 1,    /**< \brief EOF record type */
    FWREC_TYPE_EXT_SEG_ADDRESS   = 2,    /**< \brief Extended Segment Address record type */
    FWREC_TYPE_EXT_LIN_ADDRESS   = 4,    /**< \brief Extended Linear Address record type */
    FWREC_TYPE_START_LIN_ADDRESS    = 5,    /**< \brief Start Linear Address record type */


    FWREC_TYPE_INVALID
 }FWREC_Type_t;


/**
 ** \brief Data struct relevant to the hex file format record.
 */
 typedef struct
 {
    //uint8_t  Start;
    uint8_t Size;                       /**< \brief Size of the record */
    uint16_t Address;                    /**< \brief Address of the record */
    uint8_t Type;                       /**< \brief Type of the record */
    uint8_t Data[FWREC_DATA_SIZE];      /**< \brief Record Data*/
    uint8_t Cks;                        /**< \brief Checksum*/
 }__attribute__ ((__packed__))FWREC_Data_t;

 /**
  ** \brief Split the data string into records and for each record invoke the @ref FWREC_SetRecordBlock.
  ** \remark The data string must contain a integer number of records
  ** \param[in] packet_str  Data string containing the integer number of record.
  ** \param[in] packet_str_size  Data string size.
  ** \returns TRUE          If at least one record is detected and if the @ref FWREC_SetRecordBlock returns successfully.
  ** \returns FALSE         If no record is detected within the string data or if the @ref FWREC_SetRecordBlock fails (in that case the internal cycle is broken).
  */
 boolean_t FWREC_SplitIntoRecords(char packet_str[], uint16_t packet_str_size);

 /**
  ** \brief Check if an internal error has been detected by the module.
  ** \returns TRUE          If an error occurred during module operation.
  ** \returns FALSE         If no error occurred.
  */
 boolean_t FWREC_ErrorDetected(void);

 /**
  ** \brief Setup the internal module logic to load a new hex file.
  */
 void FWREC_LoadSetup(void);

 /**
  ** \brief Parses a single record in string format and save into the reserved external flash memory.
  ** \remrks This function does not validate the new firmware!
  ** \param[in] packet_str  Data string containing one record.
  ** \param[in] packet_str_size  Data string size.
  ** \returns TRUE          If the record in ascii format is correctly parsed and stored
  ** \returns FALSE         If a parsing error occurs or if the record could not been corectly stored.
  */
 boolean_t FWREC_SetRecordBlock(char packet_str[], uint16_t packet_str_size);

 /**
  ** \brief Validate the new firmware by updating the firmware record number key code into the reserved external flash memory location.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_NewFwValidate(void);

 /**
  ** \brief Check if the validation of the new received firmware is pending.
  ** \returns TRUE          If validation is pending
  ** \returns FALSE         If validation is not pending
  */
 boolean_t FWREC_GetNewFirmwareValidationPending(void);

 /**
  ** \brief Write the new firmware availble key code into the reserved external flash memory.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_WriteNewFwKey(void);

 /**
  ** \brief Reset the firmware program retry number.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_ResetNewFwRetryNum(void);

 /**
  ** \brief Write the backup firmware availble key code into the reserved external flash memory location.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_WriteBkpFwKey(void);

 /**
  ** \brief checks if the backup firmware is availble.
  ** \returns TRUE          If the backup firmware is availble
  ** \returns FALSE         If the backup firmware is not availble.
  */
 boolean_t FWREC_CheckBkpFwKey(void);

 /**
  ** \brief Write the backup firmware record number key code into the reserved external flash memory location.
  ** \param[in] record_num   Pointer to the record num parameter.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_WriteBkpFwRecordNum(uint32_t *record_num);

 /**
  ** \brief Prepare the module to force the firmware update after loading the new hex file.
  */
 void FWREC_ForceImmediateUpdate(boolean_t immediate_update);

 /**
  ** \brief Returns the number of program retry num of the new firmware hex file.
  ** \param[out] retry_num   Pointer to retry number parmeter.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_ReadNewFwRetryNum(uint32_t *retry_num);

 /**
  ** \brief Write the number of program retry num of the new firmware hex file.
  ** \param[out] retry_num   Pointer to retry number parmeter.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_WriteNewFwRetryNum(uint32_t *retry_num);

 /**
  ** \brief Returns the number of record of the new firmware hex file.
  ** \param[out] retry_num   Pointer to record number parameter.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_ReadNewFwRecordNum(uint32_t *record_num);

 /**
  ** \brief Store the number of record of the new firmware hex file and reset the firmware program retry number.
  ** \param[out] retry_num   Pointer to record number parameter.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_WriteNewFwRecordNum(uint32_t *record_num);

 /**
  ** \brief Read a hex file record of the new firmware from the reserved external flash memory.
  ** \param[in] record       Pointer to the record data.
  ** \param[in] index        Position index of the record data.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_ReadNew(FWREC_Data_t *record, uint32_t index);

 /**
  ** \brief Read a hex file record of the backup firmware from the reserved external flash memory.
  ** \param[in] record       Pointer to the record data.
  ** \param[in] index        Position index of the record data.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_ReadBkp(FWREC_Data_t *record, uint32_t index);

 /**
  ** \brief Returns the number of record of the backup firmware hex file.
  ** \returns TRUE          If the operation is successfully accomplished
  ** \returns FALSE         If the operation could not be successfully accomplished
  */
 boolean_t FWREC_ReadBkpFwRecordNum(uint32_t *record_num);

 /**
  ** \brief Returns the checksum of a record.
  ** \param[in] record       Pointer to the record data.
  ** \returns TRUE          Record checksum
  */
 uint8_t FWREC_CalcChecksum(FWREC_Data_t *rec);

#endif /* INC_FIRMWAREUPDATE_FWRECORD_H_ */
