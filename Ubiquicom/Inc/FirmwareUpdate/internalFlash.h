/**
 * @project       uTag2UWB_Persona
 *
 * @Component     Internal Flash
 *
 * @file          internalFlash.h
 *
 * @brief         Brief
 *
 * @author        Author 
 *
 * @date          04nov2020
 *
 ***********************************************************************************************************************/
#ifndef INC_FIRMWAREUPDATE_INTERNALFLASH_H_
#define INC_FIRMWAREUPDATE_INTERNALFLASH_H_

#include "main.h"

/* Defines */
#define IFLASH_MEMORY_PAGE_SIZE_SHIFT   11U
#define IFLASH_MEMORY_PAGE_SIZE         (1U << (IFLASH_MEMORY_PAGE_SIZE_SHIFT))
#define IFLASH_MEMORY_PAGE_NUM_X_BANK   256

#define IFLASH_BANK1_START_ADDRESS      0x08000000U
#define IFLASH_BANK2_START_ADDRESS      (IFLASH_BANK1_START_ADDRESS + ((IFLASH_MEMORY_PAGE_NUM_X_BANK) * (IFLASH_MEMORY_PAGE_SIZE)))

#define IFLASH_MEMORY_START_ADDRESS     IFLASH_BANK1_START_ADDRESS
#define IFLASH_MEMORY_END_ADDRESS       (IFLASH_BANK2_START_ADDRESS + ((IFLASH_MEMORY_PAGE_NUM_X_BANK) * (IFLASH_MEMORY_PAGE_SIZE)))

/* Exported functions */

void IFLASH_Init(void);

boolean_t IFLASH_Erase(uint32_t start_addr);

boolean_t IFLASH_Write(uint32_t addr, uint64_t data[], uint64_t data_size);

void IFLASH_Read(uint32_t addr, uint32_t data[], uint32_t data_size);

#endif /* INC_FIRMWAREUPDATE_INTERNALFLASH_H_ */
