/**
 * @project       uTag2UWB_Persona
 *
 * @Component     info
 *
 * @file          info.h
 *
 * @brief         Brief
 *
 * @author        Author 
 *
 * @date          04nov2020
 *
 ***********************************************************************************************************************/
#ifndef INC_FIRMWAREUPDATE_INFO_C_
#define INC_FIRMWAREUPDATE_INFO_C_

#include <stdint.h>

#define APPLICATION_NO_BOOTLOADER

#define INFO_DATA_ADDRESS     0x08010000U

#if defined(APPLICATION_BOOTLOADER) || defined(BOOTLOADER)
#define INFO_PRJ_NAME_DEFAULT "Prj+Btl App"
#else
#define INFO_PRJ_NAME_DEFAULT "TagWiFiBle Prj"
#endif


#if (INFO_DATA_ADDRESS & 0x0000000FU)
#error "INFO: INFO_DATA_ADDRESS must have 16 bytes offset"
#endif

#define INFO_PRJ_NAME_LEN_MAX    16U
#define INFO_BUILD_LEN_MAX       32U

typedef struct
{
   char  ProjectName[INFO_PRJ_NAME_LEN_MAX];
   uint8_t Version;
   char  BuildDate[INFO_BUILD_LEN_MAX];
   char  BuildTime[INFO_BUILD_LEN_MAX];
   uint8_t  available[3U];
}__attribute__ ((__packed__))INFO_Data_t;


const INFO_Data_t *INFO_GetData(void);

#endif /* INC_FIRMWAREUPDATE_INFO_C_ */
