/**
 * @project       uTag2UWB_Persona
 *
 * @Component     Flash Partition
 *
 * @file          externFlashPartition.h
 *
 * @brief         This file contains the functions that are used to read, write and validate the the external
 *                flash memory partition.
 *
 * @author        Simone Bonforti
 *
 * @date          28oct2020
 *
 ***********************************************************************************************************************/
#ifndef INCLUDE_EXTERNFLASHPARTITION_H_
#define INCLUDE_EXTERNFLASHPARTITION_H_

#include "W25Q128FV.h"

/* Defines */

#define EFP_SECTOR_SIZE                EFLASH_SECTOR_SIZE
#if(EFP_SECTOR_SIZE == 4096)
#define EFP_SECTOR_SIZE_SHIFT          ( 12 )
#else
#error "Invalid EFP_SECTOR_SIZE"
#endif

#define EFP_START_ADDR                 ( 0x00 )
#define EFP_END_ADDR                   ( 0x7FFFFF )

#define EFP_FW_DATA_SECTORS_NUM        ( 512 )
#define EFP_FW_DATA_SIZE               ( EFP_FW_DATA_SECTORS_NUM * EFP_SECTOR_SIZE )
#define EFP_FW_DATA_START_ADDR         ( EFP_START_ADDR )
#define EFP_FW_DATA_END_ADDR           ( EFP_FW_DATA_START_ADDR + EFP_FW_DATA_SIZE )

#define EFP_FW_KEY_DATA_SECTORS_NUM    ( 1 )
#define EFP_FW_KEY_DATA_SIZE           ( EFP_FW_KEY_DATA_SECTORS_NUM * EFP_SECTOR_SIZE )
#define EFP_FW_KEY_DATA_START_ADDR     ( EFP_FW_DATA_END_ADDR )
#define EFP_FW_KEY_DATA_END_ADDR       ( EFP_FW_KEY_DATA_START_ADDR + EFP_FW_KEY_DATA_SIZE )

#define EFP_RESERVED_DATA_SECTORS_NUM  ( 1 )
#define EFP_RESERVED_DATA_SIZE         ( EFP_RESERVED_DATA_SECTORS_NUM * EFP_SECTOR_SIZE )
#define EFP_RESERVED_DATA_START_ADDR   ( EFP_FW_KEY_DATA_END_ADDR )
#define EFP_RESERVED_DATA_END_ADDR     ( EFP_RESERVED_DATA_START_ADDR + EFP_RESERVED_DATA_SIZE )

#define EFP_CONF_DATA_SECTORS_NUM      ( 2 )
#define EFP_CONF_DATA_SIZE             ( EFP_CONF_DATA_SECTORS_NUM * EFP_SECTOR_SIZE )
#define EFP_CONF_DATA_START_ADDR       ( EFP_RESERVED_DATA_END_ADDR )
#define EFP_CONF_DATA_END_ADDR         ( EFP_CONF_DATA_START_ADDR + EFP_CONF_DATA_SIZE )

#define EFP_SN_DATA_SECTORS_NUM        ( 1 )
#define EFP_SN_DATA_SIZE               ( EFP_SN_DATA_SECTORS_NUM * EFP_SECTOR_SIZE )
#define EFP_SN_DATA_START_ADDR         ( EFP_CONF_DATA_END_ADDR )
#define EFP_SN_DATA_END_ADDR           ( EFP_SN_DATA_START_ADDR + EFP_SN_DATA_SIZE )

#define EFP_APP_DATA_SECTORS_NUM       ( EFLASH_SECTOR_NUM - ( ( EFP_FW_DATA_SECTORS_NUM ) + ( EFP_FW_KEY_DATA_SECTORS_NUM )  + ( EFP_RESERVED_DATA_SECTORS_NUM ) + ( EFP_CONF_DATA_SECTORS_NUM ) + ( EFP_SN_DATA_SECTORS_NUM ) ) )
#define EFP_APP_DATA_SIZE              ( EFP_APP_DATA_SECTORS_NUM * EFP_SECTOR_SIZE )
#define EFP_APP_DATA_START_ADDR        ( EFP_SN_DATA_END_ADDR )
#define EFP_APP_DATA_END_ADDR          ( EFP_APP_DATA_START_ADDR + EFP_APP_DATA_SIZE )



#if (EFP_APP_DATA_SECTORS_NUM < 0)
#error "Invalid Partition configuration"
#endif

#define EFP_DATA_TYPE_NUM              EFP_INVALID_TYPE

/* Exported types */

typedef enum
{
   EFP_FIRMWARE_TYPE = 0,     	/**< Firmware update data type partition - 0 */
   EFP_FW_KEY_TYPE,        		/**< Firmware keys data type partition 	 - 1 */
   EFP_RESERVED_TYPE,      		/**< reserved partition 				 - 2 */
   EFP_APP_TYPE,           		/**< Application data type partition 	 - 3 */
   EFP_CONF_TYPE,				/**< Configuration data type partition 	 - 4 */
   EFP_SN_TYPE,					/**< Serial Numb data type partition 	 - 5 */
   EFP_INVALID_TYPE
}EFP_Data_t;

/* Exported functions */

boolean_t EFP_WriteData(EFP_Data_t data_type, void * data, uint32_t data_size, uint32_t addr);
boolean_t EFP_ReadData(EFP_Data_t data_type,  void * data, uint32_t data_size, uint32_t addr);

#endif /* INCLUDE_EXTERNFLASHPARTITION_H_ */
