#include "tagManager.h"
#include "deca_device_api.h"
#include <port.h>
#include <stdio.h>

static uint16_t tagId = 0;

error_e InitTagManager() {
    error_e res = _NO_ERR;

    set_dw_spi_slow_rate(DW_A); 

    dwt_initialise(DWT_LOADUCODE);

    tagId   = (uint16_t)dwt_getpartid();// ((uint64_t)dwt_getlotid()<<32) + dwt_getpartid(); //valid after dwt_initialise()
    if(!tagId) {
      res = _Err;

    }
//    printf("tagId %d \r\n",tagId);

    return res;
}

uint16_t GetTagId() {
    return tagId;     
}
