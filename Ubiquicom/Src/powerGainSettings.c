#include <powerGainSettings.h>

/* configure power 
	Coarse Gain (3bits)+ fine grain (5 bits) =  xxx00000 | 000xxxxx
	e.g. 8.5 db gain = 6db + 2.5 db = 100 00000 | 000 101 = 100 00 101 = 0x85
*/	
	
    /* CoarseGain setting
    00000000 = 18 dB gain 
    00000001 = 15 dB gain 
    00000010 = 12 dB gain 
    00000011 = 9 dB gain 
    00000100 = 6 dB gain 
    00000101 = 3 dB gain 
    00000110 = 0 dB gain 
    00000111 = OFF – No output
    */
    /* FineGain setting
    00000000 = 0.0 dB gain 
    00000001 = 0.5 dB gain 
    00000010 = 1.0 dB gain 
    00000011 = 1.5 dB gain 
    00000100 = 2.0 dB gain 
    00000101 = 2.5 dB gain 
    00000110 = 3.0 dB gain 
	00000111 = 3.5 dB gain 
	00001000 = 4.0 dB gain 
	00001001 = 4.5 dB gain 
    ........ 
    00011010 = 13.0 dB gain 
    00011011 = 13.5 dB gain 
    00011100 = 14.0 dB gain 
    00011101 = 14.5 dB gain 
    00011110 = 15.0 dB gain 
    00011111 = 15.5 dB gain
    */

//static initialization: 
//0xC0 -> 0.0 db	
//0xC1 -> 0.5 db	
//0xC5 -> 2.5 db	
//0x85 -> 8.5 db	(suggested from Decawave)	
//0x89 -> 10.5 db		
static uint32_t powerGainSett = 0x85; //0x89; //0xC0;//0x89;//0x85; 
uint32_t get_powerGainSettings()
{	
      return powerGainSett;
}

void init_powerGainSettings()
{   
    uint8_t coarseGain = 0b00000000; // 18db
	uint8_t fineGain = 0b00011111; // 15.5db
	
	powerGainSett = (coarseGain<<5) | fineGain;       
    
}
