
/*****************************************************************************
 *
 *
 *
 *
 *
 ****************************************************************************/
//#include "debug_log.h"
#include <string.h>
#include <stdbool.h>
#include "BGLIB_Module.h"
#include "BLE_Protocol.h"
#include "cmsis_os.h"

#include "cmd_def.h"
#include "appConfig.h"
#include "fwRecord.h"
#include "externFlashPartition.h"

//#define DBG_ENTRY(value)  HAL_UART_Transmit_IT(&huart5, &value, sizeof(value));
#define DBG_ENTRY(value)  HAL_UART_Transmit(&huart5, &value, sizeof(value), 1000);

extern osSemaphoreId OS_EventID_BGLIB_FWUP_Handler;


extern bool isFWRecordValid;

uint8_t UpdateDataBuffer[2][50] = { 0 };
uint8_t ToggleBuffer = 0;
uint8_t CounterChar[2] = { 0 };
uint16_t TotalChars = 0;

bool isRowComplete = false;

__weak void ble_default(const void*v)
{
    //DBG_ENTRY("ble_default");
    //DBG_ENTRY("");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_reset(const void* nul)
{
	//DBG_ENTRY("ble_rsp_system_reset");
	//DBG_ENTRY("");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_hello(const void* nul)
{
	//DBG_ENTRY("ble_rsp_system_hello");

	//ble_cmd_system_reset(0);
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_address_get(const struct ble_msg_system_address_get_rsp_t *msg)
{
	  //DBG_ENTRY("ble_rsp_system_address_get");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_reg_write(const struct ble_msg_system_reg_write_rsp_t *msg)
{
	 //DBG_ENTRY("ble_rsp_system_reg_write");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_reg_read(const struct ble_msg_system_reg_read_rsp_t *msg)
{
	 //DBG_ENTRY("ble_rsp_system_reg_read");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_get_counters(const struct ble_msg_system_get_counters_rsp_t *msg)
{
	  //DBG_ENTRY("ble_rsp_system_get_counters");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_get_connections(const struct ble_msg_system_get_connections_rsp_t *msg)
{
	  //DBG_ENTRY("ble_rsp_system_get_connections");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_read_memory(const struct ble_msg_system_read_memory_rsp_t *msg)
{
	  //DBG_ENTRY("ble_rsp_system_read_memory");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_get_info(const struct ble_msg_system_get_info_rsp_t *msg)
{
	 //DBG_ENTRY("ble_rsp_system_get_info");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_endpoint_tx(const struct ble_msg_system_endpoint_tx_rsp_t *msg)
{
	  //DBG_ENTRY("ble_rsp_system_endpoint_tx");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_whitelist_append(const struct ble_msg_system_whitelist_append_rsp_t *msg)
{
	  //DBG_ENTRY("ble_rsp_system_whitelist_append");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_whitelist_remove(const struct ble_msg_system_whitelist_remove_rsp_t *msg)
{
	  //DBG_ENTRY("ble_rsp_system_whitelist_remove");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_whitelist_clear(const void* nul)
{
	  //DBG_ENTRY("ble_rsp_system_whitelist_clear");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_endpoint_rx(const struct ble_msg_system_endpoint_rx_rsp_t *msg)
{
	  //DBG_ENTRY("ble_rsp_system_endpoint_rx");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_endpoint_set_watermarks(const struct ble_msg_system_endpoint_set_watermarks_rsp_t *msg)
{
	  //DBG_ENTRY("ble_rsp_system_endpoint_set_watermarks");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_aes_setkey(const void* nul)
{
	  //DBG_ENTRY("ble_rsp_system_aes_setkey");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_aes_encrypt(const struct ble_msg_system_aes_encrypt_rsp_t *msg)
{
	 //DBG_ENTRY("ble_rsp_system_aes_encrypt");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_system_aes_decrypt(const struct ble_msg_system_aes_decrypt_rsp_t *msg)
{
	  //DBG_ENTRY("ble_rsp_system_aes_decrypt");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_flash_ps_defrag(const void* nul)
{
	  //DBG_ENTRY("ble_rsp_flash_ps_defrag");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_flash_ps_dump(const void* nul)
{
	//DBG_ENTRY("ble_rsp_flash_ps_dump");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_flash_ps_erase_all(const void* nul)
{
	//DBG_ENTRY("ble_rsp_flash_ps_erase_all");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_flash_ps_save(const struct ble_msg_flash_ps_save_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_flash_ps_save");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_flash_ps_load(const struct ble_msg_flash_ps_load_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_flash_ps_load");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_flash_ps_erase(const void* nul)
{
	//DBG_ENTRY("ble_rsp_flash_ps_erase");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_flash_erase_page(const struct ble_msg_flash_erase_page_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_flash_erase_page");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_flash_write_data(const struct ble_msg_flash_write_data_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_flash_write_data");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_flash_read_data(const struct ble_msg_flash_read_data_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_flash_read_data");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attributes_write(const struct ble_msg_attributes_write_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attributes_write");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attributes_read(const struct ble_msg_attributes_read_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attributes_read");
}

__weak void ble_rsp_attributes_read_type(const struct ble_msg_attributes_read_type_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attributes_read_type");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attributes_user_read_response(const void* nul)
{
	//DBG_ENTRY("ble_rsp_attributes_user_read_response");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attributes_user_write_response(const void* nul)
{
	//DBG_ENTRY("ble_rsp_attributes_user_write_response");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attributes_send(const struct ble_msg_attributes_send_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attributes_send");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_connection_disconnect(const struct ble_msg_connection_disconnect_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_connection_disconnect");

	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_connection_get_rssi(const struct ble_msg_connection_get_rssi_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_connection_get_rssi");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_connection_update(const struct ble_msg_connection_update_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_connection_update");
	
	// ##CC DBG_EXIT("");    
}

__weak void ble_rsp_connection_version_update(const struct ble_msg_connection_version_update_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_connection_version_update");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_connection_channel_map_get(const struct ble_msg_connection_channel_map_get_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_connection_channel_map_get");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_connection_channel_map_set(const struct ble_msg_connection_channel_map_set_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_connection_channel_map_set");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_connection_features_get(const struct ble_msg_connection_features_get_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_connection_features_get");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_connection_get_status(const struct ble_msg_connection_get_status_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_connection_get_status");
	return;
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_connection_raw_tx(const struct ble_msg_connection_raw_tx_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_connection_raw_tx");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_connection_slave_latency_disable(const struct ble_msg_connection_slave_latency_disable_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_connection_slave_latency_disable");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attclient_find_by_type_value(const struct ble_msg_attclient_find_by_type_value_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_find_by_type_value");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attclient_read_by_group_type(const struct ble_msg_attclient_read_by_group_type_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_read_by_group_type");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attclient_read_by_type(const struct ble_msg_attclient_read_by_type_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_read_by_type");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attclient_find_information(const struct ble_msg_attclient_find_information_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_find_information");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attclient_read_by_handle(const struct ble_msg_attclient_read_by_handle_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_read_by_handle");
	// ##CC DBG_INFO("result: %d", msg->result);
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attclient_attribute_write(const struct ble_msg_attclient_attribute_write_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_attribute_write");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attclient_write_command(const struct ble_msg_attclient_write_command_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_write_command");
	// ##CC DBG_INFO("result: %d", msg->result);
//	ble_cmd_attclient_read_by_handle(0, 21);
	// ##CC DBG_EXIT("");
	
}

__weak void ble_rsp_attclient_indicate_confirm(const struct ble_msg_attclient_indicate_confirm_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_indicate_confirm");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attclient_read_long(const struct ble_msg_attclient_read_long_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_read_long");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attclient_prepare_write(const struct ble_msg_attclient_prepare_write_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_prepare_write");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attclient_execute_write(const struct ble_msg_attclient_execute_write_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_execute_write");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_attclient_read_multiple(const struct ble_msg_attclient_read_multiple_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_attclient_read_multiple");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_sm_encrypt_start(const struct ble_msg_sm_encrypt_start_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_sm_encrypt_start");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_sm_set_bondable_mode(const void* nul)
{
	//DBG_ENTRY("ble_rsp_sm_set_bondable_mode");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_sm_delete_bonding(const struct ble_msg_sm_delete_bonding_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_sm_delete_bonding");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_sm_set_parameters(const void* nul)
{
	//DBG_ENTRY("ble_rsp_sm_set_parameters");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_sm_passkey_entry(const struct ble_msg_sm_passkey_entry_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_sm_passkey_entry");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_sm_get_bonds(const struct ble_msg_sm_get_bonds_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_sm_get_bonds");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_sm_set_oob_data(const void* nul)
{
	//DBG_ENTRY("ble_rsp_sm_set_oob_data");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_sm_whitelist_bonds(const struct ble_msg_sm_whitelist_bonds_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_sm_whitelist_bonds");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_sm_set_pairing_distribution_keys(const struct ble_msg_sm_set_pairing_distribution_keys_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_sm_set_pairing_distribution_keys");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_gap_set_privacy_flags(const void* nul)
{
	//DBG_ENTRY("ble_rsp_gap_set_privacy_flags");
	
	// ##CC DBG_EXIT("");
}

void ble_rsp_gap_set_mode(const struct ble_msg_gap_set_mode_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_set_mode");

#ifdef BLE_DBG
    // ##CC DBG_INFO("Result message: %d", msg->result);
#endif    
	
	// ##CC DBG_EXIT("");
}

//MYMOD: VIENE CHIAMATA ALL'AVVIO DELLA SCANSIONE
__weak void ble_rsp_gap_discover(const struct ble_msg_gap_discover_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_discover");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_gap_connect_direct(const struct ble_msg_gap_connect_direct_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_connect_direct");
	
	// ##CC DBG_EXIT("");
}

//MYMOD: VIENE CHIAMATA ALLA FINE DELLA SCANSIONE
__weak void ble_rsp_gap_end_procedure(const struct ble_msg_gap_end_procedure_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_end_procedure");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_gap_connect_selective(const struct ble_msg_gap_connect_selective_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_connect_selective");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_gap_set_filtering(const struct ble_msg_gap_set_filtering_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_set_filtering");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_gap_set_scan_parameters(const struct ble_msg_gap_set_scan_parameters_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_set_scan_parameters");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_gap_set_adv_parameters(const struct ble_msg_gap_set_adv_parameters_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_set_adv_parameters");
    
#ifdef BLE_DBG
    // ##CC DBG_INFO("Result message: %d", msg->result);
#endif 	
    
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_gap_set_adv_data(const struct ble_msg_gap_set_adv_data_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_set_adv_data");
	
#ifdef BLE_DBG
    // ##CC DBG_INFO("Result message: %d", msg->result);
#endif    
    
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_gap_set_directed_connectable_mode(const struct ble_msg_gap_set_directed_connectable_mode_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_set_directed_connectable_mode");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_gap_set_initiating_con_parameters(const struct ble_msg_gap_set_initiating_con_parameters_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_set_initiating_con_parameters");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_gap_set_nonresolvable_address(const struct ble_msg_gap_set_nonresolvable_address_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_gap_set_nonresolvable_address");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_io_port_config_irq(const struct ble_msg_hardware_io_port_config_irq_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_io_port_config_irq");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_set_soft_timer(const struct ble_msg_hardware_set_soft_timer_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_set_soft_timer");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_adc_read(const struct ble_msg_hardware_adc_read_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_adc_read");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_io_port_config_direction(const struct ble_msg_hardware_io_port_config_direction_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_io_port_config_direction");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_io_port_config_function(const struct ble_msg_hardware_io_port_config_function_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_io_port_config_function");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_io_port_config_pull(const struct ble_msg_hardware_io_port_config_pull_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_io_port_config_pull");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_io_port_write(const struct ble_msg_hardware_io_port_write_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_io_port_write");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_io_port_read(const struct ble_msg_hardware_io_port_read_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_io_port_read");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_spi_config(const struct ble_msg_hardware_spi_config_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_spi_config");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_spi_transfer(const struct ble_msg_hardware_spi_transfer_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_spi_transfer");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_i2c_read(const struct ble_msg_hardware_i2c_read_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_i2c_read");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_i2c_write(const struct ble_msg_hardware_i2c_write_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_i2c_write");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_set_txpower(const void* nul)
{
	//DBG_ENTRY("ble_rsp_hardware_set_txpower");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_timer_comparator(const struct ble_msg_hardware_timer_comparator_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_timer_comparator");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_io_port_irq_enable(const struct ble_msg_hardware_io_port_irq_enable_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_io_port_irq_enable");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_io_port_irq_direction(const struct ble_msg_hardware_io_port_irq_direction_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_io_port_irq_direction");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_analog_comparator_enable(const void* nul)
{
	//DBG_ENTRY("ble_rsp_hardware_analog_comparator_enable");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_analog_comparator_read(const struct ble_msg_hardware_analog_comparator_read_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_analog_comparator_read");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_analog_comparator_config_irq(const struct ble_msg_hardware_analog_comparator_config_irq_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_analog_comparator_config_irq");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_set_rxgain(const void* nul)
{
	//DBG_ENTRY("ble_rsp_hardware_set_rxgain");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_usb_enable(const struct ble_msg_hardware_usb_enable_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_usb_enable");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_hardware_sleep_enable(const struct ble_msg_hardware_sleep_enable_rsp_t *msg)
{
	//DBG_ENTRY("ble_rsp_hardware_sleep_enable");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_test_phy_tx(const void* nul)
{
	//DBG_ENTRY("ble_rsp_test_phy_tx");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_test_phy_rx(const void* nul)
{
	//DBG_ENTRY("ble_rsp_test_phy_rx");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_test_phy_end(const struct ble_msg_test_phy_end_rsp_t *msg)
{
//DBG_ENTRY("ble_rsp_test_phy_end");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_test_phy_reset(const void* nul)
{
//DBG_ENTRY("ble_rsp_test_phy_reset");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_test_get_channel_map(const struct ble_msg_test_get_channel_map_rsp_t *msg)
{
//DBG_ENTRY("ble_rsp_test_get_channel_map");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_test_debug(const struct ble_msg_test_debug_rsp_t *msg)
{
    //DBG_ENTRY("ble_rsp_test_debug");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_test_channel_mode(const void* nul)
{
//DBG_ENTRY("ble_rsp_test_channel_mode");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_dfu_reset(const void* nul)
{
//DBG_ENTRY("ble_rsp_dfu_reset");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_dfu_flash_set_address(const struct ble_msg_dfu_flash_set_address_rsp_t *msg)
{
//DBG_ENTRY("ble_rsp_dfu_flash_set_address");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_dfu_flash_upload(const struct ble_msg_dfu_flash_upload_rsp_t *msg)
{
//DBG_ENTRY("ble_rsp_dfu_flash_upload");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_rsp_dfu_flash_upload_finish(const struct ble_msg_dfu_flash_upload_finish_rsp_t *msg)
{
//DBG_ENTRY("ble_rsp_dfu_flash_upload_finish");
	
	// ##CC DBG_EXIT("");
}

 //MY_MOD: viene chiamata all'avvio del sistema
__weak void ble_evt_system_boot(const struct ble_msg_system_boot_evt_t *msg)
{
	//DBG_ENTRY("ble_evt_system_boot");
	//BGLIB_Init();
	//BGLIB_DBG_AdvertisingStart();


	// ##CC DBG_EXIT("");
}

__weak void ble_evt_system_debug(const struct ble_msg_system_debug_evt_t *msg)
{
	//DBG_ENTRY("ble_evt_system_debug");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_system_endpoint_watermark_rx(const struct ble_msg_system_endpoint_watermark_rx_evt_t *msg)
{
//DBG_ENTRY("ble_evt_system_endpoint_watermark_rx");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_system_endpoint_watermark_tx(const struct ble_msg_system_endpoint_watermark_tx_evt_t *msg)
{
//DBG_ENTRY("ble_evt_system_endpoint_watermark_tx");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_system_script_failure(const struct ble_msg_system_script_failure_evt_t *msg)
{
//DBG_ENTRY("ble_evt_system_script_failure");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_system_no_license_key(const void* nul)
{
//DBG_ENTRY("ble_evt_system_no_license_key");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_system_protocol_error(const struct ble_msg_system_protocol_error_evt_t *msg)
{
//DBG_ENTRY("ble_evt_system_protocol_error");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_flash_ps_key(const struct ble_msg_flash_ps_key_evt_t *msg)
{
//DBG_ENTRY("ble_evt_flash_ps_key");
	
	// ##CC DBG_EXIT("");
}

bool getRowComplete(void)
{
	return isRowComplete;
}


void setRowComplete(bool status)
{
	isRowComplete = status;
}


__weak void ble_evt_attributes_value(const struct ble_msg_attributes_value_evt_t *msg)
{
   // ##SB
   uint8_t pBuffer[BLE_PACKET_MAX_SIZE] = { 0 };
   uint8_t pValueBuffer[BLE_RETURN_MAX_SIZE] = { 0 };
   uint8_t pMsgLen = 0;
   static uint8_t bleIndex = 0;

   if( false == isFWRecordValid )
   {
	   isFWRecordValid = true;
	   BLE_buildPacket(pValueBuffer, &pMsgLen, msg->value.data, errorFWUpgrade  , pBuffer, 2);
	   ble_cmd_attributes_send(NOTIFY_ATTRIBUTE_ALL, PARAMETER_DATA_HANDLE, pMsgLen + 2, pValueBuffer);
   }
   else if( ( msg->value.data[CMD_TYPE_IDX] < (uint8_t) cmdStartUpgradeFwReq ) )
   {
	   /* verifica che la len del messaggio ricevuto sia uguale alla len del messaggio inviato via BLE. */
	   if( VALID == BLE_verifyPacket (msg))
	   {
		  switch(msg->value.data[CMD_TYPE_IDX])
		  {
			  case cmdParamReadRequest:
			  {
				 /*da usare nel pacchetto di notify*/

				 configManager(pBuffer, BLE_PACKET_MAX_SIZE, (ConfigParamId_t) msg->value.data[CMD_PARAM_IDX], read);

				 BLE_buildPacket(pValueBuffer, &pMsgLen, msg->value.data, cmdParamReadResponse , pBuffer, 2);

				 ble_cmd_attributes_send(NOTIFY_ATTRIBUTE_ALL, PARAMETER_DATA_HANDLE, pMsgLen + 2, pValueBuffer);

				 break;
			  }
			  case cmdParamWriteRequest:
			  {

				  /*da usare nel pacchetto di notify*/
				  configManager(&msg->value.data[CMD_FIRST_BYTE_IDX], 16, (ConfigParamId_t) msg->value.data[CMD_PARAM_IDX], write);

				  configManager(pBuffer, BLE_PACKET_MAX_SIZE, (ConfigParamId_t) msg->value.data[CMD_PARAM_IDX], read);

				  BLE_buildPacket(pValueBuffer, &pMsgLen, msg->value.data, cmdParamWriteResponse , pBuffer, 2);
				  ble_cmd_attributes_send(NOTIFY_ATTRIBUTE_ALL, PARAMETER_DATA_HANDLE, pMsgLen + 2, pValueBuffer);
				  break;
			  }
			  case cmdSaveFlashRequest:
			  {

				  // saveFlashResponse /*da usare nel pacchetto di notify*/
				  /*
				   * Comando che l'app deve inviare al DEVICE per confermare che si vuole effettivamente salvare la nuova
				   * configurazione parametri.
				   * La funzione salvera' in FLASH i dati
				   * */

				  writeConfigIntoFlash(getConfigParam());


				  /* Implementare il vero salvataggio in FLASH */
				  BLE_buildPacket(pValueBuffer, &pMsgLen, msg->value.data, cmdSaveFlashResponse , pBuffer, 2);
				  ble_cmd_attributes_send(NOTIFY_ATTRIBUTE_ALL, PARAMETER_DATA_HANDLE, pMsgLen + 2, pValueBuffer);


				  break;
			  }
			  case cmdAbortSavingRequest:
			  {
				 // abortSavingResponse /*da usare nel pacchetto di notify*/
				  /*
				   * Comando che l'app deve inviare al DEVICE per ANNULLARE il salvataggio dei nuovi parametri inviati.
				   * Usata in caso di  Mancata conferma di Salvataggio/uscita dalla UI di configurazione parametri.
				   * In questo caso, verranno ristabiliti i valori precedentemente salvati in FLASH/quelli di default.
				   * */


				  restoreConfig();
				  /* Restore dei parametri in FLASH al passo precedente */

				  BLE_buildPacket(pValueBuffer, &pMsgLen, msg->value.data, cmdAbortSavingResponse , pBuffer, 2);
				  ble_cmd_attributes_send(NOTIFY_ATTRIBUTE_ALL, PARAMETER_DATA_HANDLE, pMsgLen + 2, pValueBuffer);

				 break;
			  }
			  default:
			  {

				 break;
			  }
		  }
	   }
	   else
	   {
			/*da usare nel pacchetto di notify*/
		   BLE_buildPacket(pValueBuffer, &pMsgLen, msg->value.data, errorParam , NULL, 0);
		   ble_cmd_attributes_send(NOTIFY_ATTRIBUTE_ALL, PARAMETER_DATA_HANDLE, pMsgLen + 2, pValueBuffer);
	   }
   }
   else
   {
	  switch(msg->value.data[CMD_TYPE_IDX])
	  {
		  case cmdStartUpgradeFwReq:
		  {
			  BLE_buildPacket(pValueBuffer, &pMsgLen, msg->value.data, cmdStartUpgradeFwResp , pBuffer, 2);
			  ble_cmd_attributes_send(NOTIFY_ATTRIBUTE_ALL, PARAMETER_DATA_HANDLE, pMsgLen + 2, pValueBuffer);

			 break;
		  }
		  case cmdChunkFWFileReq:
		  {
			  for( uint8_t index = 0; index < msg->value.len; index ++ )
			  {
				UpdateDataBuffer[ToggleBuffer][CounterChar[ToggleBuffer]] = msg->value.data[index];

				switch(msg->value.data[index])
				{
					case '\n':
					{
					   memset( &UpdateDataBuffer[ToggleBuffer][CounterChar[ToggleBuffer]], 0, ( 50 - CounterChar[ToggleBuffer] ) );

					   ToggleBuffer ^= 1;
					   CounterChar[ToggleBuffer] = 0;
					   isRowComplete = true;
					   break;
					}
					case ':':
					default:
					{
					   CounterChar[ToggleBuffer] = ( CounterChar[ToggleBuffer] + 1 );
					   break;
				}
				}
			  }

			if( CounterChar[ToggleBuffer] >= 50)
			{
				CounterChar[ToggleBuffer] = 0;
				BLE_buildPacket(pValueBuffer, &pMsgLen, msg->value.data, errorFWUpgrade  , pBuffer, 2);
				ble_cmd_attributes_send(NOTIFY_ATTRIBUTE_ALL, PARAMETER_DATA_HANDLE, pMsgLen + 2, pValueBuffer);
			}
			else
			{
				BLE_buildPacket(pValueBuffer, &pMsgLen, msg->value.data, cmdChunkFWFileResp  , pBuffer, 2);
				ble_cmd_attributes_send(NOTIFY_ATTRIBUTE_ALL, PARAMETER_DATA_HANDLE, pMsgLen + 2, pValueBuffer);
			}


			 break;
		  }
		  case cmdEndUpgradeFwReq:
		  {
			  if( false == isFWRecordValid )
			  {
				isFWRecordValid = true;
				BLE_buildPacket(pValueBuffer, &pMsgLen, msg->value.data, errorFWUpgrade  , pBuffer, 2);
				ble_cmd_attributes_send(NOTIFY_ATTRIBUTE_ALL, PARAMETER_DATA_HANDLE, pMsgLen + 2, pValueBuffer);
			  }
			  else
			  {
				BLE_buildPacket(pValueBuffer, &pMsgLen, msg->value.data, cmdEndUpgradeFwResp , pBuffer, 2);
				ble_cmd_attributes_send(NOTIFY_ATTRIBUTE_ALL, PARAMETER_DATA_HANDLE, pMsgLen + 2, pValueBuffer);
			  }
			 break;
		  }
	  }

   }

}

__weak void ble_evt_attributes_user_read_request(const struct ble_msg_attributes_user_read_request_evt_t *msg)
{
//DBG_ENTRY("ble_evt_attributes_user_read_request");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_attributes_status(const struct ble_msg_attributes_status_evt_t *msg)
{
//DBG_ENTRY("ble_evt_attributes_status");
	
	// ##CC DBG_EXIT("");
}
uint8_t stringsss[150];
__weak void ble_evt_connection_status(const struct ble_msg_connection_status_evt_t *msg)
{
//DBG_ENTRY("");


//snprintf(stringsss, sizeof(stringsss) ,"ble_evt_connection_status Parametri -> Connection: %d;\n Flags: %d; \n Address: %d;\n Address type: %d;\n ConnInterval: %d; \n Timeout: %d; \n Latency: %d; \n Bonding: %d \n",msg->connection,msg->flags, msg->address,msg->address_type, msg->conn_interval, msg->timeout,msg->latency, msg->bonding);



//", msg->connection,msg->flags, msg->address,msg->address_type, msg->conn_interval, msg->timeout,msg->latency, msg->bonding);
//DBG_ENTRY(stringsss);
	return; // ##SB connect status
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_connection_version_ind(const struct ble_msg_connection_version_ind_evt_t *msg)
{


	//snprintf(stringsss, sizeof(stringsss) ,"ble_evt_connection_version_ind Parametri -> Connection: %d;\n Version Num: %d; \n comp_id: %d;\n sub_vers_nr: %d;\n ",msg->connection,msg->vers_nr, msg->comp_id,msg->sub_vers_nr);

 //DBG_ENTRY(stringsss);
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_connection_feature_ind(const struct ble_msg_connection_feature_ind_evt_t *msg)
{
	//snprintf(stringsss, sizeof(stringsss) ,"ble_evt_connection_feature_ind Parametri -> Connection: %d;\n  ",msg->connection);

 //DBG_ENTRY(stringsss);
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_connection_raw_rx(const struct ble_msg_connection_raw_rx_evt_t *msg)
{
//DBG_ENTRY("ble_evt_connection_raw_rx");
	
	// ##CC DBG_EXIT("");
}
uint8_t isDeviceDisconnected = 0;
__weak void ble_evt_connection_disconnected(const struct ble_msg_connection_disconnected_evt_t *msg)
{
	//BGLIB_DBG_AdvertisingStop();
	//BGLIB_DeInit();
	//

	//ble_cmd_connection_disconnect(msg->connection);
	BGLIB_DBG_AdvertisingStart();
	//ble_cmd_system_reset(0);
//	memset(stringsss, 0, sizeof(stringsss));
//	snprintf(stringsss, sizeof(stringsss) ,"ble_evt_connection_disconnected Parametri -> Connection: %d;\n  Reason: %d ",msg->connection,  msg->reason);
//
//    DBG_ENTRY(stringsss);
//	BGLIB_Init();

	// ##SB disconnected
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_attclient_indicated(const struct ble_msg_attclient_indicated_evt_t *msg)
{
//DBG_ENTRY("ble_evt_attclient_indicated");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_attclient_procedure_completed(const struct ble_msg_attclient_procedure_completed_evt_t *msg)
{
//DBG_ENTRY("ble_evt_attclient_procedure_completed");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_attclient_group_found(const struct ble_msg_attclient_group_found_evt_t *msg)
{
//DBG_ENTRY("ble_evt_attclient_group_found");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_attclient_attribute_found(const struct ble_msg_attclient_attribute_found_evt_t *msg)
{
//DBG_ENTRY("ble_evt_attclient_attribute_found");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_attclient_find_information_found(const struct ble_msg_attclient_find_information_found_evt_t *msg)
{
//DBG_ENTRY("ble_evt_attclient_find_information_found");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_attclient_attribute_value(const struct ble_msg_attclient_attribute_value_evt_t *msg)
{
//DBG_ENTRY("ble_evt_attclient_attribute_value");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_attclient_read_multiple_response(const struct ble_msg_attclient_read_multiple_response_evt_t *msg)
{
//DBG_ENTRY("ble_evt_attclient_read_multiple_response");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_sm_smp_data(const struct ble_msg_sm_smp_data_evt_t *msg)
{
//DBG_ENTRY("ble_evt_sm_smp_data");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_sm_bonding_fail(const struct ble_msg_sm_bonding_fail_evt_t *msg)
{
//DBG_ENTRY("ble_evt_sm_bonding_fail");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_sm_passkey_display(const struct ble_msg_sm_passkey_display_evt_t *msg)
{
//DBG_ENTRY("ble_evt_sm_passkey_display");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_sm_passkey_request(const struct ble_msg_sm_passkey_request_evt_t *msg)
{
//DBG_ENTRY("ble_evt_sm_passkey_request");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_sm_bond_status(const struct ble_msg_sm_bond_status_evt_t *msg)
{
//DBG_ENTRY("ble_evt_sm_bond_status");
	
	// ##CC DBG_EXIT("");
}

//MYMOD: RISPOSTA ALLA SCANSIONE
__weak void ble_evt_gap_scan_response(const struct ble_msg_gap_scan_response_evt_t *msg)
{
	//DBG_ENTRY("ble_evt_gap_scan_response");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_gap_mode_changed(const struct ble_msg_gap_mode_changed_evt_t *msg)
{
//DBG_ENTRY("ble_evt_gap_mode_changed");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_hardware_io_port_status(const struct ble_msg_hardware_io_port_status_evt_t *msg)
{
	//DBG_ENTRY("PROVA...");

	//DBG_ENTRY(	ble_evt_hardware_io_port_status);
//				"hardware_io_port_status API event is triggered immediately, since it's handled as a normal GPIO"
//				"interrupt. You should expect this event to occur and either handle it or ignore it intentionally if you are"
//				"using external control via the BGAPI protocol.");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_hardware_soft_timer(const struct ble_msg_hardware_soft_timer_evt_t *msg)
{
//DBG_ENTRY("ble_evt_hardware_soft_timer");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_hardware_adc_result(const struct ble_msg_hardware_adc_result_evt_t *msg)
{
//DBG_ENTRY("ble_evt_hardware_adc_result");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_hardware_analog_comparator_status(const struct ble_msg_hardware_analog_comparator_status_evt_t *msg)
{
//DBG_ENTRY("ble_evt_hardware_analog_comparator_status");
	
	// ##CC DBG_EXIT("");
}

__weak void ble_evt_dfu_boot(const struct ble_msg_dfu_boot_evt_t *msg)
{
//DBG_ENTRY("ble_evt_dfu_boot");
	
	// ##CC DBG_EXIT("");
}

