/**
 * @project       uTag2UWB_Persona
 *
 * @Component     Firmware record
 *
 * @file          fwRecord.c
 *
 * @brief         This file contains the functions used to create records for the firmware update.
 *
 * @author        Author 
 *
 * @date          04nov2020
 *
 ***********************************************************************************************************************/

#include <stdio.h>
#include <string.h>

#include "info.h"
#include "usart.h"
#include "externFlashPartition.h"
#include "fwRecord.h"

/* Private defines */
#define FWREC_DATA_BLOCK_SIZE    16


#define FWREC_VALIDATE_ADDRESS(addr)      ( ( addr ) == ( addr ) )

#define FWREC_TYPE_VALIDATION_MASK        ( ( 1U << FWREC_TYPE_DATA ) | \
                                          ( 1U << FWREC_TYPE_EOF ) | \
                                          ( 1U << FWREC_TYPE_EXT_SEG_ADDRESS ) | \
                                          ( 1U << FWREC_TYPE_EXT_LIN_ADDRESS ) | \
                                          ( 1U << FWREC_TYPE_START_LIN_ADDRESS ) )

#define FWREC_VALIDATE_TYPE(type)         ( ( 1U << ( type ) ) & FWREC_TYPE_VALIDATION_MASK )

#if ( VECT_TAB_OFFSET > 0 )
#warning "VECT_TAB_OFFSET value is not 0!!!"
#endif

/* Private variables */
static FWREC_Key_t FWREC_Key = {0};

static uint32_t   FWREC_StoredRecordNum = 0U;
static boolean_t FWREC_NewFwValidationPending = FALSE;
static boolean_t FWREC_Error = FALSE;
static boolean_t FWREC_ImmediateUpdate = FALSE;
static uint8_t FWREC_LoadBuffer[UART5_RX_BUFFER_SIZE];

/* Functions implementation */

boolean_t FWREC_SplitIntoRecords(char packet_str[], uint16_t packet_str_size)
{
   boolean_t res = TRUE;

   if(packet_str && packet_str_size)
   {
      uint32_t rec_cnt = 0U;
      char *pch = NULL;
//      pch=strtok((char*)packet_str, "\r\n");
      pch=strtok((char*)packet_str, "\n");

      if(pch)
      {
         rec_cnt++;
         res = FWREC_SetRecordBlock(pch, strlen(pch));

         do
         {
//            pch = strtok(NULL, "\r\n");
            pch = strtok(NULL, "\n");
            if(pch)
            {
               rec_cnt++;
               res = res && FWREC_SetRecordBlock(pch, strlen(pch));
            }
         }while(pch && res);
      }
   }

   return res;
}


static boolean_t FWREC_ValidateApplication(FWREC_Data_t *rec)
{
   boolean_t retval = TRUE;
   if(rec)
   {
      static uint32_t addr;

      if(rec->Type == FWREC_TYPE_EXT_LIN_ADDRESS)
      {
         addr = (rec->Address << 16U);
      }
      else if(rec->Type == FWREC_TYPE_DATA)
      {
         addr = addr & 0xFFFF0000U;
         addr = addr | rec->Address;

         if(addr == (uint32_t)INFO_DATA_ADDRESS)
         {
            //##SB retval = ( memcmp((void*)rec->Data, (void*)INFO_PRJ_NAME_DEFAULT, sizeof(INFO_PRJ_NAME_DEFAULT)) != 0 );
         }
      }

   }
   return retval;
}


static char *FWREC_Strtok(char *s, const char *delim)
{
    static char *lasts;
    register int ch;

    if (s == 0)
   s = lasts;
    do {
   if ((ch = *s++) == '\0')
       return 0;
    } while (strchr(delim, ch));
    --s;
    lasts = s + strcspn(s, delim);
    if (*lasts != 0)
   *lasts++ = 0;
    return s;
}


boolean_t FWREC_ErrorDetected(void)
{
    return FWREC_Error;
}


static void FWREC_Load(void)
{
    uint16_t buf_size = HAL_UART_GetRxBufferAndSize(&huart5, FWREC_LoadBuffer);

    if(buf_size)
    {
        FWREC_SetRecordBlock((char*)FWREC_LoadBuffer, buf_size);
    }
}


void FWREC_LoadSetup(void)
{
    FWREC_Error = FALSE;
    FWREC_StoredRecordNum = 0U;
//    OS_EventRemove(OS_EVENT_ID_FW_LOAD);               ##SB Integrazione CMSIS OS
//    OS_EventCreate(OS_EVENT_ID_FW_LOAD, FWREC_Load);   ##SB
}



boolean_t   FWREC_SetRecordBlock(char packet_str[], uint16_t packet_str_size)
{
   boolean_t res = TRUE;

   if(packet_str && packet_str_size && !FWREC_Error)
   {
      uint16_t packet_str_pos = 0U;
      FWREC_Data_t temp_rec;
      char *pch = NULL;

      pch = FWREC_Strtok(&packet_str[packet_str_pos], ":\r\n");

      do
      {
         memset(&temp_rec, 0, sizeof(temp_rec));

         if(pch)
         {
            packet_str_pos = 0U;
            sscanf(&pch[packet_str_pos], "%02hhx", &temp_rec.Size);

            if(temp_rec.Size <= FWREC_DATA_SIZE)
            {
               packet_str_pos = packet_str_pos + 2U;

               sscanf(&pch[packet_str_pos], "%04hx", &temp_rec.Address);

               if(FWREC_VALIDATE_ADDRESS(temp_rec.Address))
               {
                  packet_str_pos = packet_str_pos + 4U;

                  sscanf(&pch[packet_str_pos], "%02hhx", &temp_rec.Type);

                  if(FWREC_VALIDATE_TYPE(temp_rec.Type))
                  {
                     uint8_t d_pos;
                     packet_str_pos = packet_str_pos + 2U;

                     for(d_pos = 0U; d_pos < temp_rec.Size; d_pos++)
                     {
                        sscanf(&pch[packet_str_pos], "%02hhx", &temp_rec.Data[d_pos]);
                        packet_str_pos = packet_str_pos + 2U;
                     }

                     sscanf(&pch[packet_str_pos], "%02hx", &temp_rec.Cks); //##SB era una h in più
                     packet_str_pos = packet_str_pos + 2U;

                     if(temp_rec.Cks == FWREC_CalcChecksum(&temp_rec))
                     {
                        res = FWREC_ValidateApplication(&temp_rec);

                        if(res)
                        {
                           FWREC_Data_t aux_rec;

                           res = EFP_WriteData(EFP_FIRMWARE_TYPE, &temp_rec, (sizeof(FWREC_Data_t)), (EFP_FW_DATA_START_ADDR + (FWREC_StoredRecordNum *sizeof(FWREC_Data_t))));

                           res = res && EFP_ReadData(EFP_FIRMWARE_TYPE, &aux_rec, (sizeof(FWREC_Data_t)), (EFP_FW_DATA_START_ADDR + (FWREC_StoredRecordNum *sizeof(FWREC_Data_t))));

                           res = res && (memcmp((void*)&aux_rec, (void*)&temp_rec, sizeof(FWREC_Data_t)) == 0);

                           if(res)
                                    {
                                        FWREC_StoredRecordNum++;

                                        if(temp_rec.Type == FWREC_TYPE_EOF)
                                        {
                                            FWREC_NewFwValidationPending = TRUE;
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                        }
                        else
                        {
                           break;
                        }
                     }
                     else
                     {
                        res = FALSE;
                        break;
                     }
                  }
                  else
                  {
                     res = FALSE;
                     break;
                  }
               }
               else
               {
                  res = FALSE;
                  break;
               }
            }
            else
            {
               res = FALSE;
               break;
            }

            pch = FWREC_Strtok(NULL, ":\r\n");
                break;
         }
         else
         {
            break;
         }
      }while(res && (packet_str_pos < packet_str_size) && pch);


        FWREC_Error = !res;

        if(FWREC_Error)
        {
            if(UART_GetRxMode(&huart5) == UART_MODE_LOAD_FW)
            {
                UART_SetRxMode(&huart5, UART_MODE_NORMAL);
            }
        }
   }

   return res;
}


boolean_t FWREC_NewFwValidate(void)
{
    boolean_t res = FALSE;
    res = FWREC_WriteNewFwKey();
    res = res && FWREC_ResetNewFwRetryNum();
    res = res && FWREC_WriteNewFwRecordNum(&FWREC_StoredRecordNum);

    FWREC_NewFwValidationPending = FALSE;

    if(UART_GetRxMode(&huart5) == UART_MODE_LOAD_FW)
    {
        UART_SetRxMode(&huart5, UART_MODE_NORMAL);
    }
    return res;
}


boolean_t FWREC_GetNewFirmwareValidationPending(void)
{
    return  FWREC_NewFwValidationPending;
}


boolean_t FWREC_WriteNewFwKey(void)
{
   boolean_t retval = FALSE;
   uint32_t test_fw_key[2U];

   retval = EFP_ReadData(EFP_FW_KEY_TYPE, (void*)&FWREC_Key, sizeof(FWREC_Key), EFP_FW_KEY_DATA_START_ADDR);

   FWREC_Key.NewFwAvailableKey[0U] = (uint32_t)FWREC_NEW_FW_AVAILABLE_KEY;
   FWREC_Key.NewFwAvailableKey[1U] = (uint32_t)~FWREC_NEW_FW_AVAILABLE_KEY;

   retval = retval && EFP_WriteData(EFP_FW_KEY_TYPE, (void*)&FWREC_Key, sizeof(FWREC_Key), EFP_FW_KEY_DATA_START_ADDR);

   EFP_ReadData(EFP_FW_KEY_TYPE, (void*)test_fw_key, sizeof(test_fw_key), (EFP_FW_KEY_DATA_START_ADDR + FWREC_NEW_FW_AVAILABLE_KEY_OFFSET));

   if(retval && (memcmp(FWREC_Key.NewFwAvailableKey, test_fw_key, sizeof(FWREC_Key.NewFwAvailableKey)) == 0U))
   {
      retval = TRUE;
   }
    else
    {
        retval = FALSE;
    }

   return retval;
}


boolean_t FWREC_ResetNewFwRetryNum(void)
{
    boolean_t retval = FALSE;
    FWREC_Key_t key_check;

    retval = EFP_ReadData(EFP_FW_KEY_TYPE, (void*)&FWREC_Key, sizeof(FWREC_Key), EFP_FW_KEY_DATA_START_ADDR);
    FWREC_Key.NewFwProgramRetryNum = 0U;
    retval = retval && EFP_WriteData(EFP_FW_KEY_TYPE, (void*)&FWREC_Key, sizeof(FWREC_Key), EFP_FW_KEY_DATA_START_ADDR);
    retval = retval && EFP_ReadData(EFP_FW_KEY_TYPE, (void*)&key_check, sizeof(FWREC_Key), EFP_FW_KEY_DATA_START_ADDR);
    retval = retval && (memcmp((void*)&FWREC_Key, (void*)&key_check, sizeof(FWREC_Key_t)) == 0);
    return retval;
}


boolean_t FWREC_WriteBkpFwKey(void)
{
   boolean_t retval = FALSE;
   uint32_t test_fw_key[2U];

   retval = EFP_ReadData(EFP_FW_KEY_TYPE, (void*)&FWREC_Key, sizeof(FWREC_Key), EFP_FW_KEY_DATA_START_ADDR);

   FWREC_Key.BkpFwAvailableKey[0U] = (uint32_t)FWREC_BKP_FW_AVAILABLE_KEY;
   FWREC_Key.BkpFwAvailableKey[1U] = (uint32_t)~FWREC_BKP_FW_AVAILABLE_KEY;

   retval = retval && EFP_WriteData(EFP_FW_KEY_TYPE, (void*)&FWREC_Key, sizeof(FWREC_Key), EFP_FW_KEY_DATA_START_ADDR);

   EFP_ReadData(EFP_FW_KEY_TYPE, (void*)test_fw_key, sizeof(test_fw_key), (EFP_FW_KEY_DATA_START_ADDR + FWREC_BKP_FW_AVAILABLE_KEY_OFFSET));

   if(retval && (memcmp(FWREC_Key.BkpFwAvailableKey, test_fw_key, sizeof(FWREC_Key.BkpFwAvailableKey)) == 0U))
   {
      retval = TRUE;
   }

   return retval;
}


boolean_t FWREC_CheckBkpFwKey(void)
{
   boolean_t retval = FALSE;

   FWREC_Key.BkpFwAvailableKey[0U] = 0U;
   FWREC_Key.BkpFwAvailableKey[1U] = 0U;

   if(EFP_ReadData(EFP_FW_KEY_TYPE, (void*)FWREC_Key.BkpFwAvailableKey, sizeof(FWREC_Key.BkpFwAvailableKey), (EFP_FW_KEY_DATA_START_ADDR + FWREC_BKP_FW_AVAILABLE_KEY_OFFSET)))
   {
      if((FWREC_Key.BkpFwAvailableKey[0U] == (uint32_t)FWREC_BKP_FW_AVAILABLE_KEY) &&
         (FWREC_Key.BkpFwAvailableKey[1U] == (uint32_t)~FWREC_BKP_FW_AVAILABLE_KEY))
      {
         retval = TRUE;
      }
   }

   return retval;
}


boolean_t   FWREC_WriteBkpFwRecordNum(uint32_t *record_num)
{
   boolean_t retval = FALSE;

   if(record_num)
   {
      retval = EFP_ReadData(EFP_FW_KEY_TYPE, (void*)&FWREC_Key, sizeof(FWREC_Key), EFP_FW_KEY_DATA_START_ADDR);
      FWREC_Key.BkpFwRecordNum = *record_num;
      retval = retval && EFP_WriteData(EFP_FW_KEY_TYPE, (void*)&FWREC_Key, sizeof(FWREC_Key), EFP_FW_KEY_DATA_START_ADDR);
   }

   return retval;
}


void FWREC_ForceImmediateUpdate(boolean_t immediate_update)
{
    FWREC_ImmediateUpdate = immediate_update;
}


boolean_t FWREC_ReadNewFwRetryNum(uint32_t *retry_num)
{
    boolean_t retval = FALSE;
    if(retry_num)
    {
        FWREC_Key_t aux_key;
        retval = EFP_ReadData(EFP_FW_KEY_TYPE, (void*)&aux_key, sizeof(FWREC_Key_t), EFP_FW_KEY_DATA_START_ADDR);
        if(retval)
        {
            *retry_num = aux_key.NewFwProgramRetryNum;
        }
    }
    return retval;
}


boolean_t FWREC_WriteNewFwRetryNum(uint32_t *retry_num)
{
    boolean_t retval = FALSE;
    if(retry_num)
    {
        FWREC_Key_t aux_key;
        retval = EFP_ReadData(EFP_FW_KEY_TYPE, (void*)&aux_key, sizeof(FWREC_Key_t), EFP_FW_KEY_DATA_START_ADDR);
        if(retval)
        {
            aux_key.NewFwProgramRetryNum = *retry_num;
        }
    }
    return retval;
}


boolean_t FWREC_ReadNewFwRecordNum(uint32_t *record_num)
{
   boolean_t retval = FALSE;

   if(record_num)
   {
      retval = EFP_ReadData(EFP_FW_KEY_TYPE, (void*)record_num, sizeof(uint32_t), (EFP_FW_KEY_DATA_START_ADDR + FWREC_NEW_FW_RECORD_NUM_OFFSET));
   }

   return retval;
}


boolean_t FWREC_WriteNewFwRecordNum(uint32_t *record_num)
{
   boolean_t retval = FALSE;

   if(record_num)
   {
      FWREC_Key_t key_check;
      retval = EFP_ReadData(EFP_FW_KEY_TYPE, (void*)&FWREC_Key, sizeof(FWREC_Key), EFP_FW_KEY_DATA_START_ADDR);
      FWREC_Key.NewFwRecordNum = *record_num;
      FWREC_Key.NewFwProgramRetryNum = 0U;
      retval = retval && EFP_WriteData(EFP_FW_KEY_TYPE, (void*)&FWREC_Key, sizeof(FWREC_Key), EFP_FW_KEY_DATA_START_ADDR);
      EFP_ReadData(EFP_FW_KEY_TYPE, (void*)&key_check, sizeof(FWREC_Key_t), EFP_FW_KEY_DATA_START_ADDR);

      retval = retval && (memcmp((void*)&FWREC_Key, (void*)&key_check, sizeof(FWREC_Key_t)) == 0);
   }

   return retval;
}


boolean_t FWREC_ReadNew(FWREC_Data_t *record, uint32_t index)
{
   boolean_t retval = FALSE;

   if(record)
   {
      retval = EFP_ReadData(EFP_FIRMWARE_TYPE, record, sizeof(FWREC_Data_t), (EFP_FW_DATA_START_ADDR + (index *sizeof(FWREC_Data_t))));
   }

   return retval;
}

boolean_t FWREC_ReadBkp(FWREC_Data_t *record, uint32_t index)
{
   boolean_t retval = FALSE;

   if(record)
   {
      retval = EFP_ReadData(EFP_FIRMWARE_TYPE, record, sizeof(FWREC_Data_t), (FWREC_BKPUPFW_START_ADDR + (index *sizeof(FWREC_Data_t))));
   }

   return retval;
}

boolean_t FWREC_ReadBkpFwRecordNum(uint32_t *record_num)
{
   boolean_t retval = FALSE;

   if(record_num)
   {
      retval = EFP_ReadData(EFP_FW_KEY_TYPE, (void*)record_num, sizeof(uint32_t), (EFP_FW_KEY_DATA_START_ADDR + FWREC_BKP_FW_RECORD_NUM_OFFSET));
   }

   return retval;
}


uint8_t FWREC_CalcChecksum(FWREC_Data_t *rec)
{
   uint8_t retval = 0U;
   uint8_t *byte_ptr = (uint8_t*)rec;
   uint8_t i;

   for(i = 0U; i < (sizeof(FWREC_Data_t) - 1U);i++)
   {
      retval += byte_ptr[i];
   }

   retval = (uint8_t)~retval;
   retval++;

   return retval;
}
