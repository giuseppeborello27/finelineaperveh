/**
 * @project       uTag2UWB_Persona
 *
 * @Component     Firmware backup
 *
 * @file          fwBackup.c
 *
 * @brief         Brief
 *
 * @author        Author 
 *
 * @date          04nov2020
 *
 ***********************************************************************************************************************/
#include <string.h>

#include "externFlashPartition.h"
#include "fwBackup.h"
#include "fwRecord.h"
#include "internalFlash.h"

#ifndef APPLICATION_BOOTLOADER
static boolean_t  FWBKP_IsRequired(void);

static boolean_t  FWBKP_ApplicationBackup(void);
#endif

static boolean_t  FWBKP_ResetRequired(void);

void FWBKP_Handler(void)
{
#ifdef APPLICATION_BOOTLOADER

   if(FWBKP_ApplicationFirstBackup())
   {
      FWBKP_ResetRequired();
   }

#else

   if(FWBKP_IsRequired())
   {
      if(FWBKP_ApplicationBackup())
      {
         FWBKP_ResetRequired();
      }
   }
    else
    {

    }

#endif
}


#ifndef APPLICATION_BOOTLOADER
static boolean_t  FWBKP_IsRequired(void)
{
   boolean_t retval = FALSE;
   FWREC_Key_t key;

   if(EFP_ReadData(EFP_FW_KEY_TYPE, (void*)&key, sizeof(FWREC_Key_t), (EFP_FW_KEY_DATA_START_ADDR)))
   {
      /*Only the first 2 words are used for this check*/
      if(((key.NewFwAvailableKey[0U] & FWREC_FW_BACKUP_REQ_KEY_MASK) == FWREC_FW_BACKUP_REQ_KEY) &&
         ((((uint32_t)~key.NewFwAvailableKey[1U]) & FWREC_FW_BACKUP_REQ_KEY_MASK) == FWREC_FW_BACKUP_REQ_KEY))
      {
         retval = TRUE;
      }
   }

   return retval;
}


static boolean_t  FWBKP_ApplicationBackup(void)
{
   boolean_t retval = FALSE;
   FWREC_Data_t record;
   uint32_t record_num, record_i;

   retval = FWREC_ReadNewFwRecordNum(&record_num);
   if(retval)
   {
      for(record_i = 0U; record_i < record_num; record_i++)
      {
         retval = FWREC_ReadNew(&record, record_i);
         retval = retval & EFP_WriteData(EFP_FIRMWARE_TYPE,
                                 &record,
                                 (sizeof(FWREC_Data_t)),
                                 (FWREC_BKPUPFW_START_ADDR + (record_i *sizeof(FWREC_Data_t))));
         if(!retval)
         {
            break;
         }
      }

      if(retval)
      {
         /*Set backup FW as available*/
         FWREC_WriteBkpFwKey();
         FWREC_WriteBkpFwRecordNum(&record_num);
      }
   }

   return retval;
}

#endif /*APPLICATION_BOOTLOADER*/


static boolean_t  FWBKP_ResetRequired(void)
{
   boolean_t retval = FALSE;
   FWREC_Key_t key;
   uint32_t test_fw_key[6U] = {0U};

   retval = EFP_ReadData(EFP_FW_KEY_TYPE, (void*)&key, sizeof(key), EFP_FW_KEY_DATA_START_ADDR);

   /*Reset New FW key*/
   key.NewFwAvailableKey[0U] = key.NewFwAvailableKey[0U] & (uint32_t)FWREC_FW_PROGRAMMED_KEY_MASK;
   key.NewFwAvailableKey[1U] = (uint32_t)~key.NewFwAvailableKey[0U];
   key.NewFwRecordNum = 0U;

   retval = retval && EFP_WriteData(EFP_FW_KEY_TYPE, (void*)&key, sizeof(key), (EFP_FW_KEY_DATA_START_ADDR));

   EFP_ReadData(EFP_FW_KEY_TYPE, (void*)test_fw_key, sizeof(test_fw_key), (EFP_FW_KEY_DATA_START_ADDR + FWREC_NEW_FW_AVAILABLE_KEY_OFFSET));

   return retval;
}


#ifdef APPLICATION_BOOTLOADER

#warning "THIS FIRMWARE VERSION MUST BE PROGRAMMED AT ONE WITH THE BOOTLOADER!!!"

#warning "Macro FWBKP_LAST_RECORD_ADDR and FWBKP_LAST_RECORD_SIZE must be set to the last address obtained from the hex file of the application"
#define FWBKP_LAST_RECORD_ADDR   0x0801E700U
#define FWBKP_LAST_RECORD_SIZE   0x10U

#ifdef DBG_FWBKP
#include "usart.h"
void FWBKP_ShowRecord(FWREC_Data_t *record)
{
   if(record)
   {
      uint8_t i;
      char temp[4U] = {0};

      sprintf(FWBKP_DbgStr, ":%02X%04X%02X", record->Size, record->Address, record->Type);
      for(i = 0U; i< record->Size; i++)
      {
         sprintf(temp, "%02X", record->Data[i]);
         strcat(FWBKP_DbgStr, temp);
      }
      sprintf(temp, "%02X", record->Cks);
      strcat(FWBKP_DbgStr, temp);
        DBG_INFO(FWBKP_DbgStr);
      //HAL_UART_Transmit(&huart, (uint8_t*)FWBKP_DbgStr, strlen(FWBKP_DbgStr), 5000);
   }
}

void FWBKP_ShowRecordNum(uint32_t rec_num)
{
   sprintf(FWBKP_DbgStr, "\nRecord num: %u", rec_num);
   //HAL_UART_Transmit(&huart2, (uint8_t*)FWBKP_DbgStr, strlen(FWBKP_DbgStr), 5000);
   OS_Sleep(1000U);
}
void FWBKP_ShowResult(boolean_t res)
{

    DBG_INFO("\nResult: %u", res);
//    sprintf(FWBKP_DbgStr, "\nResult: %u", res);
// HAL_UART_Transmit(&huart2, (uint8_t*)FWBKP_DbgStr, strlen(FWBKP_DbgStr), 5000);
   OS_Sleep(5000U);
}
#else
#define FWBKP_ShowRecord(x)
#define FWBKP_ShowRecordNum(x)
#define FWBKP_ShowResult(x)
#endif /*DBG_FWBKP*/



boolean_t   FWBKP_ApplicationFirstBackup(void)
{
   boolean_t retval = TRUE;
   uint32_t word_num;
   uint32_t record_cnt = 0U;
   FWREC_Data_t record;
   uint32_t addr = FWREC_APPLICATION_START_ADDR;

   do
   {
      if((!(addr & 0xFFFFU)) || (addr == FWREC_APPLICATION_START_ADDR))
      {
         memset((void*)&record, 0, sizeof(FWREC_Data_t));
         record.Size = 0x02;
         record.Address = (uint16_t)0x0000U;
         record.Type = FWREC_TYPE_EXT_LIN_ADDRESS;
         record.Data[0U] = (uint8_t)((addr & 0xFF000000U) >> 24U);
         record.Data[1U] = (uint8_t)((addr & 0x00FF0000U) >> 16U);
         record.Cks = FWREC_CalcChecksum(&record);

         /* Save record*/
         retval = retval && EFP_WriteData(EFP_FIRMWARE_TYPE,
                                 &record,
                                 (sizeof(FWREC_Data_t)),
                                 (FWREC_BKPUPFW_START_ADDR + (record_cnt *sizeof(FWREC_Data_t))));
         record_cnt++;

         FWBKP_ShowRecord(&record);
      }

      record.Size = 0x10U;
      record.Address = (uint16_t)(addr & 0xFFFFU);
      record.Type = FWREC_TYPE_DATA;

      word_num = (record.Size >> 2U);
      IFLASH_Read(addr, (uint32_t*)record.Data, word_num);
      record.Cks = FWREC_CalcChecksum(&record);

      /* Save record*/
      retval = retval && EFP_WriteData(EFP_FIRMWARE_TYPE,
                              &record,
                              (sizeof(FWREC_Data_t)),
                              (FWREC_BKPUPFW_START_ADDR + (record_cnt *sizeof(FWREC_Data_t))));
      record_cnt++;

      FWBKP_ShowRecord(&record);


      addr += record.Size;

   }while((addr < FWBKP_LAST_RECORD_ADDR) && retval);


   /*Last data record*/
   memset((void*)&record, 0, sizeof(FWREC_Data_t));
   record.Size = FWBKP_LAST_RECORD_SIZE;
   record.Address = (uint16_t)(addr & 0xFFFFU);
   record.Type = FWREC_TYPE_DATA;

   word_num = (record.Size >> 2U);
   IFLASH_Read(addr, (uint32_t*)record.Data, word_num);
   record.Cks = FWREC_CalcChecksum(&record);

   /* Save record*/
   retval = retval && EFP_WriteData(EFP_FIRMWARE_TYPE,
                           &record,
                           (sizeof(FWREC_Data_t)),
                           (FWREC_BKPUPFW_START_ADDR + (record_cnt *sizeof(FWREC_Data_t))));

   record_cnt++;

   FWBKP_ShowRecord(&record);


   /*start linear address record */
   memset((void*)&record, 0, sizeof(FWREC_Data_t));
   record.Size = 0x04U;
   record.Address = 0x0000U;
   record.Type = FWREC_TYPE_START_LIN_ADDRESS;
   record.Data[0U] = (uint8_t)((*(__IO uint32_t*) (FWREC_APPLICATION_START_ADDR + 4U) & 0xFF000000U) >> 24U);
   record.Data[1U] = (uint8_t)((*(__IO uint32_t*) (FWREC_APPLICATION_START_ADDR + 4U) & 0x00FF0000U) >> 16U);
   record.Data[2U] = (uint8_t)((*(__IO uint32_t*) (FWREC_APPLICATION_START_ADDR + 4U) & 0x0000FF00U) >> 8U);
   record.Data[3U] = (uint8_t)((*(__IO uint32_t*) (FWREC_APPLICATION_START_ADDR + 4U) & 0x000000FFU) >> 0U);
   record.Cks = FWREC_CalcChecksum(&record);

   /* Save record*/
   retval = retval && EFP_WriteData(EFP_FIRMWARE_TYPE,
                           &record,
                           (sizeof(FWREC_Data_t)),
                           (FWREC_BKPUPFW_START_ADDR + (record_cnt *sizeof(FWREC_Data_t))));

   record_cnt++;

   FWBKP_ShowRecord(&record);



   /*EOF record*/
   memset((void*)&record, 0, sizeof(FWREC_Data_t));
   record.Size = 0x00U;
   record.Address = 0x0000U;
   record.Type = FWREC_TYPE_EOF;
   record.Cks = FWREC_CalcChecksum(&record);

   retval = retval && EFP_WriteData(EFP_FIRMWARE_TYPE,
                           &record,
                           (sizeof(FWREC_Data_t)),
                           (FWREC_BKPUPFW_START_ADDR + (record_cnt *sizeof(FWREC_Data_t))));

   record_cnt++;

   FWBKP_ShowRecord(&record);

   FWBKP_ShowRecordNum(record_cnt);
   FWBKP_ShowResult(retval);

   if(retval)
   {
      /*Set backup FW as available*/
      FWREC_WriteBkpFwKey();
      FWREC_WriteBkpFwRecordNum(&record_cnt);
   }

   return retval;
}

#endif /*APPLICATION_BOOTLOADER*/
