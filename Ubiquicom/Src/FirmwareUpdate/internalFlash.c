/**
 * @project       uTag2UWB_Persona
 *
 * @Component     Internal Flash
 *
 * @file          internalFlash.c
 *
 * @brief         Brief
 *
 * @author        Author 
 *
 * @date          04nov2020
 *
 ***********************************************************************************************************************/

#include "internalFlash.h"

/* Defines */

#define IFLASH_ABS(x,y)             ((x) < (y)) ? ((y)-(x)) : ((x)-(y))
#define IFLASH_MIN(x,y)             ((x) < (y)) ? ((y)-(x)) : ((x)-(y))

#define IFLASH_NO_PAGE_ERROR     0xFFFFFFFFU

/* Private functions */

static boolean_t IFLASH_Verify(uint32_t addr, uint32 word);

/* Function implementation */

void IFLASH_Init(void)
{
   HAL_FLASH_Unlock();

   /* Clear all FLASH flags */
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_SIZERR | FLASH_FLAG_OPTVERR);

   HAL_FLASH_Lock();
}


boolean_t IFLASH_Erase(uint32_t start_addr)
{
   boolean_t retval = TRUE;
   FLASH_EraseInitTypeDef desc;
   uint32_t page_error = IFLASH_NO_PAGE_ERROR;

//    LED_Control(LED_ACTION_ON, LED_LLED_RED, 0U); ##SB Usare il proprio controllo per il led
    HAL_FLASH_Unlock();

    if((start_addr >= IFLASH_BANK1_START_ADDRESS) && (start_addr < IFLASH_BANK2_START_ADDRESS))
    {
        desc.TypeErase = FLASH_TYPEERASE_PAGES;

        desc.Page = ((start_addr - IFLASH_BANK1_START_ADDRESS) >> IFLASH_MEMORY_PAGE_SIZE_SHIFT);
        desc.NbPages = ((IFLASH_BANK2_START_ADDRESS - start_addr) >> IFLASH_MEMORY_PAGE_SIZE_SHIFT);
        desc.Banks = FLASH_BANK_1;
        if(HAL_FLASHEx_Erase(&desc, &page_error) != HAL_OK)
      {
         retval = FALSE;
      }
        retval = retval && (page_error == IFLASH_NO_PAGE_ERROR);

        if(retval)
        {
            desc.Page = ((IFLASH_BANK2_START_ADDRESS - IFLASH_BANK1_START_ADDRESS) >> IFLASH_MEMORY_PAGE_SIZE_SHIFT);
            desc.NbPages = ((IFLASH_MEMORY_END_ADDRESS - IFLASH_BANK2_START_ADDRESS) >> IFLASH_MEMORY_PAGE_SIZE_SHIFT);
            desc.Banks = FLASH_BANK_2;

            if(HAL_FLASHEx_Erase(&desc, &page_error) != HAL_OK)
            {
                retval = FALSE;
            }
            retval = retval && (page_error == IFLASH_NO_PAGE_ERROR);
        }

    }
    else if((start_addr >= IFLASH_BANK2_START_ADDRESS) && (start_addr < IFLASH_MEMORY_END_ADDRESS))
    {
        desc.TypeErase = FLASH_TYPEERASE_PAGES;
        desc.Page = ((start_addr - IFLASH_BANK2_START_ADDRESS) >> IFLASH_MEMORY_PAGE_SIZE_SHIFT);
        desc.NbPages = ((IFLASH_MEMORY_END_ADDRESS - start_addr) >> IFLASH_MEMORY_PAGE_SIZE_SHIFT);
        desc.Banks = FLASH_BANK_2;

        if(HAL_FLASHEx_Erase(&desc, &page_error) != HAL_OK)
        {
            retval = FALSE;
        }
        retval = retval && (page_error == IFLASH_NO_PAGE_ERROR);
    }
    else
    {
        retval = FALSE;
    }

    HAL_FLASH_Lock();
//    LED_Control(LED_ACTION_OFF, LED_LLED_RED, 0U); ##SB Usare il proprio controllo per il led
   return retval;
}


boolean_t IFLASH_Write(uint32_t addr, uint64_t data[], uint64_t data_size)
{
   boolean_t retval = TRUE;
   uint32_t dword_cnt;
//   volatile uint64_t data64 = 0; ##SB Check if is correct that is unused

//   LED_Control(LED_ACTION_ON, LED_LLED_GREEN, 0U); ##SB Usare il proprio controllo per il led
   retval = (HAL_FLASH_Unlock() == HAL_OK);

   if(retval)
   {
//        data64 = *(volatile uint64_t *) data; ##SB Check if is correct that is unused
      for(dword_cnt = 0U; dword_cnt < data_size; dword_cnt++)
      {
         if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, addr, data[dword_cnt]) != HAL_OK)
         {
            retval = FALSE;
            break;
         }
         else
         {
            retval = IFLASH_Verify(addr, data[dword_cnt]);
            if(!retval)
            {
               break;
            }
         }

         addr += 8U;
      }
   }

    retval = retval && (HAL_FLASH_Lock() == HAL_OK);
//   LED_Control(LED_ACTION_OFF, LED_LLED_GREEN, 0U);  ##SB Usare il proprio controllo per il led

   return retval;
}


void IFLASH_Read(uint32_t addr, uint32_t data[], uint32_t data_size)
{
   uint32_t word_cnt;

   for(word_cnt = 0U; word_cnt < data_size; word_cnt++)
   {
      data[word_cnt] = (*(volatile uint32_t*) (addr));

      addr += 4U;
   }
}


static boolean_t IFLASH_Verify(uint32_t addr, uint32 word)
{
   boolean_t retval = TRUE;

   if(word != (*(volatile uint32_t*) (addr)))
   {
      retval = FALSE;
   }

   return retval;
}
