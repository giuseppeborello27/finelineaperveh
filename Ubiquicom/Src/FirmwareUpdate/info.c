/**
 * @project       uTag2UWB_Persona
 *
 * @Component     info
 *
 * @file          info.c
 *
 * @brief         Brief
 *
 * @author        Author 
 *
 * @date          04nov2020
 *
 ***********************************************************************************************************************/

#include "info.h"

static const INFO_Data_t   INFO_Data = /* __attribute__((at(INFO_DATA_ADDRESS))) */
{
   INFO_PRJ_NAME_DEFAULT,
   6U,
   __DATE__,
   __TIME__
};


const INFO_Data_t *INFO_GetData(void)
{
   return &INFO_Data;
}
