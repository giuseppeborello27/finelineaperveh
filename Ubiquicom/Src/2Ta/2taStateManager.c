#include "2taStateManager.h"
#include "frameConfiguration.h"
#include "randomSlotAssignment.h"

#define MAX(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define MIN(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

/* ************** private data  ***************************** */

/* ************** private functions  ***************************** */
static void incrementNumberOfIdentifiedPacketsSentToSameTarget(targetInfo *target) {
  if(target != NULL){
    target->numberOfIdentifiedPacketsSentToSameTarget = target->numberOfIdentifiedPacketsSentToSameTarget +1;
  }
}

static void resetNumberOfIdentifiedPacketsSentToSameTarget(targetInfo *firstTarget, targetInfo *secondTarget) {
  if(firstTarget != NULL){
    firstTarget->numberOfIdentifiedPacketsSentToSameTarget = 0;
  }
  if(secondTarget != NULL){
    firstTarget->numberOfIdentifiedPacketsSentToSameTarget = 0;
  }
}

static void resetTargetNumberOfFails(targetInfo *target) {
  if(target != NULL){
    target->numberOfAnonymousReceptionFailsInThisTalkingSlot = 0;
    target->numberOfReceptionFailsFromThisSourceId = 0;
  }
}

static bool isValidDistance(uint16_t dist_cm){
  return !((dist_cm==0) || (dist_cm==0xDEAD));
}
static targetInfo *chooseTargetByLastDistanceWhenTwpAnonymousPolls(targetInfo *firstTarget, targetInfo *secondTarget) {
  targetInfo *choosedTarget = NULL;
  
  bool isFirstDistanceValid = isValidDistance(firstTarget->receivedPacket.dist_cm);
  bool isSecondDistanceValid = isValidDistance(secondTarget->receivedPacket.dist_cm);

  if( !isFirstDistanceValid && !isSecondDistanceValid) {
    // todo: choose first or second randomly???
    choosedTarget = firstTarget; 
  }
  else if ( isFirstDistanceValid && isSecondDistanceValid) {
      choosedTarget = firstTarget->receivedPacket.dist_cm > secondTarget->receivedPacket.dist_cm? secondTarget: firstTarget;
  }
  else {
    choosedTarget = isFirstDistanceValid? firstTarget : secondTarget;
  }
  //printf("both isFirstDistanceValid %d, isSecondDistanceValid %d, (choosedTarget==secondTarget) %d \r\n",isFirstDistanceValid, isSecondDistanceValid, (choosedTarget==secondTarget));
  return choosedTarget;
}

static uint16_t switchingDistanceThreshold_cm = 30;
static targetInfo *chooseTargetByLastDistanceAnonymousAndIdentifiedPolls(targetInfo *anonymousTarget, targetInfo *identifiedTarget) {
  targetInfo *choosedTarget = NULL;
  
  bool isFirstDistanceValid = isValidDistance(anonymousTarget->receivedPacket.dist_cm);
  bool isSecondDistanceValid = isValidDistance(identifiedTarget->lastValidIdentifiedDistance);

  if( !isFirstDistanceValid && !isSecondDistanceValid) {

    // todo: choose first or second randomly???
    choosedTarget = identifiedTarget; // choose identified TODO: uniform function call and parameter meaning
  }
  else if ( isFirstDistanceValid && isSecondDistanceValid) {
      choosedTarget = (anonymousTarget->receivedPacket.dist_cm + switchingDistanceThreshold_cm ) > identifiedTarget->lastValidIdentifiedDistance? identifiedTarget: anonymousTarget;
  }
  else {
    choosedTarget = isFirstDistanceValid? anonymousTarget : identifiedTarget;
  }
  //printf("isFirstDistanceValid %d, isSecondDistanceValid %d, (choosedTarget==identifiedTarget) %d \r\n",isFirstDistanceValid, isSecondDistanceValid, (choosedTarget==identifiedTarget));
  return choosedTarget;
}

static uint32_t applySlotCorrectionToTarget(targetInfo *target) {  
  uint32_t superFrameShiftOffset = 0;
  // if (target!=NULL) {
  //   const frameConfig * frameConfiguration = GetFrameConfiguration();
  //   uint32_t timeSuperFrame_ns = frameConfiguration->timeSuperFrame_ns;
  //   int32_t currentSlot = (int32_t)target->slot_tau_ns;
  //   int32_t receivedCorrection = (target->receivedPacket).slotCorr_ns;
  //   int32_t correctedSlot = (currentSlot- receivedCorrection);
  //   if (correctedSlot<0) {      
  //     if (correctedSlot< -(frameConfiguration->minTimeAllocatedForASlot_ns)) {
  //       correctedSlot = timeSuperFrame_ns + correctedSlot;      
  //     }
  //     else {
  //       correctedSlot = 0;
  //     }     
  //     //correctedSlot = timeSuperFrame_ns + correctedSlot;      
  //   }
  //   int32_t zero = 0;
  //   int32_t lastAllocableTau =  timeSuperFrame_ns-(frameConfiguration->minTimeAllocatedForASlot_ns);
  //   uint32_t newSlotTau = MIN(MAX(correctedSlot,zero), lastAllocableTau);
    
  //   target->slot_tau_ns = newSlotTau;
  //   if(newSlotTau == 0) {
  //     superFrameShiftOffset = MIN(MAX(-correctedSlot,zero), timeSuperFrame_ns);
  //   }
  //   printf("currentSlot: %d, receivedCorretion: %d, correctedSlot: %d, newSlotTau:%d\r\n",\
  //     currentSlot, receivedCorrection,\
  //     correctedSlot, newSlotTau);
  //   //TODO: superFrameOffset should be reported back to task tag
  // }
  return superFrameShiftOffset;
}

static void updateTargetsDelays(bool updateFirstTarget, bool updateSecondTarget, targetInfo *firstTarget, targetInfo *secondTarget) {
    // TODO: update targets taus if needed (ensuring proper distance)
    // acap: we need to get the order i.e. is tauAnonymous < tauIdenfied ?
    if(updateFirstTarget && updateSecondTarget) {
      randomSlotsResult rndResult = GetTwoRandomPollsTimes();
      firstTarget->slot_tau_ns = rndResult.first_of_two_tau_ns;
      secondTarget->slot_tau_ns = rndResult.second_of_Two_tau_ns;
      //  printf("rnd1: %d, rnd2: %d\r\n",rndResult.first_of_two_tau_ns, rndResult.second_of_Two_tau_ns);
    }
    else if(updateFirstTarget && !updateSecondTarget) {
      randomSlotsResult rndResult = GetOtherRandomPollSlot(secondTarget->slot_tau_ns);
      firstTarget->slot_tau_ns = rndResult.single_tau_ns;
      // printf("rnd1: %d\r\n",rndResult.first_of_two_tau_ns);

    }
    else if (!updateFirstTarget && updateSecondTarget) {
      randomSlotsResult rndResult = GetOtherRandomPollSlot(firstTarget->slot_tau_ns);
      secondTarget->slot_tau_ns = rndResult.single_tau_ns;
      // printf("rnd2: %d\r\n",rndResult.second_of_Two_tau_ns);

    }
}

static void stateChangeWhenOnePollIsIdentifiedAndTheOtherIsAnonymous(targetInfo *identifiedTarget, targetInfo *anonymousTarget) {
  bool canAnonymousTargetSwitchToBeIdentified = false;
  bool identifiedTargetMustSwitchToBeAnonymous = false;  
  bool changeSlotToAnonymousTarget = false, changeSlotToIdentifiedTarget = false; 
  targetInfo *choosedTarget = NULL;
  uint32_t superFrameShiftOffset = 0;  
  bool hasMinimumNumberOfIdentifiedPacketsSentToSameTarget = false;


  anonymousTarget->lastValidIdentifiedDistance = 0xDEAD; 
  if (anonymousTarget->receivedPacket.status == RECEIVED_EXPECTED_RESPONSE_PACKET) {        
    // printf("s_1_an_and_1_id_1\r\n");
    canAnonymousTargetSwitchToBeIdentified = true;
    // reset target number of fails
    resetTargetNumberOfFails(anonymousTarget);
  }
  // else if anonymousTarget got an error check if it is time to change (max number of failures reached) its tau
  else if(anonymousTarget->numberOfAnonymousReceptionFailsInThisTalkingSlot >= MAX_NUMBER_OF_ANONYMOUS_FAILS) {
    //printf("s_1_an_and_1_id_2\r\n");
    changeSlotToAnonymousTarget = true;
    resetTargetNumberOfFails(anonymousTarget);
  }

  // at least x identified packets sent to this target
  hasMinimumNumberOfIdentifiedPacketsSentToSameTarget = identifiedTarget->numberOfIdentifiedPacketsSentToSameTarget > MIN_NUMBER_OF_IDENTIFIED_PACKETS_SENT_TO_SAME_TARGET;
  incrementNumberOfIdentifiedPacketsSentToSameTarget(identifiedTarget);
  

  if(identifiedTarget->receivedPacket.status == RECEIVED_EXPECTED_RESPONSE_PACKET){
    //printf("s_1_an_and_1_id_3\r\n");   
    identifiedTarget->lastValidIdentifiedDistance = identifiedTarget->receivedPacket.dist_cm;        
    resetTargetNumberOfFails(identifiedTarget);    
  }
  else if(identifiedTarget->numberOfReceptionFailsFromThisSourceId >= MAX_NUMBER_OF_IDENTIFIED_FAILS){
    //printf("s_1_an_and_1_id_4\r\n");
    identifiedTargetMustSwitchToBeAnonymous = true;
    identifiedTarget->lastValidIdentifiedDistance = 0xDEAD;        
    resetTargetNumberOfFails(identifiedTarget);      
  }

  // //acap REMOVE THIS. USED FOR DEBUGGING PURPOSES TO FORCE THE SAME POLL **********************
  // identifiedTargetMustSwitchToBeAnonymous = false;
  // canAnonymousTargetSwitchToBeIdentified = false;
  // //(END) acap REMOVE THIS. USED FOR DEBUGGING PURPOSES TO FORCE THE SAME POLL **********************

  //******* Current status is [one anonymous and one identified] *******  
  if(identifiedTargetMustSwitchToBeAnonymous && !canAnonymousTargetSwitchToBeIdentified) { // switch [identified -> anonymous | anonymous -> anonymous]
    //printf("s_1_an_and_1_id_5\r\n");
    identifiedTarget->targetInfoId = 0;               
    changeSlotToAnonymousTarget = true;
    changeSlotToIdentifiedTarget = true;
    resetNumberOfIdentifiedPacketsSentToSameTarget(identifiedTarget, anonymousTarget);
  }
  else if(identifiedTargetMustSwitchToBeAnonymous && canAnonymousTargetSwitchToBeIdentified) { // switch [identified -> anonymous | anonymous -> identified]
    //printf("s_1_an_and_1_id_6\r\n");
    anonymousTarget->targetInfoId = anonymousTarget->receivedPacket.sourceId;           
    identifiedTarget->targetInfoId = 0;               
    resetNumberOfIdentifiedPacketsSentToSameTarget(identifiedTarget, anonymousTarget);

    //printf("correction_3\r\n");
    superFrameShiftOffset = applySlotCorrectionToTarget(anonymousTarget);    
    changeSlotToIdentifiedTarget = true;
  }  
  else { // !identifiedTargetMustSwitchToBeAnonymous
    // printf("s_1_an_and_1_id_7\r\n");
    if (canAnonymousTargetSwitchToBeIdentified && hasMinimumNumberOfIdentifiedPacketsSentToSameTarget) { // 2) and at least a minimum number of identified packets sent to this target
      choosedTarget = chooseTargetByLastDistanceAnonymousAndIdentifiedPolls(anonymousTarget,identifiedTarget);
    }

    if (choosedTarget == anonymousTarget) { // switch [identified -> anonymous | anonymous -> identified] 
      anonymousTarget->targetInfoId = anonymousTarget->receivedPacket.sourceId;           
      identifiedTarget->targetInfoId = 0;               
      
      resetNumberOfIdentifiedPacketsSentToSameTarget(identifiedTarget, anonymousTarget);
    //printf("correction_1\r\n");
      superFrameShiftOffset = applySlotCorrectionToTarget(anonymousTarget);
    }
    else { // remain [identified -> identified | anonymous -> anonymous] 
    //  printf("correction_2\r\n");
      superFrameShiftOffset = applySlotCorrectionToTarget(identifiedTarget);
    }                
  } 
  // printf("s_1_an_and_1_id_8\r\n");
  updateTargetsDelays(changeSlotToAnonymousTarget, changeSlotToIdentifiedTarget, anonymousTarget, identifiedTarget);  
  // printf("s_1_an_and_1_id_9\r\n");
  //TODO: superFrameOffset should be reported back to task tag
}

static void stateChangeWhenBothPollAreAnonymous(targetInfo *firstTarget, targetInfo *secondTarget) {
  bool changeSlotToFirstTarget = false, changeSlotToSecondTarget = false; 
  targetInfo *choosedTarget = NULL;
  bool canFirsTargetSwitchToBeIdentified = false;
  bool canSecondargetSwitchToBeIdentified = false;

  // printf("firstTarget->receivedPacket.status:%d\r\n", firstTarget->receivedPacket.status);
  // printf("firstTarget->numberOfAnonymousReceptionFailsInThisTalkingSlot:%d\r\n", firstTarget->numberOfAnonymousReceptionFailsInThisTalkingSlot);
  // printf("secondTarget->receivedPacket.status:%d\r\n", secondTarget->receivedPacket.status);
  // printf("secondTarget->numberOfAnonymousReceptionFailsInThisTalkingSlot:%d\r\n", secondTarget->numberOfAnonymousReceptionFailsInThisTalkingSlot);
  // //printf("firstTarget->receivedPacket.sourceId:%d\r\n", firstTarget->receivedPacket.sourceId);
  //printf("secondTarget->receivedPacket.sourceId:%d\r\n", secondTarget->receivedPacket.sourceId);
  
  // if firstTarget got a valid response then it is a candidate for being identified, in the same slot
  if (firstTarget->receivedPacket.status == RECEIVED_EXPECTED_RESPONSE_PACKET) {        
    // printf("s1_1\r\n");
    canFirsTargetSwitchToBeIdentified = true;
    // reset target number of fails
    resetTargetNumberOfFails(firstTarget);
  }
  // else if firstTarget got an error check if it is time to change (max number of failures reached) its tau
  else if(firstTarget->numberOfAnonymousReceptionFailsInThisTalkingSlot >= MAX_NUMBER_OF_ANONYMOUS_FAILS) {
    // printf("s1_2\r\n");
    changeSlotToFirstTarget = true;
    resetTargetNumberOfFails(firstTarget);
  }
    
  // if secondTarget got a valid response then it is a candidate for being identified, in the same slot
  if (secondTarget->receivedPacket.status == RECEIVED_EXPECTED_RESPONSE_PACKET) {    
    // printf("s1_3\r\n");
    canSecondargetSwitchToBeIdentified = true;
    // reset target number of fails
    resetTargetNumberOfFails(secondTarget);       
  }
  // else if secondTarget got an error check if it is time to change (max number of failures reached) its tau
  else if(secondTarget->numberOfAnonymousReceptionFailsInThisTalkingSlot >= MAX_NUMBER_OF_ANONYMOUS_FAILS) {
    // printf("s1_4\r\n");
    changeSlotToSecondTarget = true;
    resetTargetNumberOfFails(secondTarget);       

  }  
      
  // if both targets are candidate to be identified, choose one and switch it
  if(canFirsTargetSwitchToBeIdentified && canSecondargetSwitchToBeIdentified) {    
    // printf("s1_5\r\n");
    choosedTarget = chooseTargetByLastDistanceWhenTwpAnonymousPolls(firstTarget,secondTarget);
    if (choosedTarget == firstTarget) {
      firstTarget->targetInfoId = firstTarget->receivedPacket.sourceId;           
    }
    else {
      secondTarget->targetInfoId = secondTarget->receivedPacket.sourceId;        
    }
  } else if(canFirsTargetSwitchToBeIdentified) {
    firstTarget->targetInfoId = firstTarget->receivedPacket.sourceId;       
    choosedTarget = firstTarget;    
  }
  else if(canSecondargetSwitchToBeIdentified) {
    secondTarget->targetInfoId = secondTarget->receivedPacket.sourceId;        
    choosedTarget = secondTarget;    
  }

  // printf("s1_6\r\n");
  // apply time correction to choosed target
  uint32_t superFrameShiftOffset = applySlotCorrectionToTarget(choosedTarget);
  // printf("s1_7\r\n");
  //TODO: superFrameOffset should be reported back to task tag
  //  printf("canFirsTargetSwitchToBeIdentified: %d, canSecondargetSwitchToBeIdentified: %d \r\n",canFirsTargetSwitchToBeIdentified, canSecondargetSwitchToBeIdentified);
  //  printf("changeSlotToFirstTarget: %d, changeSlotToSecondTarget: %d \r\n",changeSlotToFirstTarget, changeSlotToSecondTarget);
  updateTargetsDelays(changeSlotToFirstTarget, changeSlotToSecondTarget, firstTarget, secondTarget);  
  //  printf("s1_8\r\n");
}



/* ************** Public functions  ***************************** */
void UpdateTargetsForNextTrasmission(targetsContainer *targets) {
  targetInfo *firstTarget = &targets->firstTargetInfo;
  targetInfo *secondTarget = &targets->secondTargetInfo;

  //printf("firstTarget->receivedPacket.dist_cm:%d\r\n", firstTarget->receivedPacket.dist_cm);
  //printf("secondTarget->receivedPacket.dist_cm:%d\r\n", secondTarget->receivedPacket.dist_cm);

  if( (firstTarget->targetInfoId == 0) && (secondTarget->targetInfoId == 0)) {
    //  printf("state both entering\r\n");
    stateChangeWhenBothPollAreAnonymous(firstTarget, secondTarget);    
  }
  else if( (firstTarget->targetInfoId != 0) && (secondTarget->targetInfoId == 0)) {
    //  printf("state both first Identified second anonymous entering\r\n");
    //stateChangeWhenfirstPollIsIdentifiedSecondPollIsAnonymous(firstTarget, secondTarget);    
    stateChangeWhenOnePollIsIdentifiedAndTheOtherIsAnonymous(firstTarget,secondTarget);
  }
  else { //(firstTarget->targetInfoId == 0) && (secondTarget->targetInfoId != 0)
    //printf("state 3\r\n");
        //  both targetIds different from zero is not possible
    //stateChangeWhenfirstPollIsAnonymousSecondPollIsIdentified(firstTarget, secondTarget);    
    //  printf("state both first anonym second Identified entering\r\n");
    stateChangeWhenOnePollIsIdentifiedAndTheOtherIsAnonymous(secondTarget,firstTarget);
  }
  
  
  
}
