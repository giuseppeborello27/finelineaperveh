/**
 * @project       uTag2UWB_Persona
 *
 * @Component     Flash Partition
 *
 * @file          externFlashPartition.c
 *
 * @brief         This file contains the functions that are used to read, write and validate the the external
 *                flash memory partition.
 *
 * @author        Simone Bonforti
 *
 * @date          28oct2020
 *
 ***********************************************************************************************************************/

#include "externFlashPartition.h"

#define EFP_PAGE_SIZE_SHIFT                        ( 8 )
#define EFP_PAGE_SIZE                              ( 1 << EFP_PAGE_SIZE_SHIFT )

#define EFP_ADDR_IS_IN_RANGE(addr, low, high)      ( ( addr >= low ) && ( addr < high ) )
#define EFP_SIZE_IS_IN_RANGE(size, max)            ( size < max )
#define EFP_VALIDATE(addr, low, high, size, max)   ( EFP_ADDR_IS_IN_RANGE(addr, low, high) && EFP_SIZE_IS_IN_RANGE(size, max) )

static boolean_t EFP_Validate(EFP_Data_t data_type, uint32_t data_size, uint32_t addr);


/*!
 * @name EFP_WriteData
 * @brief Writes the specified buffer of data into the external flash memory.\n
 *         the function checks the address correctness according to the specified type of data \ref EFP_Data_t
 * @remarks If the specified address corresponds to the start address of a sector (a multiple of the sector size \ref EFP_SECTOR_SIZE) the target sector is erased.
 *          If the size of the data buffer to write exceeds the current sector boundary the subsequent sectors are automatically erased.
 * @param data_type Type of data to be written.
 * @param data Pointer to the data buffer.
 * @param data_size Number of byte of the buffer.
 * @param addr Initial address where starting to write data to.
 * @retval  - TRUE If the operation has been completed successfully.
 *          - FALSE If the operation has not been completed successfully due to an invalid address, size or data type.
 */
boolean_t EFP_WriteData(EFP_Data_t data_type, void * data, uint32_t data_size, uint32_t addr)
{
   boolean_t retval = FALSE;
   uint8_t * byte_ptr = ( uint8_t * ) data;
   uint32_t addr_to_write = addr;
   uint32_t target_page_num = 0, target_page_start = 0, target_page_end = 0;
   uint32_t bytes_to_write = 0;
   uint32_t pos = 0;

   retval = ( ( data != NULL ) && EFP_Validate(data_type, data_size, addr) );

   if( retval )
   {
      /* Iterate according to data size */
      /* Have to check if the size to write exceeds the page upper limit */
      target_page_num = addr_to_write >> EFP_PAGE_SIZE_SHIFT;
      target_page_start = target_page_num * EFP_PAGE_SIZE;
      target_page_end = target_page_start + EFP_PAGE_SIZE - 1;

      if( ( ( target_page_end - addr_to_write ) + 1 ) < data_size )
      {
         bytes_to_write = ( target_page_end - addr_to_write ) + 1;

         /* Check if address is the start of a sector */
         if( !(addr_to_write & ( ( uint32_t )EFP_SECTOR_SIZE - 1 ) ) )
         {
            /* Addr match with the first addr of the sector */
            /* Erase sector */
            EFLASH_EraseSector(addr_to_write >> EFP_SECTOR_SIZE_SHIFT);
         }

         EFLASH_Write(addr_to_write, (uint8_t*)&byte_ptr[ pos ], bytes_to_write);
         pos += bytes_to_write;
         data_size -= bytes_to_write;
         addr_to_write += bytes_to_write;

         while( data_size > 0 )
         {
            if( EFP_PAGE_SIZE <= data_size )
            {
               bytes_to_write = EFP_PAGE_SIZE;
            }
            else
            {
               bytes_to_write = (data_size & (EFP_PAGE_SIZE - 1));
            }

            /* Check if address is the start of a sector */
            if( !( addr_to_write & ( ( uint32_t )EFP_SECTOR_SIZE - 1) ) )
            {
               /* Addr match with the first addr of the sector */
               /* Erase sector */
               EFLASH_EraseSector(addr_to_write >> EFP_SECTOR_SIZE_SHIFT);
            }

            EFLASH_Write(addr_to_write, ( uint8_t * ) &byte_ptr[ pos ], bytes_to_write);
            pos += bytes_to_write;
            data_size -= bytes_to_write;
            addr_to_write += bytes_to_write;
         }
      }
      else
      {
         /* Check if address is the start of a sector */
         if( !( addr_to_write & ( ( uint32_t )EFP_SECTOR_SIZE - 1 ) ) )
         {
            /* Addr match with the first addr of the sector */
            /* Erase sector */
            EFLASH_EraseSector(addr_to_write >> EFP_SECTOR_SIZE_SHIFT);
         }

         /*Can write page in a single burst */
         EFLASH_Write(addr_to_write, (uint8_t*)byte_ptr, data_size);
      }
   }

   return retval;
}


/*!
 * @name EFP_ReadData
 * @brief Reads the specified number of bytes from the external flash memory starting from the specified address.
 *        Read values are copied int the specified buffer.
 * @param data_type Type of data to be written.
 * @param data Pointer to the data buffer.
 * @param data_size Number of byte of the buffer.
 * @param addr Initial address where starting to write data to.
 * @retval  - TRUE If the operation has been completed successfully.
 *          - FALSE If the operation has not been completed successfully due to an invalid address, size or data type.
 */
boolean_t EFP_ReadData(EFP_Data_t data_type, void * data, uint32_t data_size, uint32_t addr)
{
   boolean_t retval = FALSE;
   uint8_t * byte_ptr = ( uint8_t * ) data;

   retval = ( (NULL != data ) && EFP_Validate(data_type, data_size, addr) );

   if( retval )
   {
      EFLASH_Read(addr, byte_ptr, data_size);
   }

   return retval;
}


/*!
 * @name EFP_Validate
 * @brief Validate the data of the partition.
 * @param data_type Type of data to be validated.
 * @param data_size Number of byte of the buffer.
 * @param addr Initial address where starting to validate data to.
 * @retval  - TRUE If the operation has been completed successfully.
 *          - FALSE If the operation has not been completed successfully due to an invalid address, size or data type.
 */
static boolean_t EFP_Validate(EFP_Data_t data_type, uint32_t data_size, uint32_t addr)
{
   boolean_t retval = FALSE;

   switch((uint8_t)data_type)
   {
      case EFP_FIRMWARE_TYPE:
      {
          retval = EFP_VALIDATE(addr, EFP_FW_DATA_START_ADDR, EFP_FW_DATA_END_ADDR, data_size, EFP_FW_DATA_SIZE);
          break;
      }
      case EFP_FW_KEY_TYPE:
      {
          retval = EFP_VALIDATE(addr, EFP_FW_KEY_DATA_START_ADDR, EFP_FW_KEY_DATA_END_ADDR, data_size, EFP_FW_KEY_DATA_SIZE);
          break;
      }
      case EFP_RESERVED_TYPE:
      {
          retval = FALSE;
          break;
      }
      case EFP_APP_TYPE:
      {
		  retval = EFP_VALIDATE(addr, EFP_APP_DATA_START_ADDR, EFP_APP_DATA_END_ADDR, data_size, EFP_APP_DATA_SIZE);
		  break;
      }
      case EFP_CONF_TYPE:
      {
    	  retval = EFP_VALIDATE(addr, EFP_CONF_DATA_START_ADDR, EFP_CONF_DATA_END_ADDR, data_size, EFP_CONF_DATA_SIZE);
    	  break;
      }
      case EFP_SN_TYPE:
      {
    	  retval = EFP_VALIDATE(addr, EFP_SN_DATA_START_ADDR, EFP_SN_DATA_END_ADDR, data_size, EFP_SN_DATA_SIZE);
    	  break;
      }
      default:
      {
         break;
      }
   }

   return retval;
}
